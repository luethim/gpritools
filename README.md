# GPRItools #

The GPRItools provide a nice and pythonic interface to the Gamma GPRI software
and the files created by interferometric radar.

### What is a GPRI? ###

The GPRI is a terrestrial radar interferometer developed by 
[Gamma Remote Sensing](http://www.gamma-rs.ch).

### What are the GPRItools good for? ###

Working with data files from the GPRI is somewhat cumbersome. The GPRIfile
class provides nice access to the data and the parameters.

Scripting the excellent Gamma software with the shell is less than optimal.
Using the gamma_wrappers facilitates calling the command line tools as Python
functions, including the full documentation strings. 

*GPRItools* help with both aspects.

### Example: plotting intensity of a SLC file ###
notice: no conversion to MLI and then to PNG is required!

```python
gf = gprifile.GPRIfile('/path/to/my/slc/20161111_111111u.slc')
plt.imshow( np.log( gf.getMagnitude() ) )
```

### Example: looking at GPRI files (SLC, MLI, INT, UNW) ###
notice: no conversion to PNG is required!

####look at MLI data from lower antenna####

```bash
gpriviewer.py /data/gpri/myproject/work/mli mli -a l
```

####look at SLC data from upper antenna####
the data is displayed as intensity data (on-the-fly)

```bash
gpriviewer.py /data/gpri/myproject/slc/20161111 slc -a u
```


### Example: creating a MLI file ###
notice: no command line acrobatics needed

```python
slcname = '/path/to/my/slc/20161111_111111u.slc'
mlidir  = '/path/to/my/workdir/mli'

gf = gprifilename.GPRIfilename(slcname)
mliname  = gf.mliname(mlidir)
gw.multi_look(slcname, slcname+'.par', 
              mliname, mliname+'.par', 
              5, 1)
```

### Example: creating an interferogram ###
notice: no command line acrobatics needed

```python
slcfile0 = '/path/to/my/slc/20161010_101001u.slc'
slcfile1 = '/path/to/my/slc/20161111_111111u.slc'
offfile  = slcfile0+'.off'

# feed an empty parameter file to the interacitve command "create_offset"
open('of_par.in','w').write('\n\n\n\n\n\n')
gw.create_offset(slcfile0+'.par', slcfile1+'.par', offile, 1, of_par_in='of_par.in')

# create interferogram
gw.SLC_intf(slcfile0, slcfile, offfile, intfile, rlks, azlks, 0, '-', 0, 0)

# create correlation map
gw.cc_wave(intfile, mlifile0, mlifile, ccfile, mliparam.nrs, 3, 3, 1)
```

### Example: mapping an MLI file to cartesian coordinates ###
notice: no acrobatics with Gamma DEM format needed, 
        geocoding once, mapping often (e.g. in for loop)

```python
mliname  = '/path/to/my/workdir/mli/20161111_111111u.mli'

zsurf_fun = raster_interpolation(gdalraster('gimpdem.tif'))
gf = gprifile.GPRIfile(mliname)

# calculate lookup table
intp = PolarDEMMapper(gpricoord, param)
intp.calc_nn_lookup(xr, yr, zsurf_fun)

# remap the data 
grid = intp.map_nn_data(gf.data)
plt.imshow( np.log( grid ) )
```
