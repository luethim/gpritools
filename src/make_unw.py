# unwrap stacked inteferograms
# this unwraps the interferograms, and if a flag is set, 
# multiplies the unwrapped phase with the duration between
# first and last frame

import argparse
import glob, os
import gamma_wrappers as gw
from gprifilename import GPRIfilename
from gprifile import GPRIfile, GPRIparamfile
import numpy as np
import matplotlib.dates as md
import datetime

# set up the command line parser
parser = argparse.ArgumentParser(description='Process GPRI SLCs to INTs')
parser.add_argument('workdir', help='directory with MLI files and INT files (input/output)')
parser.add_argument('intdir',  help='directory with INT files (input)')
parser.add_argument('unwdir',  help='directory with UNW files (output)')
parser.add_argument('antenna', help='antenna (character [l/u])')
parser.add_argument('azinit',  help='reference azimuth pixel for unwrapping')
parser.add_argument('rinit',   help='reference range pixel for unwrapping')

args = parser.parse_args()

# fakeargs = ['/data/gpri/eqi2016/work', 
#             'intstack60', 'unwstack60', 'u', '1190', '440']
# args = parser.parse_args(fakeargs)

# subdirectories of workdir
intdir = '{:s}/{:s}'.format(args.workdir, args.intdir)
unwdir = '{:s}/{:s}'.format(args.workdir, args.unwdir)

# start pixel
azinit, rinit = int(args.azinit), int(args.rinit)

# create output directory
os.system('mkdir -p {:s}'.format(unwdir))

# get all files to convert
filenames = sorted(glob.glob(intdir+'/*{:s}.int'.format(args.antenna)))

dateformat = '%Y%m%d_%H%M%S'

print('\nunwrapping {:d} INT files'.format(len(filenames)))
print('\n   start pixel {:d} {:d}'.format(azinit, rinit))

for intfile in filenames:
    intfilename = GPRIfilename(intfile)
    intparam    = GPRIparamfile(intfile)
    unwfile     = '{:s}/{:s}.unw'.format(unwdir, intfilename.basename)

    # unwrap interferogram
    gw.mcf(intfile, '-', '-', unwfile, intparam.nrs, r_init=rinit, az_init=azinit, init_flag=1)

    # add some information to the parameter file
    intparam.param['image_format'] = 'FLOAT'
    intparam.param['GT_unw_intfile'] = intfile
    intparam.param['GT_unw_rinit']  = rinit
    intparam.param['GT_unw_azinit'] = azinit
    intparam.writeParam(unwfile+'.par')

