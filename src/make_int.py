# create inteferograms for all consecutive pairs of files for a directory
# upper or lower antenna can be indicated

import argparse
import glob, os
import gamma_wrappers as gw
from gprifilename import GPRIfilename
from gprifile import GPRIparamfile

# set up the command line parser
parser = argparse.ArgumentParser(description='Process GPRI SLCs to INTs')
parser.add_argument('slcdir', help='directory form which to read SLC files (input)')
parser.add_argument('workdir', help='directory with MLI files and INT files (input/output)')
parser.add_argument('antenna', help='antenna (character [l/u])')
parser.add_argument('-r', '--rlks',  type=int, default=5,  
                    help='range looks (integer)')
parser.add_argument('-a', '--azlks', type=int, default=1, 
                    help='azimuth looks (integer)')
parser.add_argument('--dtmin', type=int, default=None, 
                    help='minimum time difference in minutes to calculate interferogram')
parser.add_argument('--dtmax', type=int, default=None, 
                    help='maximum time difference in minutes to calculate interferogram')
parser.add_argument('--adf', type=int, default=0, 
                    help='create adf filtered files (integer [0/1])')

args = parser.parse_args()

# fakeargs = ['/media/usb/autobackup/data/taco17/slc/20170927', 
#             '/media/usb/work/taco17', 
#             'u', '--dtmax=21']
# args = parser.parse_args(fakeargs)

# subdirectories of workdir
intdir = '{:s}/int'.format(args.workdir)
adfdir = '{:s}/adf'.format(args.workdir)
mlidir = '{:s}/mli'.format(args.workdir)

# create output directory
os.system('mkdir -p {:s}'.format(intdir))
if args.adf == 1:
    os.system('mkdir -p {:s}'.format(adfdir))

# get all files to convert
filenames = sorted(glob.glob(args.slcdir+'/*{:s}.slc'.format(args.antenna)))

print('\nprocessing {:d} SLC files'.format(len(filenames)))

# create interferograms with respect to the previous file
slcfile0  = GPRIfilename(filenames[0])

# create on stupid offset parameter file (instead of calculating them)
# feed an empty parameter file to the interacitve command "create_offset"
ofpfile = '{:s}/of_par.in'.format(intdir)
offfile = '{:s}/intf_generic.off'.format(intdir)
open(ofpfile, 'w').write('\n\n\n\n\n\n')
gw.create_offset(slcfile0.filename+'.par', slcfile0.filename+'.par', 
                 offfile, 1, of_par_in=ofpfile)

for filename0, filename1 in zip(filenames[:-1], filenames[1:]):
    slcfile0 = GPRIfilename(filename0)
    slcfile1 = GPRIfilename(filename1)
    print('-> processing', slcfile1.basename, slcfile0.basename)

    # check for time difference between files
    if args.dtmax:
        if (slcfile1.date - slcfile0.date).seconds > (args.dtmax)*60+10:
            print('   dt={:.0f} bigger than dt_max={:.0f} min'.format((slcfile1.date - slcfile0.date).seconds/60., args.dtmax) )
            continue

    intname  = '{:s}__{:s}'.format(slcfile1.basename, slcfile0.basename)
    intfile  = '{:s}/{:s}.int'.format(intdir, intname)
    ccfile   = '{:s}/{:s}.cc'.format(intdir, intname)
    adfintfile  = '{:s}/{:s}.int'.format(adfdir, intname)
    adfccfile   = '{:s}/{:s}.cc'.format(adfdir, intname)
    mlifile0 = '{:s}/{:s}.mli'.format(mlidir, slcfile0.basename)
    mlifile1 = '{:s}/{:s}.mli'.format(mlidir, slcfile1.basename)
    mliparam = GPRIparamfile(mlifile0)

    if os.path.exists(intfile) and (os.path.getsize(intfile) > 1000):
        print('already done' , intfile)
        continue

    if not os.path.exists(mlifile0) or not os.path.exists(mlifile1):
        print('... MLI file missing', mlifile0, mlifile1)
        continue

    # create interferogram
    gw.SLC_intf(slcfile0.filename, slcfile1.filename, 
                offfile, intfile, args.rlks, args.azlks, 0, '-', 0, 0)

    # write the parameters
    intparam = GPRIparamfile(filename0)
    intparam.param['GT_slcfile0']         = slcfile0.filename
    intparam.param['GT_slcfile1']         = slcfile1.filename
    intparam.param['range_samples']       = int(intparam.param['range_samples']/args.rlks)
    intparam.param['near_range_slc']      = intparam.param['near_range_slc']+(args.rlks-1)/2.*intparam.param['range_pixel_spacing']
    intparam.param['azimuth_lines']       = int(intparam.param['azimuth_lines']/args.azlks)
    intparam.param['range_pixel_spacing'] = intparam.param['range_pixel_spacing']*args.rlks
    intparam.param['range_looks']         = int(intparam.param['range_looks']*args.rlks)
    intparam.writeParam(intfile+'.par')


    # filter interferogram with ADF (adaptive filter)
    if args.adf == 1:
        gw.adf(intfile, adfintfile, adfccfile, mliparam.nrs)
        intparam.setParamValue('GT_adfparam', 'default')
        intparam.writeParam(adfintfile+'.par')

    # create coherence map
    gw.cc_wave(intfile, mlifile0, mlifile1, ccfile, mliparam.nrs, 3, 3, 1)
    # write the parameters
    intparam.param['image_format'] = 'FLOAT'

    # write out parameter file
    intparam.writeParam(ccfile+'.par')






