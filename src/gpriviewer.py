#!/usr/bin/env python3
#
# viewer for GPRI files
#
# all files have to be of the same type

from gprifile import GPRIparamfile, GPRIfile
import numpy as np
import matplotlib
from matplotlib.widgets import Slider, Button, RadioButtons
import pylab as plt
import time, glob
import os, sys
import datetime
import argparse

class GPRIviewer(object):

    typedirs = dict(mli='mli',
                    cc ='int',
                    int='int',
                    adf='int',
                    unw='int',
                    slc='slc',
                    rslc='rslc',
                    )

    def __init__(self, datapath, mission, filetype, with_typedirs=False, with_timeline=True, 
                 minfile=0, maxfile=-1, antenna=''):
        if mission == '-': 
            mission = ''
        self.datapath = datapath
        self.mission = mission
        if with_typedirs is False:
            for k in self.typedirs:
                self.typedirs[k] = '' 
        self.with_timeline = with_timeline
        self.filetype = filetype
        #self.filepath = '/'.join((datapath, self.typedirs[filetype], mission, '*.'+filetype))
        globstring = os.path.join(datapath, '*%s.%s' % (antenna, filetype))
        self.filenames = sorted(glob.glob(globstring))#[minfile:]
        if maxfile != -1:
            self.filenames = self.filenames[:maxfile]

        if len(self.filenames) == 0:
            print ('no files found in path ', globstring)
            sys.exit(0)
        if self.with_timeline:
            self.dates = [datetime.datetime.strptime(f.split('/')[-1][:15], '%Y%m%d_%H%M%S')
                          for f in self.filenames]
            self.dates = plt.date2num(self.dates)
        self.filenr = 0
        self.filenrold = -1
        self.data = None
        self.modifyer = set()
        self.cmap = plt.cm.jet

        # initialize plot
        self.fig = plt.figure()
        # disable all matplotlib key bindings
        self.fig.canvas.mpl_disconnect(self.fig.canvas.manager.key_press_handler_id)
        self.fig.subplots_adjust(0.04,0.04,0.98,0.98,0.01)
        self.dataplot = plt.subplot2grid((8,4),(0,0),rowspan=7,colspan=4)
        self.dataplot.autoscale(1,'both',1)
        # self.y_subplot=plt.subplot2grid((8,4),(4,2),rowspan=4,colspan=2)
        #self.ax = self.fig.add_subplot(111)
        
        data = self.prepare_data()
        self.img = self.dataplot.imshow(data, cmap=self.cmap, aspect='auto')

        self.timeplot = plt.subplot2grid((8,4),(7,0),rowspan=1,colspan=4)
        if self.with_timeline:
            self.timeplot.plot_date(self.dates, np.ones(len(self.dates)), 'ko')
            self.timeline, = self.timeplot.plot([self.dates[self.filenr]]*2, [0,2], 'r-')
            self.timestring = self.timeplot.text(self.dates[0], 1.5, 'niet')
            dt = self.dates[-1]-self.dates[0]
            self.timeplot.set_xlim(self.dates[0]-0.02*dt, self.dates[-1]+0.02*dt)
            self.timeplot.set_ylim(0,2)

        dispname = self.filenames[0].split('/')[-1].split('.')[0]
        self.title = self.dataplot.set_title(dispname)
        self.fig.canvas.mpl_connect('button_press_event',self.click)
        self.fig.canvas.mpl_connect('key_press_event',self.click)
        self.clim = None
        self.climfact = [2.,2.]
        self.maskfact = 0.7

        # self.rax = self.fig.add_axes([0.025, 0.5, 0.15, 0.15], axisbg='r')
        # self.radio = RadioButtons(self.rax, ('lin', 'log'), active=1)
        # def colorfunc(label):
        #     # l.set_color(label)
        #     plt.draw()
        # self.radio.on_clicked(colorfunc)

        # axcolor = 'lightgoldenrodyellow'
        # self.axfreq = self.fig.add_axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
        # self.axamp  = self.fig.add_axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)

        # sfreq = Slider(self.axfreq, 'Freq', 0.1, 30.0, valinit=1)
        # samp = Slider(self.axamp, 'Amp', 0.1, 10.0, valinit=1)

        self.click(None)

    def prepare_data(self):
        filename = self.filenames[self.filenr]
        if self.filenr != self.filenrold:
            print ('loading data from',filename)
            self.data = GPRIfile(filename).data
        data = self.data.copy()
        if 'slc' in self.filetype:
            data = np.sqrt(data[:,:,0]**2 + data[:,:,1]**2)
        if 'int' in self.filetype:
            data = np.arctan2(data[:,:,1],data[:,:,0])
        if 'l' in self.modifyer:
            data = np.log(data)
        # mask data with cc or power
        if 'p' in self.modifyer:
            if self.filetype == 'mli':
                pdata = self.data[:]
            elif self.filetype == 'unw':
                # load the INT file from which the UNW was produced
                print('load intfile')
                intfile = GPRIparamfile(filename).param['GT_unw_intfile']
                fn = filename.split('/')[:-2]
                fn.extend(intfile.split('/')[-2:])
                filename = '/'.join(fn)
                pdata = GPRIfile(filename).getMagnitude()
            else:
                pdata = np.sqrt(self.data[:,:,0]**2 + self.data[:,:,1]**2)
                print(pdata.max(), pdata.min(), pdata.std())
                pdata /= (4.*pdata.std())
            data[pdata < self.maskfact] = np.nan

        if 'c' in self.modifyer:
            #ccfilename = filename.replace(self.typedirs[self.filetype], 'int').replace('.'+self.filetype, '.cc')
            ccfilename = filename.replace('.'+self.filetype, '.cc')
            print('---- mask with cc ----')
            print(filename)
            print(ccfilename)
            print('---- /mask with cc ----')
            if os.path.isfile(ccfilename):
                ccdata = GPRIfile(ccfilename).data
                data[ccdata < self.maskfact] = np.nan
            else:
                print(ccfilename, 'not found')
        return data

    def click(self, event):
        change_clim = ''
        update = False
        self.filenrold = self.filenr
        if type(event) is matplotlib.backend_bases.MouseEvent:
            if event.inaxes == self.dataplot:
                if event.button == 1:
                    self.filenr += 1
                    update = True
                if event.button == 3:
                    self.filenr -= 1
                    update = True
            elif event.inaxes == self.timeplot:
                if event.button == 1:
                    filenr = np.argmin(np.abs(event.xdata-self.dates))
                    self.filenr = filenr
                    update = True
        if type(event) is matplotlib.backend_bases.KeyEvent:
            # print('key event<', event.key, '>')
            update = True
            if event.key == 'q':
                plt.clf()
                import sys
                sys.exit()
            elif event.key == 'f':
                self.filenr += 1
            elif event.key == 'b':
                self.filenr -= 1
            elif event.key == 'alt+f':
                self.filenr += int(len(self.filenames)/20.)
            elif event.key == 'alt+b':
                self.filenr -= int(len(self.filenames)/20.)
            elif event.key == 'alt+a':
                self.filenr = 0
            elif event.key == 'alt+e':
                self.filenr = len(self.filenames)-1
            elif event.key == ' ':
                self.filenr += 1
            elif event.key == '.':
                self.filenr += 1
            elif event.key == 'backspace':
                self.filenr -= 1
            elif event.key == ',':
                self.filenr -= 1
            elif event.key == 'M':
                self.maskfact += 0.05
            elif event.key == 'm':
                self.maskfact -= 0.05
            elif event.key == 'c':
                if 'c' in self.modifyer:
                    self.modifyer.remove('c')
                else:
                    self.modifyer.add('c')
            elif event.key == 'p':
                if 'p' in self.modifyer:
                    self.modifyer.remove('p')
                else:
                    self.modifyer.add('p')
            elif event.key == 'l':
                if 'l' in self.modifyer:
                    self.modifyer.remove('l')
                else:
                    self.modifyer.add('l')
                change_clim = 'S'
            elif event.key in 'dDuUS':
                change_clim = event.key
            elif event.key == 'g':
                if self.cmap == plt.cm.jet:
                    self.cmap = plt.cm.gray
                else:
                    self.cmap = plt.cm.jet
            else:
                update = False

        if update is False:
            return

        self.filenr = min(self.filenr, len(self.filenames)-1)
        self.filenr = max(self.filenr, 0)
        filename = self.filenames[self.filenr]
        dispname = filename.split('/')[-1].split('.')[0]
        data = self.prepare_data()
        if not self.clim:
            change_clim = 'S'
        # change limits to standard deviation
        if change_clim:
            if change_clim == 'S':
                self.climfact = [2., 2.]
            if change_clim == 'u':
                self.climfact[1] -= 1.
            if change_clim == 'U':
                self.climfact[1] += 1.
            if change_clim == 'd':
                self.climfact[0] += 1.
            if change_clim == 'D':
                self.climfact[0] -= 1.
            mean = np.nanmean(data)
            std  = np.nanstd(data)
            d, u = self.climfact
            self.clim = [mean-d*std, mean+u*std]
        plt.setp(self.img, 'clim', self.clim)
        plt.setp(self.img, 'cmap', self.cmap)

        plt.setp(self.title, 'text', dispname)
        self.img.set_data(data)

        if self.with_timeline:
            self.timeline.set_xdata([self.dates[self.filenr]]*2)
            self.timestring.set_text(dispname)
        plt.draw()

# main call of the viewer

parser = argparse.ArgumentParser(description='Display GPRI files')
parser.add_argument('datapath', help="path to the data directory")
parser.add_argument('filetype', help="type of file to display")
parser.add_argument('--minfile', type=int, default=0,  
                    help='minimum index of file (default 0)')
parser.add_argument('--maxfile', type=int, default=-1,  
                    help='maximum index of file (default -1)')
parser.add_argument('-a', '--antenna', default='',  
                    help='antenna ("u" or "d")')

args = parser.parse_args()
#args = parser.parse_args('/data/gpri/oldhut/slc/20210628 slc'.split())

#v = GPRIviewer( datapath, mission, filetype, with_typedirs=False, with_timeline=False )
#v = GPRIviewer( datapath, mission, filetype, with_typedirs=True )
#v = GPRIviewer( datapath, mission, filetype, minfile=-110, maxfile=-1)
v = GPRIviewer( args.datapath, '', args.filetype, antenna=args.antenna,
                minfile=args.minfile, maxfile=args.maxfile )
plt.show(block=True)


