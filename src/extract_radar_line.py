# Extract amplitude and phase along a radar line (in radar coordinates)
# This will be performed on many files
#
# |slcdir| is the directory where the data files are stored
#
# |workdir| is the directory where the results are written to
#
# The line is defined by the azimuth index |az| 
#
# |dt| is the time interval in minutes between acquisitons. 
# Just the mintues divided by dt are used (similar to */dt in cron

import numpy as np
import gprifile
import glob, os, sys
import struct
import datetime
import argparse
import pylab as plt

# set up the command line parser
parser = argparse.ArgumentParser(description='Extract data along a line from a series raw SLC files')
parser.add_argument('slcdir', help='directory form which to read SLC files (input)')
parser.add_argument('workdir', help='directory to write the extracted data')
parser.add_argument('antenna', help='antenna (character [l/u])')
parser.add_argument('az', help='radar azimuth index')
parser.add_argument('--nlines', type=int, default=1, help='number of lines')
parser.add_argument('--dt', type=int, default=1, help='time span between acquisitions in minutes')

# # debug/development arguments
# fakeargs = ['extract_line.py',
#             '/data/gpri/eqi2017/slc', 
#             '/data/gpri/eqi2017/work', 
#             'u',
#             # '-94',
#             '-81.5',
#             '-400', '4800',
#             '1000', '6800', 
#             '--nlines=13',
#             '--dlines=200',
#             '--dt=1'
#             ]
# sys.argv = fakeargs

args = parser.parse_args()

# the end-points defining the line 
azofs    = float(args.azofs)
p0x, p0y = float(args.p0x), float(args.p0y)
p1x, p1y = float(args.p1x), float(args.p1y)
nlines   = int(args.nlines)
dlines   = float(args.dlines)

# pw = 10  # the "window", width=(2*pw+1), how many points to extract sideways 

# get all filenames
filenames = sorted(glob.glob(args.slcdir+'/*{:s}.slc'.format(args.antenna)))
print('number of slc files:', len(filenames))

# create output directory
os.system('mkdir -p {:s}'.format(args.workdir))

# collect all the times from the file names
time = np.array([datetime.datetime.strptime(f.split('/')[-1][:15], '%Y%m%d_%H%M%S') 
                 for f in filenames]).astype('datetime64[ns]')

# time in minutes
timem = time.astype('datetime64[m]')
idx   = timem.astype(int)%(args.dt) == 0
time  = time[idx]
filenames = list(np.compress(idx, filenames))

print('number of reduced slc files:', len(filenames))

# load one GPRI file (lazily)
g = gprifile.GPRIfile(filenames[0])

# read out the data from all results files
maxpoints = max([len(l) for l in allpoints])

# pre-create the value array, this is faster than assembling lists
data = np.zeros((len(filenames), len(allpoints), maxpoints, 2)) + np.nan
# read the points out of every array
for i, filename in enumerate(filenames):
    with open(filename, 'rb') as of:
        print('extracting data from ', filename)
        for j, points in enumerate(allpoints):
            for k, (az, r) in enumerate(points):
                of.seek((g.nrs*az+r)*2*4,0)
                data[i,j,k,:] = struct.unpack('>2f', of.read(8))
        of.close()

    # if 1:
    #     ## testing the array indexing: OK
    #     g = gprifile.GPRIfile(filename)
    #     #plt.plot(g.getMagnitude()[100,:], 'b')
    #     plt.plot(np.sqrt(data[:, 0, 3, 0]**2+data[:,0,3,1]**2), 'r')
    #     plt.show()
    #     stop

if 0:
    mag = np.sqrt(data[:, :, 0]**2+data[:,:,1]**2)
    plt.imshow(np.log(mag), extent=[0,2,0,1])
    plt.show()
    stop

attrs =dict(p0x=p0x, p0y=p0y, p1x=p1x, p1y=p1y,
            slcdir=args.slcdir, dt=args.dt, dr=g.dr,
            nlines=nlines, maxpoints=maxpoints, dlines=dlines)

# np.save('extract_signal.npy', data)
# np.save('extract_time.npy', time)
# # create a xarray dataset, and store as netcdf file
# ds = xarray.Dataset({'signal': (['time', 'line', 'points', 'complex'],  data)},
#                     coords={'time': time,
#                             'line': np.arange(nlines),
#                             'points': np.arange(maxpoints),
#                             'complex': [0,1],},
#                     attrs=attrs)
# outfilename = '{:s}/extract_{:.0f}_{:.0f}_{:.0f}_{:.0f}_{:d}_{:.0f}.nc'.format(args.workdir, p0x, p0y, p1x, p1y, nlines, args.dt)
# ds.to_netcdf(outfilename)
