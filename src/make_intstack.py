# stack inteferograms taken in a certain duration
# this can be thought of as either a running mean, 
# or an averaging over a certain time span
#
# the upper or lower antenna can be indicated
#
# stacking happens on the phase difference per minute
# the result therefore is the phase rate

import argparse
import glob, os
import gamma_wrappers as gw
from gprifilename import GPRIfilename
from gprifile import GPRIfile, GPRIparamfile
import numpy as np
import matplotlib.dates as md
import datetime

# set up the command line parser
parser = argparse.ArgumentParser(description='Process GPRI SLCs to INTs')
parser.add_argument('workdir',     help='directory with MLI files and INT files (input/output)')
parser.add_argument('intdir',      help='directory with INT files (input)')
parser.add_argument('intstackdir', help='directory with INTstack files (output)')
parser.add_argument('antenna',     help='antenna (character [l/u])')
parser.add_argument('interval',    help='time interval in minutes')
parser.add_argument('window',      help='time window in minutes (this is the total averaging window)')
parser.add_argument('-s', '--start', default='', help='start time of first stacking')
parser.add_argument('--dtmax', type=int, default=None, 
                    help='maximum time difference in minutes to calculate interferogram')

args = parser.parse_args()

# fakeargs = ['/data/gpri/mr_hut/work', 
#             'int', 'intstack', 'u', '3600', '3600'] #, '-s 20160606_120000']
# args = parser.parse_args(fakeargs)

# subdirectories of workdir
intdir      = '{:s}/{:s}'.format(args.workdir, args.intdir)
intstackdir = '{:s}/{:s}'.format(args.workdir, args.intstackdir)

# create output directory
os.system('mkdir -p {:s}'.format(intstackdir))

# get all files to convert
filenames = sorted(glob.glob(intdir+'/*{:s}.int'.format(args.antenna)))

# gather all times of interferograms, extract this information from the filenames
gprifilenames = [GPRIfilename(f) for f in filenames]
dates0 = dict((md.date2num(f.date0), f) for f in gprifilenames)

# get the size of the data array
intpar0   = GPRIfile(gprifilenames[0].filename)
nas, nrs  = intpar0.nas, intpar0.nrs

# use numpy to find files in intervals with time window |window|
dates0np  = np.array(sorted(dates0))
minperday = float(24.*60.)   # minutes per day, since window is given in minutes

window   = float(args.window)/minperday/2.   
interval = float(args.interval)/minperday
dtmax    = float(args.dtmax)    #

dateformat = '%Y%m%d_%H%M%S'

# figure out the start, either an index or the start datetime
datestart = dates0np[0]
if args.start:
    if '_' in args.start:
        datestart = datetime.datetime.strptime(args.start.strip(), dateformat)
        datestart = md.date2num(datestart)
    else:
        startidx = int(args.start)
        datestart = dates0np[startidx]

print('\nstacking {:d} INT files'.format(len(dates0np)))
print('starting at date ', md.num2date(datestart).strftime(dateformat))

def fmt_dt(x):
    return md.num2date(x).strftime(dateformat)

step = 0
while 1:
    # the starting time of the interval
    date0 = datestart + step*interval
    print('--- interval: {:5d}  {}'.format(step, fmt_dt(date0)))
    print('    stacking:        {} -- {}'.format(fmt_dt(date0-window), fmt_dt(date0+window)))
    step += 1

    # if there is nothing to do anymore, get out of the loop
    if date0 > dates0np[-1]:
        break

    # collect the filenames to stack during the time window
    idx = (dates0np >= date0-window) & (dates0np <= date0+window)
    if not idx.any():
        print('    --> no files found')
        continue
    else:
        print('    n files   {:5d}'.format(np.sum(idx)))

    # get the parameter of the closest date to the new file 
    param = GPRIparamfile(dates0[dates0np[idx][0]].filename).param

    # create empty arrays to stack data
    intdata = np.zeros((nas, nrs, 2), '>f4')
    ccdata  = np.zeros((nas, nrs), '>f4')
    nint, ncc = 0, 0
    dt_total = 0.

    # import pdb
    # pdb.set_trace()
    for key in dates0np[idx]:
        intfilename = dates0[key]
        filename = intfilename.filename
        dt_minute = (intfilename.date1 - intfilename.date0).total_seconds()/60.

        # check for time difference between files
        if args.dtmax:
            if dt_minute > args.dtmax+0.2:
                print('   dt={:.0f} bigger than dt_max={:.0f} min'.format(dt_minute, args.dtmax) )
                print('   skip, dt {:7.0f} '.format(dt_minute), filename)
                continue

        # if there is no time difference we don't care (maybe this is the topographic phase)
        if dt_minute == 0.:
            dt_minute = 1.
        else:
            dt_total += dt_minute

        print('    add, dt {:7.0f} '.format(dt_minute), filename)
        intdata += GPRIfile(filename).data/dt_minute
        param['GT_stack_int{:04d}'.format(nint)] = filename
        param['GT_stack_dt{:04d}'.format(nint)]  = '{:.2f}'.format(dt_minute)
        nint += 1
        try:
            ccfilename  = filename.replace('.int', '.cc')
            ccdata  += GPRIfile(ccfilename).data
            ncc += 1
        except:
            pass
    ## we don't need this anymore since we stack per second.
    intdata /= float(nint)
    ccdata  /= float(ncc)
    param['GT_stack_nstack'] = nint
    param['GT_stack_dt_total'] = dt_total
    outfilename = '{:s}/{:s}{:s}.int'.format(intstackdir, 
                                             md.num2date(date0).strftime(dateformat),
                                             args.antenna)

    # write the averaged int data
    gf = GPRIfile(outfilename)
    gf.updateParam(param)
    gf.setData(intdata)
    gf.write(outfilename)

    # write the averaged cc data
    outfilename = outfilename.replace('.int','.cc')
    gf = GPRIfile(outfilename)
    param['image_format'] = 'FLOAT'
    gf.updateParam(param)
    gf.setData(ccdata)
    gf.write(outfilename)



