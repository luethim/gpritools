#!/usr/bin/env python3
#
# viewer for GPRI and GEOTIFF files
#
# all files have to be of the same type

import numpy as np
import matplotlib
from matplotlib.widgets import Slider, Button, RadioButtons
import pylab as plt

import xarray      # reading/plotting Geotiff
import rioxarray   # new reading of Geotiff
#import rasterio   # reading Geotiff
from gprifile import GPRIparamfile, GPRIfile

import time, glob
import os, sys
import datetime
import argparse


class GPRIviewer(object):

    def get_dates_sentinelflow(self, filenames):
        dates = [datetime.datetime.strptime(f.split('/')[-1].split('_')[0], '%Y%m%d')
                      for f in filenames]
        return plt.date2num(dates)


    def get_dates_enveo(self, filenames):
        dates = [datetime.datetime.strptime(f.split('_')[-4], '%Y%m%d')
                      for f in filenames]
        return plt.date2num(dates)

    def get_dates_aerlen(self, filenames):
        dates = [datetime.datetime.strptime(f.split('/')[-1][:15], '%Y%m%d_%H%M%S')
                      for f in filenames]
        return plt.date2num(dates)

    def get_dates_gpri(self, filenames):
        dates = [datetime.datetime.strptime(f.split('/')[-1][:15], '%Y%m%d_%H%M%S')
                      for f in filenames]
        return plt.date2num(dates)

    def __init__(self, datapath, filetype, with_timeline=True, 
                 minfile=0, maxfile=-1, antenna='', band=0):
        self.datapath = datapath
        self.with_timeline = with_timeline
        self.filetype = filetype
        self.band = int(band)
        globstring = os.path.join(datapath, '*%s.%s' % (antenna, filetype))
        self.filenames = sorted(glob.glob(globstring))

        if maxfile != -1:
            self.filenames = self.filenames[:maxfile]

        if len(self.filenames) == 0:
            print ('no files found in path ', globstring)
            sys.exit(0)
        if self.with_timeline:
            # self.dates = self.get_dates_enveo(self.filenames)
            # self.dates = self.get_dates_sentinelflow(self.filenames)
            self.dates = self.get_dates_aerlen(self.filenames)
            idx = np.argsort(self.dates)
            self.dates = self.dates[idx]
            self.filenames = np.take(self.filenames, idx)

        self.shortnames = [f.split('/')[-1].split('.')[0] for f in self.filenames]
        self.filenr = 0
        self.filenrold = -999
        self.data = None
        self.modifyer = set()
        self.cmap = plt.cm.jet

        # picked points
        self.pickpoint = 0
        # self.pickpoints = dict()
        # create and fill with empty values -> better for plotting
        self.apickpoints = np.zeros((10, len(self.shortnames), 2), float) + np.nan
        self.showpicks_act = True
        self.showpicks_all = True
        self.lastpickpoint = [np.nan, np.nan]

        self.pick_outfilename = 'picks_{}.npy'.format(time.strftime('%FT%H:%M:%S'))

        # initialize plot
        self.fig = plt.figure()
        # disable all matplotlib key bindings
        self.fig.canvas.mpl_disconnect(self.fig.canvas.manager.key_press_handler_id)
        self.fig.subplots_adjust(0.04,0.04,0.98,0.98,0.01)
        self.dataplot = plt.subplot2grid((8,4),(0,0),rowspan=7,colspan=4)
        self.dataplot.autoscale(1,'both',1)
        # self.y_subplot=plt.subplot2grid((8,4),(4,2),rowspan=4,colspan=2)
        #self.ax = self.fig.add_subplot(111)
        
        data = self.prepare_data()
        # self.img = self.dataplot.imshow(data, cmap=self.cmap, aspect='auto')
        self.imgplot = xarray.plot.imshow(data, ax=self.dataplot, cmap=self.cmap)
        # self.pickplots    = [self.dataplot.plot([], [], '+', ms=10)[0] for i in range(10)]
        self.pickplots    = [self.dataplot.plot([], [], '+-', ms=10)[0] for i in range(10)]
        self.pickplotsact = [self.dataplot.plot([], [], 'ro', ms=6)[0] for i in range(10)]
        self.picktextact  = [self.dataplot.text(np.nan, np.nan, 'niet') for i in range(10)]

        self.timeplot = plt.subplot2grid((8,4),(7,0),rowspan=1,colspan=4)
        if self.with_timeline:
            self.timeplot.plot_date(self.dates, np.ones(len(self.dates)), 'ko')
            self.timeline, = self.timeplot.plot([self.dates[self.filenr]]*2, [0,2], 'r-')
            self.timestring = self.timeplot.text(self.dates[0], 1.5, 'niet')
            dt = self.dates[-1]-self.dates[0]
            self.timeplot.set_xlim(self.dates[0]-0.02*dt, self.dates[-1]+0.02*dt)
            self.timeplot.set_ylim(0,2)

        dispname = self.shortnames[self.filenr]
        self.title = self.dataplot.set_title(dispname)
        self.fig.canvas.mpl_connect('button_press_event',self.click)
        self.fig.canvas.mpl_connect('key_press_event',self.click)
        self.clim = None
        self.climfact = [2.,2.]
        self.maskfact = 0.7

        # self.rax = self.fig.add_axes([0.025, 0.5, 0.15, 0.15], axisbg='r')
        # self.radio = RadioButtons(self.rax, ('lin', 'log'), active=1)
        # def colorfunc(label):
        #     # l.set_color(label)
        #     plt.draw()
        # self.radio.on_clicked(colorfunc)

        # axcolor = 'lightgoldenrodyellow'
        # self.axfreq = self.fig.add_axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
        # self.axamp  = self.fig.add_axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)

        # sfreq = Slider(self.axfreq, 'Freq', 0.1, 30.0, valinit=1)
        # samp = Slider(self.axamp, 'Amp', 0.1, 10.0, valinit=1)

        self.click(None)

    def prepare_data_gpri(self):
        filename = self.filenames[self.filenr]
        if self.filenr != self.filenrold:
            print ('loading data from',filename)
            self.data = GPRIfile(filename).data
        data = self.data.copy()
        if 'slc' in self.filetype:
            data = np.sqrt(data[:,:,0]**2 + data[:,:,1]**2)
        if 'int' in self.filetype:
            data = np.arctan2(data[:,:,1],data[:,:,0])
        if 'l' in self.modifyer:
            data = np.log(data)
        # mask data with cc or power
        if 'p' in self.modifyer:
            if self.filetype == 'mli':
                pdata = self.data[:]
            elif self.filetype == 'unw':
                # load the INT file from which the UNW was produced
                print('load intfile')
                intfile = GPRIparamfile(filename).param['GT_unw_intfile']
                fn = filename.split('/')[:-2]
                fn.extend(intfile.split('/')[-2:])
                filename = '/'.join(fn)
                pdata = GPRIfile(filename).getMagnitude()
            else:
                pdata = np.sqrt(self.data[:,:,0]**2 + self.data[:,:,1]**2)
                print(pdata.max(), pdata.min(), pdata.std())
                pdata /= (4.*pdata.std())
            data[pdata < self.maskfact] = np.nan

        return data

    def prepare_data_geotiff(self):
        filename = self.filenames[self.filenr]
        print(filename)
        if self.filenr != self.filenrold:
            print ('loading data from',filename)
            ## using xarray rasterio
            data = xarray.open_rasterio(filename)
            # make the array display correctly
            if data.res[1] < 0:
                data['y'] = data.y[::-1]
        return data

    def prepare_data(self):
        return self.prepare_data_geotiff()

    def get_dates(self, filenames):
        # self.get_dates_enveo(filenames)
        self.get_dates_sentinelflow(filenames)

    def click(self, event):
        change_clim = ''
        update = False
        self.filenrold = self.filenr
        if type(event) is matplotlib.backend_bases.MouseEvent:
            if event.inaxes == self.dataplot:
                # use right mouse button to avoid conflict with zooming
                if event.button == 3:  
                    # self.pickpoints.setdefault(self.pickpoint, dict())[self.filenr] = (event.xdata, event.ydata)
                    self.lastpickpoint = self.apickpoints[self.pickpoint, self.filenr, :].copy()
                    self.apickpoints[self.pickpoint, self.filenr, :] = (event.xdata, event.ydata)
                    self.filenrold = -999
                    update = True
                    # always save the picked points
                    with open(self.pick_outfilename, 'wb') as of:
                        np.save(of, self.apickpoints)
                        np.save(of, self.shortnames)
            elif event.inaxes == self.timeplot:
                print('juppi')
                if event.button == 1:
                    filenr = np.argmin(np.abs(event.xdata-self.dates))
                    self.filenr = filenr
                    update = True
        if type(event) is matplotlib.backend_bases.KeyEvent:
            print('key event<', event.key, '>')
            update = True
            if event.key == 'q':
                plt.clf()
                import sys
                sys.exit()
            elif event.key == 'f':
                self.filenr += 1
            elif event.key == 'b':
                self.filenr -= 1
            elif event.key == 'alt+f':
                self.filenr += int(len(self.filenames)/20.)
            elif event.key == 'alt+b':
                self.filenr -= int(len(self.filenames)/20.)
            elif event.key == 'alt+a':
                self.filenr = 0
            elif event.key == 'alt+e':
                self.filenr = len(self.filenames)-1
            elif event.key == ' ':
                self.filenr += 1
            elif event.key == '.':
                self.filenr += 1
            elif event.key == 'backspace':
                self.filenr -= 1
            elif event.key == ',':
                self.filenr -= 1
            elif event.key == 'M':
                self.maskfact += 0.05
            elif event.key == 'm':
                self.maskfact -= 0.05
            elif event.key == 'h':         # toggle (show/hide) current pickpoints
                self.showpicks_act = not self.showpicks_act
                self.filenrold = -999
                update = True
            elif event.key == 'H':         # toggle (show/hide) all pickpoints
                self.showpicks_all = not self.showpicks_all
                if self.showpicks_all:
                    self.showpicks_act = True
                self.filenrold = -999
                update = True
            elif event.key == 'ctrl+z':    # undo last pickpoint
                self.apickpoints[self.pickpoint, self.filenr, :] = self.lastpickpoint
                self.filenrold = -999
                update = True
            elif event.key == 'c':
                if 'c' in self.modifyer:
                    self.modifyer.remove('c')
                else:
                    self.modifyer.add('c')
            elif event.key == 'p':
                if 'p' in self.modifyer:
                    self.modifyer.remove('p')
                else:
                    self.modifyer.add('p')
            elif event.key == 'l':
                if 'l' in self.modifyer:
                    self.modifyer.remove('l')
                else:
                    self.modifyer.add('l')
                change_clim = 'S'
            elif event.key in 'dDuUS':
                change_clim = event.key
            elif event.key == 'g':
                if self.cmap == plt.cm.jet:
                    self.cmap = plt.cm.gray
                else:
                    self.cmap = plt.cm.jet
            elif event.key in '0123456789':        # set pickpoint on the image
                self.pickpoint = int(event.key)
                self.filenrold = -999
                update = True
            else:
                update = False

        print('update', update)
        if update is False:
            return

        self.filenr = min(self.filenr, len(self.filenames)-1)
        self.filenr = max(self.filenr, 0)
        filename = self.filenames[self.filenr]

        data = self.prepare_data()
        if not self.clim:
            change_clim = 'S'
        # change color limits to standard deviation
        if change_clim:
            if change_clim == 'S':
                self.climfact = [2., 2.]
            if change_clim == 'u':
                self.climfact[1] -= 1.
            if change_clim == 'U':
                self.climfact[1] += 1.
            if change_clim == 'd':
                self.climfact[0] += 1.
            if change_clim == 'D':
                self.climfact[0] -= 1.
            mean = np.nanmean(data)
            std  = np.nanstd(data)
            d, u = self.climfact
            self.clim = [mean-d*std, mean+u*std]
        plt.setp(self.imgplot, 'clim', self.clim)
        plt.setp(self.imgplot, 'cmap', self.cmap)

        dispname = 'file: {}   pick: {}'.format(filename.split('/')[-1].split('.')[0], self.pickpoint)
        plt.setp(self.title, 'text', dispname)
        # self.img = xarray.plot.imshow(data, ax=self.dataplot, cmap=self.cmap)
        data2 = np.rollaxis(data.values, 0, 3)
        self.imgplot.set_data(data2)
        
        for p, coord in enumerate(self.apickpoints):
            if self.showpicks_all:
                self.pickplots[p].set_data(coord[:,0], coord[:,1])
                self.pickplotsact[p].set_data(coord[self.filenr,0], coord[self.filenr,1])
            else:
                self.pickplots[p].set_data([],[])
                self.pickplotsact[p].set_data([],[])

        p = self.pickpoint
        if self.showpicks_act:
            coord = self.apickpoints[p]
            self.pickplots[p].set_data(coord[:,0], coord[:,1])
            self.pickplotsact[p].set_data(coord[self.filenr,0], coord[self.filenr,1])
        else:
            self.pickplots[p].set_data([],[])
            self.pickplotsact[p].set_data([],[])


        # acoord = []    # the active coordinates
        # for p in self.pickpoints:
        #     coord = np.array([c for c in self.pickpoints[p].values()])
        #     self.pickplots[p].set_data(coord[:,0], coord[:,1])
            
        #     if self.filenr in self.pickpoints[p]:
        #         coord = self.pickpoints[p][self.filenr] 
        #         acoord.append( coord )
        #         self.picktextact[p].set_text(p)
        #         self.picktextact[p].set_position(coord)
        #     else:
        #         self.picktextact[p].set_text('')

        # acoord = np.array(acoord)
        # if len(acoord) > 0:
        #     self.pickplotact.set_data(acoord[:,0], acoord[:,1])
        # else:
        #     self.pickplotact.set_data([], [])

        if self.with_timeline:
            self.timeline.set_xdata([self.dates[self.filenr]]*2)
            self.timestring.set_text(dispname)
        plt.draw()

# main call of the viewer

parser = argparse.ArgumentParser(description='Display GPRI files')
parser.add_argument('datapath', help="path to the data directory")
parser.add_argument('filetype', help="type of file to display")
parser.add_argument('--minfile', type=int, default=0,  
                    help='minimum index of file (default 0)')
parser.add_argument('--maxfile', type=int, default=-1,  
                    help='maximum index of file (default -1)')
parser.add_argument('-a', '--antenna', default='',  
                    help='antenna ("u" or "d")')
parser.add_argument('-b', '--band', default=0,  
                    help='raster band 0, 1, 2')

#args = parser.parse_args()
#args = parser.parse_args('/data/gpri/eqi2016/work/unwstack60 unw'.split())
#args = parser.parse_args('/data/jako/velocity_enveo/vel tif --band=1'.split())
#args = parser.parse_args('/data/satellite/sentinelflow_images/jako jpg --band=1'.split())
args = parser.parse_args('/data/images-glaciers/aerlen2020-acam jpg'.split())

v = GPRIviewer( args.datapath, args.filetype, antenna=args.antenna,
                minfile=args.minfile, maxfile=args.maxfile, band=args.band,
                with_timeline=True )
plt.show(block=True)

