# Extract amplitude an phase along a line in real space
# This will be performed on many files
#
# |slcdir| is the directory where the data files are stored
#
# |workdir| is the directory where the results are written to
#
# The line is defined by two points in real space, with origin at the GPRI:
# (p0x, p0y) and (p1x, p1y)
#
# |dt| is the time interval in minutes between acquisitons. 
# Just the mintues divided by dt are used (similar to */dt in cron
# 

import numpy as np
import gprifile
import glob, os, sys
import struct
import datetime
import argparse
import pylab as plt

# set up the command line parser
parser = argparse.ArgumentParser(description='Extract data along a line from a series raw SLC files')
parser.add_argument('slcdir', help='directory form which to read SLC files (input)')
parser.add_argument('workdir', help='directory to write the extracted data')
parser.add_argument('antenna', help='antenna (character [l/u])')
parser.add_argument('azofs', help='radar azimuth offset angle (deg, counterclock-wise)')
parser.add_argument('p0x', help='x-coord point 0')
parser.add_argument('p0y', help='y-coord point 0')
parser.add_argument('p1x', help='x-coord point 1')
parser.add_argument('p1y', help='y-coord point 1')
parser.add_argument('--nlines', type=int, default=1, help='number of parallel lines')
parser.add_argument('--dlines', type=float, default=100, help='distance to parallel lines')
parser.add_argument('--dt', type=int, default=1, help='time span between acquisitions in minutes')
# parser.add_argument('pw', default=1, help='width of extraction window')

# # debug/development arguments
# fakeargs = ['extract_line.py',
#             '/data/gpri/eqi2017/slc', 
#             '/data/gpri/eqi2017/work', 
#             'u',
#             # '-94',
#             '-81.5',
#             '-400', '4800',
#             '1000', '6800', 
#             '--nlines=13',
#             '--dlines=200',
#             '--dt=1'
#             ]
# sys.argv = fakeargs

args = parser.parse_args()

# the end-points defining the line 
azofs    = float(args.azofs)
p0x, p0y = float(args.p0x), float(args.p0y)
p1x, p1y = float(args.p1x), float(args.p1y)
nlines   = int(args.nlines)
dlines   = float(args.dlines)

# pw = 10  # the "window", width=(2*pw+1), how many points to extract sideways 

# get all filenames
filenames = sorted(glob.glob(args.slcdir+'/*{:s}.slc'.format(args.antenna)))
print('number of slc files:', len(filenames))

# create output directory
os.system('mkdir -p {:s}'.format(args.workdir))

# collect all the times from the file names
time = np.array([datetime.datetime.strptime(f.split('/')[-1][:15], '%Y%m%d_%H%M%S') 
                 for f in filenames]).astype('datetime64[ns]')

# time in minutes
timem = time.astype('datetime64[m]')
idx   = timem.astype(int)%(args.dt) == 0
time  = time[idx]
filenames = list(np.compress(idx, filenames))



print('number of reduced slc files:', len(filenames))

# load one GPRI file (lazily)
g = gprifile.GPRIfile(filenames[0])

# calculate the pixels of all grid cells on the line
# the number of segments
dr2 = g.dr/2.
ns = int(np.ceil(np.sqrt((p0x-p1x)**2 + (p0y-p1y)**2)/dr2))

# calculate the extraction lines
dx = p1x-p0x
dy = p1y-p0y
veclen = np.sqrt(dx**2+dy**2)
vec = np.array([dx, dy])/veclen
vecperp = np.array([dy, -dx])/veclen

# create all extraction point coordinates
allpoints = []
alllinepoints = []
p0 = np.array([p0x, p0y])
for i in range(nlines):
    vp0 = p0 + i*dlines*vecperp
    vp1 = vp0 + vec*veclen

    # create lots of points on the line
    px = np.linspace(vp0[0], vp1[0], ns)
    py = np.linspace(vp0[1], vp1[1], ns)

    # calculate radar coordinates for lots of points
    paz = np.arctan2(py, px)
    pr  = np.sqrt(px**2 + py**2)
    azp = (np.round( (-np.rad2deg(paz) - (g.az0 + azofs) )/g.daz )).astype(np.int)
    rp  = (np.round( (pr - g.r0)/g.dr )).astype(np.int)

    # remove duplicate points
    points = np.vstack((azp, rp)).T
    points = np.unique(points,axis=0)
    
    allpoints.append(points)
    alllinepoints.append((vp0, vp1))

#test plot
if 0:
    import pylab as plt

    gpricoordu = (528888.184, 7738432.341, 150.0)

    import xarray
    krel = xarray.open_rasterio('/data/greenland/gisdata/sentinel_julien/rgb/S2A_20180729_153044_200_RGB.jpg')
    # re-arrange data so that it can be plotted easily
    bd   = np.transpose(krel.data, (1,2,0)).astype(np.int)
    plt.imshow(bd[:,:,:], extent = (krel.x[0]-gpricoordu[0], krel.x[-1]-gpricoordu[0], 
                                    krel.y[-1]-gpricoordu[1], krel.y[0]-gpricoordu[1]))

    p0az = np.arctan2(p0y, p0x)
    p1az = np.arctan2(p1y, p1x)
    p0r  = np.sqrt(p0x**2 + p0y**2)
    p1r  = np.sqrt(p1x**2 + p1y**2)
    
    azp0 = int(round( (-np.rad2deg(p0az) - g.az0)/g.daz ))
    rp0  = int(round( (p0r - g.r0)/g.dr ))
    azp1 = int(round( (-np.rad2deg(p1az) - g.az0)/g.daz ))
    rp1  = int(round( (p1r - g.r0)/g.dr ))

    # rs = np.arange(p0r)
    # plt.plot(np.cos(p0az)*rs, np.sin(p0az)*rs, 'g')
    # rs = np.arange(p1r)
    # plt.plot(np.cos(p1az)*rs, np.sin(p1az)*rs, 'g')

    # calculate the coordinates of the radar pixels

    # switch direction of GPRI angle to math convention (counterclock-wise)
    azs  = -(g.az0 + azofs + np.arange(g.nas)*g.daz)
    rs   = g.r0 + np.arange(g.nrs)*g.dr

    xs = np.outer(np.cos(np.deg2rad(azs)), rs)
    ys = np.outer(np.sin(np.deg2rad(azs)), rs)

    # dem = xarray.open_rasterio('/home/tinu/projects/gpri/data/eqi2018dtm/20180706_145101u_map.tif')[0]
    # dem.data[dem.data<1] = np.nan
    # plt.imshow(dem, 
    #            extent=(dem.x[0]-gpricoordu[0], dem.x[-1]-gpricoordu[0], dem.y[-1]-gpricoordu[1], dem.y[0]-gpricoordu[1]), 
    #            vmin=0, vmax=200)

    plt.scatter(xs[::5,::50], ys[::5,::50], c=np.log(g.getMagnitude()[::5,::50]), s=2,
                cmap=plt.cm.gray, alpha=1,
                vmin=-5, vmax=1)

    # plt.scatter(xs[azp1-2:azp1+3,rp1-2:rp1+3], ys[azp1-2:azp1+3,rp1-2:rp1+3], color='k')

    # plt.plot(xs[azp0, rp0], ys[azp0, rp0], 'm^')
    # plt.plot(xs[azp1, rp1], ys[azp1, rp1], 'm^')

    plt.plot(0,0, '^', color='yellow', ms=10)

    # for points in allpoints:
    #     azp, rp = points.T
    #     plt.plot(xs[azp, rp], ys[azp, rp], 'b.')

    for p0v, p1v in alllinepoints:
        plt.plot([p0v[0], p1v[0]], [p0v[1], p1v[1]], 'r')

    plt.axis('scaled')
    plt.xlim(-4000, 12000)
    plt.ylim(-500, 10000)
    plt.savefig('/home/tinu/projects/eqi/fig/gpri_extract_situation_mli_lines.pdf', dpi=200)
    plt.show()
    stop

# read out the data from all results files
maxpoints = max([len(l) for l in allpoints])

# pre-create the values array, this is faster than assembling lists
data = np.zeros((len(filenames), len(allpoints), maxpoints, 2)) + np.nan
# read the points out of every array
for i, filename in enumerate(filenames):
    with open(filename, 'rb') as of:
        print('extracting data from ', filename)
        for j, points in enumerate(allpoints):
            for k, (az, r) in enumerate(points):
                of.seek((g.nrs*az+r)*2*4,0)
                data[i,j,k,:] = struct.unpack('>2f', of.read(8))
        of.close()

    # if 1:
    #     ## testing the array indexing: OK
    #     g = gprifile.GPRIfile(filename)
    #     #plt.plot(g.getMagnitude()[100,:], 'b')
    #     plt.plot(np.sqrt(data[:, 0, 3, 0]**2+data[:,0,3,1]**2), 'r')
    #     plt.show()
    #     stop

if 0:
    mag = np.sqrt(data[:, :, 0]**2+data[:,:,1]**2)
    plt.imshow(np.log(mag), extent=[0,2,0,1])
    plt.show()
    stop

attrs =dict(p0x=p0x, p0y=p0y, p1x=p1x, p1y=p1y,
            slcdir=args.slcdir, dt=args.dt, dr=g.dr,
            nlines=nlines, maxpoints=maxpoints, dlines=dlines)

np.save('extract_signal.npy', data)
np.save('extract_time.npy', time)
# # create a xarray dataset, and store as netcdf file
# ds = xarray.Dataset({'signal': (['time', 'line', 'points', 'complex'],  data)},
#                     coords={'time': time,
#                             'line': np.arange(nlines),
#                             'points': np.arange(maxpoints),
#                             'complex': [0,1],},
#                     attrs=attrs)
# outfilename = '{:s}/extract_{:.0f}_{:.0f}_{:.0f}_{:.0f}_{:d}_{:.0f}.nc'.format(args.workdir, p0x, p0y, p1x, p1y, nlines, args.dt)
# ds.to_netcdf(outfilename)
