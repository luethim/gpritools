# map the gpri data on a dem
#
# input: rectangular grid of output coordinates
#
# strategy: 
# 1. calculate polar representation of input coordinates
# 2. create raster_interpolation object of data to be mapped 
#    which are a regular grid in "polar" coordinates
# 3. collect raster mapping shape functions
# 4. for each raster file
# 4a. map

import numpy as np
from gprifile import GPRIparamfile, GPRIfile
import time
import xarray

# just-in time compilation, or let it be
# from numba import jit
# def jit(func):
#     return func

class PolarDEMMapper(object):

    def __init__(self, gpricoord, param, azofs=0):
        """
        |gpricoord|   is the absolute position of the GPRI (x,y,z)
        |param|       is a class/container that contains param.az0, param.daz etc, e.g. a gpriparamfile
        """
        self.gpricoord = gpricoord
        self.param = param    # a dictionary of GPRI parameters
        self.azofs = azofs
        self.debug = True

    
    # @jit
    def calc_nn_lookup(self, xr, yr, zsurf=None):
        """ vectorized version of lookup table calculation
        |xr| and |yr| are the rectangular coordinates
        |zsurf|       is a callable that returns the surface elevation at (x, y)
        """
        if self.debug:
            tt0 = time.time()
        # get the important parameters
        nrs, nas = self.param['nrs'], self.param['nas']
        r0, az0  = self.param['r0'],  self.param['az0']
        dr, daz  = self.param['dr'],  self.param['daz']

        self.nn_lookup = np.zeros((xr.shape[0], yr.shape[0], 2), int)   # an array of the indices
        x0, y0, z0 = self.gpricoord
        dx, dy = np.meshgrid(xr-x0, yr-y0, indexing='ij')
        
        # here we would call the DEM evaluation function
        dz = np.zeros((xr.shape[0], yr.shape[0]), dtype=float)
        if zsurf:
            for i, x in enumerate(xr):
                d = []
                for j, y in enumerate(yr):
                    dz[i,j] = zsurf(x,y) - z0

        self.dz = dz
        # self.zs = z0+dz
        az = -np.rad2deg(np.arctan2(dy, dx)) - az0 + self.azofs
        az[az<0]    += 360.
        az[az>=360] -= 360.
        r  = np.sqrt(dx**2 + dy**2 + dz**2)

        # the azimuth indices, rint is rounding to next integer
        self.nn_lookup[:,:,0] = np.rint( (az-daz/2)/daz )
        self.nn_lookup[:,:,1] = np.rint((r - r0) /dr)
        # tag data that are outside the range
        idx = ((self.nn_lookup[:,:,0] >= nas) | 
               (self.nn_lookup[:,:,1] >= nrs ))
        self.nn_lookup[idx,:] = -1
        if self.debug:
            print('-- lookup -->', time.time()-tt0)


    # @jit
    def map_nn_data(self, data):
        """
        extract the data from the |data| raster using the lookup table
        numba actually makes this slower
        """
        if self.debug:
            tt0 = time.time()
        grid = np.zeros(self.nn_lookup.shape[:2])+np.nan
        for ir in range(self.nn_lookup.shape[0]):
            ij  = self.nn_lookup[ir,:]
            # find indices of valid index values
            idx = ((ij[:,0] >= 0) & (ij[:,1] >= 0))
            grid[ir,idx] = data[ij[idx,0],ij[idx,1]]
        if self.debug:
            print('-- map    -->', time.time()-tt0)
        return grid

if __name__ == '__main__':

    import pylab as plt

    if 0:
        # Eqi
        gpricoordp = (-202806.81, -2206265.26, 150)
        gpricoordu = (528888.184, 7738432.341, 150.0)
        gpricoord = gpricoordu

    if 0:
        # Taconnaz
        # gpricoord = (553534, 80469, 2562)
        gpricoord = (949476.700, 2107119.335, 2560.613)

    # Testdata # black-white grid
    if 0:
        gpricoord = [0,0,0]
        azofs = 20

        ir = 0.01
        x0, x1 = 0, 10
        y0, y1 = -5, 5
        nrs, nas = 20, 10

        data = np.zeros((nas, nrs))
        for i in range(nas):
            for j in range(nrs):
                if divmod(i+j,2)[1] == 0:
                    data[i,j] = 1
        param = dict(r0=0, az0=0, nrs=nrs, nas=nas, dr=1, daz=2)
        
        def zsurf_fun(x, y):
            return 10*np.sin(x*10)*np.sin(y*10)

    # Coebeli
    if 0:
        gpricoord = [-186926.69,-2278822.84, 333]
        azofs = 13

        # # the coordinates of the grid pixels
        # # these are the pixels where the GPRI data should be evaluated
        ir = 20
        x0, x1 = 0, 20000
        y0, y1 = -10000, 10000


        # filename = '/data/gpri/coebeli/work/int/20210730_010201l__20210730_010001l.int'
        filename = '/data/gpri/coebeli/work/mli/20210730_000001l.mli'
        gf = GPRIfile(filename)
        data = gf.data
        # data = gf.getPhase()
        param = gf.param
 
        # this would be the DEM
        def zsurf_fun(x, y):
            #return 2000*np.sin(x/500.)*np.cos(y/500.)
            return 0.


    # Coebeli with Arcticdem
    if 1:
        gpricoord = [-186926.69,-2278822.84, 333]
        azofs = 13

        dem = xarray.open_rasterio('/home/data/greenland-gisdata/setsm_arctic_dem/18_39_32m_v3.0_reg_dem.tif')
        import scipy.interpolate
        # create rectangular grid

        # # the coordinates of the grid pixels
        # # these are the pixels where the GPRI data should be evaluated
        ir = 10
        x0, x1 = 5000, 12000
        y0, y1 = -7000, 5000

        xr = gpricoord[0] + np.arange(x0, x1, ir)
        yr = gpricoord[1] + np.arange(y0, y1, ir)
        dem_x = dem.x.values
        dem_y = dem.y.values
        idxx = (xr[0]-ir <= dem_x) & (dem_x <= xr[-1]+ir)
        idxy = (yr[0]-ir <= dem_y) & (dem_y <= yr[-1]+ir)
        demx = dem.x.values[idxx]
        demy = dem.y.values[idxy]

        vals = dem.values[0,idxy,:][:,idxx]
        zsurf_fun = scipy.interpolate.interp2d(dem.x.values[idxx], dem.y.values[idxy], 
                                           vals)

        # filename = '/data/gpri/coebeli/work/int/20210730_010201l__20210730_010001l.int'
        filename = '/data/gpri/coebeli/work/mli/20210729_183602l.mli'
        gf = GPRIfile(filename)
        data = gf.data
        # data = gf.getPhase()
        param = gf.param



    ## generic code
    # create rectangular grid
    xr = gpricoord[0] + np.arange(x0, x1, ir)
    yr = gpricoord[1] + np.arange(y0, y1, ir)

    intp = PolarDEMMapper(gpricoord, param, azofs=azofs)

    ## map without a terrain model
    intp.calc_nn_lookup(xr, yr, zsurf=None)

    ## for interferogram
    # data = np.arctan2(data[:,:,1],data[:,:,0])
        
    grid = intp.map_nn_data(data)

    plt.pcolormesh(xr, yr, grid.T, vmin=0., vmax=0.1, cmap=plt.cm.gray, shading='nearest', alpha=0.9)

    stop

    ## map with a terrain model
    intp.calc_nn_lookup(xr, yr, zsurf=zsurf_fun)

    # for interferogram
    # data = np.arctan2(data[:,:,1],data[:,:,0])
        
    griddem = intp.map_nn_data(data)
    plt.pcolormesh(xr, yr, griddem.T, vmin=0, vmax=0.3, cmap=plt.cm.Reds, shading='nearest', alpha=0.9)

    stop

    # plt.pcolormesh(xr, yr, grid.T, vmin=0, vmax=2*np.pi, cmap=plt.cm.jet, shading='nearest')
    plt.axis('equal')
    # dplot = plt.imshow(dgrid, extent=[vvg.xmin, vvg.xmax, vvg.ymin, vvg.ymax]);
    # xg, yg = np.meshgrid(xr, yr)
    # plt.plot(xg, yg, 'r.')

    # plot the frame
    xx0, xx1 = xr[0], xr[-1]
    yy0, yy1 = yr[0], yr[-1]
    plt.plot([xx0, xx1, xx1, xx0, xx0], [yy0, yy0, yy1, yy1, yy0], 'r')

    plt.show()

    if 1:
        print('writing raster')
        # try to save as geotiff
        import rasterio
        # xa = xarray.DataArray(grid, dims = ['x', 'y'],
        #                       coords = {'x': xr, 'y': yr})
        # count = 1

        # Use same coefficient order as GDAL's GetGeoTransform().
        # :param c, a, b, f, d, e: 6 floats ordered by GDAL.

        attrs = dict()
        val = np.array([xr[0], xr[1]-xr[0], 0, yr[0], 0, (yr[1]-yr[0])])
        attrs['transform'] = rasterio.Affine.from_gdal(*val)
        attrs['crs'] = rasterio.crs.CRS.from_epsg(3413)

        width, height = grid.shape
        with rasterio.open('gpri.tif', 'w',
                           driver='GTiff',
                           height=height, width=width,
                           dtype='float64', count=1,
                           **attrs) as dst:
            dst.write(grid.T, 1)


if 0:
    gg = gr.GeoRaster(grid.T[::-1,:],
                      (xr[0], xr[1]-xr[0], 0, yr[0], 0, (yr[1]-yr[0])),
		      nodata_value = np.nan,
		      projection = vvg.projection,
		      datatype = 'Float32')
    gg.to_tiff('/data/gpri/gpri_speed')

    dtebee = 5705.0 /(24.*60.)           # 5705: the Christoph Ebee time difference in minutes
    pixelSpacing = 0.17211
    # gridvv = vv.band[::ir, ::ir]
    # gridvx = vx.band[::ir, ::ir]*pixelSpacing/dtebee
    # gridvy = vy.band[::ir, ::ir]*pixelSpacing/dtebee
    gridvv = vvg.raster.data[::ir, ::ir]
    gridvx = vxg.raster.data[::ir, ::ir]*pixelSpacing/dtebee
    gridvy = vyg.raster.data[::ir, ::ir]*pixelSpacing/dtebee

    vproj = gridvx*np.cos(azr.T) - gridvy*np.sin(azr.T)
    # dgrid = vvg.raster.data + vproj #-  - grid.T[::-1,:]
    dgrid = vproj - grid.T[::-1,:]
    vplot = plt.imshow(dgrid, vmin=-2, vmax=2)
    plt.colorbar(vplot)
    plt.show()

    stop


    gg = gr.GeoRaster(vproj,
                      (xr[0], xr[1]-xr[0], 0, yr[-1], 0, -(yr[1]-yr[0])),
		      nodata_value = np.nan,
		      projection = vvg.projection,
		      datatype = 'Float32')
    gg.to_tiff('/data/gpri/ebee_speed_proj')

    gg = gr.GeoRaster(dgrid,
                      (xr[0], xr[1]-xr[0], 0, yr[-1], 0, -(yr[1]-yr[0])),
		      nodata_value = np.nan,
		      projection = vv.gprojection,
		      datatype = 'Float32')
    gg.to_tiff('/data/gpri/diff_speed_proj')


    stop

    axs[0].imshow(vproj, cmap=plt.cm.jet_r, extent=vv.extent, vmin=-15, vmax=0, alpha=1)
    #axs[0].imshow(-vv.band, cmap=plt.cm.jet_r, extent=vv.extent, vmin=-15, vmax=0, alpha=1)


    # plt.imshow(np.log(grid.T), vmin=-5, vmax=2, origin='lower',
    #            extent=[xr[0], xr[-1], yr[0], yr[-1]], alpha=0.3)
    axs[1].imshow(grid.T, cmap=plt.cm.jet_r, vmin=-15, vmax=0, 
                  origin='lower', extent=[xr[0], xr[-1], yr[0], yr[-1]], 
                  alpha=1.)


    # plot the evaluation box
    xb = [xr[0], xr[-1], xr[-1], xr[0], xr[0]]
    yb = [yr[0], yr[0], yr[-1], yr[-1], yr[0]]
    axs[0].plot(xb, yb, 'r-')
    axs[1].plot(xb, yb, 'r-')
    for i in range(2):
        axs[i].set_xlim([529000, 532000])
        axs[i].set_ylim([7742000, 7745000])

    # axs[0].axis('equal')
    # plt.savefig('fig/maptest{:.0f}.png'.format(gpricoord[2]))
    fig.show()

    if 0:
        i0, i1, y0, y1 = 100/azii, 500/azii, 1000/rii, 200/rii
        i0, i1, y0, y1 = 0,-1,0,-1
        # the coordinates of the radar pixels
        xp = np.outer(np.cos(np.deg2rad(azs)), rs)
        yp = np.outer(np.sin(np.deg2rad(azs)), rs)

        plt.scatter(xp[i0:i1, y0:y1], yp[i0:i1, y0:y1], 
                    marker = 'o', s=25, lw=0, 
                    c=np.log(data[i0:i1, y0:y1]), vmin=-5, vmax=0)
        xxr, yyr = np.meshgrid(xr, yr)
        plt.scatter(xxr, yyr, marker='o', lw=0, c='r')
        plt.axis('equal')
        plt.show()
        stop


    if 0:
        am, rm = 1, 6

        rss, azss = np.meshgrid(rs, azs)
        plt.scatter(rss, -azss, marker='.', color='b')
        #plt.plot(rss[am, rm], -azss[am, rm], 'ms')

        plt.figure()
        plt.scatter(xp, yp, marker='.', color='b')
        #plt.plot(xp[am, rm], yp[am, rm], 'ms')

        xxr, yyr = np.meshgrid(xr, yr)
        plt.scatter(xxr, yyr, color='r', marker='o')
        plt.axis('equal')

        azstart = -(gf.GPRI_az_start_angle)
        azstop  = azstart - gf.nas*gf.GPRI_az_angle_step
        xe, ye = 7000*np.cos(np.deg2rad(azstart)), 7000*np.sin(np.deg2rad(azstart))
        plt.plot([0, xe], [0,ye], 'k-')
        xe, ye = 7000*np.cos(np.deg2rad(azstop)), 7000*np.sin(np.deg2rad(azstop))
        plt.plot([0, xe], [0,ye], 'k-')
        plt.show()
        stop



    # intp = PolarDEMMapper(gpricoord, gf)
    # intp.calc_nn_lookup(xr, yr, zsurf_fun)
    # grid = intp.map_nn_data(data)
    # grid = intp.map_nn_data(data)
    # grid = intp.map_nn_data(data)


    # @jit
    # def calc_nn():
    #     # calculate nearest neighbor

    #     tt0 = time.time()
    #     lookup = np.zeros((xr.shape[0], yr.shape[0], 2), np.int)
    #     for ir, x in enumerate(xr):
    #         for jr, y in enumerate(yr):
    #             dx = x-x0
    #             dy = y-y0
    #             r  = np.sqrt(dx**2 + dy**2)
    #             az = -np.rad2deg(np.arctan2(dy, dx)) 
    #             i = int(np.rint((az-az0)/daz))
    #             j = int(np.rint((r -r0) /dr))
    #             lookup[ir, jr] = [i, j]
    #     print('--code-->', time.time()-tt0)

    #     tt0 = time.time()
    #     grid = np.zeros((xr.shape[0], yr.shape[0]))+np.nan
    #     for ir in range(xr.shape[0]):
    #         for jr in range(yr.shape[0]):
    #             i, j = lookup[ir, jr]
    #             if i < 0 or j < 0:
    #                 grid[ir, jr] = np.nan
    #             else:
    #                 grid[ir, jr] = data[i, j]
    #     print('--eval-->', time.time()-tt0)
    #     return grid

    # grid = calc_nn()


    # tt0 = time.time()
    # shapes = np.zeros((xr.shape[0], yr.shape[0], 4))
    # lookup = np.zeros((xr.shape[0], yr.shape[0], 2), np.int)
    # for ri, x in enumerate(xr):
    #     for rj, y in enumerate(yr):
    #         dx = x-x0
    #         dy = y-y0
    #         r  = np.sqrt(dx**2 + dy**2)
    #         az = np.rad2deg(np.arctan2(dx, dy)) 
    #         i, j, shp = rinterp.calcCellShape((az,r))
    #         shapes[ri, rj] = shp
    #         lookup[ri, rj] = [i, j]
    # print('--code-->', time.time()-tt0)


    # tt0 = time.time()
    # grid = np.zeros((xr.shape[0], yr.shape[0]))
    # for shp in shps:
    #     ri, rj, (i, j, shape) = shp

    #     nodval = [data[i,j], data[i+1,j], data[i+1,j+1], data[i,j+1]]
    #     val = np.dot(shape, nodval)
    #     grid[ri, rj] = val

    # print('--eval-->', time.time()-tt0)

    #print(np.nanmean(np.abs(grid-yr)))

    # import pylab as plt
    # plt.imshow(np.log(grid.T), vmin=-5, vmax=2, origin='lower',
    #            extent=[xr[0], xr[-1], yr[0], yr[-1]])
    # # plt.scatter(xp, yp, marker='.', color='r')

    # # xxr, yyr = np.meshgrid(xr, yr)
    # # plt.scatter(yyr, xxr, color='k', marker='.')

    # # plot the evaluation box
    # xb = [xr[0], xr[-1], xr[-1], xr[0], xr[0]]
    # yb = [yr[0], yr[0], yr[-1], yr[-1], yr[0]]
    # plt.plot(xb, yb, 'r-')

    # plt.axis('equal')
    # # plt.savefig('fig/maptest{:.0f}.png'.format(gpricoord[2]))
    # plt.show()

    # # save the averaged data to a new file
    phase2m = 0.0087209/(2.*np.pi)   # conversion from phase to meter

