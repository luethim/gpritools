# read a simple parameter file, and represent it as dict

import os
from collections import OrderedDict

class ParamFile(object):

    def __init__(self, paramfile):

        self.param = OrderedDict()
        if os.path.exists(paramfile):
            # read the parameter file
            with open(paramfile) as of:
                for line in of.readlines()[1:]:
                    if line.startswith('#') or len(line) < 5:
                        continue
                    k, v = line.split('#')[0].split(':',1)
                    self.param[k.strip()] = v.strip()

            # convert parameters to something useful, i.e. int or float
            for k, v in self.param.items():
                vv = v.split()[0]   # strip off the unit string
                try:
                    self.param[k] = int(vv)
                except:
                    try:
                        self.param[k] = float(vv)
                    except:
                        self.param[k] = v
                    pass
        self.__dict__.update(self.param)


if __name__ == '__main__':

    p = ParamFile('../data/rec_param_eqi2016_full.par')
