# create wrappers for all GAMMA programs, so that they are easily callable from Python
# we support named optional variables, even if GAMMA does not support this in their C version
# 
# for this we parse all the descriptions when calling the Gamma software
# 
# we also assume a gamma software wrapper like ../bin/gamma
# if this does not exist just set it to ''

import glob, os

gammawrapper = '/home/tinu/bin/gamma'

# interactive programs, do not scan these
noscan = ('get_value,polyras,bpf,coord_trans,').split(',')

GAMMAENVS = dict([line.strip().split('=') for line in file(gammawrapper).readlines()
                  if line.count('=') == 1])

GAMMA_HOME = GAMMAENVS['GAMMA_HOME']

of = file('gamma_wrappers_auto.py', 'w')
of.write("""# DO NOT EDIT: automatically generated
#
# wrapper functions for the GAMMA software

         
import os, inspect

verbose = True
gammawrapper = '%s' 
""" % gammawrapper)

of.write("""
def makecmd(callname, args, optargs):
    cmd = gammawrapper + ' ' + callname + ' '
    for arg in args: 
        cmd += str(arg)+' ' 
    for arg in optargs: 
        if not arg is None: 
            cmd += str(arg)+' ' 
    if verbose:
         print '------------------ calling -------------------'
         print cmd
         print '----------------------------------------------'

    return cmd
\n\n""")


# scan all executables, assumed to be in GAMMA_HOME
filenames = glob.glob(GAMMA_HOME+'/*/bin/*')
for filename in filenames[:]:
    basename = os.path.basename(filename)
    print filename
    if basename in noscan:
        continue
    lines = os.popen('%s %s' % (gammawrapper, basename)).readlines()
    # change the function name with dash to underscore for Python
    basename = basename.replace('-','_')
    for line in lines:
        if 'usage:' in line:
            line = line.replace('def', 'def_').replace('/','_').replace('-','_').replace('xmax>','xmax')   # def is a statement in Python
            line = line.replace(']}',']').replace('.','')
            w = line.split()
            paramargs    = [p[1:-1] for p in w[2:] if p.startswith('<')]
            optparamargs = [p[1:-1] for p in w[2:] if p.startswith('[')]

            defparamargs    = ', '.join(paramargs)
            defoptparamargs = ', '.join([p+'=None' for p in optparamargs])


            of.write('def %s(%s, %s):' %(basename, defparamargs, defoptparamargs)+'\n')
            of.write('    """')
            of.write('    '.join(lines))
            of.write('    """\n\n')
            of.write('    args = [%s]\n' % (' ,'.join(paramargs)) )
            of.write('    optargs = [%s]\n' % (' ,'.join(optparamargs)) )
            of.write("    cmd = makecmd('%s', args, optargs) \n" % basename)
            of.write("    os.system(cmd)" )
            of.write('\n\n')
of.close()
            
            

