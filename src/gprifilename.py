# a class to hold GPRI filenames

import os.path
import datetime

class GPRIfilename(object):

    """A class holding the necessary information of GPRI filenames
    The assumed naming scheme is assumed to be (D is the datetime string)

    o for simple filenames Da.ext  %Y%m%d_%H%M%Sa.ext   where "a" is "u" or "l"  (Antenna)
    
    o for interferograms:  D1a__D2b.ext // %Y%m%d_%H%M%Sa__%Y%m%d_%H%M%Sb.ext
      where "a" and "b" refer to the antennas

    """

    dateformat = '%Y%m%d_%H%M%S'

    filetype = {'slc': 'slc',
                'rslc': 'slc',
                'cc': 'cc',
                'int': 'int',
                'unw': 'unw',
                'adf.int': 'int',
                'int.adf': 'int',
                'adf.unw': 'int',
                'mli': 'mli'
                }

    def __init__(self, filename):
        self.filename = filename
        self.path, self.basenameext = os.path.split(filename)
        ws = self.basenameext.split('.')
        self.basename, self.ext = ws[0], '.'.join(ws[1:])
        ds = self.basename.split('__')
        try:
            self.date = datetime.datetime.strptime(ds[0][:-1], self.dateformat)
        except:
            self.date = None
        # try to extract a second date
        try: 
            self.date1 = datetime.datetime.strptime(ds[1][:-1], self.dateformat)
            # make consistently date0 smaller than date1
            if self.date > self.date1:
                self.date, self.date1 = self.date1, self.date
        except:
            self.date1 = None
        self.date0 = self.date

        self.antenna = ds[0][-1]
        # try to find a second antenna
        try:
            self.antenna1 = ds[1][-1]
        except:
            self.antenna1 = None
        self.antenna0 = self.antenna

        try:
            self.type = self.filetype[self.ext]
        except:
            self.type = self.ext

    def __repr__(self):
        return self.filename

    def __str__(self):
        return self.filename

    def __call__(self):
        return self.filename

    def u(self):
        w = self.filename.split('/')
        x = w[-1].split('.')
        w[-1] = x[0][:-1]+'u.'+x[1]
        return '/'.join(w)

    def l(self):
        w = self.filename.split('/')
        x = w[-1].split('.')
        w[-1] = x[0][:-1]+'l.'+x[1]
        return '/'.join(w)

    def mliname(self, mlidir=''):
        if not mlidir:
            ps = list(os.path.split(self.path)[:-1])
            ps.extend('.mli')
            mlidir = '/'.join(ps)
        return os.path.join(mlidir, self.basename+'.mli')

if __name__ == '__main__':
    fn = GPRIfilename('/data/gpri/eqi2015_all/dtm/20150621_120001u__20150621_120001l.int.adf')
    print(fn)

    print(fn.path)
    print(fn.basename)
    print(fn.date0, fn.antenna0)
    print(fn.date1, fn.antenna1)
    print(fn.ext, '--->', fn.type)
    print()

    fn = GPRIfilename('/data/gpri/eqi2015_all/slc/20150621_120001u.slc')
    print(fn)
    print(fn.mliname())
