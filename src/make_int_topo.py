# create topographic (u/l) inteferograms for all files for a directory

import argparse
import glob, os
import gamma_wrappers as gw
from gprifilename import GPRIfilename
from gprifile import GPRIparamfile

# set up the command line parser
parser = argparse.ArgumentParser(description='Process GPRI SLCs to INTs, do this for all pairs of u/l files')
parser.add_argument('slcdir', help='directory form which to read SLC files (input)')
parser.add_argument('workdir', help='directory with MLI files and INT files (input/output)')
parser.add_argument('-r', '--rlks',  type=int, default=5,  
                    help='range looks (integer)')
parser.add_argument('-a', '--azlks', type=int, default=1, 
                    help='azimuth looks (integer)')
parser.add_argument('--adf', type=int, default=0, 
                    help='create adf filtered files (integer [0/1])')

args = parser.parse_args()

# fakeargs = ['/data/gpri/gwalp/slc/20101119/',
#             '/data/gpri/gwalp/work' ]
#args = parser.parse_args(fakeargs)

# subdirectories of workdir
intdir = '{:s}/int_topo'.format(args.workdir)
adfdir = '{:s}/adf'.format(args.workdir)
mlidir = '{:s}/mli'.format(args.workdir)

# create output directory
os.system('mkdir -p {:s}'.format(intdir))

# get all files to convert
filenamesu = set(glob.glob(args.slcdir+'/*u.slc'))
filenamesl = set(glob.glob(args.slcdir+'/*l.slc'))

# check whether pairs of u/l antennas exist
filenames = [fn for fn in filenamesu if fn.replace('u','l') in filenamesl]

print('\nprocessing {:d} SLC files'.format(len(filenames)))

# create interferograms with respect to the previous file
slcfile0  = GPRIfilename(filenames[0])

# create on stupid offset parameter file (instead of calculating them)
# feed an empty parameter file to the interacitve command "create_offset"
ofpfile = '{:s}/of_par.in'.format(intdir)
offfile = '{:s}/intf_generic.off'.format(intdir)
open(ofpfile, 'w').write('\n\n\n\n\n\n')
gw.create_offset(slcfile0.filename+'.par', slcfile0.filename+'.par', 
                 offfile, 1, of_par_in=ofpfile)

for filename0 in filenames:
    filename1 = filename0.replace('u','l')
    slcfile0 = GPRIfilename(filename0)
    slcfile1 = GPRIfilename(filename1)
    print('-> processing', slcfile1.basename, slcfile0.basename)

    intname  = '{:s}__{:s}'.format(slcfile1.basename, slcfile0.basename)
    intfile  = '{:s}/{:s}.int'.format(intdir, intname)
    ccfile   = '{:s}/{:s}.cc'.format(intdir, intname)
    adfintfile  = '{:s}/{:s}.int'.format(adfdir, intname)
    adfccfile   = '{:s}/{:s}.cc'.format(adfdir, intname)
    mlifile0 = '{:s}/{:s}.mli'.format(mlidir, slcfile0.basename)
    mlifile1 = '{:s}/{:s}.mli'.format(mlidir, slcfile1.basename)
    mliparam = GPRIparamfile(mlifile0)

    if os.path.exists(intfile) and (os.path.getsize(intfile) > 1000):
        print('already done' , intfile)
        continue

    if not os.path.exists(mlifile0) or not os.path.exists(mlifile1):
        print('... MLI file missing', mlifile0, mlifile1)
        continue

    # create interferogram
    gw.SLC_intf(slcfile0.filename, slcfile1.filename, 
                offfile, intfile, args.rlks, args.azlks, 0, '-', 0, 0)

    # write the parameters
    intparam = GPRIparamfile(filename0)
    intparam.param['GT_slcfile0']         = slcfile0.filename
    intparam.param['GT_slcfile1']         = slcfile1.filename
    intparam.param['range_samples']       = int(intparam.param['range_samples']/args.rlks)
    intparam.param['near_range_slc']      = intparam.param['near_range_slc']+(args.rlks-1)/2.*intparam.param['range_pixel_spacing']
    intparam.param['azimuth_lines']       = int(intparam.param['azimuth_lines']/args.azlks)
    intparam.param['range_pixel_spacing'] = intparam.param['range_pixel_spacing']*args.rlks
    intparam.param['range_looks']         = int(intparam.param['range_looks']*args.rlks)
    intparam.writeParam(intfile+'.par')


    # filter interferogram with ADF (adaptive filter)
    if args.adf == 1:
        gw.adf(intfile, adfintfile, adfccfile, mliparam.nrs)
        intparam.setParamValue('GT_adfparam', 'default')
        intparam.writeParam(adfintfile+'.par')

    # create coherence map
    gw.cc_wave(intfile, mlifile0, mlifile1, ccfile, mliparam.nrs, 3, 3, 1)
    # write the parameters
    intparam.param['image_format'] = 'FLOAT'

    # write out parameter file
    intparam.writeParam(ccfile+'.par')


