# create MLI files for a directory

import argparse
import glob, os
import gamma_wrappers as gw
from  gprifilename import GPRIfilename

# set up the command line parser
parser = argparse.ArgumentParser(description='Process GPRI SLCs to MLIs')
parser.add_argument('slcdir', help='directory form which to read SLC files (input)')
parser.add_argument('workdir', help='work directory under which products (MLI files) are saved (output)')
parser.add_argument('-r', '--rlks',  type=int, default=5,  
                    help='range looks (integer)')
parser.add_argument('-a', '--azlks', type=int, default=1, 
                    help='azimuth looks (integer)')

args = parser.parse_args()

# fakeargs = ['/data/gpri/eqi2015_all/rslc', 
#             '/data/gpri/eqi2015_all/work']
#args = parser.parse_args(fakeargs)

# subdirectories of workdir
mlidir = '{:s}/mli'.format(args.workdir)

# create output directory
os.system('mkdir -p {:s}'.format(mlidir))

# get all files to convert
filenames = sorted(glob.glob(args.slcdir+'/*slc'))

# create mli files
for filename in filenames:
    gf = GPRIfilename(filename)
    mliname  = gf.mliname(mlidir)

    if not os.path.exists(mliname):
        print(filename)
        gw.multi_look(filename, filename+'.par', 
                      mliname, mliname+'.par', 
                      args.rlks, args.azlks)
