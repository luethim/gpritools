# create MLI in cartesion coordinates

import argparse
import glob, os
import gamma_wrappers as gw
from gprifile import GPRIfile
from paramfile import ParamFile
from polar_dem_mapper import PolarDEMMapper
import numpy as np
import pylab as plt

# fakeargs = ['/data/gpri/eqi2016/work/unwstack60', 
#             '/data/gpri/eqi2016/work/mlirec', 
#             '../data/rec_param_eqi2016_full.par', 
#             'unw',
#             '5']

# set up the command line parser
parser = argparse.ArgumentParser(description='Map MLIs to cartesian coordinates')
parser.add_argument('indir', help='work directory under which products are saved (output)')
parser.add_argument('outdir', help='work directory under which products are saved (output)')
parser.add_argument('parfile', help='parameter file for the GPRI location')
parser.add_argument('filetype', help='the filetype (e.g. "mli", "cc"')
parser.add_argument('pix_size', type=int, help='output pixel size (meters)')

args = parser.parse_args()
#args = parser.parse_args(fakeargs)

p = ParamFile(args.parfile)

# # GPRI coordinates in UTM
# gpricoordp = (-202806.81, -2206265.26, 230)
# gpricoordu = (528888.184, 7738432.341, 230.0)
# gpricoord = (0., 0., 230.0)
# azofs = -76.5

# the coordinates of the grid pixels
# these are the pixels where the GPRI data should be evaluated
xr = p.gprix + np.arange(p.xr0, p.xr1, args.pix_size)
yr = p.gpriy + np.arange(p.yr0, p.yr1, args.pix_size)

# # gpricoord = np.array((506580.3721, 8621799.4381, 500.4358))
# gpricoord = np.array((0, 0, 230.))
# #azofs = -34.5    # ca., mit GIMPDEM referenziert for polarstereo
# azofs = -30    # ca., mit GIMPDEM referenziert for polarstereo

# if 1:
#     try:
#         gimpdem
#     except:
#         import gdalraster
#         gimpdem = gdalraster.GdalRaster('/data/greenland/gisdata/gimpdem/gimpdem_bowdoin_hillshade_utm19.tif')
#     plt.imshow(gimpdem.band[:,:], extent=gimpdem.extent, cmap=plt.cm.gray)


# # the coordinates of the grid pixels
# # these are the pixels where the GPRI data should be evaluated
# xr = gpricoord[0] + np.arange( 2000, 8000, pix_size) 
# yr = gpricoord[1] + np.arange( -500, 4000, pix_size)

# GPRI point decimation (mainly for testing)
azii = 1
rii  = 1

# this would be the DEM
def zsurf_fun(x, y):
    return 200.   # sea level

def saveAsNETCDF4(grid, outfilename):
    from netCDF4 import Dataset
    rootgrp = Dataset(outfilename, 'w', format='NETCDF4')
    x = rootgrp.createDimension('x', grid.shape[0])
    y = rootgrp.createDimension('y', grid.shape[1])
    wdata = rootgrp.createVariable('data','f4',('x','y'))
    wdata[:] = grid[:]
    rootgrp.close()

# create output directory
os.system('mkdir -p {:s}'.format(args.outdir))

# get all files to convert
filenames = sorted(glob.glob('{}/*.{}'.format(args.indir, args.filetype)))

# # decimate GPRI points (mainly for testing)
# rs, azs = rs[::rii], azs[::azii]
# daz     = daz*azii
# dr      = dr*rii

# read the first file and setup parameters
gf   = GPRIfile(filenames[0])
gf.setParamValue('az0', gf.az0 + p.azofs)

try:
    intp
except:
    # create interpolation object
    intp = PolarDEMMapper((p.gprix, p.gpriy, p.gpriz), gf)
    intp.calc_nn_lookup(xr, yr, zsurf_fun)

fig, ax = plt.subplots()

for filename in filenames[1:]:
    print(filename)
    # calculate the cartesian coordinates of the GPRI data in the (az, slant-range) plane
    gf   = GPRIfile(filename)
    data = gf.data #[::azii, ::rii]
    grid = intp.map_nn_data(data)

    saveAsNETCDF4(grid.T, '{}/unw_rec_{}.nc'.format(args.outdir, gf.fn.basename))
    # saveAsNETCDF4(grid.T, '{}/cc_rec_{}.nc'.format(args.outdir, gf.fn.basename))
    #saveAsNETCDF4(grid.T, '{}/mli_rec_{}.nc'.format(args.outdir, gf.fn.basename))

    if 0:
        ax.imshow(np.log(grid.T), vmin=-5, vmax=2, origin='lower',
                  cmap = plt.cm.gray,
                  extent=[xr[0], xr[-1], yr[0], yr[-1]])

        # # plot the evaluation box
        # xb = [xr[0], xr[-1], xr[-1], xr[0], xr[0]]
        # yb = [yr[0], yr[0], yr[-1], yr[-1], yr[0]]
        # plt.plot(xb, yb, 'r-')
        # plt.show()
        # stop
        ax.set_aspect('equal')
        fig.savefig('{}/{}.png'.format(args.outdir, gf.fn.basename), dpi=200)
        plt.show()
        stop
        #plt.cla()


