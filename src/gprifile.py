import numpy as np
import os, struct
from collections import OrderedDict
import datetime
import gprifilename

class GPRIparamfile(object):
    """A class representing a GAMMA parameter data file
    Parameters are read and converted automatically if possible
    We have to work around some annonying inconsistencies in the
    Gamma file naming scheme and parameter naming schemes
    """
    def __init__(self, filename, paramfile=None, width=None):
        """read a GPRI file including the parameter file"""

        self.fn = gprifilename.GPRIfilename(filename)
        # set width if given
        if width:
            param['nas'] = int(width)
        # figure out the parameter file name
        if paramfile is None:
            paramfile = filename + '.par'

        # try to find a generic parameter file for the whole collection
        # this file is called 'allint.par' or 'allmli.par' etc
        if not os.path.exists(paramfile):
            paramfile = os.path.dirname(filename) + '/all{:s}.par'.format(self.fn.ext)

        if os.path.exists(paramfile):
            # read the parameter file
            param = OrderedDict()
            with open(paramfile) as of:
                for line in of.readlines()[1:]:
                    if len(line) < 5:
                        continue
                    k, v = line.split(':',1)
                    param[k.strip()] = v.strip()

            # convert parameters to something useful, i.e. int or float
            for k, v in param.items():
                vv = v.split()[0]   # strip off the unit string
                try:
                    param[k] = int(vv)
                except:
                    try:
                        param[k] = float(vv)
                    except:
                        param[k] = v
                    pass
        try: 
            param
        except: 
            print ('no parameter file and no width given')
            param = dict()

        # set the data type and number of components of the data

        # ">f4" and 1 are the default values
        self.dtype, self.ncomps  = '>f4', 1

        try:
            imformat = param['image_format'].strip()
            if imformat.strip() == 'FCOMPLEX':
                self.dtype, self.ncomps  = '>f4', 2
            elif imformat.strip() == 'FLOAT':
                self.dtype, self.ncomps  = '>f4', 1
        except: 
            pass

        # add consistent names for the number of range and azimuth samples
        try:
            try:
                param['nas'] = param['azimuth_lines']
                param['nrs'] = param['range_samples']
            except:
                param['nas'] = param['az_samp_1']
                param['nrs'] = param['range_samp_1']
        except:
            pass
        # add simple names for samples
        try:
            param['r0']  = param['near_range_slc']
            param['dr']  = param['range_pixel_spacing']
            param['az0'] = param['GPRI_az_start_angle']
            param['daz'] = param['GPRI_az_angle_step']
        except:
            pass

        # set the parameter values as attributes
        self.param = param
        self.__dict__.update(param)

    def setParamValue(self, name, value):
        self.param[name] = value
        self.__dict__.update(self.param)

    def updateParam(self, param):
        self.param = param
        self.__dict__.update(param)

    def writeParam(self, outfilename):
        with open(outfilename, 'w') as of:
            of.write('Gamma Interferometric SAR Processor (ISP) - Image Parameter File\n')
            for key in self.param:
                of.write('{:s}: {:s} \n'.format(key, str(self.param[key])))

    def calcCartesianCoordinates(self, azii=1, rii=1):
        """return the cartesian coordinates of radar pixels"""
        r0   = self.near_range_slc
        dr   = self.range_pixel_spacing
        az0  = self.GPRI_az_start_angle + azofs
        daz  = self.GPRI_az_angle_step
        # switch direction of GPRI angle to math convention (counterclock-wise)
        azs  = -(az0 + np.arange(self.nas)*daz)
        rs   = r0 + np.arange(self.nrs)*dr

        # decimate GPRI points
        if azii > 1 or rii > 1:
            rs, azs = rs[::rii], azs[::azii]

        # the coordinates of the radar pixels
        xp = np.outer(np.cos(np.deg2rad(azs)), rs)
        yp = np.outer(np.sin(np.deg2rad(azs)), rs)
        return xp, yp


class GPRIfile(GPRIparamfile):
    """A class representing a GAMMA data file
    Parameters are read and converted automatically if possible

    lazy data reading
    """

    def __init__(self, filename, paramfile=None, width=None):
        """does not do much due to lazy data reading"""
        GPRIparamfile.__init__(self, filename, paramfile, width=width)
        self.filename = filename
        self.paramfile = paramfile
        self.data = None

    def __getattribute__(self, key):
        """implement lazy data reading: 
        read only when requested by attribute access"""
        if key == 'data':
            try:
                self._data
            except:
                self.readData()
            return self._data
        else:
            return object.__getattribute__(self, key)

    def setData(self, data):
        """set the data field and the
        corresponding parameters
        """
        self._data = data
        self.param['nas'], self.param['nrs'] = data.shape[:2]
        self.param['azimuth_lines']  = self.param['nas'] 
        self.param['range_samples'] = self.param['nrs'] 

        if len(data.shape) == 3:
            self.param['ncomp'] = data.shape[2]
        else:
            self.param['ncomp'] = 1
        self.__dict__.update(self.param)

    def readData(self):
        """read the data file"""
        #print('--> reading data')
        data = np.fromfile(self.filename, dtype = '>f4')
        # TODO: should read the parfile filetype, and decode data according to that
        if not self.ncomps == data.shape[0]/(self.nas*self.nrs):
            print('incompatible array information', self.nas, self.nrs)
            stop
        if self.ncomps == 1:
            self._data = data.reshape((self.nas, self.nrs))
        else:
            self._data = data.reshape((self.nas, self.nrs, self.ncomps))

    def writeData(self, outfilename):
        """write a data file"""
        np.asarray(self._data, '>f4').tofile(outfilename)
        
    def write(self, outfilename):
        self.writeData(outfilename)
        self.writeParam(outfilename+'.par')

    def readPixels(self, pixels):
        fmt = '>f'
        if self.ncomps == 2:
            fmt = '>2f'
        res = []
        with open(self.filename, 'rb') as of:
            for iy, ix in pixels:
                of.seek((self.nrs*iy+ix)*8,0) # 8 is 2*4 (2 dims, 4 bytes)
                byt = of.read(8)
                res.append( struct.unpack(fmt, byt) )
        return res

    def getPhase(self):
        """calculate and return the phase"""
        return np.arctan2(self.data[:,:,1], self.data[:,:,0])

    def getMagnitude(self):
        """calculate and return the magnitude"""
        return np.sqrt(self.data[:,:,0]**2 + self.data[:,:,1]**2)

    def getPixelCoordinates(self, azofs=0., npa=1, npr=1):
        """calculate and return the absolute coordinates of the pixels in 2D space"""
        dr  = self.range_pixel_spacing
        r0  = self.near_range_slc
        az0 = self.GPRI_az_start_angle
        daz = self.GPRI_az_angle_step
        azs = -(az0 + np.arange(self.nas)*daz) + azofs
        rs  = r0 + np.arange(self.nrs)*dr

        # decimate coordinates
        azs  = azs[::npa]
        rs   = rs[::npr]

        xs   = np.outer(np.sin(np.deg2rad(azs)), rs)
        ys   = np.outer(np.cos(np.deg2rad(azs)), rs)
        return xs, ys

    def saveAsNETCDF4(self, outfilename):
        from netCDF4 import Dataset
        rootgrp = Dataset(outfilename, 'w', format='NETCDF4')
        az = rootgrp.createDimension('azimuth', self.nas)
        r  = rootgrp.createDimension('range', self.nrs)
        if self.ncomps > 1:
            comps = rootgrp.createDimension('comps', self.ncomps)
            data = rootgrp.createVariable('data','f4',('azimuth','range','comps'))
        else:
            data = rootgrp.createVariable('data','f4',('azimuth','range'))
        data[:] = self.data[:]
        rootgrp.close()


if __name__ == '__main__':
    import pylab as plt

    #g = GPRIfile('../data/eqi_bellevue/processing/int/20140628_160001l_20140628_160101l.diff')
    #g = GPRIfile('../data/eqi_bellevue/processing/int/20140628_160401l_20140628_160501l.unw')
    #g = GPRIfile('/data/gpri/eqi_bellevue/tsunami20140702/mli/20140701_135001l.mli')
    g = GPRIfile('/data/gpri/eqi_bellevue/tsunami20140702/slgc/20140702_140502u.slc')
    # g = GPRIfile('/data/gpri/taco17/work_tux/unw/20170927_091201u__20170927_091101u.unw')
    stop
    # g = GPRIfile('/data/gpri/eqi2015_all/rslc/20150620_000001l.rslc')
    #g = GPRIparamfile('/data/gpri/eqi2016/work/mli/20160819_182644u.mli')
    #g = GPRIfile('/data/gpri/bowdoin/work/int/20160708_000002u__20160708_000201u.int')

    g.saveAsNETCDF4('test.nc')
    stop


    # import glob
    # filenames = sorted(glob.glob('../data/eqi_bellevue/processing/int/*.diff'))
    # g = GPRIfile(filenames[0])
    # gcollect = g.data

    # # for filename in filenames[1:]:
    # #     gcollect += GPRIfile(filename).data

    # ph = np.arctan(gcollect[:,:,0], gcollect[:,:,1])
    # plt.imshow(ph)
    # plt.show()
    # stop


    # ph1 = np.arctan2(img1[:,:,0], img1[:,:,1])
    # ph2 = np.arctan2(img2[:,:,0], img2[:,:,1])

    # power = img1[:,:,0]**2+img1[:,:,1]**2
    # dph = np.ma.masked_array(ph1-ph2, np.log(power > 0.2))

    #coh = (img1[:,:,0]*img2[:,:,0]-img1[:,:,1]*img2[:,:,1])

    
    ## test whether we read the data correctly: YES!
    # gw.cpx_to_real(filenames[0], 'o_real', 548, 0)
    # gw.cpx_to_real(filenames[0], 'o_imag', 548, 1)
    # gw.cpx_to_real(filenames[0], 'o_phi', 548, 4)

    # gf = gprifile.GPRIfile(filenames[0])
    # phi0 = np.arctan2(gf.data[:,:,1], gf.data[:,:,0])

    # real = np.fromfile('o_real', dtype = '>f4')
    # imag = np.fromfile('o_imag', dtype = '>f4')
    # phi = np.fromfile('o_phi', dtype = '>f4')

    # stop





    import pylab as plt
    # plt.imshow(dph[:,::10])
    # plt.imshow(np.log(coh[:,::10]))

    # plt.show()
    # stop

    # plot data in rectangular coordinates

    dr   = g.range_pixel_spacing
    r0   = g.near_range_slc
    phi0 = g.GPRI_az_start_angle
    dphi = g.GPRI_az_angle_step
    phis = phi0 + np.arange(g.nas)*dphi
    rs   = r0 + np.arange(g.nrs)*dr

    npx = 5

    phis = phis[100:800][::npx]
    rs   = rs[::npx]
    pimg = np.log(g.data[100:800,:][::npx, ::npx])
    xs   = np.outer(np.sin(np.deg2rad(phis)), rs)
    ys   = np.outer(np.cos(np.deg2rad(phis)), rs)

    plt.scatter(xs, ys, c=pimg,   cmap=plt.cm.jet,
                #s=5e-4*(rs[:, np.newaxis])**1.5/float(npx),
                marker='o', lw=0, vmin=0.7*pimg.min(), vmax=0.7*pimg.max())
    plt.axis('equal')
    plt.show()
