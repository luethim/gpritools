# PLEASE EDIT: static version from automatically generated file
#
# wrapper functions for the GAMMA software

import os

verbose = True
gammawrapper = '/home/tinu/bin/gamma'

def makecmd(callname, args, optargs):
    cmd = 'nice ' + gammawrapper + ' ' + callname + ' '
    for arg in args:
        cmd += str(arg)+' '
    for arg in optargs:
        if not arg is None:
            cmd += str(arg)+' '
    if verbose:
         print ('------------------ calling -------------------')
         print (cmd)
         print ('----------------------------------------------')

    return cmd


def rasSLC(SLC, width, start=None, nlines=None, pixavr=None, pixavaz=None, scale=None, exp=None, LR=None, data_type=None, header=None, rasf=None):
    """*** DISP Program rasSLC ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of intensity of complex (SLC) data using power law scaling ***
    
    usage: rasSLC <SLC> <width> [start] [nlines] [pixavr] [pixavaz] [scale] [exp] [LR] [data_type] [header] [rasf]
    
    input parameters:
      SLC        (input) SLC image (FCOMPLEX or SCOMPLEX data type)
      width      complex samples per row
      start      starting line of SLC (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35, .5=magnitude)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      data_type  SLC data type:
                   0: FCOMPLEX  pairs of 4-byte float values (default)
                   1: SCOMPLEX  pairs of 2-byte short integers
      hdrsz      line header size in bytes (default: 0, ESA SLC products have 12 byte headers)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
    """

    args = [SLC, width]
    optargs = [start, nlines, pixavr, pixavaz, scale, exp, LR, data_type, header, rasf]
    cmd = makecmd('rasSLC', args, optargs) 
    os.system(cmd)

def flip(infile, outfile, width, format=None, sense=None):
    """*** DISP Program: flip.c ***
    *** Copyright 2004, Gamma Remote Sensing, v1.2 29-Jun-2004 uw/clw ***
    *** flip data right/left, top/bottom or both (multiple formats) ***
    
    usage: flip <infile> <outfile> <width> [format] [sense]
    
    input parameters: 
      infile   (input) input data file (float)
      outfile  (output) output data file (flipped)
      width    number of data values/line
      format   input/output data file format flag
               (default=0:float,  1:fcomplex,  2:scomplex,  3:uchar,  4:short)
      sense    flip sense flag
               (default=1:right/left,  2:top/bottom,  3:right/left, and top/bottom)
    
    """

    args = [infile, outfile, width]
    optargs = [format, sense]
    cmd = makecmd('flip', args, optargs) 
    os.system(cmd)

def gcp_2ras(ras1, ras2, gcp, mag=None, win_sz=None):
    """*** DISP gcp_2ras: Select corresponding pairs of points in SUN raster or BMP images ***
    *** Copyright 2014, Gamma Remote Sensing, v1.4 27-Feb-2014 clw ***
    
    usage: gcp_2ras <ras1> <ras2> <gcp> [mag] [win_sz]
    
    input parameters: 
      ras1    (input) image 1 SUN raster *.ras, or BMP *.bmp format
      ras2    (input) image 2 SUN raster *.ras, or BMP *.bmp format
              NOTE: the image depth (bits/pixel) of ras1 and ras2 must be the same
      gcp     (output) ground control point file (text format)
      mag     zoom magnification factor (default: 3)
      win_sz  zoom window size before magnification (default: 120)
    
    """

    args = [ras1, ras2, gcp]
    optargs = [mag, win_sz]
    cmd = makecmd('gcp_2ras', args, optargs) 
    os.system(cmd)

def dismph_pwr24(cpx, pwr, width, start_cpx=None, start_pwr=None, nlines=None, scale=None, exp=None):
    """*** DISP Program dismph_pwr24 ***
    *** Copyright 2013, Gamma Remote Sensing, v1.4 25-Aug-2013 clw ***
    *** Display phase derived from complex data + intensity ***
    
    usage: dismph_pwr24 <cpx> <pwr> <width> [start_cpx] [start_pwr] [nlines] [scale] [exp]
    
    input parameters:
      cpx        (input) complex image to determine phase (fcomplex, e.g. interferogram)
      pwr        (input) intensity image (float)
      width      number of samples/row of cpx and pwr
      start_cpx  starting line of cpx (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      intensity display scale factor (default=1.)
      exp        intensity display exponent (default=.3)
    
    """

    args = [cpx, pwr, width]
    optargs = [start_cpx, start_pwr, nlines, scale, exp]
    cmd = makecmd('dismph_pwr24', args, optargs) 
    os.system(cmd)

def gcp_ras(ras, GCP, mag=None, win_sz=None):
    """*** gcp_ras: select GCPs using a SUN raster or BMP format reference image ***
    *** Copyright 2012, Gamma Remote Sensing, v1.3 22-Jan-2012 clw ***
    
    usage: gcp_ras <ras> <GCP> [mag] [win_sz]
    
    input parameters: 
      ras     (input) SUN raster *.ras, or BMP *.bmp format image
      GCP     (output) GCP data file (text format)
      mag     zoom magnification factor (default=3)
      win_sz  zoom window size before magnification (default=120)
    
    """

    args = [ras, GCP]
    optargs = [mag, win_sz]
    cmd = makecmd('gcp_ras', args, optargs) 
    os.system(cmd)

def rasbyte(raw, width, start=None, nlines=None, pixavr=None, pixavaz=None, scale=None, LR=None, rasf=None):
    """*** DISP Program rasbyte ***
    *** Copyright 2011, Gamma Remote Sensing, v2.3 28-Sep-2011 clw/uw ***
    *** Generate raster graphics image of unsigned byte data ***
    
    usage: rasbyte <raw> <width> [start] [nlines] [pixavr] [pixavaz] [scale] [LR] [rasf]
    
    input parameters: 
      raw      (input) unsigned byte data (uchar, e.g. SAR raw data)
      width    bytes per row of raw
      start    starting line of raw (default=1)
      nlines   number of lines to display (default=0: to end of file)
      pixavr   number of pixels to average in range (default: 1)
      pixavaz  number of pixels to average in azimuth (default: 1)
      scale    display scale factor (default=1.)
      LR       left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf     (output) image filename, enter - for default, extension determines format:
                 *.bmp BMP format
                 *.ras Sun raster format (default)
    """

    args = [raw, width]
    optargs = [start, nlines, pixavr, pixavaz, scale, LR, rasf]
    cmd = makecmd('rasbyte', args, optargs) 
    os.system(cmd)

def dis2rmg(unw1, unw2, width1, width2, start=None, nlines=None, roff=None, azoff=None, ph_scale=None, ph_offset=None):
    """*** DISP dis2rmg: alternating display of two unwrapped phase images ***
    *** Copyright 2005, Gamma Remote Sensing, v1.5 24-Aug-2005 clw ***
    
    usage: dis2rmg <unw1> <unw2> <width1> <width2> [start] [nlines] [roff] [azoff] [ph_scale] [ph_offset]
    
    input parameters: 
      unw1       (input) unwrapped phase image 1 (float)
      unw2       (input) unwrapped phase image 2 (float)
      width1     samples per row of unw1
      width2     samples per row of unw2
      start      starting line of unw1 (default=1) 
      nlines     number of lines to display (default=0: to end of file)
      roff       range offset of unw2 relative to unw1
      azoff      azimuth offset of unw2 relative to unw1
      ph_scale   phase display scale factor (default=.33333)
      ph_offset  phase offset in radians subtracted from unw2 (default=.0)
    
    """

    args = [unw1, unw2, width1, width2]
    optargs = [start, nlines, roff, azoff, ph_scale, ph_offset]
    cmd = makecmd('dis2rmg', args, optargs) 
    os.system(cmd)

def ras8_color_scale(rasf, color_model=None, h0=None, hrange=None, i=None, sat=None, chip_width=None, gap=None, chip_height=None):
    """*** DISP Program ras8_color_scale ***
    *** Copyright 2011, Gamma Remote Sensing, v1.4 1-Oct-2011 clw ***
    *** Generate 8-bit raster graphics image of the selected color scale ***
    
    usage: ras8_color_scale <rasf> [color_model] [h0] [hrange] [i] [sat] [chip_width] [gap] [chip_height]
    
    input parameters:
      rasf        (output) color scale as 8-bit raster graphics image
                    *.bmp BMP format
                    *.ras Sun raster format
      color_model color model selection flag:
                    0: CMY color model used in Gamma Software (default)
                    1: HLS (double hexagon color model)
                    2: HSV (single hexagon color model)
                    3: SIN (sinusoidal color model)
      h0          starting hue (color for starting value, default: 0.0)
      hrange      range of hue values in degrees (-360. ... 360., default: 360.)
      i           brightness value (0.0 ... 1.0, default: 0.80)
      sat         color saturation value (0.0 ... 1.0, default: 0.75)
     chip_width  width of color chips (default: 8)
     gap         number of pixels between color chips (default: 1)
     chip_height height of color chips (default=8)
    
    """

    args = [rasf]
    optargs = [color_model, h0, hrange, i, sat, chip_width, gap, chip_height]
    cmd = makecmd('ras8_color_scale', args, optargs) 
    os.system(cmd)

def dis2SLC(SLC1, SLC2, width1, width2, start=None, nlines=None, roff=None, azoff=None, scale=None, exp=None, data_type=None):
    """*** DISP dis2SLC: alternating display of two SLC images ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 4-Apr-2012 clw ***
    
    usage: dis2SLC <SLC1> <SLC2> <width1> <width2> [start] [nlines] [roff] [azoff] [scale] [exp] [data_type]
    
    input parameters:
      SLC1       (input) SLC image 1 (FCOMPLEX or SCOMPLEX data type)
      SLC2       (input) SLC image 2
      width1     samples per row of SLC1
      width2     samples per row of SLC2
      start      starting line of slc1 (default=1)
      nlines     number of lines to display (default=0: to end of file)
      roff       range offset of slc2 relative to slc1
      azoff      azimuth offset of slc2 relative to slc1
      scale      display scale factor (default=1.)
      exp        display exponent (default=.5)
      data_type  SLC1 and SLC2 data format
                   0: FCOMPLEX
                   1: SCOMPLEX (default)
    
    """

    args = [SLC1, SLC2, width1, width2]
    optargs = [start, nlines, roff, azoff, scale, exp, data_type]
    cmd = makecmd('dis2SLC', args, optargs) 
    os.system(cmd)

def disras(ras, mag=None, win_sz=None):
    """*** DISP disras: display of SUN raster and BMP format images ***
    *** Copyright 2013, Gamma Remote Sensing, v1.6 25-Aug-2013 clw ***
    
    usage: disras <ras> [mag] [win_sz]
    
    input parameters: 
      ras     (input) SUN raster *.ras, or BMP *.bmp format image
      mag     zoom magnification factor (default=3)
      win_sz  zoom window size before magnification (default=120)
    
    """

    args = [ras]
    optargs = [mag, win_sz]
    cmd = makecmd('disras', args, optargs) 
    os.system(cmd)

def ras_cpt_scale(rasf, cpt, color_model=None, width=None, nlines=None, start_value=None, end_value=None):
    """*** DISP ras_cpt_scale: generate a 24-bit raster color scale from a color map file ***
    *** Copyright 2011, Gamma Remote Sensing, v1.2 28-Sep-2011 awi ***
    
    usage: ras_cpt_scale <rasf> <cpt> [color_model] [width] [nlines] [start_value] [end_value]
    
    input parameters:
      rasf       (output) image filename, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format
      cpt         (input) color map file, *.cpt 
      color_model color model selection flag: (default=0)
                    0: RGB color model
                    1: HLS (double hexagon color model)
                    2: HSV (single hexagon color model)
     width        number of pixels of the color scale image (default=200)
     nlines       number of lines of the color scale image (default=20)
     start_value  start value of color scale (default=0)
     end_value    end value of color scale (default=255)
    
    """

    args = [rasf, cpt]
    optargs = [color_model, width, nlines, start_value, end_value]
    cmd = makecmd('ras_cpt_scale', args, optargs) 
    os.system(cmd)

def rasdt_pwr24(data, pwr, width, start_data=1, start_pwr=1, nlines=0, pixavr=1, pixavaz=1, cycle=2e-3, scale=1., exp=0.35, LR=None, rasf=None, cc=None, start_cc=None, cc_min=None):
    """*** DISP Program rasdt_pwr24 ***
    *** Copyright 2014, Gamma Remote Sensing, v1.8 22-Jan-2014 clw ***
    *** Generate 24-bit raster graphics image of deformation or phase + intensity data ***
    
    usage: rasdt_pwr24 <data> <pwr> <width> [start_data] [start_pwr] [nlines] [pixavr] [pixavaz] [cycle] [scale] [exp] [LR] [rasf] [cc] [start_cc] [cc_min]
    
    input parameters:
      data       (input) real-valued image (float, e.g. deformation rate, terrain height)
      pwr        (input) intensity image (enter - for none, float)
      width      number of samples/row of data and pwr
      start_data starting line of data (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      cycle      data value per color cycle (default =  2.0000e-03)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      cc         display threshold data file (float): e.g. correlation
      start_cc   starting line of cc data file (default=1)
      cc_min     pixels with cc values below cc_min are displayed using greyscale (default=.2)
    
    """

    args = [data, pwr, width]
    optargs = [start_data, start_pwr, nlines, pixavr, pixavaz, cycle, scale, exp, LR, rasf, cc, start_cc, cc_min]
    cmd = makecmd('rasdt_pwr24', args, optargs) 
    os.system(cmd)

def ras_dB(pwr, width, start=None, nlines=None, pixavr=None, pixavaz=None, min_dB=None, max_dB=None, dB_offset=None, LR=None, rasf=None, abs_flag=None, inverse=None, channel=None):
    """*** DISP Program ras_dB ***
    *** Copyright 2013, Gamma Remote Sensing, v2.6 25-Aug-2013 clw ***
    *** Generate 8-bit raster graphics image of image intensity using logarithmic dB scale ***
    
    usage: ras_dB <pwr> <width> [start] [nlines] [pixavr] [pixavaz] [min_dB] [max_dB] [dB_offset] [LR] [rasf] [abs_flag] [inverse] [channel]
    
    input parameters: 
      pwr        (input) intensity image (float)
      width      samples per row of pwr
      start      starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      min_dB     minimum dB value for display (default=-40.0)
      max_dB     maximum dB value for display (default=10.0)
      dB_offset  dB value subtracted from input data (default=0.0)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      abs_flag   absolute value flag (default=0: normal, 1: absolute value)
      inverse    inverse flag (default=1: float_to_raster, -1: raster_to_float)
      channel    RBG channel flag (default=1:red, 2:green, 3:blue)
    
    """

    args = [pwr, width]
    optargs = [start, nlines, pixavr, pixavaz, min_dB, max_dB, dB_offset, LR, rasf, abs_flag, inverse, channel]
    cmd = makecmd('ras_dB', args, optargs) 
    os.system(cmd)

def mapshd(DEM, width, col_post, row_post, theta0, phi0, shade, format=None, zero_flag=None):
    """*** DISP Program mapshd ***
    *** Copyright 2014, Gamma Remote Sensing, v1.0 28-Jun-2014 uw/clw ***
    *** Generate shaded relief intensity image from DEM ***
    
    usage: mapshd <DEM> <width> <col_post> <row_post> <theta0> <phi0> <shade> [format] [zero_flag]
    
    input parameters:
      DEM        (input) digital elevation model (float)
      width      samples per row of DEM
      col_post   posting between cols (same unit as DEM values)
      row_post   posting between rows (enter - for default: col_post)
      theta0     illumination elevation angle in deg. (enter - for default: 45.)
      phi0       illumination orientation angle in deg. (enter - default: 135)
                   0.:right, 90:top, 180:left, 270:bottom
      shade      (output) output shaded relief intensity image (float)
      format     DEM data format (enter - for default):
                   0: float (default)
                   1: short integer
      zero_flag  zero data handling (enter - for default):
                   0: 0.0 interpreted at missing value (default)
                   1: 0.0 interpreted as valid data
    """

    args = [DEM, width, col_post, row_post, theta0, phi0, shade]
    optargs = [format, zero_flag]
    cmd = makecmd('mapshd', args, optargs) 
    os.system(cmd)

def dis2byte(image1, image2, width1, width2, start=None, nlines=None, roff=None, azoff=None, scale=None):
    """*** DISP dis2byte: alternating display of two unsigned byte images ***
    *** Copyright 2005, Gamma Remote Sensing, v1.5 24-Aug-2005 clw ***
    
    usage: dis2byte <image1> <image2> <width1> <width2> [start] [nlines] [roff] [azoff] [scale]
    
    input parameters:
      image1  (input) unsigned byte image 1
      image2  (input) unsigned byte image 2
      width1  samples per row of image1
      width2  samples per row of image2
      start   starting line of image1 (default=1)
      nlines  number of lines to display (default=0: to end of file)
      roff    range offset of image2 relative to image1
      azoff   azimuth offset of image2 relative to image1
      scale   display scale factor (default=1.)
    
    """

    args = [image1, image2, width1, width2]
    optargs = [start, nlines, roff, azoff, scale]
    cmd = makecmd('dis2byte', args, optargs) 
    os.system(cmd)

def rashgt_shd(hgt1, hgt2, width, col_post, row_post=None, start=None, nlines=None, pixavr=None, pixavaz=None, theta0=None, phi0=None, color0=None, m_cycle=None, LR=None, rasf=None):
    """*** DISP Program rashgt_shd ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6 22-Jan-2014 uw/clw ***
    *** Generate 8-bit raster graphics image of data (height, cc, deformation) + shaded relief from height ***
    
    usage: rashgt_shd <hgt1> <hgt2> <width> <col_post> [row_post] [start] [nlines] [pixavr] [pixavaz] [theta0] [phi0] [color0] [m/cycle] [LR] [rasf]
    
    input parameters:
      hgt1      (input) height data (float), source of shaded relief
      hgt2      (input) height data 2 (float), source of raster image colors
      width     samples per row of hgt1 and hgt2
      col_post  posting between cols (same unit as hgt1 values)
      row_post  posting between rows (same unit as hgt1, default=col_post)
      start     starting line of hgt1 (default=1)
      nlines    number of lines to display (default=0: to end of file)
      pixavr    number of pixels to average in range (default=1)
      pixavaz   number of pixels to average in azimuth (default=1)
      theta0    illumination elevation angle in deg. (default=45.)
      phi0      illumination orientation angle in deg. (default=135.)
                (0.:right, 90:top, 180:left, 270:bottom)
      color0    starting value for color scale (in units of hgt2, default=0.0)
      m/cycle   meters per color cycle (default=160.)
      LR        left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf      (output) image filename, enter - for default, extension determines format:
                  *.bmp BMP format
                  *.ras Sun raster format (default)
    """

    args = [hgt1, hgt2, width, col_post]
    optargs = [row_post, start, nlines, pixavr, pixavaz, theta0, phi0, color0, m_cycle, LR, rasf]
    cmd = makecmd('rashgt_shd', args, optargs) 
    os.system(cmd)

def discc(cc, pwr, width, start_cc=None, start_pwr=None, nlines=None, min_corr=None, max_corr=None, scale=None, exp=None):
    """*** DISP discc: display of coherence + intensity image ***
    *** Copyright 2012, Gamma Remote Sensing, v3.3 4-Apr-2012 clw ***
    
    usage: discc <cc> <pwr> <width> [start_cc] [start_pwr] [nlines] [min_corr] [max_corr] [scale] [exp]
    
    input parameters: 
      cc         (input) coherence image (float)
      pwr        (input) intensity image (float, enter - if not available)
      width      samples per row of cc and pwr
      start_cc   starting line of cc (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      cmin       minimum coherence value used for linear cc display (default=.1)
      cmax       maximum coherence value used for linear cc display (default=.9)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
    
    """

    args = [cc, pwr, width]
    optargs = [start_cc, start_pwr, nlines, min_corr, max_corr, scale, exp]
    cmd = makecmd('discc', args, optargs) 
    os.system(cmd)

def rasdt_pwr(data, pwr, width, start_data=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, cycle=None, scale=None, exp=None, LR=None, rasf=None, cc=None, start_cc=None, cc_min=None):
    """*** DISP Program rasdt_pwr ***
    *** Copyright 2014, Gamma Remote Sensing, v1.1 4-Mar-2014 clw ***
    *** Generate 8-bit raster graphics image of deformation or phase + intensity data ***
    
    usage: rasdt_pwr <data> <pwr> <width> [start_data] [start_pwr] [nlines] [pixavr] [pixavaz] [cycle] [scale] [exp] [LR] [rasf] [cc] [start_cc] [cc_min]
    
    input parameters:
      unw         (input) unwrapped phase data (float)
      pwr         (input) intensity data (float, enter - for none)
      width       samples per row of unwrapped phase and intensity files
      start_data  starting line of unwrapped phase file (default: 1)
      start_pwr   starting line of intensity file (default: 1)
      nlines      number of lines to display (default 0: to end of file)
      pixavr      number of pixels to average in range (default: 1)
      pixavaz     number of pixels to average in azimuth (default: 1)
      cycle       data value change per color cycle (default =  2.0000e-03)
      scale       pwr display scale factor (default: 1.0)
      exp         pwr display exponent (default: .35)
      LR          left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf        (output) image filename, enter - for default, extension determines the format:
                    *.bmp BMP format
                    *.ras Sun raster format (default)
      cc          (input) correlation threshold data file (float): e.g. correlation
      start_cc    starting line of threshold data file (default=1)
      cc_min      pixels with cc values below cc_min are displayed using greyscale (default=.2)
    
    """

    args = [data, pwr, width]
    optargs = [start_data, start_pwr, nlines, pixavr, pixavaz, cycle, scale, exp, LR, rasf, cc, start_cc, cc_min]
    cmd = makecmd('rasdt_pwr', args, optargs) 
    os.system(cmd)

def dis2mph(cpx1, cpx2, width1, width2, start=None, nlines=None, roff=None, azoff=None, scale=None, exp=None):
    """*** DISP dis2mph: alternating display of magnitude/phase of two complex images ***
    *** Copyright 2012, Gamma Remote Sensing, v1.9 27-Dec-2012 clw ***
    
    usage: dis2mph <cpx1> <cpx2> <width1> <width2> [start] [nlines] [roff] [azoff] [scale] [exp]
    
    input parameters: 
      cpx1    (input) complex image 1 (fcomplex, e.g. interferogram)
      cpx2    (input) complex image 2 (fcomplex, e.g. interferogram)
      width1  complex samples per row of cpx1
      width2  complex samples per row of cpx2
      start   starting line of cpx1 (default=1)
      nlines  number of lines to display (default=0: to end of file)
      roff    range offset of cpx2 relative to cpx1
      azoff   azimuth offset of cpx2 relative to cpx1
      scale   display scale factor (default=1.)
      exp     display exponent (default=.35)
    
    """

    args = [cpx1, cpx2, width1, width2]
    optargs = [start, nlines, roff, azoff, scale, exp]
    cmd = makecmd('dis2mph', args, optargs) 
    os.system(cmd)

def disdt_pwr24(data, pwr, width, start_data=None, start_pwr=None, nlines=None, cycle=None, scale=None, exp=None):
    """*** DISP Program disdt_pwr24 ***
    *** Copyright 2007, Gamma Remote Sensing, v1.4 21-May-2007 clw ***
    *** Display float parameter (e.g. height or deformation) + intensity image ***
    
    usage: disdt_pwr24 <data> <pwr> <width> [start_data] [start_pwr] [nlines] [cycle] [scale] [exp]
    
    input parameters:
      data        (input) real valued image (float, e.g. deformation rate, terrain height)
      pwr         (input) intensity image (float)
      width       number of samples/row of data and pwr
      start_data  starting line of data (default=1)
      start_pwr   starting line of pwr (default=1)
      nlines      number of lines to display (default=0: to end of file)
      cycle       data value per color cycle (default =  1.0000e-02)
      scale       intensity display scale factor (default=1.)
      exp         intensity display exponent (default=.35)
    
    """

    args = [data, pwr, width]
    optargs = [start_data, start_pwr, nlines, cycle, scale, exp]
    cmd = makecmd('disdt_pwr24', args, optargs) 
    os.system(cmd)

def rasrmg(unw, pwr, width, start_unw=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, ph_scale=None, scale=None, exp=None, ph_offset=None, LR=None, rasf=None, cc=None, start_cc=None, cc_min=None):
    """*** DISP Program rasrmg ***
    *** Copyright 2014, Gamma Remote Sensing, v2.9 3-Mar-2014 clw ***
    *** Generate 8-bit raster graphics image from unwrapped phase + intensity data ***
    
    usage: rasrmg <unw> <pwr> <width> [start_unw] [start_pwr] [nlines] [pixavr] [pixavaz] [ph_scale] [scale] [exp] [ph_offset] [LR] [rasf] [cc] [start_cc] [cc_min]
    
    input parameters:
      unw        (input) unwrapped phase data (float)
      pwr        (input) intensity data (float, enter - for none)
      width      samples per row of unwrapped phase and intensity files
      start_unw  starting line of unwrapped phase file (default: 1)
      start_pwr  starting line of intensity file (default: 1)
      nlines     number of lines to display (default 0: to end of file)
      pixavr     number of pixels to average in range (default: 1)
      pixavaz    number of pixels to average in azimuth (default: 1)
      ph_scale   phase display scale factor (default:  0.33333)
                 NOTE: one color cycle: 2PI/ph_scale
      scale      pwr display scale factor (default: 1.0)
      exp        pwr display exponent (default: .35)
      ph_offset  phase offset in radians subtracted from unw (default: 0.0)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines the format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      cc         (input) correlation threshold data file (float): e.g. correlation
      start_cc   starting line of threshold data file (default=1)
      cc_min     pixels with cc values below cc_min are displayed using greyscale (default=.2)
    
    """

    args = [unw, pwr, width]
    optargs = [start_unw, start_pwr, nlines, pixavr, pixavaz, ph_scale, scale, exp, ph_offset, LR, rasf, cc, start_cc, cc_min]
    cmd = makecmd('rasrmg', args, optargs) 
    os.system(cmd)

def disflag(flag, width, start=None, nlines=None):
    """*** DISP disflag: display phase unwrapping flag file ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 24-Aug-20012 clw ***
    
    usage: disflag <flag> <width> [start] [nlines]
    
    input parameters:
      flag    (input) phase unwrapping flag file (unsigned char)
      width   samples per row of flag file
      start   starting line of flag file (default=1)
      nlines  number of lines to display (default=0: to end of file)
    
    """

    args = [flag, width]
    optargs = [start, nlines]
    cmd = makecmd('disflag', args, optargs) 
    os.system(cmd)

def dis2pwr(pwr1, pwr2, width1, width2, start=None, nlines=None, roff=None, azoff=None, scale=None, exp=None, format=None, hdrz=None):
    """*** DISP dis2pwr: alternating display of two intensity images (power-law scaling) ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 4-Apr-2012 clw ***
    
    usage: dis2pwr <pwr1> <pwr2> <width1> <width2> [start] [nlines] [roff] [azoff] [scale] [exp] [format] [hdrz]
    
    input parameters: 
      pwr1    (input) intensity image 1 (float)
      pwr2    (input) intensity image 2 (float)
      width1  samples per row of pwr1
      width2  samples per row of pwr2
      start   starting line of pwr1 (default=1)
      nlines  number of lines to display (default=0: to end of file)
      roff    range offset of pwr2 relative to pwr1
      azoff   azimuth offset of pwr2 relative to pwr1
      scale   display scale factor (default=1.)
      exp     display exponent (default=.35)
      format  input data format
                0: FLOAT (default)
                1: SHORT INTEGER
    
    """

    args = [pwr1, pwr2, width1, width2]
    optargs = [start, nlines, roff, azoff, scale, exp, format, hdrz]
    cmd = makecmd('dis2pwr', args, optargs) 
    os.system(cmd)

def dis_linear(pwr, width, start=None, nlines=None, min=None, max=None):
    """*** DISP dis_linear: display of intensity image using linear scaling ***
    *** Copyright 2005, Gamma Remote Sensing, v1.7 24-Aug-2005 uw/clw ***
    
    usage: dis_linear <pwr> <width> [start] [nlines] [min] [max]
    
    input parameters:
      pwr     (input) intensity image (float)
      width   samples per row of pwr
      start   starting line of pwr (default=1)
      nlines  number of lines to display (default=0: to end of file)
      min     minimum value used for display (default=0.0)
      max     maximum value used for display (default=1.0)
    
    """

    args = [pwr, width]
    optargs = [start, nlines, min, max]
    cmd = makecmd('dis_linear', args, optargs) 
    os.system(cmd)

def rasmph_pwr(cpx, pwr, width, start_cpx=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, scale=None, exp=None, LR=None, rasf=None, cc=None, start_cc=None, cc_min=None):
    """*** DISP Program rasmph_pwr ***
    *** Copyright 2014, Gamma Remote Sensing, v2.7 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of the phase of complex data combined with intensity data ***
    
    usage: rasmph_pwr <cpx> <pwr> <width> [start_cpx] [start_pwr] [nlines] [pixavr] [pixavaz] [scale] [exp] [LR] [rasf] [cc] [start_cc] [cc_min]
    
    input parameters:
      cpx        (input) complex image (fcomplex, e.g. interferogram)
      pwr        (input) intensity image
      width      samples per row of cpx and pwr
      start_cpx  starting line of cpx (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
      LR         left/right flipping flag, (default=1: normal, -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      cc         (input) correlation data file (float)
      start_cc   starting line of correlation data file (default = 1)
      cc_min     pixels with cc values below cc_min are displayed using greyscale (default=.2)
    
    """

    args = [cpx, pwr, width]
    optargs = [start_cpx, start_pwr, nlines, pixavr, pixavaz, scale, exp, LR, rasf, cc, start_cc, cc_min]
    cmd = makecmd('rasmph_pwr', args, optargs) 
    os.system(cmd)

def dismph_pk(cpx, width, start=None, nlines=None, scale=None, exp=None, data_type=None):
    """*** DISP dismph_pk: display of magnitude/phase of complex image, scale relative to the peak ***
    *** Copyright 2005, Gamma Remote Sensing, v1.8 10-Oct-2005 clw ***
    
    usage: dismph_pk <cpx> <width> [start] [nlines] [scale] [exp] [data_type]
    
    input parameters:
      cpx        (input) complex data (FCOMPLEX or SCOMPLEX format)
      width      complex samples per line
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      data_type  input data type
                   0: FCOMPLEX (default)
                   1: SCOMPLEX
    
    """

    args = [cpx, width]
    optargs = [start, nlines, scale, exp, data_type]
    cmd = makecmd('dismph_pk', args, optargs) 
    os.system(cmd)

def float_math(d1, d2, d_out, width, mode, roff=None, loff=None, nr=None, nl=None, c0=None, zflg=None):
    """*** DISP Tools: float_math ***
    *** Copyright 2014, Gamma Remote Sensing, v1.6 1-Apr-2014 ***
    *** Perform arithmetic operations on data files (float) ***
    
    usage: float_math <d1> <d2> <d_out> <width> <mode> [roff] [loff] [nr] [nl] [c0] [zflg]
    
    input parameters:
      d1     (input) data file 1 (float)
      d2     (input) data file 2 (float) (enter - for none)
              NOTE: if no input file is provided for modes 0->3, d2 values are set to value specified by c0 parameter
      d_out  (output) output of math operation on the input data (float)
      width  number of samples/line
      mode   math operation to perform on data:
               0:  addition, d1 + d2
               1:  subtraction, d1 - d2
               2:  multiplication, d1 * d2
               3:  division, d1/d2
               4:  10 * log10(d1), evaluate dB value
               5:  d1*d1
      roff   range pixel offset to center of the reference region (enter - for default: no reference correction)
      loff   line offset to center of the reference region (enter - for default: no reference correction)
      nr     number of range pixels to average in the reference region (enter - for default: 13)
      nl     number of lines average in the reference region (enter - for default: 13)
      c0     constant value (enter - for default: 1.18e-38 for addition and subtraction, 1.0 for multiplication and division)
      zflg   zero data flag (enter - for default:0):
               0: values of 0.0 in d1 or d2 are considered as no-data and the output is set to 0.0 (default)
               1: values of 0.0 are considered as valid data
    
      NOTE: when specifying a reference region with roff, loff, nr, nl parameters:
            modes 0, 1: reference region average value is subtracted from d1 and d2 data respectively
            modes 2, 3: d1 and d2 data are divided by the average in the reference region
            mode 4:     data values in d1 are divided by the average in the reference region
    
    """

    args = [d1, d2, d_out, width, mode]
    optargs = [roff, loff, nr, nl, c0, zflg]
    cmd = makecmd('float_math', args, optargs) 
    os.system(cmd)

def cp_data(infile, outfile, lbytes, start=None, nlines=None, offset=None, file_ldr=None, offb=None, nbyte=None):
    """*** DISP copy: file copy utility ***
    *** Copyright 2004, Gamma Remote Sensing, v3.4 24-Jun-2004 clw/uw ***
    
    usage: cp_data <infile> <outfile> <lbytes> [start] [nlines] [offset] [file_ldr] [offb] [nbyte]
    
    input parameters:
      infile    (input) input data file
      outfile   (output) output data file
      lbytes    number of bytes per row of input file
      start     starting line (default=1)
      nlines    number of lines to copy (default=0: to end of file)
      offset    offset in bytes from the start of the file to skip (default=0)
      file_ldr  size of file leader (bytes) to copy (default=0)
      offb      offset in bytes for each line to start copy (default=0)
      nbyte     number of bytes to copy from each line (default=(lbytes - offb))
    
    """

    args = [infile, outfile, lbytes]
    optargs = [start, nlines, offset, file_ldr, offb, nbyte]
    cmd = makecmd('cp_data', args, optargs) 
    os.system(cmd)

def rasmph_pwr24(cpx, pwr, width, start_cpx=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, scale=None, exp=None, LR=None, rasf=None, cc=None, start_cc=None, cc_min=None):
    """*** DISP Program rasmph_pwr24 ***
    *** Copyright 2014, Gamma Remote Sensing, v1.7 22-Jan-2014 clw ***
    *** Generate 24-bit raster graphics image of interferogram phase + intensity data ***
    
    usage: rasmph_pwr24 <cpx> <pwr> <width> [start_cpx] [start_pwr] [nlines] [pixavr] [pixavaz] [scale] [exp] [LR] [rasf] [cc] [start_cc] [cc_min]
    
    input parameters:
      cpx        (input) complex image (fcomplex, e.g. interferogram)
      pwr        (input) intensity image (float)
      width      number of samples/row of cpx and pwr
      start_cpx  starting line of cpx (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      cc         display threshold data file (float, e.g. correlation)
      start_cc   starting line of cc data file (default=1)
      cc_min     pixels with cc values below cc_min are displayed using greyscale (default=.2)
    
    """

    args = [cpx, pwr, width]
    optargs = [start_cpx, start_pwr, nlines, pixavr, pixavaz, scale, exp, LR, rasf, cc, start_cc, cc_min]
    cmd = makecmd('rasmph_pwr24', args, optargs) 
    os.system(cmd)

def ras_cpt(data, width, cpt, color_model=None, start=None, nlines=None, pixavr=None, pixavaz=None, LR=None, rasf=None):
    """*** DISP Program ras_cpt ***
    *** Copyright 2011, Gamma Remote Sensing, v1.6 28-Sep-2011 clw ***
    *** Generate a 24-bit raster graphics image of float data using a color map file ***
    
    usage: ras_cpt <data> <width> <cpt> [color_model] [start] [nlines] [pixavr] [pixavaz] [LR] [rasf]
    
    input parameters:
      data        (input) data file (float)
      width       samples per row of input data
      cpt         (input) color map file, *.cpt 
      color_model color model selection flag:
                    0: RGB color model (default)
                    1: HLS (double hexagon color model)
                    2: HSV (single hexagon color model)
      start       starting line to display of the input data (default=1)
      nlines      number of lines to display (default=0: to end of file)
      pixavr      number of pixels to average in range (default=1)
      pixavaz     number of pixels to average in azimuth (default=1)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
    """

    args = [data, width, cpt]
    optargs = [color_model, start, nlines, pixavr, pixavaz, LR, rasf]
    cmd = makecmd('ras_cpt', args, optargs) 
    os.system(cmd)

def rascc(cc, pwr, width, start_cc=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, cmin=None, cmax=None, scale=None, exp=None, LR=None, rasf=None):
    """*** DISP Program rascc ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of correlation coefficient + intensity data ***
    
    usage: rascc <cc> <pwr> <width> [start_cc] [start_pwr] [nlines] [pixavr] [pixavaz] [cmin] [cmax] [scale] [exp] [LR] [rasf]
    
    input parameters:
      cc         (input) correlation coefficient image (float)
      pwr        (input) intensity data (float, enter - if not available)
      width      samples per row of cc and pwr
      start_cc   starting line of cc (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      cmin       minimum correlation value used for linear cc display (default=.1)
      cmax       maximum correlation value used for linear cc display (default=.9)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
    """

    args = [cc, pwr, width]
    optargs = [start_cc, start_pwr, nlines, pixavr, pixavaz, cmin, cmax, scale, exp, LR, rasf]
    cmd = makecmd('rascc', args, optargs) 
    os.system(cmd)

def ascii2float(data_in, width, data_out, loff=None, nl=None, coff=None, nv=None):
    """*** DISP program: ascii2float.c ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 7-Jun-2012 clw/ts ***
    *** Convert text format data to float format ***
    
    usage: ascii2float <data_in> <width> <data_out> [loff] [nl] [coff] [nv]
    
    input parameters: 
      data_in   (input) input text format data file with,  or whitespace between columns
      width     number of data values (columns) in each line of text
      data_out  (output) output data file (float)
      loff      offset lines to start of data (enter - for default: 0)
      nl        number of lines (enter - for default: all) 
      coff      offset columns to begin reading data values (enter - for default: 0)
      nv        number of values to read (enter - for default: to end of line)
    """

    args = [data_in, width, data_out]
    optargs = [loff, nl, coff, nv]
    cmd = makecmd('ascii2float', args, optargs) 
    os.system(cmd)

def float2ascii(din, width, data_out, loff=None, nl=None):
    """*** DISP program: float2ascii.c ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 7-Jun-2012 clw/ts ***
    *** Convert float format data to text format ***
    
    usage: float2ascii <din> <width> <data_out> [loff] [nl]
    
    input parameters: 
      din       (input) input data file (float)
      width     number of data values per line
      data_out  (output) input text format data file with whitespace between columns
      loff      offset lines to start of data (default: 0)
      nl        number of lines (enter - for default: to end of file) 
    """

    args = [din, width, data_out]
    optargs = [loff, nl]
    cmd = makecmd('float2ascii', args, optargs) 
    os.system(cmd)

def dis2hgt(hgt1, hgt2, width1, width2, start=None, nlines=None, roff=None, azoff=None, m_cycle=None):
    """*** DISP dis2hgt: alternating display of two height images ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 4-Apr-2012 clw ***
    
    usage: dis2hgt <hgt1> <hgt2> <width1> <width2> [start] [nlines] [roff] [azoff] [m/cycle]
    
    input parameters: 
      hgt1     (input) height file 1 (float)
      hgt2     (input) height file 2 (float)
      width1   samples per row of hgt1
      width2   samples per row of hgt2
      start    starting line of hgt1 (default=1)
      nlines   number of lines to display (default=0: to end of file)
      roff     range offset of hgt2 relative to hgt1
      azoff    azimuth offset of hgt2 relative to hgt1
      m/cycle  meters per color cycle (default=160.)
    
    """

    args = [hgt1, hgt2, width1, width2]
    optargs = [start, nlines, roff, azoff, m_cycle]
    cmd = makecmd('dis2hgt', args, optargs) 
    os.system(cmd)

def dis_dB(pwr, width, start=None, nlines=None, min_dB=None, max_dB=None):
    """*** DISP dis_dB: display of intensity image using logarithmic dB scale ***
    *** Copyright 2005, Gamma Remote Sensing, v1.7 24-Aug-2005 clw ***
    
    usage: dis_dB <pwr> <width> [start] [nlines] [min_dB] [max_dB]
    
    input parameters:
      pwr     (input) intensity image (float)
      width   samples per row of pwr
      start   starting line of pwr (default=1)
      nlines  number of lines to display (default=0: to end of file)
      min_dB  minimum dB value used for display
      max_dB  maximum dB value used for display
              defaults(min_dB=max_dB=0.0) selects a 20  dB range about the average
    
    """

    args = [pwr, width]
    optargs = [start, nlines, min_dB, max_dB]
    cmd = makecmd('dis_dB', args, optargs) 
    os.system(cmd)

def disdem_par(DEM, DEM_par, start=None, nlines=None, exaggerate=None, theta0=None, phi0=None):
    """*** DISP disdem_par: Display DEM with DEM/MAP parameter file as shaded relief ***
    *** Copyright 2012, Gamma Remote Sensing, v3.5 21-Jan-2012 clw/uw ***
    
    usage: disdem_par <DEM> <DEM_par> [start] [nlines] [exaggerate] [theta0] [phi0]
    
    input parameters:
      DEM         (input) Digital elevation model (float or short as specified in DEM/MAP_par)
      DEM_par     (input) DIFF/GEO DEM parameter file
      start       starting line of DEM (default=1)
      nlines      number of lines to display (default=0: to end of file)
      exaggerate  relief exaggeration factor to increase contrast of display (default=1.0)
      theta0      illumination elevation angle in deg. (default=45.)
      phi0        illumination orientation angle in deg. (default=135.)
                  (0.:right, 90:top, 180:left, 270:bottom)
    
    """

    args = [DEM, DEM_par]
    optargs = [start, nlines, exaggerate, theta0, phi0]
    cmd = makecmd('disdem_par', args, optargs) 
    os.system(cmd)

def replace_values(f_in, value, new_value, f_out, width, rpl_flg=None, type=None):
    """*** DISP Program replace_values.c ***
    *** Copyright 2008, Gamma Remote Sensing, v1.4 6-May-2008 uw/clw/ts ***
    
    *** Replace value(s) of input data file with specified value or values extracted from a second input data file ***
    usage: replace_values <f_in> <value> <new_value> <f_out> <width> [rpl_flg] [type]
    
    input parameters: 
      f_in       (input) input data file
      value      threshold value or nan
      new_value  new value to use or second input data file (float)
      f_out      (output) output data file
      width      number of samples/row
      rpl_flg    replacement option flag (default = 0)
                   0: replace all points == value with new_value
                   1: replace all points >= value with new_value
                   2: replace all points <= value with new_value
      type       data type
                   2: float or fcomplex (default)
                   4: short int
    
    """

    args = [f_in, value, new_value, f_out, width]
    optargs = [rpl_flg, type]
    cmd = makecmd('replace_values', args, optargs) 
    os.system(cmd)

def data2tiff(data, width, type, TIFF, nodata=None, xspacing=None, yspacing=None):
    """*** DISP data2tiff: convert raster data into tiff format ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 18-Jan-2013 awi ***
    
    usage: data2tiff <data> <width> <type> <TIFF> [nodata] [xspacing] [yspacing]
    
    input parameters: 
      data     (input) data file
      width    input data file width
      type     input data type:
                 0: SUN raster/BMP format
                 1: SHORT integer
                 2: FLOAT (4 bytes/value)
                 3: SCOMPLEX (short complex)
                 4: FCOMPLEX (float complex)
                 5: BYTE
      TIFF     (output) TIFF file (.tif is the recommended extension)
      nodata   nodata value (default=0.0)
      xspacing (ignored if omitted)
      yspacing (ignored if omitted, default=xspacing)
    
    """

    args = [data, width, type, TIFF]
    optargs = [nodata, xspacing, yspacing]
    cmd = makecmd('data2tiff', args, optargs) 
    os.system(cmd)

def ras_linear(pwr, width, start=None, nlines=None, pixavr=None, pixavaz=None, min=None, max=None, LR=None, rasf=None, inverse=None, channel=None):
    """*** DISP Program ras_linear ***
    *** Copyright 2014, Gamma Remote Sensing, v2.5 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of data intensity using a linear scale ***
    
    usage: ras_linear <pwr> <width> [start] [nlines] [pixavr] [pixavaz] [min] [max] [LR] [rasf] [inverse] [channel]
    
    input parameters: 
      pwr        (input) intensity image (float)
      width      samples per row of pwr
      start      starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default: 1)
      pixavaz    number of pixels to average in azimuth (default: 1)
      min        minimum value for display (default: 0.0)
      max        maximum value for display (default: 1.0)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      inverse    inverse flag (1: float_to_raster (default), -1: raster_to_float)
      channel    RBG channel flag (1:red (default), 2:green, 3:blue)
    
    """

    args = [pwr, width]
    optargs = [start, nlines, pixavr, pixavaz, min, max, LR, rasf, inverse, channel]
    cmd = makecmd('ras_linear', args, optargs) 
    os.system(cmd)

def tree_edit(flag, ras, mag=None, win_sz=None, LR=None):
    """*** DISP tree_edit: edit phase unwrapping flag file ***
    *** Copyright 2013, Gamma Remote Sensing, v1.4 25-Aug-2013 clw ***
    
    usage: tree_edit <flag> <ras> [mag] [win_sz] [LR]
    
    input parameters: 
      flag    (input) phase unwrapping flag file with same dimensions as the overlay image
      ras     (input) 8-bit SUN raster (*.ras) or BMP format (*.bmp) overlay image
      mag     magnification factor (default=5)
      win_sz  size of input data region to be magnified (default=80)
    
    """

    args = [flag, ras]
    optargs = [mag, win_sz, LR]
    cmd = makecmd('tree_edit', args, optargs) 
    os.system(cmd)

def cpd(din, dout, width, type, xoff, nx, yoff, ny, ):
    """*** DISP cpd: copy segments of float and fcomplex format data ***
    *** Copyright 2014, Gamma Remote Sensing, v1.0 24-Feb-2014 clw ***
    
    usage: cpd <din> <dout> <width> <type> <xoff> <nx> <yoff> <ny>
    
    input parameters:
      din       (input) input data file
      dout      (output) output data file
      width     number of data values/line in the input data file
      type      input data type:
                  0: FLOAT (4 bytes/value)
                  1: FCOMPLEX (4 bytes real, 4 bytes imaginary)
      xoff      offset to starting sample (samples) (enter - for default: 0)
      nx        number of output samples/line (enter - for default: to end of line)
      yoff      offset to starting line (lines) (enter - for default: 0)
      ny        number of output lines (enter - for default: to end of file)
    """

    args = [din, dout, width, type, xoff, nx, yoff, ny]
    optargs = []
    cmd = makecmd('cpd', args, optargs) 
    os.system(cmd)

def ras8_float(f1, f2, width, rasf, color_model=None, h0=None, hrange=None, imin=None, imax=None, sat=None, sc1=None, A1=None, B1=None, cyclic1=None, sc2=None, A2=None, B2=None, start_f1=None, start_f2=None, nlines=None, pixavr=None, pixavaz=None, LR=None):
    """*** DISP Program ras8_float ***
    *** Copyright 2014, Gamma Remote Sensing, v1.5 22-Jan-2014 uw/clw ***
    *** Generate 8-bit raster graphics image of 1 or 2 float data files ***
    
    usage: ras8_float <f1> <f2> <width> <rasf> [color_model] [h0] [hrange] [imin] [imax] [sat] [sc1] [A1] [B1] [cyclic1] [sc2] [A2] [B2] [start_f1] [start_f2] [nlines] [pixavr] [pixavaz] [LR]
    
    input parameters:
      f1          (input) file 1 (float, enter - if not available --> greyscale image for f2)
      f2          (input) file 2 (float, enter - if not available --> 8-bit color image for f1)
      width       samples per row of f1 and f2
      rasf        (output) image filename, extension determines format:
                    *.bmp BMP format
                    *.ras Sun raster format
                  Color model definition parameters:
      color_model color model selection flag: (default: 0)
                  0: OLD color model used in Gamma Software
                  1: HLS (double hexagon color model)
                  2: HSV (single hexagon color model)
                  3: SIN (sinus color model)
                  4: GREY (greyscale model for f1)
      h0          starting hue (color for starting value, default: 0.0)
      hrange      range of hue values in degrees (-360. ... 360., default: 360.)
      imin        minimum brightness value (0.0 ... 1.0, default: 0.15)
      imax        maximum brightness value (0.0 ... 1.0, default: 0.85)
      sat         color saturation value (0.0 ... 1.0, default: 0.75)
                  Input image f1 scaling parameters:
      sc1         Scaling model flag (default: 0: lin, 1: log, 2: power-law)
      A1          Scaling parameter A (minimum or scale; default: 0.0)
      B1          Scaling parameter B (maximum or exponent; default: 6.2832)
      cyclic1     Cyclic scaling model (default: 1; 0: no 1: yes)
                  Input image f2 scaling parameters:
      sc2         Scaling model flag (default: 2; 0: lin, 1: log, 2: power-law)
      A2          Scaling parameter A (minimum or scale; default: 1.0)
      B2          Scaling parameter B (maximum or exponent; default: 0.35)
                  General parameters:
      start_f1    starting line of f1 (default: 1)
      start_f2    starting line of f2 (default: 1)
      nlines      number of lines to process (default: 0: to end of file)
      pixavr      number of pixels to average in range (default: 1)
      pixavaz     number of pixels to average in azimuth (default: 1)
      LR          left/right mirror image flag, (1: normal (default), -1: mirror image)
    """

    args = [f1, f2, width, rasf]
    optargs = [color_model, h0, hrange, imin, imax, sat, sc1, A1, B1, cyclic1, sc2, A2, B2, start_f1, start_f2, nlines, pixavr, pixavaz, LR]
    cmd = makecmd('ras8_float', args, optargs) 
    os.system(cmd)

def ras24_float(f1, f2, f3, width, rasf, color_model=None, h0=None, hrange=None, imin=None, imax=None, sat_min=None, sat_max=None, sc1=None, A1=None, B1=None, cyclic1=None, sc2=None, A2=None, B2=None, start_f1=None, start_f2=None, nlines=None, pixavr=None, pixavaz=None, LR=None):
    """*** DISP Program ras24_float ***
    *** Copyright 2014, Gamma Remote Sensing, v1.5 22-Jan-2014 uw/clw ***
    *** Generate 24-bit raster graphics image of of 1,2, or 3 float data files ***
    
    usage: ras24_float <f1> <f2> <f3> <width> <rasf> [color_model] [h0] [hrange] [imin] [imax] [sat_min] [sat_max] [sc1] [A1] [B1] [cyclic1] [sc2] [A2] [B2] [start_f1] [start_f2] [nlines] [pixavr] [pixavaz] [LR]
    
    input parameters:
      f1          (input) file 1 (float, red/hue           enter - if not available)
      f2          (input) file 2 (float, green/brightness, enter - if not available)
      f3          (input) file 3 (float, blue/saturation,  enter - if not available)
      width       samples per row of f1, f2, and f3
      rasf        (output) image filename, extension determines format:
                    *.bmp BMP format
                    *.ras Sun raster format
                  Color model definition parameters:
      color_model color model selection flag: (default=0)
                    0: RGB color model
                    1: HLS (double hexagon color model)
                    2: HSV (single hexagon color model)
      h0          starting hue (color for starting value, default: 0.0)
      hrange      range of hue values in degrees (-360. ... 360., default: 360.)
      imin        minimum brightness value (0.0 ... 1.0, default: 0.15)
      imax        maximum brightness value (0.0 ... 1.0, default: 0.85)
      sat_min     minimum color saturation value (0.0 ... 1.0, default: 0.0)
      sat_max     maximum color saturation value (0.0 ... 1.0, default: 1.0)
                  Input image f1 scaling parameters:
      sc1         Scaling model flag (default: 0: lin, 1: log, 2: power-law)
      A1          Scaling parameter A (minimum or scale; default: 0.0)
      B1          Scaling parameter B (maximum or exponent; default: 6.2832)
      cyclic1     Cyclic scaling model (default: 1; 0: no 1: yes)
                  Input image f2 scaling parameters:
      sc2         Scaling model flag (default: 2; 0: lin, 1: log, 2: power-law)
      A2          Scaling parameter A (minimum or scale; default: 1.0)
      B2          Scaling parameter B (maximum or exponent; default: 0.35)
                  Input image f3 scaling parameters:
      sc3         Scaling model flag (default: 0; 0: lin, 1: log, 2: power-law)
      A3          Scaling parameter A (minimum or scale; default: 0.0)
      B3          Scaling parameter B (maximum or exponent; default: 1.0)
                  General parameters:
      start_f1    starting line of f1 (default: 1)
      start_f2    starting line of f2 (default: 1)
      start_f3    starting line of f3 (default: 1)
      nlines      number of lines to process (default: 0: to end of file)
      pixavr      number of pixels to average in range (default: 1)
      pixavaz     number of pixels to average in azimuth (default: 1)
      LR          left/right mirror image flag, (1: normal (default), -1: mirror image)
    """

    args = [f1, f2, f3, width, rasf]
    optargs = [color_model, h0, hrange, imin, imax, sat_min, sat_max, sc1, A1, B1, cyclic1, sc2, A2, B2, start_f1, start_f2, nlines, pixavr, pixavaz, LR]
    cmd = makecmd('ras24_float', args, optargs) 
    os.system(cmd)

def rashgt(hgt, pwr, width, start_hgt=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, m_cycle=None, scale=None, exp=None, LR=None, rasf=None):
    """*** DISP Program rashgt ***
    *** Copyright 2014, Gamma Remote Sensing, v2.5 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of height and intensity ***
    
    usage: rashgt <hgt> <pwr> <width> [start_hgt] [start_pwr] [nlines] [pixavr] [pixavaz] [m/cycle] [scale] [exp] [LR] [rasf]
    
    input parameters:
      hgt        (input) height data (float)
      pwr        (input) intensity data (float, enter - if not available)
      width      samples per row of hgt and pwr
      start_hgt  starting line of hgt (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      m/cycle    meters per color cycle (default=160.)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
    """

    args = [hgt, pwr, width]
    optargs = [start_hgt, start_pwr, nlines, pixavr, pixavaz, m_cycle, scale, exp, LR, rasf]
    cmd = makecmd('rashgt', args, optargs) 
    os.system(cmd)

def dishgt(hgt, pwr, width, start_hgt=None, start_pwr=None, nlines=None, m_cycle=None, scale=None, exp=None):
    """*** DISP dishgt: display of height + intensity image ***
    *** Copyright 2013, Gamma Remote Sensing, v1.5 15-Mar-2013 clw ***
    
    usage: dishgt <hgt> <pwr> <width> [start_hgt] [start_pwr] [nlines] [m_cycle] [scale] [exp]
    
    input parameters:
      hgt        (input) height image (float)
      pwr        (input) intensity image (float, enter - if not available)
      width      samples per row of hgt and pwr
      start_hgt  starting line of hgt (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      m_cycle    meters per color cycle (default=160.)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
    
    """

    args = [hgt, pwr, width]
    optargs = [start_hgt, start_pwr, nlines, m_cycle, scale, exp]
    cmd = makecmd('dishgt', args, optargs) 
    os.system(cmd)

def swap_bytes(infile, outfile, swap_type, ):
    """*** DISP swap_bytes: swap bytes for binary format data ***
    *** Copyright 2005, Gamma Remote Sensing, v1.8 13-Oct-2005 uw/clw ***
    
    usage: swap_bytes <infile> <outfile> <swap_type>
    
    input parameters: 
      infile     (input) input data file
      outfile    (output) output data file
      swap_type  swap type flag (bytes/value)
                 2: (1,2,3,4,5,6,7,8...) --> (2,1,4,3,6,5,8,7...) (SHORT, SCOMPLEX)
                 4: (1,2,3,4,5,6,7,8...) --> (4,3,2,1,8,7,6,5...) (INT, FLOAT, FCOMPLEX)
                 8: (1,2,3,4,5,6,7,8...) --> (8,7,6,5,4,3,2,1...) (DOUBLE)
    
    """

    args = [infile, outfile, swap_type]
    optargs = []
    cmd = makecmd('swap_bytes', args, optargs) 
    os.system(cmd)

def raspwr(pwr, width, start=None, nlines=None, pixavr=None, pixavaz=None, scale=None, exp=None, LR=None, rasf=None, data_type=None, hdrz=None):
    """*** DISP Program raspwr ***
    *** Copyright 2014, Gamma Remote Sensing, v2.8 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of intensity data ***
    
    usage: raspwr <pwr> <width> [start] [nlines] [pixavr] [pixavaz] [scale] [exp] [LR] [rasf] [data_type] [hdrz] 
    
    input parameters: 
      pwr        (input) intensity image (FLOAT or SHORT INTEGER data type)
      width      samples per row
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines the format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      data_type  input data type (default=0)
                   0: FLOAT
                   1: SHORT INTEGER
      hdrsz      line header size in bytes (default=0, ESA PAF for PRI products=12)
    
    """

    args = [pwr, width]
    optargs = [start, nlines, pixavr, pixavaz, scale, exp, LR, rasf, data_type, hdrz]
    cmd = makecmd('raspwr', args, optargs) 
    os.system(cmd)

def real_to_cpx(data1, cpx, width, type, ):
    """*** DISPLAY Tools: Program real_to_cpx.c ***
    *** Copyright 2012, Gamma Remote Sensing, v1.2 9-May-2012 clw ***
    *** Generate fcomplex data from real and imaginary or magnitude and phase components ***
    
    usage: real_to_cpx <data1> data2> <cpx> <width> <type>
    input parameters:
      data1  (input) input data file data1 (enter - for none, float)
      data2  (input) input data file data2 (enter - for none, float)
      cpx    (output) output data file (fcomplex)
      width  samples per line of data files
      type   input data type:
               0: data1: real   data2: imaginary
               1: data1: magnitude   data2: phase
    
    """

    args = [data1, cpx, width, type]
    optargs = []
    cmd = makecmd('real_to_cpx', args, optargs) 
    os.system(cmd)

def dismph(cpx, width, start=None, nlines=None, scale=None, exp=None, data_type=None):
    """*** DISP dismph: display of magnitude/phase of complex image ***
    *** Copyright 2012, Gamma Remote Sensing, v2.1 23-Sep-2012 clw ***
    
    usage: dismph <cpx> <width> [start] [nlines] [scale] [exp] [data_type]
    
    input parameters:
      cpx        (input) complex data (FCOMPLEX or SCOMPLEX format)
      width      complex samples per line
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      data_type  input data type
                   0: FCOMPLEX (default)
                   1: SCOMPLEX
    
    """

    args = [cpx, width]
    optargs = [start, nlines, scale, exp, data_type]
    cmd = makecmd('dismph', args, optargs) 
    os.system(cmd)

def kml_pt(table, lat_col, lon_col, val1_col, val1_label, val2_col, val2_label, val3_col, val3_label, id_col, kml, icon_URL=None, logo_URL=None, legend_URL=None, color_model=None, h0=None, hrange=None, imin=None, imax=None, sat_min=None, sat_max=None, sc1=None, A1=None, B1=None, cyclic1=None, sc2=None, A2=None, B2=None, start_f1=None, start_f2=None):
    """*** DISP Program kml_pt.c  ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 5-Jun-2012 awi***
    *** Create kml XML file with icons indicating values from ascii table  ***
    
    usage: kml_pt <table> <lat_col> <lon_col> <val1_col> <val1_label> <val2_col> <val2_label> <val3_col> <val3_label> <id_col> <kml> [icon_URL] [logo_URL] [legend_URL] [color_model] [h0] [hrange] [imin] [imax] [sat_min] [sat_max] [sc1] [A1] [B1] [cyclic1] [sc2] [A2] [B2] [start_f1] [start_f2] 
    
    input parameters:
      table       (input) ascii file with columns of latitude, longitude and value
      lat_col     column number containg latitude information (WGS84, decimal)
      lon_col     column number containg longitude information (WGS84, decimal)
      val1_col    column number containg the value for red/hue (enter - if not available)
      val1_label  label of val1_col (enter - if values shall be omitted in the popup)
      val2_col    column number containg the value for green/brightness (enter - if not available)
      val2_label  label of val2_col (enter - if values shall be omitted in the popup)
      val3_col    column number containg the value for blue/saturation (enter - if not available)
      val3_label  label of val3_col (enter - if values shall be omitted in the popup)
      id_col      column number containg the id values (enter - if no ID values shall be displayed)
      kml         (output) kml output file
      icon_URL    Uniform Resource Locator to the master icon (- for default: http://www.gamma-rs.ch/media/images/kml/button_master.png)
      logo_URL    Uniform Resource Locator to the logo image (- for none, gamma: http://www.gamma-rs.ch/media/images/kml/gamma_logo.png)
      legend_URL  Uniform Resource Locator to the legend image (- for none)
      color_model color model selection flag: (default=0)
                  0: RGB color model
                  1: HLS (double hexagon color model)
                  2: HSV (single hexagon color model)
      h0          starting hue (color for starting value, default=0.0)
      hrange      range of hue values in degrees (-360. ... 360., default=360.)
      imin        minimum brightness value (0.0 ... 1.0, default=0.15)
      imax        maximum brightness value (0.0 ... 1.0, default=0.85)
      sat_min     minimum color saturation value (0.0 ... 1.0, default=0.0)
      sat_max     maximum color saturation value (0.0 ... 1.0, default=1.0)
                  Input image f1 scaling parameters:
      sc1         Scaling model flag (default=0: lin, 1: log, 2: power-law)
      A1          Scaling parameter A (minimum or scale; default=0.0)
      B1          Scaling parameter B (maximum or exponent; default=6.2832)
      cyclic1     Cyclic scaling model (default=1; 0: no 1: yes)
                  Input image f2 scaling parameters:
      sc2         Scaling model flag (default=2; 0: lin, 1: log, 2: power-law)
      A2          Scaling parameter A (minimum or scale; default=1.0)
      B2          Scaling parameter B (maximum or exponent; default=0.35)
                  Input image f3 scaling parameters:
      sc3         Scaling model flag (default=0; 0: lin, 1: log, 2: power-law)
      A3          Scaling parameter A (minimum or scale; default=0.0)
      B3          Scaling parameter B (maximum or exponent; default=1.0)
    
    """

    args = [table, lat_col, lon_col, val1_col, val1_label, val2_col, val2_label, val3_col, val3_label, id_col, kml]
    optargs = [icon_URL, logo_URL, legend_URL, color_model, h0, hrange, imin, imax, sat_min, sat_max, sc1, A1, B1, cyclic1, sc2, A2, B2, start_f1, start_f2]
    cmd = makecmd('kml_pt', args, optargs) 
    os.system(cmd)

def data2geotiff(DEM_par, data, type, GeoTIFF, nodata=None):
    """*** DISP data2geotiff: convert raster data with DEM parameter file to GeoTIFF format ***
    *** Copyright 2014, Gamma Remote Sensing, v1.8 11-Apr-2014 awi/clw ***
    
    usage: data2geotiff <DEM_par> <data> <type> <GeoTIFF> [nodata]
    
    input parameters: 
      DEM_par  (input) DIFF/GEO DEM parameter file
      data     (input) data file
      type     input data type:
                 0: SUN raster/BMP format
                 1: SHORT integer (2 bytes/value)
                 2: FLOAT (4 bytes/value)
                 3: SCOMPLEX (short complex)
                 4: FCOMPLEX (float complex)
                 5: BYTE
      GeoTIFF  (output) GeoTIFF file (.tif is the recommended extension)
      nodata   nodata value (default: 0.0)
    
    """

    args = [DEM_par, data, type, GeoTIFF]
    optargs = [nodata]
    cmd = makecmd('data2geotiff', args, optargs) 
    os.system(cmd)

def cpx_to_real(cpx, real, width, type, ):
    """*** Display Tools: Program cpx_to_real.c ***
    *** Copyright 2013, Gamma Remote Sensing, v1.5 2-Sep-2013 uw/clw/ts ***
    *** calculate real part, imaginary part, intensity, magnitude, or phase from FCOMPLEX data ***
    
    usage: cpx_to_real <cpx> <real> <width> <type>
    input parameters: 
      cpx    (input) complex valued data (fcomplex)
      real   (output) real valued data (float)
      width  samples per line of input fcomplex data
      type   output data type
               0: real part
               1: imaginary part
               2: intensity (re*re + im*im)
               3: magnitude (sqrt(re*re + im*im))
               4: phase (atan2(im, re))
    
    """

    args = [cpx, real, width, type]
    optargs = []
    cmd = makecmd('cpx_to_real', args, optargs) 
    os.system(cmd)

def ras3pwr(d1, d2, d3, width, start=None, nlines=None, pixavr=None, pixavaz=None, scale1=None, scale2=None, scale3=None, exp=None, rasf=None):
    """*** DISP Program ras3pwr ***
    *** Copyright 2014, Gamma Remote Sensing, v1.6 24-Mar-2014 clw ***
    *** Generate 24-bit RGB raster graphics image of 3 intensity files ***
    
    usage: ras3pwr <d1> <d2> <d3> <width> [start] [nlines] [pixavr] [pixavaz] [scale1] [scale2] [scale3] [exp] [rasf]
    
    input parameters:
      d1       (input) data channel 1 (used for image scaling) (float) (red)
      d2       (input) data channel 2 (float)(green)
      d3       (input) data channel 3 (float)(blue)
      width    number of samples/row in the input data files
      start    starting line (enter - for default: 1
      nlines   number of lines (enter - for default: to end of file)
      pixavr   number of pixels to average in range (default: 1)
      pixavaz  number of pixels to average in azimuth (default: 1)
      scale1   relative display scale factor for channel 1 red   (default: 1.)
      scale2   relative display scale factor for channel 2 green (default: 1.)
      scale3   relative display scale factor for channel 3 blue  (default: 1.)
      exp      display exponent (default: 0.5)
      rasf     (output) image filename, enter - for default, extension determines format:
                  *.bmp BMP format
                  *.ras Sun raster format (default)
    
    """

    args = [d1, d2, d3, width]
    optargs = [start, nlines, pixavr, pixavaz, scale1, scale2, scale3, exp, rasf]
    cmd = makecmd('ras3pwr', args, optargs) 
    os.system(cmd)

def fill(d1, d2, dout, width, ):
    """*** DISP Tools: fill ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 11-Apr-2013 ***
    *** Fill gaps in data file 1 with values from data file 2 or a constant value (float format) ***
    
    usage: fill <d1> <d2> <dout> <width>
    
    input parameters:
      d1     (input) data file 1 (float)
      d2     (input) data file 2 used to fill gaps in d1, or a constant value (float)
      dout   (output) d1 with gaps filled (float)
      width  number of samples/line
    """

    args = [d1, d2, dout, width]
    optargs = []
    cmd = makecmd('fill', args, optargs) 
    os.system(cmd)

def dispwr(pwr, width, start=None, nlines=None, scale=None, exp=None, data_type=None, hdrz=None):
    """*** DISP dispwr: display of intensity image (power-law scaling) ***
    *** Copyright 2013, Gamma Remote Sensing, v1.9 15-Feb-2013 clw ***
    
    usage: dispwr <pwr> <width> [start] [nlines] [scale] [exp] [data_type] [hdrz]
    
    input parameters:
      pwr        (input) intensity image (FLOAT or SHORT INTEGER data type)
      width      samples per row
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      data_type  input data type 
                   0: FLOAT (default)
                   1: SHORT INTEGER
    
    """

    args = [pwr, width]
    optargs = [start, nlines, scale, exp, data_type, hdrz]
    cmd = makecmd('dispwr', args, optargs) 
    os.system(cmd)

def dis2ras(ras1, ras2, mag=None, win_sz=None):
    """*** DISP dis2ras: alternating display of 2 SUN raster or BMP format images ***
    *** Copyright 2013, Gamma Remote Sensing, v1.7 25-Aug-2013 clw ***
    
    usage: dis2ras <ras1> <ras2> [mag] [win_sz]
    
    input parameters:
      ras1    (input) image 1 SUN raster *.ras, or BMP *.bmp format
      ras2    (input) image 2 SUN raster *.ras, or BMP *.bmp format
              image depth (bits/pixel) of image-1 and image-2 must be identical
      mag     zoom magnification factor (default=3)
      win_sz  zoom window size before magnification (default=120)
    
    """

    args = [ras1, ras2]
    optargs = [mag, win_sz]
    cmd = makecmd('dis2ras', args, optargs) 
    os.system(cmd)

def disrmg(unw, pwr, width, start_unw=None, start_pwr=None, nlines=None, ph_scale=None, scale=None, exp=None, ph_offset=None):
    """*** DISP disrmg: display of unwrapped phase + intensity image ***
    *** Copyright 2012, Gamma Remote Sensing, v1.4 4-Apr-2012 clw ***
    
    usage: disrmg <unw> <pwr> <width> [start_unw] [start_pwr] [nlines] [ph_scale] [scale] [exp] [ph_offset]
    
    input parameters:
      unw        (input) unwrapped phase image (float)
      pwr        (input) intensity image (float, enter - for none)
      width      samples per row of unw and pwr
      start_unw  starting line of unw (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      ph_scale   phase display scale factor (default=.33333)
      scale      pwr display scale factor (default=1.)
      exp        pwr display exponent (default=.35)
      ph_offset  phase offset in radians subtracted from unw (default=.0)
    
    """

    args = [unw, pwr, width]
    optargs = [start_unw, start_pwr, nlines, ph_scale, scale, exp, ph_offset]
    cmd = makecmd('disrmg', args, optargs) 
    os.system(cmd)

def rasmph(cpx, width, start=None, nlines=None, pixavr=None, pixavaz=None, scale=None, exp=None, LR=None, rasf=None, data_type=None):
    """*** DISP Program rasmph ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6 22-Jan-2014 clw ***
    *** Generate 8-bit raster graphics image of the phase and intensity of complex data ***
    
    usage: rasmph <cpx> <width> [start] [nlines] [pixavr] [pixavaz] [scale] [exp] [LR] [rasf] [data_type]
    
    input parameters:
      cpx        (input) complex data (SCOMPLEX or FCOMPLEX data type)
      width      samples per row
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default=1)
      pixavaz    number of pixels to average in azimuth (default=1)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      data_type  input data type (default=0)
                   0: FCOMPLEX (pairs of floats)
                   1: SCOMPLEX (pairs of short integers)
    
    """

    args = [cpx, width]
    optargs = [start, nlines, pixavr, pixavaz, scale, exp, LR, rasf, data_type]
    cmd = makecmd('rasmph', args, optargs) 
    os.system(cmd)

def dis2cc(cc1, cc2, width1, width2, start=None, nlines=None, roff=None, azoff=None, cmin=None, cmax=None):
    """*** DISP dis2cc: alternating display of two coherence images ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 4-Apr-2012 clw/uw ***
    
    usage: dis2cc <cc1> <cc2> <width1> <width2> [start] [nlines] [roff] [azoff] [cmin] [cmax]
    
    input parameters: 
      cc1     (input) coherence file 1 (float)
      cc2     (input) coherence file 2 (float)
      width1  samples per row of cc1
      width2  samples per row of cc2
      start   starting line of cc1 (default=1) 
      nlines  number of lines to display (default=0: to end of file)
      roff    range offset of cc2 relative to cc1
      azoff   azimuth offset of cc2 relative to cc1
      cmin    minimum coherence value used for linear display (default=.1)
      cmax    maximum coherence value used for linear display (default=.9)
    
    """

    args = [cc1, cc2, width1, width2]
    optargs = [start, nlines, roff, azoff, cmin, cmax]
    cmd = makecmd('dis2cc', args, optargs) 
    os.system(cmd)

def disSLC(SLC, width, start=None, nlines=None, scale=None, exp=None, data_type=None):
    """*** DISP Program disSLC ***
    *** Copyright 2012, Gamma Remote Sensing, v1.6 4-Apr-2012 clw ***
    *** Generate 8-bit SUN/BMP raster file of SLC intensity ***
    
    usage: disSLC <SLC> <width> [start] [nlines] [scale] [exp] [data_type]
    
    input parameters:
      SLC        (input) single-look complex image (FCOMPLEX or SCOMPLEX data type)
      width      complex samples per row of the SLC
      start      starting line (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.5)
      data_type  input SLC data type
                   0: FCOMPLEX
                   1: SCOMPLEX (default)
    """

    args = [SLC, width]
    optargs = [start, nlines, scale, exp, data_type]
    cmd = makecmd('disSLC', args, optargs) 
    os.system(cmd)

def dismph_pwr(cpx, pwr, width, start_cpx=None, start_pwr=None, nlines=None, scale=None, exp=None):
    """*** DISP dismph_pwr: display of interferogram phase + intensity image ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 4-Apr-2012 clw ***
    
    usage: dismph_pwr <cpx> <pwr> <width> [start_cpx] [start_pwr] [nlines] [scale] [exp]
    
    input parameters:
      cpx        (input) complex image (fcomplex, e.g. interferogram)
      pwr        (input) intensity image (float, enter - if not available)
      width      samples per row of cpx and pwr
      start_cpx  starting line of cpx (default=1)
      start_pwr  starting line of pwr (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      intensity display scale factor (default=1.)
      exp        intensity display exponent (default=.35)
    
    """

    args = [cpx, pwr, width]
    optargs = [start_cpx, start_pwr, nlines, scale, exp]
    cmd = makecmd('dismph_pwr', args, optargs) 
    os.system(cmd)

def disshd(DEM, width, col_post, row_post=None, start=None, nlines=None, theta0=None, phi0=None, data_type=None):
    """*** DISP disshd: display DEM as shaded relief ***
    *** Copyright 2005, Gamma Remote Sensing, v1.5 24-Aug-2005 uw/clw ***
    
    usage: disshd <DEM> <width> <col_post> [row_post] [start] [nlines] [theta0] [phi0] [data_type]
    
    input parameters:
      DEM        (input) digital elevation model (float)
      width      samples per row of DEM
      col_post   posting between cols (same unit as DEM values)
      row_post   posting between rows (same unit as DEM, default=col_post)
      start      starting line of DEM (default=1)
      nlines     number of lines to display (default=0: to end of file)
      theta0     illumination elevation angle in deg. (default=45.)
      phi0       illumination orientation angle in deg. (default=135.)
                 (0.:right, 90:top, 180:left, 270:bottom)
      data_type  data type
    
                 0: FLOAT (default)
                 1: SHORT INTEGER
    
    """

    args = [DEM, width, col_post]
    optargs = [row_post, start, nlines, theta0, phi0, data_type]
    cmd = makecmd('disshd', args, optargs) 
    os.system(cmd)

def dismph_ub(cpx, width, start=None, nlines=None, scale=None, exp=None):
    """*** DISP dismph_ub: display of magnitude/phase of complex unsigned byte image ***
    *** Copyright 2005, Gamma Remote Sensing, v1.5 24-Aug-2005 clw ***
    
    usage: dismph_ub <cpx> <width> [start] [nlines] [scale] [exp]
    
    input parameters:
      cpx     (input) IQ unsigned byte complex image (e.g. SAR raw data)
      width   bytes per line
      start   starting line of input data (default=1)
      nlines  number of lines to display (default=0: to end of file)
      scale   display scale factor (default=1.)
      exp     display exponent (default=.35)
    
    """

    args = [cpx, width]
    optargs = [start, nlines, scale, exp]
    cmd = makecmd('dismph_ub', args, optargs) 
    os.system(cmd)

def kml_map(image, dem_par, kml, ):
    """*** DISP: Program kml_map.c ***
    *** Copyright 2007, Gamma Remote Sensing, v1.0 21-May-2007 awi/cw ***
    *** Create kml XML file with link to image  ***
    
    usage: kml_map <image> <dem_par> <kml> 
    
    input parameters:
      image   (input) URL of background image in bmp, png or jpg format, 
              needs to be in EQA projection with WGS84 for Google Earth
      dem_par (input) corresponding dem_par file 
      kml     (output) kml output file (default = *.kml)
    
    """

    args = [image, dem_par, kml]
    optargs = []
    cmd = makecmd('kml_map', args, optargs) 
    os.system(cmd)

def distree(flag, unw, cpx, width, start=None, nlines=None, ph_scale=None):
    """*** DISP distree: display of unwrapped phase + wrapped phase + flags ***
    *** Copyright 2005, Gamma Remote Sensing, v2.5 24-Aug-2005 clw ***
    
    usage: distree <flag> <unw> <cpx> <width> [start] [nlines] [ph_scale]
    
    input parameters: 
      flag      (input) phase unwrapping flag file (unsigned char)
      unw       (input) unwrapped phase image (float)
      cpx       (input) complex valued interferogram (fcomplex, used for wrapped phase)
      width     samples per row of flag, unw, and cpx
      start     starting line of flag, unw, and cpx (default=1)
      nlines    number of lines to display (default=0: to end of file)
      ph_scale  unwrapped phase display scale factor (default=.33333)
    
    """

    args = [flag, unw, cpx, width]
    optargs = [start, nlines, ph_scale]
    cmd = makecmd('distree', args, optargs) 
    os.system(cmd)

def dismph_fft(cpx, width, start=None, nlines=None, scale=None, exp=None, nfft=None, mag=None, data_type=None):
    """*** DISP dismph_fft: display of magnitude/phase and 2D FFT of complex data***
    *** Copyright 2005, Gamma Remote Sensing, v1.7 24-Aug-2005 clw ***
    
    usage: dismph_fft <cpx> <width> [start] [nlines] [scale] [exp] [nfft] [mag] [data_type]
    
    input parameters:
      cpx        (input) complex data (scomplex or fcomplex format)
      width      complex samples per row
      start      starting line to display (default=1)
      nlines     number of lines to display (default=0: to end of file)
      scale      display scale factor (default=1.)
      exp        display exponent (default=.35)
      nfft       2D FFT size nfft x nfft (default=128)
      mag        zoom and FFT window magnification factor (default=2)
      data_type  input data type
                   0: FCOMPLEX (default)
                   1: SCOMPLEX
    
    """

    args = [cpx, width]
    optargs = [start, nlines, scale, exp, nfft, mag, data_type]
    cmd = makecmd('dismph_fft', args, optargs) 
    os.system(cmd)

def set_value(PAR_in, PAR_out, keyword, value, new_key=None):
    """*** Update keyword:value in text parameter files ***
    *** Copyright 2006, Gamma Remote Sensing, v1.4 20-Nov-2006 clw ***
    
    usage: set_value <PAR_in> <PAR_out> <keyword> <value> [new_key]
    
    input parameters: 
      PAR_in   (input) keyword:value based parameter file
      PAR_out  (output) keyword:value based parameter file (can be the same file as PAR_in)
      keyword  search keyword of keyword:value pair
      value    new value (note: delimit value with double quotes if it contains spaces or punctuation)
      new_key  options for new keyword_value pair
                 0: update value of existing keyword:value pair (default)
                 1: append new keyword:value pair to PAR_out
    
    """

    args = [PAR_in, PAR_out, keyword, value]
    optargs = [new_key]
    cmd = makecmd('set_value', args, optargs) 
    os.system(cmd)

def disbyte(image, width, start=None, nlines=None, scale=None):
    """*** DISP disbyte: display of unsigned byte images ***
    *** Copyright 2009, Gamma Remote Sensing, v1.7 12-Aug-2009 clw ***
    
    usage: disbyte <image> <width> [start] [nlines] [scale]
    
    input parameters:
      image   (input) unsigned byte image
      width   samples per row of image
      start   starting line of image1 (default: 1)
      nlines  number of lines to display (default: 0: to end of file)
      scale   display scale factor (default: 1.)
    
    """

    args = [image, width]
    optargs = [start, nlines, scale]
    cmd = makecmd('disbyte', args, optargs) 
    os.system(cmd)

def rasshd(DEM, width, col_post, row_post=None, start=None, nlines=None, pixavr=None, pixavaz=None, theta0=None, phi0=None, LR=None, rasf=None, format=None, zero_flag=None):
    """*** DISP Program rasshd ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 9-May-2012 uw/clw ***
    *** Generate 8-bit raster graphics image of DEM data as shaded relief ***
    
    usage: rasshd <DEM> <width> <col_post> [row_post] [start] [nlines] [pixavr] [pixavaz] [theta0] [phi0] [LR] [rasf] [format] [zero_flag]
    
    input parameters:
      DEM        (input) digital elevation model (float)
      width      samples per row of DEM
      col_post   posting between cols (same unit as DEM values)
      row_post   posting between rows (same unit as DEM, default: col_post)
      start      starting line of DEM (default: 1)
      nlines     number of lines to display (default: 0, to end of file)
      pixavr     number of pixels to average in range (enter - for default: 1)
      pixavaz    number of pixels to average in azimuth (enter - for default: 1)
      theta0     illumination elevation angle in deg. (enter - for default: 45.)
      phi0       illumination orientation angle in deg. (enter - default: 135.)
                   0.:right, 90:top, 180:left, 270:bottom
      LR         left/right mirror image flag, (1: normal (enter - for default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      format     DEM data format (enter - for default):
                   0: float (default)
                   1: short integer
      zero_flag  zero data handling (enter - for default):
                   0: 0.0 interpreted at missing value (default)
                   1: 0.0 interpreted as valid data
    """

    args = [DEM, width, col_post]
    optargs = [row_post, start, nlines, pixavr, pixavaz, theta0, phi0, LR, rasf, format, zero_flag]
    cmd = makecmd('rasshd', args, optargs) 
    os.system(cmd)

def cpx_math(d1, d2, d_out, width, mode, roff=None, loff=None, nr=None, nl=None, c_re=None, c_im=None, zflg=None):
    """*** DISP Tools: cpx_math ***
    *** Copyright 2013, Gamma Remote Sensing, v1.3 20-Sep-2013 ***
    *** Perform arithmetic operations on data files (fcomplex) ***
    
    usage: cpx_math <d1> <d2> <d_out> <width> <mode> [roff] [loff] [nr] [nl] [c_re] [c_im] [zflg]
    
    input parameters:
      d1     (input) data file 1 (fcomplex)
      d2     (input) data file 2 (fcomplex) (enter - for none)
             NOTE: if no input file is provided, d2 values are set to value specified by c_re and c_im parameters
      d_out  (output) output of math operation on d1 and d2 (float)
      width  number of samples/line
      mode   math operation to perform on data:
               0:  addition, d1 + d2
               1:  subtraction, d1 - d2
               2:  multiplication, d1 * d2
               3:  division, d1/d2
      roff   range pixel offset to center of the reference region (enter - for default: no reference correction)
      loff   line offset to center of the reference region (enter - for default: no reference correction)
      nr     number of range pixels to average in the reference region (enter - for default: 13)
      nl     number of lines average in the reference region (enter - for default: 13)
      c_re   constant real component (enter - for default: 1.18e-38 for addition and subtraction, 1.0 for multiplication and division)
      c_im   constant imaginary component (enter - for default: 0.0)
      zflg   zero data flag (enter - for default:0):
               0: values of 0.0 in d1 or d2 are considered as no-data and the output is set to 0.0 (default)
               1: values of 0.0 are considered as valid data
    
      NOTE: when specifying a reference region with roff, loff, nr, nl parameters:
            modes 0, 1: reference region average value is subtracted from d1 and d2 data respectively
            modes 2, 3: d1 and d2 data are divided by the average in the reference region
    
    """

    args = [d1, d2, d_out, width, mode]
    optargs = [roff, loff, nr, nl, c_re, c_im, zflg]
    cmd = makecmd('cpx_math', args, optargs) 
    os.system(cmd)

def rastree(flag, unw, cpx, width, start=None, nlines=None, ph_scale=None, rasf=None):
    """*** DISP Program rastree ***
    *** Copyright 2011, Gamma Remote Sensing, v1.3 27-Sep-2011 clw ***
    *** Generate 8-bit raster graphics image of unwrapped phase, wrapped phase, and unwrapping flags ***
    
    usage: rastree <flag> <unw> <cpx> <width> [start] [nlines] [ph_scale] [rasf]
    
    input parameters: 
      flag      (input) phase unwrapping flag file (unsigned char)
      unw       (input) unwrapped phase image (float)
      cpx       complex valued interferogram (fcomplex, used for wrapped phase)
      width     samples per row of flag, unw, and cpx
      start     starting line of flag, unw, and cpx (default=1)
      nlines    number of lines to display (default=0: to end of file)
      ph_scale  unwrapped phase display scale factor (default=.33333)
      rasf      (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
      Notice: ph_scale < 1. is selected to better detect phase unwrapping errors
    
    """

    args = [flag, unw, cpx, width]
    optargs = [start, nlines, ph_scale, rasf]
    cmd = makecmd('rastree', args, optargs) 
    os.system(cmd)

def disras_dem_par(ras, DEM_par, mag=None, win_sz=None):
    """*** DISP disdem_par: display raster image with DEM/MAP parameter file ***
    *** Copyright 2013, Gamma Remote Sensing, v1.5 25-Aug-2013 clw/uw ***
    
    usage: disras_dem_par <ras> <DEM_par> [mag] [win_sz]
    
    input parameters: 
      ras      (input) raster image with same dimensions as in DEM_par (SUN *.ras, or BMP *.bmp format)
      DEM_par  (input) DIFF DEM/MAP parameter file
      mag      zoom magnification factor (default=3)
      win_sz   zoom window size before magnification (default=120)
    
    """

    args = [ras, DEM_par]
    optargs = [mag, win_sz]
    cmd = makecmd('disras_dem_par', args, optargs) 
    os.system(cmd)

def multi_real(data_in, OFF_par_in, data_out, OFF_par_out, rlks=None, azlks=None, loff=None, nlines=None, roff=None, nsamp=None):
    """*** Calculate multi-look averaged or interpolated 2D image (float data) ***
    *** Copyright 2012, Gamma Remote Sensing, v2.5 16-Jul-2013 clw/uw ***
    
    usage: multi_real <data_in> <OFF_par_in> <data_out> <OFF_par_out> [rlks] [azlks] [loff] [nlines] [roff] [nsamp]
    
    input parameters: 
      data_in      (input) input float image file
      OFF_par_in   (input) interferogram/offset parameter file for input image
      data_out     (output) output multi-look or interpolated float data file
      OFF_par_out  (input/output) interferogram/offset parameter file for output, if already existing, used as input
      rlks         number of range looks, values < -1, interpreted as an image oversampling factor (default: 1)
      azlks        number azimuth looks,  values < -1, interpreted as an image oversampling factor (default: 1)
      loff         line offset to starting line (default:0)
      nlines       number of lines (default:0, to end of file)
      roff         offset to starting range sample (default:0)
      nsamp        number of range samples to extract (default:0, to end of line)
    
    """

    args = [data_in, OFF_par_in, data_out, OFF_par_out]
    optargs = [rlks, azlks, loff, nlines, roff, nsamp]
    cmd = makecmd('multi_real', args, optargs) 
    os.system(cmd)

def neutron(intensity, flag, width, n_thres, ymin=None, ymax=None):
    """*** Generate phase unwrapping neutrons using image intensity ***
    *** Copyright 2014, Gamma Remote Sensing, v2.3 20-Jan-2014 clw/uw ***
    
    usage: neutron <intensity> <flag> <width> <n_thres> [ymin] [ymax]
    
    input parameters: 
      intensity  (input) image intensity 
      flag       (input) phase unwrapping flag file
      width      number of samples/row
      n_thres    neutron threshold, multiples of the average intensity (default=6.0)
      ymin       offset to starting azimuth row (default = 0)
      ymax       offset to last azimuth row (default = nlines-1)
    
    """

    args = [intensity, flag, width, n_thres]
    optargs = [ymin, ymax]
    cmd = makecmd('neutron', args, optargs) 
    os.system(cmd)

def adapt_filt(int, sm, width, low_SNR_thr=None, filt_width=None, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Adaptive bandpass filtering of interferograms ***
    *** Copyright 2008, Gamma Remote Sensing, v3.4 clw 5-Sep-2008 ***
    
    usage: adapt_filt <int> <sm> <width> [low_SNR_thr] [filt_width] [xmin] [xmax] [ymin] [ymax]
    
    input parameters: 
      int          (input) complex interferogram image filename
      sm           (output) smoothed interferogram filename
      width        number of samples/row
      low_snr_thr  low SNR threshold (default = .25);
      filt_width   filter width in pixels (default = 2.0)
      xmin         offset to starting range pixel(default = 0)
      xmax         offset last range pixel (default = width-1)
      ymin         offset to starting azimuth row (default = 0)
      ymax         offset to last azimuth row (default = nlines-1)
    
    """

    args = [int, sm, width]
    optargs = [low_SNR_thr, filt_width, xmin, xmax, ymin, ymax]
    cmd = makecmd('adapt_filt', args, optargs) 
    os.system(cmd)

def base_copy(SLC1_par, baseline_1, SLC2_par, baseline_2, time_rev=None):
    """*** Calculate baseline file for a subsection of a reference SLC ***
    *** Copyright 2003, Gamma Remote Sensing, v1.1 6-Jan-2003 ts/clw/uw ***
    
    usage: base_copy <SLC1_par> <baseline-1> <SLC2_par> <baseline-2> [time_rev] 
    
    input parameters: 
      SLC1_par    (input) ISP image parameter file of the reference SLC
      baseline-1  (input) baseline file derived using the reference SLC geometry
      SLC2_par    (input) ISP image parameter file corresponding to the subsecton of the reference SLC
      baseline-2  (output) baseline file derived using the geometry and timing of the SLC subsection
      time_rev    SLC image normal=1,  time-reversed = -1 (default=1)
    
    """

    args = [SLC1_par, baseline_1, SLC2_par, baseline_2]
    optargs = [time_rev]
    cmd = makecmd('base_copy', args, optargs) 
    os.system(cmd)

def offset_fit(offs, snr, OFF_par, coffs=None, coffsets=None, thres=None, npoly=None, interact_flag=None):
    """*** Range and azimuth offset polynomial estimation ***
    *** Copyright 2011, Gamma Remote Sensing, v3.2 11-Apr-2011 clw/uw ***
    
    usage: offset_fit <offs> <snr> <OFF_par> [coffs] [coffsets] [thres] [npoly] [interact_flag]
    
    input parameters: 
      offs          (input) range and azimuth offset estimates (fcomplex)
      snr           (input) SNR values of offset estimates (float)
      OFF_par       (input) ISP offset/interferogram parameter file
      coffs         (output) culled range and azimuth offset estimates (fcomplex, enter - for none)
      coffsets      (output) culled offset estimates and SNR values (text format, enter - for none)
      thres         SNR threshold (enter - for default from OFF_par)
      npoly         number of model polynomial parameters (enter - for default, 1, 3, 4, 6, default: 4)
      interact_mode interactive culling of input data:
                      0: off (default)
                      1: on
    """

    args = [offs, snr, OFF_par]
    optargs = [coffs, coffsets, thres, npoly, interact_flag]
    cmd = makecmd('offset_fit', args, optargs) 
    os.system(cmd)

def az_spec_SLC(SLC, spectrum, roff=None, namb=None, parfile=None):
    """*** Doppler centroid estimate from SLC SAR images ***
    *** Copyright 2012, Gamma Remote Sensing, v2.8 clw 16-Jun-2012 ***
    
    usage: az_spec_SLC <SLC> <spectrum> [roff] [namb]
    
    input parameters:
      SLC       (input) SAR image data file (fcomplex or scomplex format)
      spectrum  (output) Doppler spectrum (text format)
      roff      range sample offset to center of estimation window (enter - for default=center_swath)
      namb      number of multiples of the PRF to add to the estimated centroid (default=0)
    
    """
    if not parfile:
        SLC_par = SLC+'.par'

    args = [SLC, SLC_par, spectrum]
    optargs = [roff, namb]
    cmd = makecmd('az_spec_SLC', args, optargs) 
    os.system(cmd)

def offset_tracking(offs, snr, SLC_par, OFF_par, disp_map, disp_val=None, mode=None, thres=None, poly_flag=None):
    """*** Conversion of range and azimuth offsets files to displacement map ***
    *** Copyright 2008, Gamma Remote Sensing, v1.7 20-Aug-2008 ts/clw/uw ***
    
    usage: offset_tracking <offs> <snr> <SLC_par> <OFF_par> <disp_map> [disp_val] [mode] [thres] [poly_flag]
    
    input parameters: 
      offs       (input) range and azimuth offset estimates (fcomplex)
      snr        (input) SNR values of offset estimates (float)
      SLC_par    (input) SLC parameter file of reference SLC
      OFF_par    (input) offset parameter file used in the offset tracking
      disp_map   (output) range and azimuth displacement estimates (fcomplex)
      disp_val   (output) range and azimuth displacement estimates and SNR values (enter - for none) (text)
      mode       flag indicating displacement mode:
                   0: displacement in range and azimuth pixels
                   1: displacement in meters in slant range and azimuth directions
                   2: displacement in meters in ground range and azimuth directions (default)
      thres      SNR threshold to accept offset value (default from off_par)
      poly_flag  flag indicating if trend calculated using offset polynomials from OFF_par is subtracted:
                   0: do not subtract polynomial trend from offset data
                   1: subtract polynomial trend from offset data (default)
    
    """

    args = [offs, snr, SLC_par, OFF_par, disp_map]
    optargs = [disp_val, mode, thres, poly_flag]
    cmd = makecmd('offset_tracking', args, optargs) 
    os.system(cmd)

def par_CS_SLC_TIF(TIF, XML, trunk, ):
    """*** Generate ISP SLC parameter and image files for Cosmo Skymed SCS data in TIF format ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 9-Oct-2013 awi/ms ***
    
    usage: par_CS_SLC_TIF <TIF> <XML> <trunk>
    
    input parameters:
      TIF  (input) SCS data file in TIF format
      XML  (input) SCS meta data file in XML format
      trunk (output) output file name trunk used for output filenames 
            (example: yyyymmdd -> yyyymmdd_pol_beamid.slc yyyymmdd_pol_beamid.slc.par)
    
    """

    args = [TIF, XML, trunk]
    optargs = []
    cmd = makecmd('par_CS_SLC_TIF', args, optargs) 
    os.system(cmd)

def par_RSAT2_SLC(annotation_XML, lut_XML, GeoTIFF, polarization, SLC_par, SLC, ):
    """*** Generate SLC parameter and image files for Radarsat 2 SLC data  ***
    *** Copyright 2013, Gamma Remote Sensing, v2.1 7-Mar-2013 awi/clw  ***
    
    usage: par_RSAT2_SLC <annotation_XML> <lut_XML> <GeoTIFF> <polarization> <SLC_par> <SLC>
    
    input parameters:
      annotation_XML (input) Radarsat2 product annotation XML file (product.xml)
      lut_XML        (input) Radarsat2 calibration XML file (lutSigma.xml), use - for no calibration
      GeoTIFF        (input) image data file in GeoTIFF format (imagery_pp.tif)
      polarization   (input) polarization HH, VV, HV, VH to extract
      SLC_par        (output) ISP SLC parameter file (example: yyyymmdd_pp.slc.par)
      SLC            (output) SLC data file (example: yyyymmdd_pp.slc)
    """

    args = [annotation_XML, lut_XML, GeoTIFF, polarization, SLC_par, SLC]
    optargs = []
    cmd = makecmd('par_RSAT2_SLC', args, optargs) 
    os.system(cmd)

def init_offset(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, rlks=None, azlks=None, rpos=None, azpos=None, offr=None, offaz=None, thres=None, rwin=None, azwin=None, cflag=None):
    """*** Determine initial offset between SLC images using correlation of image intensity ***
    *** Copyright 2011, Gamma Remote Sensing, v2.8 clw 8-Apr-2011 ***
    
    usage: init_offset <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> [rlks] [azlks] [rpos] [azpos] [offr] [offaz] [thres] [rwin] [azwin] [cflag]
    
    input parameters:
      SLC-1     (input) single-look complex image 1 (reference)
      SLC-2     (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      rlks      number of range looks (default: 1)
      azlks     number of azimuth looks (default: 1)
      rpos      center of patch in range (samples) (enter - for default: image center)
      azpos     center of patch in azimuth (lines) (enter - for default: image center)
      offr      initial range offset (samples) (enter - for default: 0)
      offaz     initial azimuth offset (lines) (enter - for default: 0)
      thres     cross-correlation SNR threshold (enter - for default: 7.000)
      rwin      range window size (default: 512)
      azwin     azimuth window size (default: 512)
      cflag     copy offsets to the range and azimuth offset polynomials in the OFF_par
                  0: no
                  1: yes (default)
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par]
    optargs = [rlks, azlks, rpos, azpos, offr, offaz, thres, rwin, azwin, cflag]
    cmd = makecmd('init_offset', args, optargs) 
    os.system(cmd)

def SLC_mosaic_S1_TOPS(SLC_tab, SLC, SLC_par, ):
    """*** Calculate a SLC mosiac of S1 TOPS SLC data ***
    *** Copyright 2014, Gamma Remote Sensing v2.2 4-Jul-2014 awi/clw ***
    
    usage: SLC_mosaic_S1_TOPS <SLC_tab> <SLC> <SLC_par>
    
    input parameters:
      SLC_tab  (input) 3 column list of SLC, SLC_par, Sentinel-1 TOPS_par sorted in the order IW1, IW2, IW3
      SLC      (output) SLC mosaic image
      SLC_par  (output) SLC mosaic image parameter file
    """

    args = [SLC_tab, SLC, SLC_par]
    optargs = []
    cmd = makecmd('SLC_mosaic_S1_TOPS', args, optargs) 
    os.system(cmd)

def af_SLC(SLC_par, SLC, rwin=None, azwin=None, dr=None, daz=None, thres=None, a1_flg=None, b0_flg=None, offsets=None, n_ovr=None, roff=None, azoff=None):
    """*** Focus testing for SLC data using autofocus estimation of effective velocity ***
    *** Copyright 2010, Gamma Remote Sensing, v1.2 13-Sep-2010 clw/uw ***
    
    usage: af_SLC <SLC_par> <SLC> [rwin] [azwin] [dr] [daz] [thres] [a1_flg] [b0_flg] [offsets] [n_ovr] [roff] [azoff]
    
    input parameters: 
      SLC_par    (input) ISP SLC image parameter file
      SLC        (input) single-look complex image
      rwin       range window size (enter - for default: 1024)
      azwin      azimuth window size (enter - for default: 4096)
      dr         range sample increment (enter - for default: 1024,  enter 0 for single patch)
      daz        azimuth line increment (enter - for default: 8192,  enter 0 for single patch)
      thres      offset estimation SNR threshold (enter - for default: 10.000)
      a1_flg     fit a1 for first derivative of the effective velocity w.r.t.range
                   0: no (default)
                   1: yes
      b0_flg     fit b0 for first derivative of the effective velocity w.r.t. along-track time
                   0: no (default)
                   1: yes
      offsets    (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr      SLC oversampling factor (1,2,4: enter - for default: 1)
      roff       range offset for single patch center
      azoff      azimuth offset for single patch center
    """

    args = [SLC_par, SLC]
    optargs = [rwin, azwin, dr, daz, thres, a1_flg, b0_flg, offsets, n_ovr, roff, azoff]
    cmd = makecmd('af_SLC', args, optargs) 
    os.system(cmd)

def ptarg_cal_SLC(SLC_par, SLC, r_samp, az_samp, psigma, c_r_samp, c_az_samp, ptr_image, r_plot, az_plot, pcal, osf=None, win=None, pltflg=None, psz=None, csz=None, c_image=None):
    """*** Point target analysis and radiometric calibration of SLC images ***
    *** Copyright 2014, Gamma Remote Sensing, v2.1  4-Jun-2014 clw ***
    
    usage: ptarg_cal_SLC <SLC_par> <SLC> <r_samp> <az_samp> <psigma> <c_r_samp> <c_az_samp> <ptr_image> <r_plot> <az_plot> <pcal> [osf] [win] [pltflg] [psz] [csz] [c_image]
    
    input parameters:
      SLC_par    (input) SLC image parameter file
      SLC        (input) SLC image in FCOMPLEX or SCOMPLEX format
      r_samp     point target range sample number, target region size is 16x16
      az_samp    point target azimuth line number, target region size is 16x16
      psigma     radar cross-section of the calibration target in m**2
      c_r_samp   clutter region center range sample number, clutter region size is 16x16
      c_az_samp  clutter region center azimuth line number, clutter region size is 16x16
      ptr_image  (output) oversampled point target image, with and without phase gradient, nominal width: 256
      r_plot     (output) range point target response plot data (text format)
      az_plot    (output) azimuth point target response plot data (text format)
      pcal       (output) measured point target parameters and radiometric calibration factor (text format)
      osf        image over-sampling factor, 2, 4, 8, 16, 32, 64 (enter - for default: 16)
      win        maximum search window offset (samples) (enter - for default: 1)
      pltflg     plotting mode flag:
                   0: none
                   1: output plots in PNG format (default)
                   2: screen output and PNG format plots
                   3: output plots in PDF format
      psz        point target region size (samples) (enter - for default: 16)
      csz        clutter region size (samples) (enter - for default: 16)
      c_image    (output) clutter region image (FCOMPLEX format)
    
    """

    args = [SLC_par, SLC, r_samp, az_samp, psigma, c_r_samp, c_az_samp, ptr_image, r_plot, az_plot, pcal]
    optargs = [osf, win, pltflg, psz, csz, c_image]
    cmd = makecmd('ptarg_cal_SLC', args, optargs) 
    os.system(cmd)

def rascc_mask(cc, pwr, width, start_cc=None, start_pwr=None, nlines=None, pixavr=None, pixavaz=None, cc_thres=None, pwr_thres=None, cc_min=None, cc_max=None, scale=None, exp=None, LR=None, rasf=None):
    """*** Generate phase unwrapping validity mask using correlation and intensity ***
    *** Copyright 2012, Gamma Remote Sensing, v1.7 12-Jan-2012 clw/uw ***
    
    usage: rascc_mask <cc> <pwr> <width> [start_cc] [start_pwr] [nlines] [pixavr] [pixavaz] [cc_thres] [pwr_thres] [cc_min] [cc_max] [scale] [exp] [LR] [rasf]
    
    input parameters:
      cc         (input)coherence image (float)
      pwr        (input)intensity image (float, enter - if not available)
      width      number of samples/row
      start_cc   starting line of coherence image (default: 1)
      start_pwr  starting line of intensity image (default: 1)
      nlines     number of lines to display (default=0: to end of file)
      pixavr     number of pixels to average in range (default: 1)
      pixavaz    number of pixels to average in azimuth (default: 1)
      cc_thres   coherence threshold for masking, pixels with cc < cc_thres are set to 0 (default: 0.0)
      pwr_thres  intensity threshold, pixels with relative intensity below pwr_thres are set to 0 (default: 0.)
      cc_min     minimum coherence value used for color display (default: 0.1)
      cc_max     maximum coherence value used for color display (default: 0.9)
      scale      intensity display scale factor (default: 1.)
      exp        intensity display exponent (default: .35)
      LR         left/right mirror image flag, (1: normal (default), -1: mirror image)
      rasf       (output) image filename, enter - for default, extension determines format:
                   *.bmp BMP format
                   *.ras Sun raster format (default)
    """

    args = [cc, pwr, width]
    optargs = [start_cc, start_pwr, nlines, pixavr, pixavaz, cc_thres, pwr_thres, cc_min, cc_max, scale, exp, LR, rasf]
    cmd = makecmd('rascc_mask', args, optargs) 
    os.system(cmd)

def offset_pwr(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr, rwin=None, azwin=None, offsets=None, n_ovr=None, nr=None, naz=None, thres=None, pflag=None):
    """*** Offsets between SLC images using intensity cross-correlation ***
    *** Copyright 2013, Gamma Remote Sensing, v3.6 13-Oct-2013 clw/uw ***
    
    usage: offset_pwr <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <offs> <snr> [rwin] [azwin] [offsets] [n_ovr] [nr] [naz] [thres] [pflag]
    
    input parameters: 
      SLC-1     (input) single-look complex image 1 (reference)
      SLC-2     (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rwin      search window size (range pixels, (enter - for default from offset parameter file))
      azwin     search window size (azimuth pixels, (enter - for default from offset parameter file))
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     SLC oversampling factor (integer 2**N (1,2,4) default = 2)
      nr        number of offset estimates in range direction (enter - for default from offset parameter file)
      naz       number of offset estimates in azimuth direction (enter - for default from offset parameter file)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      pflag     print flag (0: print offset summary (default)  1: print all offset data)
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr]
    optargs = [rwin, azwin, offsets, n_ovr, nr, naz, thres, pflag]
    cmd = makecmd('offset_pwr', args, optargs) 
    os.system(cmd)

def offset_pwr_tracking(SLC1, SLC2, SLC1_par, SLC2_par, OFF_par, offs, snr, rwin=None, azwin=None, offsets=None, n_ovr=None, thres=None, rstep=None, azstep=None, rstart=None, rstop=None, azstart=None, azstop=None, c_ovr=None, pflag=None, pltflg=None):
    """*** Offset tracking between SLC images using intensity cross-correlation ***
    *** Copyright 2014, Gamma Remote Sensing, v3.3 clw 26-May-2014 ***
    
    usage: offset_pwr_tracking <SLC1> <SLC2> <SLC1_par> <SLC2_par> <OFF_par> <offs> <snr> [rwin] [azwin] [offsets] [n_ovr] [thres] [rstep] [azstep] [rstart] [rstop] [azstart] [azstop] [c_ovr] [pflag] [pltflg]
    
    input parameters: 
      SLC1      (input) single-look complex image 1 (reference)
      SLC2      (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rwin      search region size (range pixels, (enter - for default from offset parameter file)
      azwin     search region size (azimuth pixels, (enter - for default from offset parameter file)
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     SLC oversampling factor (integer 2**N (1,2,4), enter - for default: 2)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      rstep     step in range pixels (enter - for default: rwin/2)
      azstep    step in azimuth pixels (enter - for default: azwin/2)
      rstart    offset to starting range pixel (enter - for default: 0)
      rstop     offset to ending range pixel (enter - for default: nr-1)
      azstart   offset to starting azimuth line (enter - for default: 0)
      azstop    offset to ending azimuth line  (enter - for default: nlines-1)
      c_ovr     correlation function oversampling factor (integer 2**N (1,2,4,8,16) default: 4)
      pflag     print flag
                  0:print offset summary
                  1:print all offset data)
      pltflg    plotting flag:
                  0: none (default)
                  1: output plots in PNG format
                  2: screen output and PNG format plots
                  3: output plots in PDF format
    
    """

    args = [SLC1, SLC2, SLC1_par, SLC2_par, OFF_par, offs, snr]
    optargs = [rwin, azwin, offsets, n_ovr, thres, rstep, azstep, rstart, rstop, azstart, azstop, c_ovr, pflag, pltflg]
    cmd = makecmd('offset_pwr_tracking', args, optargs) 
    os.system(cmd)

def par_RISAT_SLC(CEOS_leader, BAND_META, SLC_par, CEOS_image, SLC=None, line_dir=None, pix_dir=None, cal_flg=None, KdB=None):
    """*** Read RISAT-1 CEOS format SLC data and perform radiometric calibration ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 3-Jun-2013 clw ***
    
    usage: par_RISAT_SLC <CEOS_leader> <BAND_META> <SLC_par> <CEOS_image> [SLC] [line_dir] [pix_dir] [cal_flg] [KdB]
    
    input parameters:
      CEOS_leader  (input) CEOS SAR leader file (example: lea_01.001)
      BAND_META    (input) BAND_META.txt, additional RISAT system parameters for the scene (format keywork=value)
      SLC_par      (output) ISP SLC image parameter file (example: YYYYMMDD.grd.par)
      CEOS_image   (input) CEOS SLC image file (example: dat_01.001)
      SLC          (output) SLC data with file and line headers removed (enter - for none: example: YYYYMMDD.grd)
      line_dir     set output image line direction (enter - for default):
                     0: used value derived from CEOS leader file
                     1: retain input data line direction  (default)
                    -1: reverse input data line direction
      pix_dir      set output pixel direction (enter - for default):
                     0: used value derived from CEOS leader file
                     1: retain input data pixel direction (default)
                    -1: reverse input data pixel direction
      cal_flg      calibration flag (enter - for default):
                     0: do not apply radiometric calibration
                     1: apply radiometric calibration including KdB and incidence angle correction (default)
      KdB          calibration constant (dB) (enter - to use value in the CEOS leader)
    
    """

    args = [CEOS_leader, BAND_META, SLC_par, CEOS_image]
    optargs = [SLC, line_dir, pix_dir, cal_flg, KdB]
    cmd = makecmd('par_RISAT_SLC', args, optargs) 
    os.system(cmd)

def init_offset_orbit(SLC1_par, SLC2_par, OFF_par, rpos=None, azpos=None, cflag=None):
    """*** Initial SLC image offset estimation from orbit state-vector data ***
    *** Copyright 2012, Gamma Remote Sensing, v1.6 23-oct-2012 clw/uw ***
    
    usage: init_offset_orbit <SLC1_par> <SLC2_par> <OFF_par> [rpos] [azpos] [cflag]
    
    input parameters:
      SLC1_par  (input) SLC-1 parameter file
      SLC2_par  (input) SLC-2 parameter file
      OFF_par   (input/output) ISP/offset parameter file
      rpos      range position for offset estimation (enter - for default: center of SLC-1)
      azpos     azimuth position for offset estimation (enter - for default: center of SLC-1)
      cflag     copy offsets to the range and azimuth offset polynomials in the OFF_par
                  0: no (default)
                  1: yes
    """

    args = [SLC1_par, SLC2_par, OFF_par]
    optargs = [rpos, azpos, cflag]
    cmd = makecmd('init_offset_orbit', args, optargs) 
    os.system(cmd)

def par_KC_PALSAR_slr(facter_m, CEOS_leader, SLC_par, pol, pls_mode, KC_data, pwr, fdtab=None):
    """*** Generate ISP parameter file, Doppler table, and images for PALSAR KC Slant-Range data ***
    *** Copyright 2013, Gamma Remote Sensing, v1.9.1 20-Aug-2013 ms,awi,clw ***
    usage: par_KC_PALSAR_slr  <facter_m> <CEOS_leader> <SLC_par> <pol> <pls_mode> <KC_data> <pwr> [fdtab]
    
    input parameters:
      facter_m    (input) PALSAR Kyoto-Carbon parameter file
      CEOS_leader (input) PALSAR Kyoto-Carbon leader file (LED)
      SLC_par     (output) ISP image parameter file (example: yyyymmdd.mli.par)
      pol         polarization e.g. HH or HV
      pls_mode    PALSAR acquisition mode:
                    1: Fine Beam Single
                    2: Fine Beam Double
                    3: Wide Beam
      KC_data     (input) PALSAR Kyoto-Carbon data (short, little endian, amplitude)
      pwr         (output) PALSAR intensity (float, GAMMA Software endianness)
      fdtab       (output)table of output polynomials, one polynomial/block used as input to gc_map_fd
    
    """

    args = [facter_m, CEOS_leader, SLC_par, pol, pls_mode, KC_data, pwr]
    optargs = [fdtab]
    cmd = makecmd('par_KC_PALSAR_slr', args, optargs) 
    os.system(cmd)

def adf(interf, sm, cc, width, alpha=None, nfft=None, cc_win=None, step=None, loff=None, nlines=None, wfrac=None):
    """*** Adaptive spectral filtering for complex interferograms ***
    *** Copyright 2013, Gamma Remote Sensing, v3.2 14-Oct-2013 clw ***
    
    usage: adf <interf> <sm> <cc> <width> [alpha] [nfft] [cc_win] [step] [loff] [nlines] [wfrac]
    
    input parameters:
      interf  (input) interferogram (fcomplex)
      sm      (output) smoothed interferogram (fcomplex)
      cc      (output) coherence derived from smoothed interferogram (float)
      width   number of samples/line
      alpha   exponent for non-linear filtering (enter - for default: 0.40)
      nfft    filtering FFT window size, 2**N, 8 --> 512, (enter - for default: 32)
      cc_win  coherence parameter estimation window size odd, max: 15 (enter - for default: 5)
      step    processing step (enter - for default: nfft/8)
      loff    offset to starting line to process (enter - for default: 0)
      nlines  number of lines to process (enter - for default: to end of file)
      wfrac   minimum fraction of points required to be non-zero in the filter window (enter - for default: 0.200)
    
    """

    args = [interf, sm, cc, width]
    optargs = [alpha, nfft, cc_win, step, loff, nlines, wfrac]
    cmd = makecmd('adf', args, optargs) 
    os.system(cmd)

def base_est_fft(interf, SLC1_par, OFF_par, baseline, nazfft=None, r_samp=None, az_line=None):
    """*** Estimate baseline from interferogram fringe spectrum ***
    *** Copyright 2013, Gamma Remote Sensing, v2.0 clw/uw 28-Jun-2013 ***
    
    usage: base_est_fft <interf> <SLC1_par> <OFF_par> <baseline> [nazfft] [r_samp] [az_line]
    
    input parameters:
      interf    (input) multi-look interferogram with range phase 
      SLC1_par  (input) SLC-1 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      baseline  (output) baseline file
      nazfft    size of azimuth FFT (lines read from file, 2**N) (default=512)
      r_samp    range pixel offset to center of the FFT window (default=center)
      az_line   line offset from start of the interf. for the  FFT window (default=center)
    
    """

    args = [interf, SLC1_par, OFF_par, baseline]
    optargs = [nazfft, r_samp, az_line]
    cmd = makecmd('base_est_fft', args, optargs) 
    os.system(cmd)

def hgt_map(unw, SLC_par, OFF_par, baseline, hgt, gr, ph_flag=None, loff=None, nlines=None, SLC2R_par=None):
    """*** Interferometric height/ground range estimation vs. slant range ***
    *** Copyright 2005, Gamma Remote Sensing, v5.1 clw/uw 9-Sep-2005 ***
    
    usage: hgt_map <unw> <SLC_par> <OFF_par> <baseline> <hgt> <gr> [ph_flag] [loff] [nlines] [SLC2R_par]
    
    input parameters: 
      unw        (input) unwrapped interferometric phase
      SLC_par    (input) ISP parameter file for the reference SLC
      OFF_par    (input) ISP offset/interferogram processing parameters
      baseline   (input) baseline parameter file
      hgt        (output) height file (in slant range geometry) relative to the WGS-84 ellipsoid
      gr         (output) cross-track ground ranges on the WGS-84 ellipsoid (in slant range geometry)
      ph_flag    restore phase slope flag (0:no phase change  default=1:add back phase ramp)
      loff       offset to starting line (default = 0)
      nlines     number of lines to calculate (enter - for default: to end of file)
    
       SLC2R_par  (input) parameter file of resampled SLC, required if SLC-2 frequency differs from SLC-1
    
    """

    args = [unw, SLC_par, OFF_par, baseline, hgt, gr]
    optargs = [ph_flag, loff, nlines, SLC2R_par]
    cmd = makecmd('hgt_map', args, optargs) 
    os.system(cmd)

def interp_ad(data_in, data_out, width, r_max=None, np_min=None, np_max=None, w_mode=None, type=None, cp_data=None):
    """*** Weighted interpolation of gaps in 2D data using an adaptive smoothing window ***
    *** Copyright 2013, Gamma Remote Sensing, v2.0 14-Nov-2013 clw/uw ***
    
    usage: interp_ad <data_in> <data_out> <width> [r_max] [np_min] [np_max] [w_mode] [type] [cp_data]
    
    input parameters:
      data_in   (input) data with gaps
      data_out  (output) data with gaps filled by interpolation
      width     number of samples/row
      r_max     maximum interpolation window radius (default(-): 16)
      np_min    minimum number of points used for the interpolation (default(-): 16)
      np_max    maximum number of points used for the interpolation (default(-): 16)
      w_mode    data weighting mode (enter - for default):
                  0: constant
                  1: 1 - (r/r_max)
                  2: 1 - (r/r_max)**2  (default)
                  3: exp(-2.*(r**2/r_max**2))
      type      input and output data type:
                  0: FCOMPLEX
                  1: SCOMPLEX
                  2: FLOAT (default)
                  3: INT
                  4: SHORT
      cp_data   copy data flag:
                  0: do not copy input data values to output
                  1: copy input data values to output (default)
    
    """

    args = [data_in, data_out, width]
    optargs = [r_max, np_min, np_max, w_mode, type, cp_data]
    cmd = makecmd('interp_ad', args, optargs) 
    os.system(cmd)

def par_EORC_JERS_SLC(CEOS_SAR_leader, SLC_par, CEOS_data=None, slc=None):
    """*** Reformat EORC processed JERS-1 SLC and generate the ISP parameter file ***
    *** Copyright 2008, Gamma Remote Sensing, v1.4 9-Oct-2008 clw ***
    
    usage: par_EORC_JERS_SLC <CEOS_SAR_leader> <SLC_par> [CEOS_data] [slc]
    
    input parameters:
      CEOS_SAR_leader  (input) CEOS SAR leader file for JERS SLC processed by EORC
      SLC_par          (output) ISP image parameter file
      CEOS_data        (input) CEOS format SLC data (IMOP_01.DAT, enter - for none)
      SLC              (output) reformated JERS SLC (example: yyyymmdd.slc, enter - for none)
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = [CEOS_data, slc]
    cmd = makecmd('par_EORC_JERS_SLC', args, optargs) 
    os.system(cmd)

def par_RSI_ERS(CEOS_SAR_leader, SLC_par, ):
    """*** ISP parameter file for RSI processed ERS SLC data ***
    *** Copyright 2003, Gamma Remote Sensing, v1.7 4-Aug-2003 clw/uw ***
    
    usage: par_RSI_ERS <CEOS_SAR_leader> <SLC_par>
    
    input parameters:
    CEOS_SAR_leader (input) ERS CEOS SAR leader file
    SLC_par         (output) ISP SLC parameter file (example <orbit>.slc.par)
    
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = []
    cmd = makecmd('par_RSI_ERS', args, optargs) 
    os.system(cmd)

def ptarg_cal_MLI(MLI_par, MLI, r_samp, az_samp, psigma, c_r_samp, c_az_samp, ptr_image, r_plot, az_plot, pcal, osf=None, win=None, pltflg=None, psz=None, csz=None):
    """*** Point target analysis and radiometric calibration of slant-range and ground-range (GRD) images ***
    *** Copyright 2014, Gamma Remote Sensing, v2.2 4-Jun-2014 clw ***
    
    usage: ptarg_cal_MLI <MLI_par> <MLI> <r_samp> <az_samp> <psigma> <c_r_samp> <c_az_samp> <ptr_image> <r_plot> <az_plot> <pcal> [osf] [win] [pltflg] [psz] [csz]
    
    input parameters:
      MLI_par    (input) slant-range or ground-range image parameter file for detected intensity data
      MLI        (input) ground-range or slant range detected image in FLOAT format
      r_samp     point target range sample number, target region size is 16x16
      az_samp    point target azimuth line number, target region size is 16x16
      psigma     radar cross-section of the calibration target in m**2
      c_r_samp   clutter region center range sample number, clutter region size is 16x16
      c_az_samp  clutter region center azimuth line number, clutter region size is 16x16
      ptr_image  (output) oversampled point target image, with and without phase gradient, nominal width: 256
      r_plot     (output) range point target response plot data (text format)
      az_plot    (output) azimuth point target response plot data (text format)
      pcal       (output) measured point target parameters and radiometric calibration factor (text format)
      osf        image over-sampling factor, 2, 4, 8, 16, 32, 64 (enter - for default: 16)
      win        maximum search window offset (samples) (enter - for default: 1)
      pltflg     plotting mode flag:
                   0: none
                   1: output plots in PNG format (default)
                   2: screen output and PNG format plots
                   3: output plots in PDF format
      psz       point target region size (samples) (enter - for default: 16)
      clsz      clutter region size (samples) (enter - for default: 16)
    
    """

    args = [MLI_par, MLI, r_samp, az_samp, psigma, c_r_samp, c_az_samp, ptr_image, r_plot, az_plot, pcal]
    optargs = [osf, win, pltflg, psz, csz]
    cmd = makecmd('ptarg_cal_MLI', args, optargs) 
    os.system(cmd)

def clear_flag(flag, width, flag_bits, xmin, xma, ymin, ymax, ):
    """*** Clear phase unwrapping flag bits ***
    *** Copyright 2005, Gamma Remote Sensing, v1.6 clw 17-Oct-2005 ***
    
    usage: clear_flag <flag> <width> <flag_bits> <xmin> <xmax> <ymin> <ymax>  
    
    input parameters: 
      flag       (input)phase unwrapping flag filename 
      width      number of samples/row
      flag_bits  byte with value of flag(s) to be cleared: 
    
    	Charges = 3	Guides = 4	Low SNR = 8	Visited = 16
    	BRANCH PT. = 32	Cuts   = 64	Lawn    = 128
    
      xmin       starting range pixel offset (default = 0)
      xmax       last range pixel offset (default = width-1)
      ymin       starting azimuth row offset, relative to start (default = 0)
      ymax       last azimuth row offset, relative to start (default = nlines-1)
    
    """

    args = [flag, width, flag_bits, xmin, xma, ymin, ymax]
    optargs = []
    cmd = makecmd('clear_flag', args, optargs) 
    os.system(cmd)

def par_PulSAR(CEOS_SAR_leader, SLC_par, ):
    """*** ISP parameter file generation for ERS SLC data from the PULSAR SAR processor ***
    *** Copyright 2003, Gamma Remote Sensing, v1.2 4-Aug-2003 clw/uw ***
    
    usage: par_PulSAR <CEOS_SAR_leader> <SLC_par>
    
    input parameters:
    CEOS_SAR_leader  (input) ERS CEOS SAR leader file
    SLC_par          (output) ISP SLC parameter file (example <orbit>.slc.par)
    
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = []
    cmd = makecmd('par_PulSAR', args, optargs) 
    os.system(cmd)

def offset_SLC_tracking(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr, rsw=None, azsw=None, offsets=None, n_ovr=None, thres=None, rstep=None, azstep=None, rstart=None, rstop=None, azstart=None, azstop=None, ISZ=None, pflag=None):
    """*** Offset tracking between SLC images using fringe visibility ***
    *** Copyright 2014, Gamma Remote Sensing, v3.5 clw 10-Feb-2014 ***
    
    usage: offset_SLC_tracking <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <offs> <snr> [rsw] [azsw] [offsets] [n_ovr] [thres] [rstep] [azstep] [rstart] [rstop] [azstart] [azstop] [ISZ] [pflag]
    
    input parameters: 
      SLC-1     (input) single-look complex image 1 (reference)
      SLC-2     (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rsw       range search window size (range pixels) (enter - for default from offset parameter file)
      azsw      azimuth search window size (azimuth lines) (enter - for default from offset parameter file)
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     SLC over-sampling factor (integer 2**N (1,2,4) default: 2)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      rstep     step in range pixels (enter - for default: rsw/2)
      azstep    step in azimuth pixels (enter - for default: azsw/2)
      rstart    starting range pixel (enter - for default: rsw/2)
      rstop     ending range pixel (enter - for default: nr - rsw/2)
      azstart   starting azimuth line (enter - for default: azsw/2)
      azstop    ending azimuth line  (enter - for default: nlines - azsw/2)
      ISZ       search chip interferogram size (in non-oversampled pixels, default: 16)
      pflag     print flag:
                  0: print offset summary  (default)
                  1: print all offset data
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr]
    optargs = [rsw, azsw, offsets, n_ovr, thres, rstep, azstep, rstart, rstop, azstart, azstop, ISZ, pflag]
    cmd = makecmd('offset_SLC_tracking', args, optargs) 
    os.system(cmd)

def par_RSAT_SCW(CEOS_leader, CEOS_trailer, CEOS_data, GRD_par, GRD, sc_dB=None, dt=None):
    """*** ISP parameter file for SCANSAR Wide Swath Data ***
    *** Copyright 2012, Gamma Remote Sensing, v2.0 14-Feb-2012 clw ***
    
    usage: par_RSAT_SCW <CEOS_leader> <CEOS_trailer> <CEOS_data> <GRD_par> <GRD> [sc_dB] [dt]
    
    input parameters:
      CEOS_leader  (input) CEOS SAR leader file
      CEOS_trailer (input) CEOS SAR trailer file
      CEOS_data    (input) CEOS data file binary)
      GRD_par      (output) ISP ground range image parameter file (example <orbit>.mli.par)
      GRD          (output) ISP ground range image (example <orbit>.mli) (enter -  for none, float)
      sc_dB        intensity scale factor in dB (enter - for default:   0.00)
      dt           azimuth image time offset (s) (enter - for default = 0.0)
    
    """

    args = [CEOS_leader, CEOS_trailer, CEOS_data, GRD_par, GRD]
    optargs = [sc_dB, dt]
    cmd = makecmd('par_RSAT_SCW', args, optargs) 
    os.system(cmd)

def split_WB(data_in, data_par_in, data_tab, dtype, ):
    """*** ISP: Program split_WB.c ***
    *** Copyright 2011, Gamma Remote Sensing, v1.2 31-May-2011 clw ***
    *** Split WB mosaic image into individual beams using ISP parameter files ***
    
    usage: split_WB <data_in> <data_par_in> <data_tab> <dtype>
    
    input parameters: 
      data_in      (input) input mosaicked data in slant-range geometry (e.g. DEM data)
      data_par_in  (input) ISP image parameter file for data in the input mosaic
      data_tab     (input) 2 column list of output data filenames and ISP image parameter files for each beam in the mosaic (text)
      dtype        (input) input data type:
                      0: FLOAT
                      1: FCOMPLEX
    """

    args = [data_in, data_par_in, data_tab, dtype]
    optargs = []
    cmd = makecmd('split_WB', args, optargs) 
    os.system(cmd)

def par_ASAR(ASAR_file, output_name, K_dB=None):
    """*** Extract SLC/MLI image parameters and images from ENVISAT ASAR SLC, WSS, APP, and PRI products ***
    *** Copyright 2007, Gamma Remote Sensing, v2.6 20-Oct-2013 clw/uw/awi ***
    
    usage: par_ASAR <ASAR_file> <output_name> [K_dB]
    
    input parameters: 
      ASAR_file    (input)ASAR data file including header and image as provided by ESA
      output_name  (output)common part of output file names (e.g. orbit number)
      K_dB         Calibration factor in dB (nominal value for all ASAR modes = 55.0)
                   NOTE: Use - for the calibration coefficient provided in the header of the ASAR_file
    
                   NOTE: In the case that a calibration factor is provided, PRI images are converted
                   to radiometrically calibrated ground-range intensity images in float format
    
    """

    args = [ASAR_file, output_name]
    optargs = [K_dB]
    cmd = makecmd('par_ASAR', args, optargs) 
    os.system(cmd)

def PRC_vec(SLC_par, PRC, nstate=None):
    """*** State vectors from ERS PRC orbit data for ISP processing clw/uw ***
    *** Copyright 2008, Gamma Remote Sensing, v1.7 clw 11-Jun-2008 ***
    
    usage: PRC_vec <SLC_par> <PRC> [nstate]
    
    input parameters: 
      SLC_par  (input/output) ISP SLC/MLI image parameter file
      PRC      (input) PRC state vector file
      nstate   number of state vectors (default=5, maximum=64)
    
    """

    args = [SLC_par, PRC]
    optargs = [nstate]
    cmd = makecmd('PRC_vec', args, optargs) 
    os.system(cmd)

def multi_cpx(data_in, OFF_par_in, data_out, OFF_par_out, rlks=None, azlks=None, loff=None, nlines=None, roff=None, nsamp=None):
    """*** Calculate multi-look averaged or interpolated 2D image (fcomplex data) ***
    *** Copyright 2013, Gamma Remote Sensing, v2.5 28-Mar-2013 clw/uw ***
    
    usage: multi_cpx <data_in> <OFF_par_in> <data_out> <OFF_par_out> [rlks] [azlks] [loff] [nlines] [roff] [nsamp]
    input parameters: 
      data_in      (input) input fcomplex image file
      OFF_par_in   (input) offset parameter file for input image
      data_out     (output) output multi-look or interpolated fcomplex data file
      OFF_par_out  (input/output) offset parameter file for output, if already exists, then used as input
      rlks         number of range looks, values < -1, interpreted as an image oversampling factor (default: 1)
      azlks        number azimuth looks,  values < -1, interpreted as an image oversampling factor (default: 1)
      loff         line offset to starting line (default: 0)
      nlines       number of lines (default: 0, to end of file)
      roff         offset to starting range sample (default:0)
      nsamp        number of range samples to extract (default: 0, to end of line)
    
    """

    args = [data_in, OFF_par_in, data_out, OFF_par_out]
    optargs = [rlks, azlks, loff, nlines, roff, nsamp]
    cmd = makecmd('multi_cpx', args, optargs) 
    os.system(cmd)

def ptarg_SLC(SLC_par, SLC, r_samp, az_samp, ptr_image, r_plot, az_plot, ptr_par=None, osf=None, win=None, pltflg=None):
    """*** Point target response analysis and interpolation for SLC images ***
    *** Copyright 2014, Gamma Remote Sensing, v1.5 3-Jun-2014 clw***
    
    usage: ptarg_SLC <SLC_par> <SLC> <r_samp> <az_samp> <ptr_image> <r_plot> <az_plot> [ptr_par] [osf] [win] [pltflg]
    
    input parameters:
      SLC_par    (input) SLC image parameter file
      SLC        (input) SLC image in FCOMPLEX or SCOMPLEX format
      r_samp     point target range sample number
      az_samp    point target azimuth line number
      ptr_image  (output) oversampled point target image (fcomplex, 1024x1024 samples), with and without phase gradient
      r_plot     (output) range point target response plot data (text format)
      az_plot    (output) azimuth point target response plot data (text format)
      ptr_par    (output) measured point target parameters (text format)
      osf        image over-sampling factor, 2, 4, 8, 16, 32, 64 (enter - for default: 16)
      win        maximum search window offset (samples) (enter - for default: 1)
      pltflg     plotting mode flag:
                   0: none
                   1: output plots in PNG format (default)
                   2: screen output and PNG format plots
                   3: output plots in PDF format
    """

    args = [SLC_par, SLC, r_samp, az_samp, ptr_image, r_plot, az_plot]
    optargs = [ptr_par, osf, win, pltflg]
    cmd = makecmd('ptarg_SLC', args, optargs) 
    os.system(cmd)

def grasses(int, flag, unw, width, xmin=None, xmax=None, ymin=None, ymax=None, xinit=None, yinit=None, init_ph=None):
    """*** Phase unwrapping by region growing ***
    *** Copyright 2005, Gamma Remote Sensing, v4.2 1-Mar-2005 clw/uw ***
    
    usage: grasses <int> <flag> <unw> <width> [xmin] [xmax] [ymin] [ymax] [xinit] [yinit] [init_ph]
    
    input parameters: 
      int      (input) interferogram filename
      flag     (input) unwrapping flag filename
      unw      (output) unwrapped phase filename
      width    number of samples/row
      xmin     starting range pixel offset (default = 0)
      xmax     last range pixel offset (default=width-1)
      ymin     starting azimuth row offset, relative to start (default = 0)
      ymax     last azimuth row offset, relative to start (default = nlines-1)
      xinit    starting range pixel for unwrapping (default = width/2)
      yinit    starting row to unwrap (default = height/2)
    
      init_ph  flag to set phase at starting point to 0.0 (default 0: not set to 0.0, 1: set to 0.0)
    
    """

    args = [int, flag, unw, width]
    optargs = [xmin, xmax, ymin, ymax, xinit, yinit, init_ph]
    cmd = makecmd('grasses', args, optargs) 
    os.system(cmd)

def par_ESA_ERS(CEOS_SAR_leader, SLC_par, CEOS_DAT=None, SLC=None):
    """*** ISP parameter file generation for ERS SLC data from the PGS, VMP, and SPF processors ***
    *** Copyright 2012, Gamma Remote Sensing, v1.4 12-Jan-2012 clw/uw ***
    
    usage: par_ESA_ERS <CEOS_SAR_leader> <SLC_par> [CEOS_DAT] [SLC] 
    
    input parameters:
    CEOS_SAR_leader  (input) ERS CEOS SAR leader file
    SLC_par          (output) ISP SLC parameter file (example: <date>.slc.par)
    CEOS_DAT         (input) CEOS data file (example: DAT_01.001)
    SLC              (output) SLC data with file and line headers removed (example: <date>.slc)
    
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = [CEOS_DAT, SLC]
    cmd = makecmd('par_ESA_ERS', args, optargs) 
    os.system(cmd)

def par_ASF_91(CEOS_leader, CEOS_trailer, SLC_par, ):
    """*** SLC parameter file for data data from theAlaska SAR Facility (1991-1996) ***
    *** Copyright 2008, Gamma Remote Sensing, v3.3 25-Mar-2008 clw/uw ***
    
    usage: par_ASF_91 <CEOS_leader> <CEOS_trailer> <SLC_par>
    
    input parameters: 
      CEOS_leader   (input) ASF CEOS leader file
      CEOS_trailer  (input) ASF CEOS trailer file
      SLC_par       (output) ISP SLC image parameter file
    
    """

    args = [CEOS_leader, CEOS_trailer, SLC_par]
    optargs = []
    cmd = makecmd('par_ASF_91', args, optargs) 
    os.system(cmd)

def par_PRI(CEOS_SAR_leader, PRI_par, CEOS_DAT, PRI, ):
    """*** ISP parameter file generation for ERS PRI data from the PGS and VMP processors ***
    *** Copyright 2012, Gamma Remote Sensing, v1.6 12-Jan-2012 clw ***
    
    usage: par_PRI <CEOS_SAR_leader> <PRI_par> <CEOS_DAT> <PRI>
    
    input parameters:
      CEOS_SAR_leader (input) ERS CEOS SAR leader file for PRI product
      PRI_par         (output) ISP image parameter file (example: <yyyymmdd>.pri.par)
      CEOS_DAT        (input) CEOS data file (example: DAT_01.001)
      PRI             (output) PRI data with file and line headers removed (example: <yyyymmdd>.pri)
    
    """

    args = [CEOS_SAR_leader, PRI_par, CEOS_DAT, PRI]
    optargs = []
    cmd = makecmd('par_PRI', args, optargs) 
    os.system(cmd)

def ORB_prop_SLC(SLC_par, nstate=None, interval=None, extra=None, mode=None):
    """*** Calculate state vectors using orbit propagation and interpolation ***
    *** Copyright 2008, Gamma Remote Sensing, v1.8 11-Jun-2008 clw/awi ***
    
    usage: ORB_prop_SLC <SLC_par> [nstate] [interval] [extra] [mode]
    
    input parameters:
      SLC_par   (input) ISP image parameter file with at least 1 state vector
      nstate    number of state vectors to calculate (enter - for default: nstate from image duration + extra)
      interval  time interval between state vectors (enter - for default: state vector time interval in SLC_par)
      extra     extra time for state vectors at start and end of image (sec.) (enter - for default: 30.0)
      mode      orbit propagation mode:
                  0: polynomial interpolation (default, if 3 or more state vectors available)
                  1: integration of the equations of motion (default, if less than 3 state vectors available)
                  2: interpolate between state vectors, minimum of 3 state vectors;
                     interpolation of the equations of motion outside of the time span of the existing state vectors
    
    """

    args = [SLC_par]
    optargs = [nstate, interval, extra, mode]
    cmd = makecmd('ORB_prop_SLC', args, optargs) 
    os.system(cmd)

def multi_look_MLI(MLI_in, MLI_in_par, MLI_out, MLI_out_par, rlks, azlks, loff=None, nlines=None, scale=None):
    """*** Multi-looking of intensity (MLI) images ***
    *** Copyright 2005, Gamma Remote Sensing, v1.5 23-Aug-2005 clw/uw ***
    
    usage: multi_look_MLI <MLI_in> <MLI_in_par> <MLI_out> <MLI_out_par> <rlks> <azlks> [loff] [nlines] [scale]
    
    input parameters:
      MLI_in       (input) multi_look intensity image (MLI) file (float)
      MLI in_par   (input) MLI parameter file
      MLI_out      (output) multi-looked MLI image (float)
      MLI_out_par  (output) MLI parameter file for output MLI
      rlks         range looks for multi-looking
      azlks        azimuth looks for multi-looking
      loff         offset to starting line (default = 0)
      nlines       number of input MLI lines to process(default = entire file)
      scale        scale factor for output MLI(default = 1.0)
    
    """

    args = [MLI_in, MLI_in_par, MLI_out, MLI_out_par, rlks, azlks]
    optargs = [loff, nlines, scale]
    cmd = makecmd('multi_look_MLI', args, optargs) 
    os.system(cmd)

def par_ASF_SLC(CEOS_SAR_leader, SLC_par, CEOS_data=None, SLC=None):
    """*** Generate SLC image parameter file and reformat data ***
    *** Copyright 2012, Gamma Remote Sensing, v1.0 27-Aug-2012 clw/uw ***
    
    usage: par_ASF_SLC <CEOS_SAR_leader> <SLC_par> [CEOS_data] [SLC]
    
    input parameters:
      CEOS_leader  (input) CEOS SAR leader file
      SLC_par      (output) ISP SLC parameter file (example <date>.slc.par)
      CEOS_data    (input) CEOS data file (example: dat_01.001)
      SLC          (output) SLC data with file and line headers removed (example: <date>.slc)
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = [CEOS_data, SLC]
    cmd = makecmd('par_ASF_SLC', args, optargs) 
    os.system(cmd)

def radcal_pwr_stat(SLC_tab, SLC_tab_cal, plist, MSR_cal, PWR_cal, roff=None, loff=None, nr=None, nl=None, plist_out=None):
    """*** Generate calibrated SLC image files using point targets determined from the Mean/Sigma Ratio and Intensity ***
    *** Copyright 2012, Gamma Remote Sensing, v1.3 11-May-2012 clw/uw ***
    
    usage: radcal_pwr_stat <SLC_tab> <SLC_tab_cal> <plist> <MSR_cal> <PWR_cal> [roff] [loff] [nr] [nl] [plist_out]
    
    input parameters:
      SLC_tab      (input) two column list of the SLC filenames and SLC parameter filenames of the uncalibrated SLC images
      SLC_tab_cal  (input) two column list of the SLC filenames and SLC parameter filenames of the calibrated SLC images (enter - for none)
      plist        (input) point list for the point to use for calibraton (int, enter - to use the data to determine the calibration points)
      MSR_cal      mean/sigma ratio for point target selection for relative calibration between scenes:    1.500
      PWR_cal      intensity threshold ratio for point target selection for relative calibration between scenes:    1.000
      roff         offset to starting range of section to analyze (default -: 0)
      loff         offset to starting line of section to analyze (default -: 0)
      nr           number of range pixels to analyze (default -: to end of line)
      nl           number of azimuth lines to analyze (default -: to end of file)
      plist_out    point list of points used to determine calibration using MSR_cal and PWR_cal thresholds
    """

    args = [SLC_tab, SLC_tab_cal, plist, MSR_cal, PWR_cal]
    optargs = [roff, loff, nr, nl, plist_out]
    cmd = makecmd('radcal_pwr_stat', args, optargs) 
    os.system(cmd)

def multi_SLC_WSS(SLC, SLC_par, MLI, MLI_par, ):
    """*** Calculate multi-look intensity image (MLI) from a ASAR Wide-Swath SLC ***
    *** Copyright 2008, Gamma Remote Sensing v1.2 08-Jan-2008 clw/awi ***
    
    usage: multi_SLC_WSS <SLC> <SLC_par> <MLI> <MLI_par> 
    
    input parameters:
      SLC      (input) ASAR Wide-Swath SLC image
      SLC_par  (input) ASAR Wide-Swath SLC image parameter file
      MLI      (output) multi-look intensity image
      MLI_par  (output) MLI image parameter file
    """

    args = [SLC, SLC_par, MLI, MLI_par]
    optargs = []
    cmd = makecmd('multi_SLC_WSS', args, optargs) 
    os.system(cmd)

def sbi_filt(SLC_1, SLC1_par, SLC2R_par, SLCf, SLCf_par, SLCb, SLCb_par, norm_sq, iwflg=None):
    """*** Azimuth filtering of SLC data to support split-beam interferometry to measure azimuth offsets ***
    *** Copyright 2011, Gamma Remote Sensing, v1.1 24-Nov-2011 ***
    
    usage: sbi_filt <SLC-1>  <SLC1_par> <SLC2R_par> <SLCf> <SLCf_par> <SLCb> <SLCb_par> <norm_sq> [iwflg]
    input parameters:
      SLC-1     (input) SLC image (scomplex of fcomplex format)
      SLC1_par  (input) SLC image parameter file
      SLC2R_par (input) SLC2 ISP image parameter file for the co-registered image of the interferometric pair,
                used to determine azimuth common-band for each output SLC (enter - for none)
      SLCf      (output) single-look complex image (forward-looking, fcomplex format)
      SLCf_par  (output) SLC parameter file (forward-looking)
      SLCb      (output) single-look complex image (backward-looking, fcomplex format)
      SLCb_par  (output) SLC parameter file (backward-looking)
      norm_sq   squint between beams as a fraction of the azimuth spectrum width (default: 0.5)
      iwflg     inverse weighting flag:
                  0: no compensation for azimuth spectrum weighting
                  1: compensate for the azimuth spectrum weighting (default)
    """

    args = [SLC_1, SLC1_par, SLC2R_par, SLCf, SLCf_par, SLCb, SLCb_par, norm_sq]
    optargs = [iwflg]
    cmd = makecmd('sbi_filt', args, optargs) 
    os.system(cmd)

def par_PRI_ESRIN_JERS(CEOS_SAR_leader, PRI_par, CEOS_DAT, PRI, ):
    """*** ISP GRD parameter file for ESRIN processed JERS PRI data ***
    *** Copyright 2008, Gamma Remote Sensing, v1.8 16-May-2008 clw/uw ***
    
    usage: par_PRI_ESRIN_JERS <CEOS_SAR_leader> <PRI_par> <CEOS_DAT> <PRI>
    
    input parameters:
      CEOS_SAR_leader (input) ERS CEOS SAR leader file for PRI product
      PRI_par         (output) ISP image parameter file (example: <yyyymmdd>.pri.par)
      CEOS_DAT        (input) CEOS data file (example: DAT_01.001)
      PRI             (output) PRI data with file and line headers removed (example: <yyyymmdd>.pri)
    
    """

    args = [CEOS_SAR_leader, PRI_par, CEOS_DAT, PRI]
    optargs = []
    cmd = makecmd('par_PRI_ESRIN_JERS', args, optargs) 
    os.system(cmd)

def SR_to_GRD(MLI_par, OFF_par, GRD_par, in_file, out_file, rlks=None, azlks=None, interp_mode=None, grd_rsp=None, grd_azsp=None):
    """*** Conversion to ground range for ISP MLI and INSAR data of type float ***
    *** Copyright 2009, Gamma Remote Sensing, v1.9 7-May-2009 uw/clw ***
    
    usage: SR_to_GRD <MLI_par> <OFF_par> <GRD_par> <in_file> <out_file> [rlks] [azlks] [interp_mode] [grd_rsp] [grd_azsp]
    
    input parameters: 
      MLI_par      (input) MLI image parameter file of input slant range image (float)
      OFF_par      (input) ISP offset/interferogram parameter file of input image (enter - image in MLI geometry)
      GRD_par      (input/output) image parameter file of output ground range image
      in_file      (input) slant range image (float)
      out_file     (output) ground range image (float)
      rlks         multi-looking in range (prior to resampling, default=1)
      azlks        multi-looking in azimuth (prior to resampling, default=1)
      interp_mode  interpolation mode
                     0: nearest neighbor (default)
                     1: spline
                     2: spline log
      grd_rsp      output image ground range sample spacing (m) (default = (input image azimuth spacing) * azlks)
      grd_azsp     output image azimuth sample spacing (m) (default = (input image azimuth spacing) * azlks)
    
    """

    args = [MLI_par, OFF_par, GRD_par, in_file, out_file]
    optargs = [rlks, azlks, interp_mode, grd_rsp, grd_azsp]
    cmd = makecmd('SR_to_GRD', args, optargs) 
    os.system(cmd)

def par_IECAS_SLC(aux_data, slc_Re, slc_Im, date, SLC_par, SLC, ):
    """*** Generate SLC parameter and image files for IECAS SLC data  ***
    *** Copyright 2011, Gamma Remote Sensing, v1.2 24-Jan-2011 ***
    
    usage: par_IECAS_SLC <aux_data> <slc_Re> <slc_Im> <date> <SLC_par> <SLC>
    
    input parameters:
      aux_data   (input) IECAS SAR auxillary data (POS*.dat)
      slc_Re     (input) real part of complex SLC data
      slc_Im     (input) imaginary part of complex SLC data
      date       (input) acquistion date format: YYYYMMDD (example 20110121) from aux_data filename
      SLC_par    (output) ISP SLC parameter file
      SLC        (output) SLC image
    """

    args = [aux_data, slc_Re, slc_Im, date, SLC_par, SLC]
    optargs = []
    cmd = makecmd('par_IECAS_SLC', args, optargs) 
    os.system(cmd)

def par_TX_SLC(annotation_XML, COSAR, SLC_par, SLC, pol=None):
    """*** Generate SLC parameter file and SLC image from a Terrasar-X SSC data set ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 25-Sep-2012 awi/clw  ***
    
    usage: par_TX_SLC <annotation_XML> <COSAR> <SLC_par> <SLC> [pol]
    
    input parameters:
      annotation_XML (input) Terrasar-X product annotation XML file
      COSAR          (input) COSAR SSC data file
      SLC_par        (output) ISP SLC parameter file (example: yyyymmdd.slc.par)
      SLC            (output) SLC data file (example: yyyymmdd.slc)
      pol            polarisation HH, HV, VH, VV (default: first polarisation found in the annotation_XML)
    
    """

    args = [annotation_XML, COSAR, SLC_par, SLC]
    optargs = [pol]
    cmd = makecmd('par_TX_SLC', args, optargs) 
    os.system(cmd)

def multi_S1_TOPS(SLC_tab, MLI, MLI_par, rlks, azlks, ):
    """*** Calculate multi-look intensity image (MLI) from a S1 TOPS SLC ***
    *** Copyright 2014, Gamma Remote Sensing v2.2 4-Jul-2014 awi/clw ***
    
    usage: multi_S1_TOPS <SLC_tab> <MLI> <MLI_par> <rlks> <azlks>
    
    input parameters:
      SLC_tab  (input) 3 column list of SLC, SLC_par, Sentinel-1 TOPS_par sorted in the order IW1, IW2, IW3
      MLI      (output) multi-look intensity image
      MLI_par  (output) MLI image parameter file
      rlks     number of range looks
      azlks    number of azimuth looks
    
    """

    args = [SLC_tab, MLI, MLI_par, rlks, azlks]
    optargs = []
    cmd = makecmd('multi_S1_TOPS', args, optargs) 
    os.system(cmd)

def tree_cc(flag, width, mbl=None, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Phase unwrapping tree generation with low correlation search (modified ARW algorithm) ***
    *** Copyright 2014, Gamma Remote Sensing, v2.9 20-Jan-2014 clw/uw ***
    
    usage: tree_cc <flag> <width> [mbl] [xmin] [xmax>] [ymin] [ymax]
    
    input parameters: 
      flag   (input) phase unwrapping flag file
      width  number of samples/row
      mbl    maximum branch length (default=32, maximum=64) 
      xmin   starting range pixel offset (default = 0)
      xmax   last range pixel offset (default = width-1)
      ymin   starting azimuth row, relative to start (default = 0)
      ymax   last azimuth row, relative to start (default = nlines-1)
    
    """

    args = [flag, width]
    optargs = [mbl, xmin, xmax, ymin, ymax]
    cmd = makecmd('tree_cc', args, optargs) 
    os.system(cmd)

def ave_image(im_list, width, ave, start=None, nlines=None, pixav_x=None, pixav_y=None, zflag=None, nmin=None):
    """*** Calculate average of a stack of images (float format) ***
    *** Copyright 2011, Gamma Remote Sensing, v1.8 27-May-2011 clw ***
    
    usage: ave_image <im_list> <width> <ave> [start] [nlines] [pixav_x] [pixav_y] [zflag] [nmin]
    
    input parameters:
      im_list  (input) text file with names of co-registered images in column 1 (float)
      width    number of samples/line
      ave      (output) average of input image data files (float)
      start    starting line (default: 1)
      nlines   number of lines to process (enter -  for default: entire file)
      pixav_x  number of pixels to average in width  (default: 1)
      pixav_y  number of pixels to average in height (default: 1)
      zflag    zero flag:
                 0: interpret 0.0 as missing data value (default)
                 1: interpret 0.0 as valid data
      nmin     minimum number of images required to calculate the average if zflag = 0 (default: 3/4*nfiles)
    
    """

    args = [im_list, width, ave]
    optargs = [start, nlines, pixav_x, pixav_y, zflag, nmin]
    cmd = makecmd('ave_image', args, optargs) 
    os.system(cmd)

def multi_look(SLC, SLC_par, MLI, MLI_par, rlks, azlks, loff=None, nlines=None, scale=None, exp=None):
    """*** Calculate a multi-look intensity (MLI) image from an SLC image ***
    *** Copyright 2012, Gamma Remote Sensing, v3.9 1-Mar-2012 clw/uw ***
    
    usage: multi_look <SLC> <SLC_par> <MLI> <MLI_par> <rlks> <azlks> [loff] [nlines] [scale] [exp]
    
    input parameters:
      SLC      (input) single-look complex image
      SLC_par  (input) SLC ISP image parameter file
      MLI      (output) multi-look intensity image
      MLI_par  (output) MLI ISP image parameter file
      rlks     number of range looks
      azlks    number of azimuth looks
      loff     offset to starting line (default: 0)
      nlines   number of SLC lines to process (enter - for default: entire file)
      scale    scale factor for output MLI (default: 1.0)
      exp      exponent for the output MLI (default: 1.0)
    
    """

    args = [SLC, SLC_par, MLI, MLI_par, rlks, azlks]
    optargs = [loff, nlines, scale, exp]
    cmd = makecmd('multi_look', args, optargs) 
    os.system(cmd)

def SLC_burst_copy(SLC_in, SLC_par_in, TOPS_par, SLC_out, SLC_par_out, burst=None, fcase=None, sc=None):
    """*** Copy selected burst from Scansar SLC with options for data format conversion, and burst selection ***
    *** Copyright 2014, Gamma Remote Sensing, v1.1 14-May-2014 awi                                        ***
    
    usage: SLC_burst_copy <SLC_in> <SLC_par_in> <TOPS_par> <SLC_out> <SLC_par_out> [burst] [fcase] [sc]
    
    input parameters: 
      SLC_in       (input) SLC (FCOMPLEX or SCOMPLEX format)
      SLC_par_in   (input) ISP SLC parameter file for input SLC
      TOPS_par     (input) SLC burst annotation file for scansar data
      SLC_out      (output) selected SLC section (FCOMPLEX or SCOMPLEX format)
      SLC_par_out  (output) ISP SLC parameter file of output SLC
      burst        id of burst to be selected (default burst #1)
      fcase        data format conversion (enter - for default: output format = input format)
                     1: FCOMPLEX --> FCOMPLEX (default sc = 1.0)
                     2: FCOMPLEX --> SCOMPLEX (default sc = 10000.0)
                     3: SCOMPLEX --> FCOMPLEX (default sc = 0.0001)
                     4: SCOMPLEX --> SCOMPLEX (default sc = 1.0)
      sc           scale factor for input SLC data (enter - for default)
    
    """

    args = [SLC_in, SLC_par_in, TOPS_par, SLC_out, SLC_par_out]
    optargs = [burst, fcase, sc]
    cmd = makecmd('SLC_burst_copy', args, optargs) 
    os.system(cmd)

def dcomp_sirc_quad(infile, outfile, samples, parameter, loff=None, nlines=None):
    """*** Extract SIR-C MLC or SLC compressed quad-pol data ***
    *** Copyright 2009, Gamma Remote Sensing, v1.4 16-Oct-2009 uw/clw ***
    
    usage: dcomp_sirc_quad <infile> <outfile> <samples> <parameter> [loff] [nlines]
    
    input parameters: 
      infile     (input) SIR-C SLC or MLC quad-pol compressed data
      outfile    (output) complex floating point data
      samples    number of polarimetric samples per input line (10 bytes/sample)
      parameter  polarimetric parameter to extract from SLC or MLC product:
                   0:  SLC total power
                   1:  SLC-HH
                   2:  SLC-HV
                   3:  SLC-VH
                   4:  SLC-VV
                   5:  MLC total power
                   6:  MLC-HVHV*
                   7:  MLC-VVVV*
                   8:  MLC-HHHH*
                   9:  MLC-HHHV*
                   10: MLC-HHVV*
                   11: MLC-HVVV*
      loff       offset to starting line (default: 0)
      nlines     number of lines to copy(default: entire file, 0 = entire file)
    """

    args = [infile, outfile, samples, parameter]
    optargs = [loff, nlines]
    cmd = makecmd('dcomp_sirc_quad', args, optargs) 
    os.system(cmd)

def par_ERSDAC_PALSAR(VEXCEL_SLC_par, SLC_par, ):
    """*** Generate the ISP image parameter file from ERSDAC PALSAR level 1.1 SLC data ***
    *** Copyright 2011, Gamma Remote Sensing, v1.5 31-Oct-2011 clw ***
    
    usage: par_ERSDAC_PALSAR <VEXCEL_SLC_par> <SLC_par>
    
    input parameters:
    ERSDAC_SLC_par (input) ERSDAC SLC parameter file Level 1.1 (PASL11*.SLC.par)
    SLC_par        (output) ISP image parameter file (example: yyyymmdd.slc.par)
    """

    args = [VEXCEL_SLC_par, SLC_par]
    optargs = []
    cmd = makecmd('par_ERSDAC_PALSAR', args, optargs) 
    os.system(cmd)

def phase_slope(interf, slopes, width, win_sz=None, thres=None, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Calculate interferogram phase slopes in range and azimuth ***
    *** Copyright 2011, Gamma Remote Sensing, v1.3 19-Apr-2011 clw/uw ***
    
    usage: phase_slope <interf> <slopes> <width> [win_sz] [thres] [xmin] [xmax] [ymin] [ymax]
    
    input parameters: 
      interf  (input) interferogram (fcomplex)
      slopes  (output) range and azimuth phase slopes (fcomplex)
      width   number of samples/row
      win_sz  size of region used for slopes determination (default = 5)
      thres   correlation threshold for accepting slope estimates 0.0 -> 1.0 (default=.4)
      xmin    starting range pixel offset (default = 0)
      xmax    last range pixel offset (default = width-1)
      ymin    starting azimuth row offset (default = 0)
      ymax    last azimuth row offset (default = nlines-1)
    
    """

    args = [interf, slopes, width]
    optargs = [win_sz, thres, xmin, xmax, ymin, ymax]
    cmd = makecmd('phase_slope', args, optargs) 
    os.system(cmd)

def sbi_offset(sbi_unw, SLCf_par, SLCb_par, OFF_par, az_offset, ):
    """*** Calculate azimuth offsets from unwrapped split-beam interferogram ***
    *** Copyright 2011, Gamma Remote Sensing, v1.0 25-Nov-2011 ***
    
    usage: sbi_offset <sbi_unw> <SLCf_par> <SLCb_par> <OFF_par> <az_offset>
    
    input parameters:
      sbi_unw   (input) unwrapped phase of split-beam interferogram (float)
      SLCf_par  (input) reference SLC parameter file (forward-looking)
      SLCb_par  (input) reference SLC parameter file (backward-looking)
      OFF_par   (input) offset parameter file
      az_offset (output) azimuth offsets (m)
    """

    args = [sbi_unw, SLCf_par, SLCb_par, OFF_par, az_offset]
    optargs = []
    cmd = makecmd('sbi_offset', args, optargs) 
    os.system(cmd)

def par_ATLSCI_ERS(CEOS_SAR_leader, CEOS_Image, SLC_par, ):
    """*** ISP parameter file for ATL-SCI ERS SLC data ***
    *** Copyright 2003, Gamma Remote Sensing, v2.8 24-Nov-2003 clw ***
    
    usage: par_ATLSCI_ERS <CEOS_SAR_leader> <CEOS_Image> <SLC_par>
    
    input parameters:
    CEOS_SAR_leader (input) CEOS SAR leader file (LEA_01.001)
    CEOS_Image      (input) CEOS image data segment (DAT_01.001)
    SLC_par         (output) ISP SLC parameter file (example <orbit>.slc.par)
    
    """

    args = [CEOS_SAR_leader, CEOS_Image, SLC_par]
    optargs = []
    cmd = makecmd('par_ATLSCI_ERS', args, optargs) 
    os.system(cmd)

def residue_cc(int, flag, width, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Determine interferometric phase unwrapping residues considering low coherence regions ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6  20-Jan-2014 clw/uw/ts ***
    
    usage: residue_cc <int> <flag> <width> [xmin] [xmax] [ymin] [ymax]
    
    input parameters: 
      int    (input) interferogram (fcomplex)
      flag   (input) flag file (unsigned char)
      width  number of samples/row
      xmin   offset to starting range pixel(default = 0)
      xmax   offset last range pixel (default = width-1)
      ymin   offset to starting azimuth row (default = 0)
      ymax   offset to last azimuth row (default = nlines-1)
    
    """

    args = [int, flag, width]
    optargs = [xmin, xmax, ymin, ymax]
    cmd = makecmd('residue_cc', args, optargs) 
    os.system(cmd)

def SLC_ovr(SLC, SLC_par, SLC_ovr, SLC_ovr_par, r_ovr, ):
    """*** ISP Program SLC_ovr.c ***
    *** Copyright 2008, Gamma Remote Sensing, v1.6 15-Apr-2008 clw ***
    *** Oversample or subsample SLC data in range ***
    
    usage: SLC_ovr <SLC> <SLC_par> <SLC_ovr> <SLC_ovr_par> <r_ovr>
    
    input parameters:
      SLC         (input) SLC file (fcomplex or scomplex)
      SLC_par     (input) SLC parameter file of SLC file
      SLC_ovr     (output) range resampled SLC file (fcomplex or scomplex)
      SLC_ovr_par (output) SLC parameter file of range resampled SLC data file
      r_ovr       integer range oversampling factor (2 --> 16)
                  if r_ovr < 0, the SLC will be subsampled, integer range subsampling factor (-2 --> -16)
    """

    args = [SLC, SLC_par, SLC_ovr, SLC_ovr_par, r_ovr]
    optargs = []
    cmd = makecmd('SLC_ovr', args, optargs) 
    os.system(cmd)

def SLC_copy(SLC_in, SLC_par_in, SLC_out, SLC_par_out, fcase=None, sc=None, roff=None, nr=None, loff=None, nl=None, swap=None, header_lines=None):
    """*** Copy SLC with options for data format conversion, segment extraction, and byte swapping ***
    *** Copyright 2013, Gamma Remote Sensing, v5.0 10-Jan-2013 uw/clw ***
    
    usage: SLC_copy <SLC_in> <SLC_par_in> <SLC_out> <SLC_par_out> [fcase] [sc] [roff] [nr] [loff] [nl] [swap] [header_lines]
    
    input parameters: 
      SLC_in       (input) SLC (FCOMPLEX or SCOMPLEX format)
      SLC_par_in   (input) ISP SLC parameter file for input SLC
      SLC_out      (output) selected SLC section (FCOMPLEX or SCOMPLEX format)
      SLC_par_out  (output) ISP SLC parameter file of output SLC
      fcase        data format conversion (enter - for default: output format = input format)
                     1: FCOMPLEX --> FCOMPLEX (default sc = 1.0)
                     2: FCOMPLEX --> SCOMPLEX (default sc = 10000.0)
                     3: SCOMPLEX --> FCOMPLEX (default sc = 0.0001)
                     4: SCOMPLEX --> SCOMPLEX (default sc = 1.0)
      sc           scale factor for input SLC data (enter - for default)
      roff         offset to starting range sample (enter - for default: 0)
      nr           number of range samples (enter - for default: to end of line)
      loff         offset to starting line (enter - for default: 0)
      nl           number of lines to copy (enter - for default: to end of file)
      swap         swap data (enter - for default)
                     0: normal (default)
                     1: swap real/imaginary part of complex data
                     2: swap left/right (near/far range)
      header_lines  number of input file header lines (enter - for default: 0)
                    NOTE: CEOS format SLC data have 1 header line
                    NOTE: file offset pointer size (bytes): 8
    
    """

    args = [SLC_in, SLC_par_in, SLC_out, SLC_par_out]
    optargs = [fcase, sc, roff, nr, loff, nl, swap, header_lines]
    cmd = makecmd('SLC_copy', args, optargs) 
    os.system(cmd)

def rascc_mask_thinning(ras_in, in_file, width, ras_out, nmax=None, thresh_1=None, thresh_nmax=None):
    """*** Adaptive sampling reduction for phase unwrapping validity mask ***
    *** Copyright 2013, Gamma Remote Sensing, v1.4 22-Aug-2013 uw/clw ***
    
    usage: rascc_mask_thinning <ras_in> <in_file> <width> <ras_out> [nmax] [thresh_1] ... [thresh_nmax]
    
    input parameters:
      ras_in       (input) validity mask (8-bit SUN raster or BMP format image)
      in_file      (input) file used for adaptive sampling reduction, e.g. coherence (float)
      width        number of samples/row of in_file
      ras_out      (output) validity mask with reduced sampling (8-bit SUN rasterfile or BMP format image)
      nmax         number of sampling reduction runs (default: 3)
      thresh_1     first threshold (used for smallest scale sampling reduction)
      ...          further thresholds
      thresh_nmax  threshold nmax (used for largest scale sampling reduction)
    
    """

    args = [ras_in, in_file, width, ras_out]
    optargs = [nmax, thresh_1, thresh_nmax]
    cmd = makecmd('rascc_mask_thinning', args, optargs) 
    os.system(cmd)

def par_ASF_RSAT_SS(CEOS_leader, CEOS_data, GRD_par, GRD, ):
    """*** ISP parameter file for ASF Radarsat-1 SCANSAR images ***
    *** Copyright 2004, Gamma Remote Sensing, v1.0 27-Aug-2004 clw/uw ***
    
    usage: par_ASF_RSAT_SS <CEOS_leader> <CEOS_data> <GRD_par> <GRD>
    
    input parameters:
      CEOS_leader  (input) CEOS leader file (Radarsat-1 SCANSAR)
      CEOS_data    (input) CEOS data file (Radarsat-1 SCANSAR)
      GRD_par      (output) ISP image parameter file (example <orbit>.mli.par)
      GRD          (output) ISP image (example <orbit>.mli) (enter -  for none, short integer)
    """

    args = [CEOS_leader, CEOS_data, GRD_par, GRD]
    optargs = []
    cmd = makecmd('par_ASF_RSAT_SS', args, optargs) 
    os.system(cmd)

def par_MSP(SAR_par, PROC_par, SLC_MLI_par, image_format=None):
    """*** ISP image parameter file from MSP processing parameter and sensor files ***
    *** Copyright 2010, Gamma Remote Sensing, v3.3 2-Oct-2010 clw/uw ***
    
    usage: par_MSP <SAR_par> <PROC_par> <SLC/MLI_par> [image_format]
    
    input parameters: 
      SAR_par       (input) MSP SAR sensor parameter file
      PROC_par      (input) MSP processing parameter file
      SLC/MLI_par   (output) ISP SLC/MLI image parameter file
      image_format  image format flag (default: from MSP processing parameter file)
                      0: fcomplex (pairs of 4-byte float)
                      1: scomplex (pairs of 2-byte short integer)
                      2: float (4-bytes/value)
    """

    args = [SAR_par, PROC_par, SLC_MLI_par]
    optargs = [image_format]
    cmd = makecmd('par_MSP', args, optargs) 
    os.system(cmd)

def fspf(data_in, data_out, width, type=None, r_max=None, spf_type=None, MLI_par=None):
    """*** ISP Program fspf.c ***
    *** Copyright 2014, Gamma Remote Sensing, v1.2 28-May-2014 of/clw/uw ***
    *** Fast spatial filter for 2D data ***
    
    usage: fspf <data_in> <data_out> <width> [type] [r_max] [spf_type] [MLI_par]
    
    input parameters:
      data_in    (input) input image data
      data_out   (output) spatially filtered image data
      width      number of samples/row
      type       data type (enter - for default):
                    0:fcomplex
                    1:scomplex
                    2:float (default)
      r_max      maximum radius range samples (enter - for default: 64)
      spf_type   spatial filter type (enter - for default):
                   0: uniform average (default for fcomplex and scomplex)
                   1: triangular weighted average: 1 - (r/r_max)
                   2: quadratic weighted average: 1 - (r/r_max)**2
                   3: Gaussian weighted average: exp(-2.*(r**2/r_max**2))
                   4: linear least-squares (default for float data)
      MLI_par    MLI or SLC parameter file with the same number of looks as the data that are being filtered
    """

    args = [data_in, data_out, width]
    optargs = [type, r_max, spf_type, MLI_par]
    cmd = makecmd('fspf', args, optargs) 
    os.system(cmd)

def base_orbit(SLC1_par, SLC2_par, baseline, ):
    """*** Estimate baseline from orbit state vectors ***
    *** Copyright 2011, Gamma Remote Sensing, v4.0 clw 27-Feb-2011 ***
    
    usage: base_orbit <SLC1_par> <SLC2_par> <baseline>
    
    input parameters: 
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      baseline  (output) baseline file (text format, enter - for none)
    
    """

    args = [SLC1_par, SLC2_par, baseline]
    optargs = []
    cmd = makecmd('base_orbit', args, optargs) 
    os.system(cmd)

def res_map(hgt, gr, data, SLC_par, OFF_par, res_hgt, res_data, report_file, nr=None, naz=None, azps_res=None, loff=None, nlines=None):
    """*** Slant range to ground range transformation based on interferometric ground-range ***
    *** Copyright 2008, Gamma Remote Sensing, v2.3 5-Sep-2008 clw/uw ***
    
    usage: res_map <hgt> <gr> <data> <SLC_par> <OFF_par> <res_hgt> <res_data> [nr] [naz] [azps_res] [loff] [nlines] > <report_file>
    
    input parameters: 
      hgt        (input) height file in slant range geometry
      gr         (input) ground range file in slant range geometry
      data       (input) data file in slant range geometry (float) (intensity *.pwr or correlation *.cc)
      SLC_par    (input) ISP parameter file of reference SLC
      OFF_par    (input) offset/interferogram processing parameters
      res_hgt    (output) resampled height file in ground range geometry
      res_data   (output) resampled data file in ground range geometry
      nr         number of range samples for L.S. estimate (default=7, must be odd)
      naz        number of azimuth samples for L.S. extimate (default=7, must be odd)
      azps_res   azimuth output map sample spacing in meters (default=azimuth spacing)
      loff       offset to starting line for height calculations (default=0)
      nlines     number of lines to calculate (default=to end of file)
    
    """

    args = [hgt, gr, data, SLC_par, OFF_par, res_hgt, res_data, report_file]
    optargs = [nr, naz, azps_res, loff, nlines]
    cmd = makecmd('res_map', args, optargs) 
    os.system(cmd)

def az_integrate(data, width, azi, cflg, scale=None, lz=None):
    """*** Calculate azimuth integral of float data (unwrapped phase or azimuth offsets) ***
    *** Copyright 2012, Gamma Remote Sensing, v1.2 6-Feb-2012 ***
    
    usage: az_integrate <data> <width> <azi> <cflg> [scale] [lz]
    
    input parameters:
      data      (input) input data (example: SBI dtrapped phase) (float)
      width     (input) number of range samples/line 
      azi       (output) input data integrated along azimuth (float)
      cflg      integration constant flag:
                   0: set azimuth integral value to 0.0 at specified line
                   1: set average of the azimuth integral to 0.0
      scale     scale factor to apply to the data (enter - for default, default: 1.0)
      lz        line offset where the azimuth integral is set to 0.0 (cflg = 0, enter - for default, default: 0)
    
    """

    args = [data, width, azi, cflg]
    optargs = [scale, lz]
    cmd = makecmd('az_integrate', args, optargs) 
    os.system(cmd)

def radcal_MLI(MLI, MLI_PAR, OFF_par, CMLI, antenna=None, rloss_flag=None, ant_flag=None, refarea_flag=None, sc_dB=None, K_dB=None, pix_area=None):
    """*** Radiometric calibration for multi-look intensity (MLI) data ***
    *** Copyright 2013, Gamma Remote Sensing, v1.9 2-May-2013 uw/clw/of ***
    
    usage: radcal_MLI <MLI> <MLI_PAR> <OFF_par> <CMLI> [antenna] [rloss_flag] [ant_flag] [refarea_flag] [sc_dB] [K_dB] [pix_area]
    
    input parameters: 
      MLI           (input) MLI image (float)
      MLI_PAR       (input) SLC parameter file of input MLI image
      OFF_par       (input) ISP offset/interferogram parameter file (enter - for images in MLI geometry)
      CMLI          (output) radiometrically calibrated output MLI (float)
      antenna       (input) 1-way antenna gain pattern file or - if not provided
      rloss_flag    range spreading loss correction:
                       0: no correction (default)
                       1: apply r^3 correction  (all modes except ASAR APS)
                       2: apply r^4 correction (used only for ASAR APS mode)
                      -1: undo r^3 correction
                      -2: undo r^4 correction)
      ant_flag      antenna pattern correction:
                       0: no correction (default)
                       1: apply antenna pattern correction
                      -1: undo antenna pattern correction)
      refarea_flag  reference pixel area correction:
                       0: no pixel area correction (default)
                       1: calculate sigma0, scale area by sin(inc_ang)/sin(ref_inc_ang)
                       2: calculate gamma0, scale area by sin(inc_ang)/(cos(inc_ang)*sin(ref_inc_ang)
                      -1: undo sigma0 area scaling factor
                      -2: undo gamma0 area scaling factor
      sc_dB         scale factor in dB (default: 0.0)
      K_dB          calibration factor in dB (default: -(value from MLI_PAR))
      pix_area      (output) ellipsoid-based ground range sigma0 or gamma0 pixel reference area (float)
                       refarea_flag 1 or -1: sigma0 ref. area
                       refarea_flag 2 or -2: gamma0 ref. area
    
    """

    args = [MLI, MLI_PAR, OFF_par, CMLI]
    optargs = [antenna, rloss_flag, ant_flag, refarea_flag, sc_dB, K_dB, pix_area]
    cmd = makecmd('radcal_MLI', args, optargs) 
    os.system(cmd)

def mask_data(data_in, width, data_out, mask, format_flag=None):
    """*** Mask data using a Sun or BMP format raster image ***
    *** Copyright 2014, Gamma Remote Sensing, v1.2 23-May-2014 clw ***
    
    usage: mask_data <data_in> <width> <data_out> <mask> [format_flag]
    
    input parameters:
      data_in      (input) data file (FLOAT or FCOMPLEX format)
      width        width of input data file
      data_out     (output) data file, same data format as input
      mask         (input) mask file, Sun raster or BMP format, 8-bits/pixel
                   output data values are set to 0.0 at all locations where the mask is black (0,0,0) or dn = 0
                   NOTE: mask file must have the same width as the input data file
      format_flag  data format:
                     0: FLOAT (default)
                     1: FCOMPLEX
    
    """

    args = [data_in, width, data_out, mask]
    optargs = [format_flag]
    cmd = makecmd('mask_data', args, optargs) 
    os.system(cmd)

def base_ls(SLC_par, OFF_par, gcp_ph, baseline, ph_flag=None, bc_flag=None, bn_flag=None, bcdot_flag=None, bndot_flag=None, bperp_min=None, SLC2R_par=None):
    """*** Least squares baseline estimation using terrain heights ***
    *** Copyright 2005, Gamma Remote Sensing, v2.2 5-Sep-2005 clw/uw ***
    
    usage: base_ls <SLC_par> <OFF_par> <gcp_ph> <baseline> [ph_flag] [bc_flag] [bn_flag] [bcdot_flag] [bndot_flag] [bperp_min] [SLC2R_par]
    
    input parameters:
      SLC_par     (input) ISP parameter file of the reference SLC
      OFF_par     (input) ISP interferogram/offset parameter file
      gcp_ph      (input) ground control point heights + extracted unwrapped phase (text format)
      baseline    (input) baseline parameter file
      ph_flag     restore range phase ramp (default=0: do not restore  1: restore)
      bc_flag     cross-track baseline component estimate (0:orbit derived  1:estimate from data, default=1)
      bn_flag     normal baseline component estimate      (0:orbit derived  1:estimate from data, default=1)
      bcdot_flag  cross-track baseline rate estimate      (0:orbit derived  1:estimate from data, default=1)
      bndot_flag  normal baseline rate estimate           (0:orbit derived  1:estimate from data, default=0)
      bperp_min   minimum perpendicular baseline required for L.S estimation (m, default=  10.0)
      SLC2R_par   (input) parameter file of resampled SLC, required if SLC-2 frequency differs from SLC-1
    
    """

    args = [SLC_par, OFF_par, gcp_ph, baseline]
    optargs = [ph_flag, bc_flag, bn_flag, bcdot_flag, bndot_flag, bperp_min, SLC2R_par]
    cmd = makecmd('base_ls', args, optargs) 
    os.system(cmd)

def unw_model(interf, unw_model, unw, width, xinit=None, yinit=None, ref_ph=None, width_model=None):
    """*** Phase unwrapping using a model of the unwrapped phase ***
    *** Copyright 2008, Gamma Remote Sensing, v1.6 5-Sep-2008 clw/uw ***
    
    usage: unw_model <interf> <unw_model> <unw> <width> [xinit] [yinit] [ref_ph] [width_model]
    
    input parameters:
      interf       (input) complex interferogram
      unw_model    (input) approximate unwrapped phase model (float)
      unw          (output) unwrapped phase (float)
      width        number of samples/row of the interferogram
      xinit        offset to phase reference location in range (col)
      yinit        offset to phase reference location in azimuth (row)
      ref_ph       reference point phase (radians) (enter - for phase at the reference point )
      width_model  number of samples/row of the unwrapped phase model (default: interferogram width)
    
    """

    args = [interf, unw_model, unw, width]
    optargs = [xinit, yinit, ref_ph, width_model]
    cmd = makecmd('unw_model', args, optargs) 
    os.system(cmd)

def par_S1_GRD(GeoTIFF, annotation_XML, calibration_XML, noise_XML, GRD_par, GRD, ):
    """*** Generate SLC parameter and image files for Sentinel-1 GRD data  ***
    *** Copyright 2014, Gamma Remote Sensing, v1.0 14-May-2014 awi      ***
    
    usage: par_S1_GRD <GeoTIFF> <annotation_XML> <calibration_XML> <noise_XML> <GRD_par> <GRD>
    
    input parameters:
      GeoTIFF         (input) image data file in GeoTIFF format (imagery_pp.tif)
      annotation_XML  (input) Sentinel-1 product annotation XML file 
      calibration_XML (input) Sentinel-1 product annotation XML file (- no calibration)
      noise_XML       (input) Sentinel-1 product annotation XML file (- no noise correction)
      GRD_par         (output) ISP SLC parameter file (example: yyyymmdd_pp.slc.par)
      GRD             (output) SLC data file (example: yyyymmdd_pp.slc)
    """

    args = [GeoTIFF, annotation_XML, calibration_XML, noise_XML, GRD_par, GRD]
    optargs = []
    cmd = makecmd('par_S1_GRD', args, optargs) 
    os.system(cmd)

def par_ACS_ERS(CEOS_SAR_leader, SLC_par, ):
    """*** ISP parameter file generation for ERS SLC data from the ACS processor ***
    *** Copyright 2005, Gamma Remote Sensing, v1.3 17-Oct-2005 clw/uw ***
    
    usage: par_ACS_ERS <CEOS_SAR_leader> <SLC_par>
    
    input parameters:
    CEOS_SAR_leader  (input) ERS CEOS SAR leader file
    SLC_par          (output) ISP SLC parameter file (example <orbit>.slc.par)
    
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = []
    cmd = makecmd('par_ACS_ERS', args, optargs) 
    os.system(cmd)

def radcal_SLC(SLC, SLC_PAR, CSLC, CSLC_PAR, fcase=None, antenna=None, rloss_flag=None, ant_flag=None, refarea_flag=None, sc_dB=None, K_dB=None, pix_area=None):
    """*** Radiometric calibration of SLC data ***
    *** Copyright 2013, Gamma Remote Sensing, v2.2 2-May-2013 uw/clw/of ***
    
    usage: radcal_SLC <SLC> <SLC_PAR> <CSLC> <CSLC_PAR> [fcase] [antenna] [rloss_flag] [ant_flag] [refarea_flag] [sc_dB] [K_dB] [pix_area]
    
    input parameters: 
      SLC           (input) SLC (fcomplex or scomplex)
      SLC_PAR       (input) SLC parameter file of input SLC
      CSLC          (output) radiometrically calibrated SLC (fcomplex or scomplex)
      CSLC_PAR      (output) SLC parameter file of output calibrated SLC
      fcase         format case (default = 1)
                      1: fcomplex --> fcomplex (pairs of float)
                      2: fcomplex --> scomplex (pairs of short integer)
                      3: scomplex --> fcomplex
                      4: scomplex --> scomplex
      antenna       1-way antenna gain pattern file or - (if not provided)
      rloss_flag    range spreading loss correction:
                      0: no correction (default)
                      1: apply r^3 correction  (all modes except ASAR APS)
                      2: apply r^4 correction (used only for ASAR APS mode)
                     -1: undo r^3 correction
                     -2: undo r^4 correction)
      ant_flag      antenna pattern correction:
                      0: no correction (default)
                      1: apply antenna pattern correction
                     -1: undo antenna pattern correction)
      refarea_flag  reference pixel area correction:
                      0: no pixel area correction (default)
                      1: calculate sigma0, scale area by sin(inc_ang)/sin(ref_inc_ang)
                      2: calculate gamma0, scale area by sin(inc_ang)/(cos(inc_ang)*sin(ref_inc_ang)
                     -1: undo sigma0 area scaling factor
                     -2: undo gamma0 area scaling factor
      sc_dB         scale factor in dB (default: 0.0)
      K_dB          calibration factor in dB (default: -(value from SLC_PAR) )
      pix_area      (output) ellipsoid-based ground range sigma0 or gamma0 pixel reference area (float)
                      refarea_flag 1 or -1: sigma0 ref. area
                      refarea_flag 2 or -2: gamma0 ref. area
    
    """

    args = [SLC, SLC_PAR, CSLC, CSLC_PAR]
    optargs = [fcase, antenna, rloss_flag, ant_flag, refarea_flag, sc_dB, K_dB, pix_area]
    cmd = makecmd('radcal_SLC', args, optargs) 
    os.system(cmd)

def corr_flag(corr, flag, width, corr_thr, xmin=None, xmax=None, ymin=None, ymax=None, border=None):
    """*** Low correlation region detection for phase unwrapping ***
    *** Copyright 2005, Gamma Remote Sensing, v2.4 1-Mar-2005 clw/uw ***
    
    usage: corr_flag <corr> <flag> <width> <corr_thr> [xmin] [xmax] [ymin] [ymax] [border]
    
    input parameters:
      corr      (input)interferometric correlation file
      flag      (input/output) phase unwrapping flag filename 
      width     number of samples/row
      corr_thr  corrrelation threshold (0 --> 1.0)
      xmin      starting range pixel offset (default = 0)
      xmax      last range pixel offset (default = width-1)
      ymin      starting azimuth row offset, relative to start (default = 0)
      ymax      last azimuth row offset, relative to start (default = nlines-1)
      border    effective range of low coherence pixels to set low coherence flag (default=2)
    
    """

    args = [corr, flag, width, corr_thr]
    optargs = [xmin, xmax, ymin, ymax, border]
    cmd = makecmd('corr_flag', args, optargs) 
    os.system(cmd)

def par_ASF_PRI(CEOS_leader, CEOS_data, GRD_par, GRD, ):
    """*** ISP parameter file for ASF detected ground range images (L1) Sep 1996 --> present ***
    *** Copyright 2014, Gamma Remote Sensing, v1.3 3-Apr-2014 clw/uw ***
    
    usage: par_ASF_PRI <CEOS_leader> <CEOS_data> <GRD_par> <GRD>
    
    input parameters:
      CEOS_leader  (input) CEOS leader file
      CEOS_data    (input) CEOS data file binary)
      GRD_par      (output) ISP ground range image parameter file
      GRD          (output) ISP ground range image (enter -  for none, float intensity)
    
      NOTE: The input data converted to intensity using the expression:  (dn/1000.)**2
    """

    args = [CEOS_leader, CEOS_data, GRD_par, GRD]
    optargs = []
    cmd = makecmd('par_ASF_PRI', args, optargs) 
    os.system(cmd)

def gcp_phase(unw, OFF_par, gcp, gcp_ph, win_sz=None):
    """*** Extract unwrapped phase at GCP locations ***
    *** Copyright 2006, Gamma Remote Sensing, v1.5 8-Mar-2006 clw ***
    
    usage: gcp_phase <unw> <OFF_par> <gcp> <gcp_ph> [win_sz]
    
    input parameters:
      unw      (input) unwrapped interferometric phase
      OFF_par  (input) ISP interferogram/offset parameter file
      gcp      (input) ground control point data (text format)
      gcp_ph   (output) ground control point data + extracted unwrapped phase (text)
      win_sz   window size for averaging phase for each GCP, must be odd (default: 1)
    
    """

    args = [unw, OFF_par, gcp, gcp_ph]
    optargs = [win_sz]
    cmd = makecmd('gcp_phase', args, optargs) 
    os.system(cmd)

def tree_gzw(flag, width, mbl=None, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Phase unwrapping tree generation (GZW algorithm) ***
    *** Copyright 2008, Gamma Remote Sensing, v3.6 5-Sep-2008 clw/uw ***
    
    usage: tree_gzw <flag> <width> [mbl] [xmin] [xmax>] [ymin] [ymax]
    
    input parameters: 
      flag   (input) phase unwrapping flag file
      width  number of samples/row
      mbl    maximum branch length (default=32)
      xmin   starting range pixel offset (default = 0)
      xmax   last range pixel offset (default = width-1)
      ymin   starting azimuth row, relative to start (default = 0)
      ymax   last azimuth row, relative to start (default = nlines-1)
    
    """

    args = [flag, width]
    optargs = [mbl, xmin, xmax, ymin, ymax]
    cmd = makecmd('tree_gzw', args, optargs) 
    os.system(cmd)

def DELFT_vec2(SLC_par, DELFT_dir, nstate=None, interval=None, ODR=None):
    """*** Extract and interpolate DELFT ERS-1, ERS-2, and ENVISAT state vectors ***
    *** Copyright 2012, Gamma Remote Sensing, v2.6 clw 24-Oct-2012 ***
    
    usage: DELFT_vec2 <SLC_par> <DELFT_dir> [nstate] [interval] [ODR]
    
    input parameters:
      SLC_par    (input) ISP image parameter file
      DELFT_dir  directory containing Delft orbit arclist and ODR files for ERS-1, ERS-2 or ENVISAT
                   NOTE: enter . for current directory
      nstate     number of state vectors to generate (enter - for default (>= 15)
      interval   time interval between state vectors in the ISP image parameter file (s) (default: 10.0)
      ODR        ODR file to use (include path) rather than ODR file determined from the Delft orbit arclist
    
    """

    args = [SLC_par, DELFT_dir]
    optargs = [nstate, interval, ODR]
    cmd = makecmd('DELFT_vec2', args, optargs) 
    os.system(cmd)

def dcomp_sirc(infile, outfile, samples, loff=None, nlines=None):
    """*** Extract SIR-C SLC compressed single-pol data ***
    *** Copyright 2009, Gamma Remote Sensing, v1.4 16-Oct-2009 clw ***
    
    usage: dcomp_sirc <infile> <outfile> <samples> [loff] [nlines]
    
    input parameters: 
      infile     (input) SIR-C single-pol SLC compressed data
      outfile    (output) complex floating point data
      samples    number of polarimetric samples per input line (4 bytes/sample)
      loff       offset to starting line (default: 0)
      nlines     number of lines to copy(default: entire file, 0 = entire file)
    """

    args = [infile, outfile, samples]
    optargs = [loff, nlines]
    cmd = makecmd('dcomp_sirc', args, optargs) 
    os.system(cmd)

def base_perp(baseline, SLC1_par, OFF_par, time_rev=None):
    """*** Calculate baseline components perpendicular and parallel to look vector ***
    *** Copyright 2005, Gamma Remote Sensing, v3.5 10-May-2005 clw/uw ***
    
    usage: base_perp <baseline> <SLC1_par> <OFF_par> [time_rev]
    
    input parameters: 
      baseline  (input) baseline file (text)
      SLC1_par  (input) ISP parameter file of SLC-1 (reference SLC)
      OFF_par   (input) ISP interferogram/offset parameter file
      time_rev  SLC image normal=1 (default), image time-reversed = -1
    
    """

    args = [baseline, SLC1_par, OFF_par]
    optargs = [time_rev]
    cmd = makecmd('base_perp', args, optargs) 
    os.system(cmd)

def residue(int, flag, width, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Determine interferometric phase unwrapping residues ***
    *** Copyright 2014, Gamma Remote Sensing, v2.6 14-Jan-2014 clw/uw ***
    
    usage: residue <int> <flag> <width> [xmin] [xmax] [ymin] [ymax]
    
    input parameters: 
      int    (input) interferogram (fcomplex)
      flag   (input) flag file (unsigned char)
      width  number of samples/row
      xmin   offset to starting range pixel(default = 0)
      xmax   offset last range pixel (default = width-1)
      ymin   offset to starting azimuth row (default = 0)
      ymax   offset to last azimuth row (default = nlines-1)
    
    """

    args = [int, flag, width]
    optargs = [xmin, xmax, ymin, ymax]
    cmd = makecmd('residue', args, optargs) 
    os.system(cmd)

def SLC_interp_map(SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_2R, SLC2R_par, OFF_par2, coffs_sm, loff=None, nlines=None):
    """*** SLC image resampling using a 2-D offset map ***
    *** Copyright 2008, Gamma Remote Sensing, v3.5 14-Feb-2008 clw/uw ***
    usage: SLC_interp_map <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <SLC-2R> <SLC2R_par> <OFF_par2> <coffs_sm> [loff] [nlines]
    
    input parameters:
      SLC-2      (input) SLC-2 image to be resampled to the reference SLC-1 reference image
      SLC1_par   (input) SLC-1 ISP image parameter file
      SLC2_par   (input) SLC-2 ISP image parameter file
      OFF_par    (input) ISP offset/interferogram parameter file
      SLC-2R     (output) single-look complex image 2 coregistered to SLC-1
      SLC2R_par  (output) SLC-2R ISP image parameter file for co-registered image
      OFF_par2   (input) ISP offset/interferogram parameter file used for residual offsets map (coffs2_sm)
      coffs2_sm  (input) smoothed residual range and azimuth offsets (fcomplex)
      loff       offset to first valid output line (in SLC-1 lines) (default=0)
      nlines     number of valid output lines (default=0: to end of file)
    
    """

    args = [SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_2R, SLC2R_par, OFF_par2, coffs_sm]
    optargs = [loff, nlines]
    cmd = makecmd('SLC_interp_map', args, optargs) 
    os.system(cmd)

def par_TX_GRD(annotation_XML, COSAR, GRD_par, GRD, pol=None):
    """*** Generate SLC parameter and image files for Terrasar-X MGD data  ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 25-Sep-2012 awi/clw  ***
    
    usage: par_TX_GRD <annotation_XML> <COSAR> <GRD_par> <GRD> [pol]
    
    input parameters:
      annotation_XML (input) Terrasar-X product annotation XML file
      GEOTIFF        (input) image data file in geotiff format
                     NOTE: make sure the dataset contains the selected polarisation)
      GRD_par        ISP ground range image parameter file (example: yyyymmdd.grd.par
      GRD            (output) calibrated ground range data file (example: yyyymmdd.grd)
      pol            polarisation HH, HV, VH, VV (default: first polarisation found in the annotation_XML)
    
    """

    args = [annotation_XML, COSAR, GRD_par, GRD]
    optargs = [pol]
    cmd = makecmd('par_TX_GRD', args, optargs) 
    os.system(cmd)

def DORIS_vec(SLC_PAR, DOR, nstate=None):
    """*** Extract ENVISAT DORIS state vectors and write to an ISP image parameter file ***
    *** Copyright 2008, Gamma Remote Sensing, v1.4 11-Jun-2008 clw ***
    
    usage: DORIS_vec <SLC_PAR> <DOR> [nstate]
    
    input parameters:
      SLC_par  (input/output)ISP SLC/MLI image parameter file
      DOR      (input) ASAR DORIS data file (DOR_VOR_AXVF)
      nstate   number of state vectors to extract (enter - for default: 11)
    """

    args = [SLC_PAR, DOR]
    optargs = [nstate]
    cmd = makecmd('DORIS_vec', args, optargs) 
    os.system(cmd)

def par_S1_SLC(GeoTIFF, annotation_XML, calibration_XML, noise_XML, SLC_par, SLC, TOPS_par=None):
    """*** Generate SLC parameter and image files for Sentinel-1 SLC data  ***
    *** Copyright 2014, Gamma Remote Sensing, v1.4 1-Jul-2014 awi/clw  ***
    
    usage: par_S1_SLC <GeoTIFF> <annotation_XML> <calibration_XML> <noise_XML> <SLC_par> <SLC> [TOPS_par]
    
    input parameters:
      GeoTIFF         (input) image data file in GeoTIFF format (imagery_pp.tif)
      annotation_XML  (input) Sentinel-1 product annotation XML file 
      calibration_XML (input) Sentinel-1 product annotation XML file (- no calibration)
      noise_XML       (input) Sentinel-1 product annotation XML file (- no noise correction)
      SLC_par         (output) ISP SLC parameter file (example: yyyymmdd_pp.slc.par)
      SLC             (output) SLC data file (example: yyyymmdd_pp.slc)
      TOPS_par        (output) SLC burst annotation file for scansar data (example: yyyymmdd_pp.txt)
    """

    args = [GeoTIFF, annotation_XML, calibration_XML, noise_XML, SLC_par, SLC]
    optargs = [TOPS_par]
    cmd = makecmd('par_S1_SLC', args, optargs) 
    os.system(cmd)

def RSAT2_vec(SLC_par, RSAT2_orb, nstate=None):
    """*** Extract Radarsat-2 state vectors from a definitive orbit file ***
    *** Copyright 2010, Gamma Remote Sensing, v1.0 clw 13-May-2010 ***
    
    usage: RSAT2_vec <SLC_par> <RSAT2_orb> [nstate]
    
    input parameters:
      SLC_par    (input) ISP image parameter file
      RSAT2_orb  Radarsat-2 definitive orbit data file available from MDA. (orbit_number_def.orb)
      nstate     number of state vectors to extract (enter - for default: 9)
    """

    args = [SLC_par, RSAT2_orb]
    optargs = [nstate]
    cmd = makecmd('RSAT2_vec', args, optargs) 
    os.system(cmd)

def ASAR_XCA(ASA_XCA, antenna, swath=None, pol=None):
    """*** Interpretation of ASAR external calibration data file (ASA_XCA) ***
    *** Copyright 2006, Gamma Remote Sensing, v1.1 7-June-2006 awi/uw/clw ***
    
    usage: ASAR_XCA <ASA_XCA> <antenna> [swath] [pol]
    
    input parameters: 
      ASA_XCA        (input) ASAR external calibration data file (binary)
      antenna        (output) 1-way antenna gain pattern file or '-' (if not provided)
                     or 'all' to generate all ASAR antenna diagrams
      swath          ASAR swath (IS1,IS2,...IS7;SS1,SS2,...SS5)
      pol            polarization (HH,VV,HV,VH)
    
    """

    args = [ASA_XCA, antenna]
    optargs = [swath, pol]
    cmd = makecmd('ASAR_XCA', args, optargs) 
    os.system(cmd)

def par_RSAT_SGF(CEOS_leader, CEOS_data, GRD_par, GRD, sc_dB=None, dt=None):
    """*** ISP parameter file for RSI/Atlantis Radarsat SGF (ground range) and SCANSAR SCW16 data ***
    *** Copyright 2012, Gamma Remote Sensing, v2.2 14-Feb-2012 clw ***
    
    usage: par_RSAT_SGF <CEOS_leader> <CEOS_data> <GRD_par> <GRD> [sc_dB] [dt]
    
    input parameters:
      CEOS_leader (input) CEOS leader file (RSI SGF or SCW16 products, LEA_01.001)
      CEOS_data   (input) CEOS data file (RSI SGF or SCW16 products, DAT_01.001) 
      GRD_par     (output) ISP ground range image parameter file (example <orbit>.mli.par)
      GRD         (output) ISP ground range image (example <orbit>.grd.par) (enter -  for none, float)
      sc_dB       intensity scale factor in dB (enter - for default:   0.00)
      dt          azimuth image time offset (s) (enter - for default = 0.0)
    
    """

    args = [CEOS_leader, CEOS_data, GRD_par, GRD]
    optargs = [sc_dB, dt]
    cmd = makecmd('par_RSAT_SGF', args, optargs) 
    os.system(cmd)

def cc_wave(interf, pwr1, pwr2, corr, width, bx=None, by=None, wflg=None, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Estimate interferometric coherence ***
    *** Copyright 2013, Gamma Remote Sensing, v5.7 14-Oct-2013 clw/uw ***
    
    usage: cc_wave <interf> <pwr1> <pwr2> <corr> <width> <bx> <by> <wflg> <xmin> <xmax> <ymin> <ymax>
    
    input parameters: 
      interf  (input) complex interferogram
      pwr1    (input) intensity image of the first scene (or -)
      pwr2    (input) intensity image of the second scene (or -)
      corr    (output) estimated degree of coherence filename
      width   number of samples/row
      bx      coherence window size (columns) (default = 5.0)
      by      coherence window size (rows) (default = 5.0)
      wflg    magnitude weighting function:
                0: constant (default)
                1: triangular
                2: gaussian
                3: none (phase only)
      xmin    starting range pixel offset (default = 0)
      xmax    last range pixel offset (default = width-1)
      ymin    starting azimuth row offset, relative to start (default = 0)
      ymax    last azimuth row offset, relative to start (default = nlines-1)
    
      NOTE: omitting pwr1 and pwr2 or setting wflg = 3 selects a coherence estimate algorithm that only
            uses the complex interferogram values. In the case of wflg = 3, only the interferogram phase is used.
    """

    args = [interf, pwr1, pwr2, corr, width]
    optargs = [bx, by, wflg, xmin, xmax, ymin, ymax]
    cmd = makecmd('cc_wave', args, optargs) 
    os.system(cmd)

def ORRM_vec(SLC_par, ORRM, nstate=None):
    """*** Calculate state vectors extraction from ORRM file ***
    *** Copyright 2008, Gamma Remote Sensing, v1.4 15-Nov-2004 clw ***
    
    usage: ORRM_vec <SLC_par> <ORRM> [nstate]
    
    input parameters: 
      SLC_par  (input/output) ISP SLC/MLI image parameter file
      ORRM     (input) ORRM state vector file
      nstate   number of state vectors (default=5, maximum=64)
    
    """

    args = [SLC_par, ORRM]
    optargs = [nstate]
    cmd = makecmd('ORRM_vec', args, optargs) 
    os.system(cmd)

def radcal_PRI(PRI, PRI_PAR, GRD, GRD_PAR, K_dB=None, inc_ref=None, roff=None, nr=None, loff=None, nl=None):
    """*** Convert ESA processed short integer format PRI to radiometrically calibrated GRD image (float) ***
    *** Copyright 2009, Gamma Remote Sensing, v1.4 4-Feb-2009 uw/clw ***
    
    usage: radcal_PRI <PRI> <PRI_PAR> <GRD> <GRD_PAR> [K_dB] [inc_ref] [roff] [nr] [loff] [nl]
    
    input parameters: 
      PRI       (input) PRI ground-range image (short integer, sqrt(backscat. intensity)
      PRI_PAR   (input) SLC parameter file of input PRI ground-range image (yyyymmdd.pri.par)
      GRD       (output) calibrated ground-range image (float, backscat. intensity)
      GRD_PAR   (output) ISP image parameter file of output calibrated ground-range image (yyyymmdd.grd.par)
      K_dB      calibration factor in decibels (default: 59.75 dB)
                ERS1 (D-Paf,ESRIN): 58.24 dB, ERS2 (D-Paf,ESRIN,I-Paf,UK-Paf after 1997): 59.75 dB
                ENVISAT ASAR: 55.0 dB (all modes)
                details see product specifications and ESA publications.
      inc_ref   reference incidence angle in deg. (default: 23.0 deg.)
                ENVISAT ASAR: 90.0 deg. (all modes)
      roff      offset to starting range sample (default=0)
      nr        number of range samples (default: 0, to end of line)
      loff      offset to starting line (default: 0, 1 header line in the input file is assumed for ERS)
      nl        number of lines to copy (default: 0, to end of file)
    
    """

    args = [PRI, PRI_PAR, GRD, GRD_PAR]
    optargs = [K_dB, inc_ref, roff, nr, loff, nl]
    cmd = makecmd('radcal_PRI', args, optargs) 
    os.system(cmd)

def base_init(SLC1_par, SLC2_par, OFF_par, interf, base, mflag=None, nrfft=None, nazfft=None, r_samp=None, az_line=None):
    """*** Estimate initial baseline using orbit state vectors, offsets, and interferogram phase ***
    *** Copyright 2012, Gamma Remote Sensing, v2.3 clw/uw 29-Mar-2012 ***
    
    usage: base_init <SLC1_par> <SLC2_par> <OFF_par> <interf> <base> [mflag] [nrfft] [nazfft] [r_samp] [az_line]
    
    input parameters:
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file (enter - for none)
      interf    (input) unflattened interferogram (enter - for none)
      baseline  (output) baseline parameter file
      mflag     baseline estimation method flag (enter - for default)
                mflag	 b_para    b_perp    input
                  0:	 orbits    orbits    p1,p2  (default)
                  1:	 offsets   offsets   p1,p2,off
                  2:	 orbits    fft       p1,p2,off,int
                  3:	 offsets   fft       p1,p2,off,int
                  4:	 fft	   fft       p1,off,int   
      nrfft     size of range FFT   (512, 1024,...) (enter - for default, default: 1024)
      nazfft    size of azimuth FFT (512, 1024,...) (enter - for default, default: 1024)
      r_samp    range pixel offset to center of the FFT window (enter - for default, default: range center)
      az_line   line offset from start of the interf. for the  FFT window (enter - for default, default: azimuth center)
    
      NOTE: Not all  input data files are required for the different methods
    
            enter - for files that are not provided
    
    """

    args = [SLC1_par, SLC2_par, OFF_par, interf, base]
    optargs = [mflag, nrfft, nazfft, r_samp, az_line]
    cmd = makecmd('base_init', args, optargs) 
    os.system(cmd)

def par_SIRC(CEOS_leader, SLC_par, UTC_MET=None):
    """*** ISP SLC parameter file from SIR-C CEOS leader file ***
    *** Copyright 2009, Gamma Remote Sensing, v2.5 30-Oct-2009 clw/uw ***
    
    usage: par_SIRC <CEOS_leader> <SLC_par> [UTC/MET]
    
    input parameters:
      CEOS_leader   (input) JPL SIR-C CEOS leader file
      SLC_par       (output) ISP SLC parameter file
      UTC/MET       time reference for state vectors:
                    MET (Mission Elapsed Time) or UTC (default=UTC)
    """

    args = [CEOS_leader, SLC_par]
    optargs = [UTC_MET]
    cmd = makecmd('par_SIRC', args, optargs) 
    os.system(cmd)

def SLC_corners(SLC_par, terra_alt=None):
    """*** Calculate SLC/MLI image corners in geodetic latitude and longitude (deg.) ***
    *** Copyright 2010, Gamma Remote Sensing, v1.5 5-Oct-2010 clw ***
    
    usage: SLC_corners <SLC_par> [terra_alt]
    
    input parameters:
      SLC_par   (input) ISP SLC/MLI image parameter file
      terra_alt (input) average terrain altitude (default: 300.000 meters)
    """

    args = [SLC_par]
    optargs = [terra_alt]
    cmd = makecmd('SLC_corners', args, optargs) 
    os.system(cmd)

def subtract_phase(interf_in, phase_file, interf_out, width, factor=None):
    """*** Land Application Tools: Program subtract_phase.c ***
    *** Copyright 2001, Gamma Remote Sensing, v3.1 23-Jan-2001 uw/clw ***
    
    *** subtract scaled phase image from a complex interferogram ***
    
    usage: subtract_phase <interf_in> <phase_file> <interf_out> <width> [factor]
    
    input parameters: 
      interf_in   (input) input interferogram (fcomplex format)
      phase_file  (input) unwrapped interferometric phase (float)
      interf_out  (output) output interferogram (input interferogram - scaled phase) (fcomplex)
      width       number of samples/line
      factor      constant scale factor for input phase data [default=1.0]
    
    """

    args = [interf_in, phase_file, interf_out, width]
    optargs = [factor]
    cmd = makecmd('subtract_phase', args, optargs) 
    os.system(cmd)

def bridge(int, flag, unw, bridge, width, xmin=None, xmax=None, ymin=None, ymax=None):
    """*** Phase unwrap new regions with bridges to regions already unwrapped ***
    *** Copyright 2010, Gamma Remote Sensing, v1.5 clw 4-Nov-2010 ***
    
    usage: bridge <int> <flag> <unw> <bridge> <width> [xmin] [xmax] [ymin] [ymax]
    
    input parameters:
      int     (input) interferogram (fcomplex)
      flag    (input) unwrapping flag file
      unw     (input/output) unwrapped phase (float) 
      bridge  (input) bridge data file (text format)
      width   number of samples/row
      xmin    starting range pixel offset to unwrap (default = 0)
      xmax    last range pixel offset to unwrap (default=width-1)
      ymin    starting azimuth row offset to unwrap, relative to start (default = 0)
      ymax    last azimuth row offset to unwrap, relative to start (default = nlines-1)
    
    """

    args = [int, flag, unw, bridge, width]
    optargs = [xmin, xmax, ymin, ymax]
    cmd = makecmd('bridge', args, optargs) 
    os.system(cmd)

def SLC_intf(SLC_1, SLC_2R, OFF_par, interf, rlks, azlks, loff=None, nlines=None, sps_flg=None, azf_flg=None, rp1_flg=None, rp2_flg=None, SLC_1s=None, SLC_2Rs=None, SLC1s_par=None, SLC2Rs_par=None):
    """*** Interferogram generation from co-registered SLC data ***
    *** Copyright 2012, Gamma Remote Sensing, v4.4 clw/uw 10-Apr-2012 ***
    
    usage: SLC_intf <SLC-1> <SLC-2R> <SLC1_par> <SLC2R_par> <OFF_par> <interf> <rlks> <azlks> [loff] [nlines] [sps_flg] [azf_flg] [rp1_flg] [rp2_flg] [SLC-1s] [SLC-2Rs][SLC-1s_par] SLC-2Rs_par]
    
    input parameters:
      SLC-1      (input) single-look complex image 1 (reference)
      SLC-2R     (input) single-look complex image 2 coregistered to SLC-1
      OFF_par    (input) ISP offset/interferogram parameter file
      interf     (output) interferogram from SLC-1 and SLC-2R
      rlks       number of range looks
      azlks      number of azimuth looks
      loff       offset to starting line relative to SLC-1 for interferogram (default=0)
      nlines     number of SLC lines to process (enter - for default: to end of file)
      sps_flg    range spectral shift flag:
                   1: apply spectral shift filter (default)
                   0: do not apply spectral shift filter
      azf_flg    azimuth common band filter flag:
                   1: apply azimuth common-band filter (default)
                   0: do not apply azimuth common band filter
      rp1_flg    SLC-1 range phase mode
                   0: nearest approach (zero-Doppler) phase
                   1: ref. function center (Doppler centroid) (default)
      rp2_flg    SLC-2 range phase mode
                   0: nearest approach (zero-Doppler) phase
                   1: ref. function center (Doppler centroid) (default)
      SLC-1s     SLC-1 after range spectral shift and azimuth common-band filtering (FCOMPLEX format)
      SLC-2Rs    SLC-2R after range spectral shift and azimuth common-band filtering (FCOMPLEX format)
      SLC1s_par  SLC-1s ISP image parameter file
      SLC2Rs_par SLC-2Rs ISP image parameter file
    
    """
    
    SLC1_par = SLC_1+'.par'
    SLC2R_par = SLC_2R+'.par'

    args = [SLC_1, SLC_2R, SLC1_par, SLC2R_par, OFF_par,interf, rlks, azlks]
    optargs = [loff, nlines, sps_flg, azf_flg, rp1_flg, rp2_flg, SLC_1s, SLC_2Rs, SLC1s_par, SLC2Rs_par]
    cmd = makecmd('SLC_intf', args, optargs) 
    os.system(cmd)

def slant_range(SLC_par, slr, ):
    """*** Calculate slant range for every range sample ***
    *** Copyright 2013, Gamma Remote Sensing v1.1 28-Aug-2013 ***
    
    usage: slant_range <SLC_par> <slr>
    
    input parameters: 
      SLC_par  (input) SLC or MLI image parameter file
      slr      (output) slant range for every sample in the image (float)
    
    """

    args = [SLC_par, slr]
    optargs = []
    cmd = makecmd('slant_range', args, optargs) 
    os.system(cmd)

def par_ASF_96(CEOS_SAR_leader, SLC_par, ):
    """*** ISP parameter file for ASF data 1996-->present v1.1 ***
    *** Copyright 2003, Gamma Remote Sensing, v1.4 4-Aug-2003 clw/uw ***
    
    usage: par_ASF_96 <CEOS_SAR_leader> <SLC_par>
    
    input parameters:
    CEOS_SAR_leader  (input) CEOS SAR leader file
    SLC_par          (output) ISP SLC parameter file (example <orbit>.slc.par)
    """

    args = [CEOS_SAR_leader, SLC_par]
    optargs = []
    cmd = makecmd('par_ASF_96', args, optargs) 
    os.system(cmd)

def SLC_cat(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_3, SLC3_par, dopflg=None, iflg=None, phflg=None):
    """*** Concatenate two SLC images using 2-D SINC interpolation ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 15-Nov-2012 clw ***
    
    usage: SLC_cat <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <SLC-3> <SLC3_par> [dopflg] [iflg] [phflg]
    
    input parameters:
      SLC-1      (input) SLC-1 image
      SLC-2      (input) SLC-2 image to be appended to SLC-1
      SLC1_par   (input) SLC-1 ISP image parameter file
      SLC2_par   (input) SLC-2 ISP image parameter file
      OFF_par    (input) ISP offset parameter file containing offset polynomials between SLC-1 and SLC-2
      SLC-3      (output) concatenated SLC
      SLC3_par   (output) ISP image parameter file for concatenated image
      dopflg     Doppler flag:
                   0: ignore Doppler centroid information, assume 0 Doppler centroid
                   1: use Doppler centroid information for interpolation (default)
      iflg       input data type flag: 
                   0: input data are SLC images, use data type specified in SLC_par files (SCOMPLEX or FCOMPLEX) (default)
                   1: input scenes are interferograms, force FCOMPLEX data type
      phflg      phase offset correction flag:
                   0: no phase offset correction for SLC-2
                   1: apply phase offset correction to SLC-2 (default)
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_3, SLC3_par]
    optargs = [dopflg, iflg, phflg]
    cmd = makecmd('SLC_cat', args, optargs) 
    os.system(cmd)

def mcf(interf, wgt, mask, unw, width, tri_mode=0, roff=0, loff=0, nr='-', nlines='-', npat_r=1, npat_az=1, ovrlap=128, r_init='-', az_init='-', init_flag=0):
    """*** Phase unwrapping using Minimum Cost Flow (MCF) and triangulation ***
    *** Copyright 2013, Gamma Remote Sensing, v1.9 clw/uw 17-May-2013 ***
    
    usage: mcf <interf> <wgt> <mask> <unw> <width> [tri_mode] [roff] [loff] [nr] [nlines] [npat_r] [npat_az] [ovrlap] [r_init] [az_init] [init_flag]
    
    input parameters:
      interf     (input) interferogram (*.int,*.flt)(fcomplex)
      wgt        (input) weight factors (0 -> 1.0) file (float)(enter - for uniform weight)
      mask       (input) validity mask (SUN raster or BMP format, value 0 -> pixel not used)(enter - if no mask)
      unw        (output) unwrapped phase image (*.unw)(float)
      width      number of samples/row
      tri_mode   triangulation mode
                   0: filled triangular mesh (default)
                   1: Delaunay triangulation
      roff       offset to starting range of section to unwrap (default: 0)
      loff       offset to starting line of section to unwrap (default: 0)
      nr         number of range samples of section to unwrap (default(-): width - roff)
      nlines     number of lines of section to unwrap (default(-): total number of lines - loff)
      npat_r     number of patches in range
      npat_az    number of patches in azimuth
      ovrlap     overlap between patches in pixels (overlap >= 7, default(-): 512)
      r_init     phase reference point range offset (default(-): roff)
      az_init    phase reference point azimuth offset (default(-): loff)
      init_flag  flag to set phase at reference point
                   0: use initial point phase value (default)
                   1: set phase to 0.0 at initial point
    
    """

    args = [interf, wgt, mask, unw, width]
    optargs = [tri_mode, roff, loff, nr, nlines, npat_r, npat_az, ovrlap, r_init, az_init, init_flag]
    cmd = makecmd('mcf', args, optargs) 
    os.system(cmd)

def par_UAVSAR_SLC(ann, SLC_MLI_par, image_type, image_format, ):
    """*** ISP image parameter file from UAVSAR annotation file (ann) for SLC and MLC products ***
    *** Copyright 2012, Gamma Remote Sensing, v1.2 4-Nov-2012 clw***
    
    usage: par_UAVSAR_SLC <ann> <SLC/MLI_par> <image_type> <image_format>
    
    input parameters: 
      ann           (input) UAVSAR annotation file (*ann.txt)
      SLC/MLI_par   (output) ISP image parameter file
      image_type    image type flag
                      0: SLC (slc) in slant range coordinates
                      1: MLC (mlc) in slant range coordinates
                         HHHH*, VVVV*, HVHV* are FLOAT format
                         HHHV*, HHVV*, HVVV* are FCOMPLEX format
      image_format  image data format flag
                      0: FCOMPLEX (pairs of 4-byte float (re,im))
                      2: FLOAT  (4-bytes/value)
    """

    args = [ann, SLC_MLI_par, image_type, image_format]
    optargs = []
    cmd = makecmd('par_UAVSAR_SLC', args, optargs) 
    os.system(cmd)

def GRD_to_SR(GRD_par, SLC_par, OFF_par, in_file, out_file, rlks=None, azlks=None, interp_mode=None, sr_rsp=None, sr_azsp=None):
    """*** Conversion to slant range for ISP MLI and INSAR ground range data of type float ***
    *** Copyright 2014, Gamma Remote Sensing, v2.0 4-Apr-2014 uw/clw ***
    
    usage: GRD_to_SR <GRD_par> <SLC_par> <OFF_par> <in_file> <out_file> [rlks] [azlks] [interp_mode] [sr_rsp] [sr_azsp]
    
    input parameters: 
      GRD_par      (input) SLC parameter file of output ground range image
      MLI_par      (output) MLI ISP image parameter file for slant range image (float)
      OFF_par      (input) ISP offset/interferogram parameter file of input image (enter - image in MLI geometry)
      in_file      (input) ground range image (float)
      out_file     (output) slant range image (float)
      rlks         multi-looking in range (prior to resampling, default=1)
      azlks        multi-looking in azimuth (prior to resampling, default=1)
      interp_mode  interpolation mode
                     0: nearest neighbor (default)
                     1: spline
                     2: spline log
      sr_rsp      output image slant range sample spacing (m) (default = c/(2*adc_sampling_rate)
      sr_azsp     output image azimunt sample spacing (m) (default = (input image azimuth spacing) * azlks)
    
    """

    args = [GRD_par, SLC_par, OFF_par, in_file, out_file]
    optargs = [rlks, azlks, interp_mode, sr_rsp, sr_azsp]
    cmd = makecmd('GRD_to_SR', args, optargs) 
    os.system(cmd)

def interf_SLC(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, MLI_1, MLI_2, interf, nrlk=None, nazlk=None, loff=None, nltot=None, rfilt=None, azfilt=None, s_off=None):
    """*** Interferogram generation using a pair of SLC images ***
    *** Copyright 2009, Gamma Remote Sensing, v4.10 clw/uw 27-Oct-2009 ***
    
    usage: interf_SLC <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <MLI-1> <MLI-2> <interf> [nrlk] [nazlk] [loff] [nltot] [rfilt] [azfilt] [s_off]
    
    input parameters:
      SLC-1     (input) single-look complex image 1 (reference)
      SLC-2     (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_pa   (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      MLI-1     (output) multi-look intensity image 1
      MLI-2     (output) multi-look intensity image 2
      interf    interferogram from SLC-1 and SLC-2
      nrlk      number of interferogram range looks (default: 2)
      nazlk     number of interferogram azimuth looks (default: 10)
      loff      offset to starting line of interferogram (relative to start of SLC-1) (default: 0)
      nltot     number of SLC lines to process (default: 0, to end of file)
      rfilt     range common band filtering flag
                  0: OFF
                  1: ON (default)
      azfilt    azimuth common band filtering flag
                  0: OFF
                  1: ON (default)
      s_off     offset to the nominal range spectral shift (frac. of range sampling freq.) (default: 0.0)
    
    NOTE: enter - as filename to avoid creation of corresponding output file
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, MLI_1, MLI_2, interf]
    optargs = [nrlk, nazlk, loff, nltot, rfilt, azfilt, s_off]
    cmd = makecmd('interf_SLC', args, optargs) 
    os.system(cmd)

def par_RSAT2_SG(annotation_XML, lut_XML, GEOTIFF, polarisation, GRD_par, GRD, ):
    """*** Generate SLC parameter and ground range image files for Radarsat 2 SGF/SGX data  ***
    *** Copyright 2013, Gamma Remote Sensing, v1.4 23-Feb-2013 awi/cw ***
    
    usage: par_RSAT2_SG <annotation_XML> <lut_XML> <GEOTIFF> <polarisation> <GRD_par> <GRD>
    
    input parameters:
      annotation_XML (input) Radarsat2 product annotation XML file (product.xml)
      lut_XML        (input) Radarsat2 calibration XML file (lutSigma.xml), use - for no calibration
      GEOTIFF        (input) image data file in geotiff format (imagery_pp.tif)
      polarisation   (input) polarisation HH, VV, HV, VH to extract
      GRD_par        (output) ISP GRD parameter file (example: yyyymmdd_pp.grd.par)
      GRD            (output) float GRD data file (example: yyyymmdd_pp.grd)
    """

    args = [annotation_XML, lut_XML, GEOTIFF, polarisation, GRD_par, GRD]
    optargs = []
    cmd = makecmd('par_RSAT2_SG', args, optargs) 
    os.system(cmd)

def offset_add(OFF_par1, OFF_par2, OFF_par3, ):
    """*** Add range and azimuth offset polynomial coefficients ***
    *** Copyright 2008, Gamma Remote Sensing, v1.1 112-Feb-2008 clw ***
    
    usage: offset_add <OFF_par1> <OFF_par2> <OFF_par3>
    
    input parameters: 
      OFF_par1  (input) ISP offset/interferogram parameter file
      OFF_par2  (input) ISP offset/interferogram parameter file
      OFF_par3  (output) ISP offset/interferogram parameter file with sums of the
                range and azimuth offset polynomials in OFF_par1 and OFF_par2
    
    """

    args = [OFF_par1, OFF_par2, OFF_par3]
    optargs = []
    cmd = makecmd('offset_add', args, optargs) 
    os.system(cmd)

def mosaic_WB(data_tab, dtype, data_out, data_par_out, sc_flg=None):
    """*** ISP: Program mosaic_WB.c ***
    *** Copyright 2011, Gamma Remote Sensing, v1.2 6-Apr-2011 clw ***
    *** Mosaic Wide-Beam ScanSAR data processed by the MSP ***
    
    usage: mosaic_WB <data_tab> <dtype> <data_out> <data_par_out> [sc_flg]}
    
    input parameters: 
      data_tab      (input) 2 column list of data  and ISP image parameter files for the beams in the mosaic (text)
      dtype         (input) input data type:
                       0: FLOAT
                       1: FCOMPLEX
      data_out      (output) output image mosaic
      data_par_out  (output) ISP image parameter file for output image mosaic
      sc_flg        intensity scaling flag:
                      0: do not scale different beam data values
                      1: use overlap regions to scale beam intensities (default)
    
    """

    args = [data_tab, dtype, data_out, data_par_out]
    optargs = [sc_flg]
    cmd = makecmd('mosaic_WB', args, optargs) 
    os.system(cmd)

def offset_SLC(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr, rwin=None, azwin=None, offsets=None, n_ovr=None, nr=None, naz=None, thres=None, ISZ=None, pflag=None):
    """*** Offsets between SLC images using fringe visibility ***
    *** Copyright 2008, Gamma Remote Sensing, v2.8 clw 14-Feb-2008 ***
    
    usage: offset_SLC <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <offs> <snr> [rwin] [azwin] [offsets] [n_ovr] [nr] [naz] [thres] [ISZ] [pflag]
    
    input parameters: 
      SLC-1     (input) single-look complex image 1 (reference)
      SLC-2     (input) single-look complex image 2
      SLC1_par  (input) SLC-1 ISP image parameter file
      SLC2_par  (input) SLC-2 ISP image parameter file
      OFF_par   (input) ISP offset/interferogram parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rwin      search window size (range pixels, (enter - for default from offset parameter file))
      azwin     search window size (azimuth lines, (enter - for default from offset parameter file))
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     SLC oversampling factor (integer 2**N (1,2,4) default = 2)
      nr        number of offset estimates in range direction (enter - for default from offset parameter file)
      naz       number of offset estimates in azimuth direction (enter - for default from offset parameter file)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      ISZ       search chip interferogram size (in non-oversampled pixels, default=16)
      pflag     print flag (0:print offset summary  default=1:print all offset data)
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, offs, snr]
    optargs = [rwin, azwin, offsets, n_ovr, nr, naz, thres, ISZ, pflag]
    cmd = makecmd('offset_SLC', args, optargs) 
    os.system(cmd)

def par_EORC_PALSAR(CEOS_leader, SLC_par, CEOS_data, SLC=None):
    """*** Reformat EORC PALSAR level 1.1 CEOS format SLC data and generate the ISP parameter file ***
    *** Copyright 2013, Gamma Remote Sensing, v1.8 25-Jul-2013 clw ***
    
    usage: par_EORC_PALSAR <CEOS_leader> <SLC_par> <CEOS_data> [SLC]
    
    input parameters:
      CEOS_leader  (input) CEOS leader file for PALSAR Level 1.1 SLC data (LED...)
      SLC_par      (output) ISP image parameter file (example: yyyymmdd.slc.par)
      CEOS_data    (input) PALSAR CEOS format Level 1.1 SLC (IMG...)
      SLC          (output) reformated PALSAR SLC (example: yyyymmdd.slc, enter - for none)
    
    """

    args = [CEOS_leader, SLC_par, CEOS_data]
    optargs = [SLC]
    cmd = makecmd('par_EORC_PALSAR', args, optargs) 
    os.system(cmd)

def par_RSAT_SLC(CEOS_leader, SLC_par, CEOS_data, SLC=None, sc_dB=None, dt=None):
    """*** ISP parameter file for RSI/Atlantis/ASF processed Radarsat SLC data ***
    *** Copyright 2012, Gamma Remote Sensing, v4.0 5-Sep-2012 clw ***
    
    usage: par_RSAT_SLC <CEOS_leader> <SLC_par> <CEOS_data> [SLC] [sc_dB] [dt]
    
    input parameters:
      CEOS_leader  (input) CEOS SAR leader file (example: lea_01.001)
      SLC_par      (output) ISP SLC parameter file (example: <date>.slc.par)
      CEOS_data    (input) CEOS data file (example: dat_01.001)
      SLC          (output) SLC data with file and line headers removed (example: <date>.slc)
      sc_dB        intensity scale factor in dB (enter - for default:  60.00)
      dt           azimuth image time offset (s) (enter - for default = 0.0)
    
    """

    args = [CEOS_leader, SLC_par, CEOS_data]
    optargs = [SLC, sc_dB, dt]
    cmd = makecmd('par_RSAT_SLC', args, optargs) 
    os.system(cmd)

def SLC_interp(SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_2R, SLC2R_par, loff=None, nlines=None):
    """*** SLC complex image resampling using 2-D SINC interpolation ***
    *** Copyright 2014, Gamma Remote Sensing, v4.1 6-Feb-2014 clw ***
    
    usage: SLC_interp <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <SLC-2R> <SLC2R_par> [loff] [nlines]
    
    input parameters:
      SLC-2      (input) SLC-2 image to be resampled to the geometry of the SLC-1 reference image
      SLC1_par   (input) SLC-1 ISP image parameter file
      SLC2_par   (input) SLC-2 ISP image parameter file
      OFF_par    (input) ISP offset/interferogram parameter file
      SLC-2R     (output) single-look complex image 2 coregistered to SLC-1
      SLC2R_par  (output) SLC-2R ISP image parameter file for coregistered image
      loff       offset to first valid output line (in SLC-1 lines) (default: 0)
      nlines     number of valid output lines (default: 0, to end of file)
    
    """

    args = [SLC_2, SLC1_par, SLC2_par, OFF_par, SLC_2R, SLC2R_par]
    optargs = [loff, nlines]
    cmd = makecmd('SLC_interp', args, optargs) 
    os.system(cmd)

def create_offset(SLC1_par, SLC2_par, OFF_par, algorithm=None, rlks=None, azlks=None, iflg=None, of_par_in=None):
    """*** Create and update ISP offset and interferogram parameter files ***
    *** Copyright 2013 Gamma Remote Sensing v5.1 clw/uw 3-May-2013 ***
    
    usage: create_offset <SLC1_par> <SLC2_par> <OFF_par> [algorithm] [rlks] [azlks] [iflg]
    
    input parameters:
      SLC1_par   (input) SLC-1/MLI-1 ISP image parameter filename (reference)
      SLC2_par   (input) SLC-2/MLI-2 ISP image parameter filename
      OFF_par    (input/output) ISP offset/interferogram parameter file
      algorithm  offset estimation algorithm
                   1: intensity cross-correlation (default)
                   2: fringe visibility
      rlks       number of interferogram range looks (enter -  for default: 1)
      azlks      number of interferogram azimuth looks (enter - for default: 1)
      iflg       interactive mode flag (enter -  for default)
                   0: non-interactive
                   1: interactive (default)
    
    """

    args = [SLC1_par, SLC2_par, OFF_par]
    optargs = [algorithm, rlks, azlks, iflg]
    cmd = makecmd('create_offset', args, optargs) 
    if of_par_in:
        cmd += ' < '+of_par_in
    os.system(cmd)

def par_RISAT_GRD(CEOS_leader, BAND_META, GRD_par, CEOS_image, GRD=None, line_dir=None, pix_dir=None, cal_flg=None, KdB=None):
    """*** Read RISAT-1 Ground-Range data from a CEOS data set and perform radiometric calibration ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 3-Jun-2013 clw ***
    
    usage: par_RISAT_GRD <CEOS_leader> <BAND_META> <GRD_par> <CEOS_image> [GRD] [line_dir] [pix_dir] [cal_flg] [KdB]
    
    input parameters:
      CEOS_leader  (input) CEOS SAR leader file (example: lea_01.001)
      BAND_META    (input) BAND_META.txt, additional RISAT system parameters for the scene (format keywork=value)
      GRD_par      (output) ISP GRD parameter file (example: YYYYMMDD.grd.par)
      CEOS_image   (input) CEOS Ground-Range image file (example: dat_01.001)
      GRD          (output) Ground-Range data with file and line headers removed (enter - for none: example: YYYYMMDD.grd)
      line_dir     set output image line direction (enter - for default):
                     0: used value derived from CEOS leader file
                     1: retain input data line direction  (default)
                    -1: reverse input data line direction
      pix_dir      set output pixel direction (enter - for default):
                     0: used value derived from CEOS leader file
                     1: retain input data pixel direction (default)
                    -1: reverse input data pixel direction
      cal_flg      calibration flag (enter - for default):
                     0: do not apply radiometric calibration
                     1: apply radiometric calibration including KdB and incidence angle correction (default)
      KdB          calibration constant (dB) (enter - to use value in the CEOS leader)
    
    """

    args = [CEOS_leader, BAND_META, GRD_par, CEOS_image]
    optargs = [GRD, line_dir, pix_dir, cal_flg, KdB]
    cmd = makecmd('par_RISAT_GRD', args, optargs) 
    os.system(cmd)

def ph_slope_base(int_in, SLC_par, OFF_par, base, int_out, int_type=None, inverse=None):
    """*** Subtract/add interferogram flat-Earth phase trend as estimated from initial baseline ***
    *** Copyright 2006, Gamma Remote Sensing, v4.4 3-Nov-2006 clw ***
    
    usage: ph_slope_base <int_in> <SLC_par> <OFF_par> <base> <int_out> [int_type] [inverse]
    
    input parameters: 
      int_in    (input) interferogram (FCOMPLEX) or unwrapped phase (FLOAT) (unflattened)
      SLC_par   (input) ISP parameter file for the reference SLC
      OFF_par   (input) ISP offset/interferogram parameter file
      base      (input) baseline file
      int_out   (output) interferogram (FCOMPLEX) or unwrapped phase (FLOAT) with phase trend subtracted/added
      int_type  interferogram type: 0=unwrapped phase, 1=complex interf. (default=1)
      inverse   subtract/add inversion flag (0=subtract phase ramp, 1=add phase ramp (default=0)
    """

    args = [int_in, SLC_par, OFF_par, base, int_out]
    optargs = [int_type, inverse]
    cmd = makecmd('ph_slope_base', args, optargs) 
    os.system(cmd)

def MLI_copy(MLI_in, MLI_in_par, MLI_out, MLI_out_par, roff=None, nr=None, loff=None, nl=None):
    """*** Copy MLI data file with options for segment extraction ***
    *** Copyright 2013, Gamma Remote Sensing, v4.4 10-Jan-2013 uw/clw ***
    
    usage: MLI_copy <MLI_in> <MLI_in_par> <MLI_out> <MLI_out_par> [roff] [nr] [loff] [nl]
    
    input parameters: 
      MLI_in       (input) multi-look intensity image (float format)
      MLI_in_par   (input) ISP image parameter file for input MLI
      MLI_out      (output) selected MLI section (float format)
      MLI_out_par  (output) ISP image parameter file for output MLI
      roff         offset to starting range sample (enter - for default: 0)
      nr           number of range samples (enter - for default: to end of line
      loff         offset to starting line (enter - for default: 0)
      nl           number of lines to copy (enter - for default: to end of file)
    """

    args = [MLI_in, MLI_in_par, MLI_out, MLI_out_par]
    optargs = [roff, nr, loff, nl]
    cmd = makecmd('MLI_copy', args, optargs) 
    os.system(cmd)

def sigma2gamma(pwr1, inc, gamma, width, ):
    """*** Land Application Tools: Program sigma2gamma.c ***
    *** Copyright 2007, Gamma Remote Sensing, v1.0 25-Oct-2007 awi ***
    
    *** Compute backscatter coefficient gamma (sigma0)/cos(inc) ***
    
    usage: sigma2gamma <pwr1> <inc> <gamma> <width>
    
    input parameters: 
      pwr1   (input) intensity image of sigma nought
      inc    (input) intensity image of local incidence angles
      gamma  (output) gamma = pwr1 / cos(inc) file 
      width  number of samples/row
    
    """

    args = [pwr1, inc, gamma, width]
    optargs = []
    cmd = makecmd('sigma2gamma', args, optargs) 
    os.system(cmd)

def looks(pwr, width, polygon, ):
    """*** Land Application Tools: Program looks.c ***
    *** Copyright 2009, Gamma Remote Sensing, v3.4 3-Apr-2009 uw/clw ***
    *** Equivalent number of looks (ENL) estimation for test areas (polygon regions) ***
    
    usage: looks <pwr> <width> <polygon>
    
      input parameters: 
      pwr      (input) input intensity image file (floating point)
      width    image width in range pixels
      polygon  polygon data file 
    """

    args = [pwr, width, polygon]
    optargs = []
    cmd = makecmd('looks', args, optargs) 
    os.system(cmd)

def quad2cp(SLC_HH, SLC_HV, SLC_VH, SLC_VV, SLC_HH_par, SLC_HV_par, SLC_VH_par, SLC_VV_par, CP, TX_pol, ):
    """*** Synthesize Compact Polarization Data from quad-pol HH, VV, HV, VH SLC data ***
    *** Transmit polarization: RCP or LCP, receive polarization: H and V linear ***
    *** Copyright 2013, Gamma Remote Sensing, v1.0  27-Jun-2013 clw ***
    
    usage: quad2cp <SLC_HH>  <SLC_HV> <SLC_VH> <SLC_VV> <SLC_HH_par> <SLC_HV_par> <SLC_VH_par> <SLC_VV_par> <CP> <TX_pol>
    
    input parameters:
      SLC_HH      (input) HH single-look complex image (scomplex or fcomplex format)
      SLC_HV      (input) HV single-look complex image (coregistered with SLC_HH)(scomplex or fcomplex format)
      SLC_VH      (input) VH single-look complex image (coregistered with SLC_HH)(scomplex or fcomplex format)
      SLC_VV      (input) VV single-look complex image (coregistered with SLC_HH)(scomplex or fcomplex format)
      SLC_HH_par  (input) SLC_HH image parameter file
      SLC_HV_par  (input) SLC_HV image parameter file
      SLC_VH_par  (input) SLC_VH image parameter file
      SLC_VV_par  (input) SLC_VV image parameter file
      CP          (output) root file name of the 2 linearly polarized images (H,V): (fcomplex format)
                  RCP transmit: *_RH: 1/(sqrt(2) (S_HH - iS_HV)
                                *_RV: 1/(sqrt(2) (S_VH - iS_VV)
                  LCP transmit: *_LH: 1/(sqrt(2) (S_HH + iS_HV)
                                *_LV: 1/(sqrt(2) (S_VH + iS_VV)
      Note: SLC parameter files are created from the CP SLC name + .par
      TX_pol      transmit polarization of synthesized compact mode data:
                    0: RCP Right Circular Polarziation
                    1: LCP Left  Circular Polarziation
    
    """

    args = [SLC_HH, SLC_HV, SLC_VH, SLC_VV, SLC_HH_par, SLC_HV_par, SLC_VH_par, SLC_VV_par, CP, TX_pol]
    optargs = []
    cmd = makecmd('quad2cp', args, optargs) 
    os.system(cmd)

def ras_ras(ras_in, ras_out, col_looks=None, row_looks=None, LR=None, r_lin_log=None, g_lin_log=None, b_lin_log=None, force24=None):
    """*** Land Application Tools: Program ras_ras.c ***
    *** Copyright 2012, Gamma Remote Sensing, v4.5 4-May-2012 clw/uw ***
    *** Multi-looking, left/right flip, and format conversion of SUN raster and BMP format images ***
    
    usage: ras_ras <ras_in> <ras_out> [col_looks] [row_looks] [LR] [r_lin/log] [g_lin/log] [b_lin/log] [force24]
    
    input parameters:
      ras_in       (input) SUN raster or BMP format image (8 or 24 bits/sample)
      ras_out      (output) SUN raster or BMP format image
      col_looks    number of column looks (across)
      row_looks    number of row looks (down)
      LR           left/right flipping (default=1: normal, -1: mirror image
      r_lin/log    lin/log scale red/grey channel (default=0: linear scale data, 1: log scale data)
      g_lin/log    lin/log scale green channel    (default=0: linear scale data, 1: log scale data)
      b_lin/log    lin/log scale blue channel     (default=0: linear scale data, 1: log scale data)
      force24      default=0: bits/sample unchanged   1: output is 24 bits/sample
    """

    args = [ras_in, ras_out]
    optargs = [col_looks, row_looks, LR, r_lin_log, g_lin_log, b_lin_log, force24]
    cmd = makecmd('ras_ras', args, optargs) 
    os.system(cmd)

def diplane_helix(LL, RR, SLC_par, diplane, helix, MLI_par, rlks, azlks, loff=None, nlines=None, scale=None, image_format=None):
    """*** LAT Calculate Helix and Diplane composition from RR and LL circular components ***
    *** Copyright 2003, Gamma Remote Sensing, v1.0 31-Mar-2003 awi ***
    
    usage: diplane_helix <LL> <RR> <SLC_par> <diplane> <helix> <MLI_par> <rlks> <azlks> [loff] [nlines] [scale] [image_format]
    
    input parameters:
      LL          (input) single-look complex image with LL circular component
      RR          (input) single-look complex image with RR circular component
      SLC_par     (input) SLC ISP image parameter file
      diplane     (output) multi-look intensity image with diplane component
      helix       (output) multi-look intensity image with helix component
      MLI_par     (output) MLI ISP image parameter file
      rlks        number of range looks
      azlks       number of azimuth looks
      loff        offset to starting line (default = 0)
      nlines      number of SLC lines to process (default = entire file)
      scale       scale factor for output MLI (default = 1.0)
    
    """

    args = [LL, RR, SLC_par, diplane, helix, MLI_par, rlks, azlks]
    optargs = [loff, nlines, scale, image_format]
    cmd = makecmd('diplane_helix', args, optargs) 
    os.system(cmd)

def pauli(SLC_HH, SLC_VV, SLC_HV, SLC_HH_par, SLC_VV_par, SLC_HV_par, P, ):
    """*** Calculate Pauli polarimetric decomposition from HH, VV, and HV SLC images ***
    *** Copyright 2013, Gamma Remote Sensing, v1.3 2-Jul-2013 clw ***
    
    usage: pauli <SLC_HH> <SLC_VV> <SLC_HV> <SLC_HH_par> <SLC_VV_par> <SLC_HV_par> <P>
    
    input parameters:
      SLC_HH      (input) HH single-look complex image (scomplex or fcomplex format)
      SLC_VV      (input) VV single-look complex image coregistered with SLC_HH (scomplex or fcomplex format)
      SLC_HV      (input) HV single-look complex image coregistered with SLC_HH (scomplex or fcomplex format)
      SLC_HH_par  (input) SLC_HH image parameter file
      SLC_VV_par  (input) SLC_VV image parameter file
      SLC_HV_par  (input) SLC_HV image parameter file
      P           (output) root file name of Pauli decomposition images: P_alpha.slc, P_beta.slc, P_gamma.slc (fcomplex format)
                    alpha: (S_HH + S_VV)/sqrt(2.0)
                    beta:  (S_HH - S_VV)/sqrt(2.0)
                    gamma: sqrt(2.0)*S_HV
      Note: SLC image parameter files are generated: P_alpha.slc.par, P_beta.slc.par, P_gamma.slc.par
    """

    args = [SLC_HH, SLC_VV, SLC_HV, SLC_HH_par, SLC_VV_par, SLC_HV_par, P]
    optargs = []
    cmd = makecmd('pauli', args, optargs) 
    os.system(cmd)

def ras_majority(ras_in, ras_out, filter_width=None, LR=None):
    """*** Land Application Tools: Program ras_majority.c ***
    *** Copyright 2000, Gamma Remote Sensing, v3.3 8-Jan-2000 uw/clw ***
    *** Majority filtering of 8-bit SUN raster/BMP images ***
    
    usage: ras_majority <ras_in> <ras_out> [filter_width] [LR]
    
    input parameters: 
      ras_in        input SUN raster or BMP image file (8 bits/pixel) (*.ras, *.bmp)
      ras_out       output SUN raster or BMP image (8 bits/pixel) (*.ras, *.bmp)
      filter_width  filter width (select from 3, 5, 7, default = 3)
      LR            LR = 1 normal, LR = -1 mirror image (default = 1)
    
    """

    args = [ras_in, ras_out]
    optargs = [filter_width, LR]
    cmd = makecmd('ras_majority', args, optargs) 
    os.system(cmd)

def cc_ad(interf, pwr1, pwr2, slope, texture, cc_ad, width, box_min=None, box_max=None, wgt_flag=None, loff=None, nl=None):
    """*** Land Application Tools: Program cc_ad.c ***
    *** Copyright 2012, Gamma Remote Sensing, v3.7 1-Mar-2012 clw/uw ***
    
    *** Adaptive coherence estimation with consideration of phase slope and texture ***
    
    usage: cc_ad <interf> <pwr1> <pwr2> <slope> <texture> <cc_ad> <width> [box_min] [box_max] [wgt_flag] [loff] [nl]
    
    input parameters:
      interf    (input) complex interferogram
      pwr1      (input) intensity image of first scene (or -)
      pwr2      (input) intensity image of second scene (or -)
      slope     (input) phase slope data (or -)
      texture   (input) backscatter texture data (or - )
      cc_ad     (output) adaptive coherence estimate
      width     number of samples/row
      box_min   smallest correlation average box size (default: 3.0)
      box_max   largest correlation average box size  (default: 9.0)
      wgt_flag  weighting function
                  0: constant(default)
                  1: gaussian
      loff      offset in lines to starting line (default:0)
      nl        number of lines to process (default: 0, to end of file)
    
    """

    args = [interf, pwr1, pwr2, slope, texture, cc_ad, width]
    optargs = [box_min, box_max, wgt_flag, loff, nl]
    cmd = makecmd('cc_ad', args, optargs) 
    os.system(cmd)

def hsi_color_scale(file_out, nval=None, chip=None, gap=None, height=None):
    """*** Land Application Tools: Program hsi_color_scale.c ***
    *** Copyright 2005, Gamma Remote Sensing, v3.4 7-Oct-2005 clw/uw ***
    *** Generate color scale for hue/saturation/intensity (HSI) images ***
    
    usage: hsi_color_scale <file_out> [nval] [chip] [gap] [height]
    
    input parameters:
      <file_out>     (output) (24-bit) SUN raster, BMP image file
      [nval]         number of discrete color values to display
                       0: continuous color wheel (default)
      [chip_width]   width of color chips (default=8)
      [gap]          number of pixels between color chips (default=2)
      [height]       height of color scale (default=chip_width)
    
    """

    args = [file_out]
    optargs = [nval, chip, gap, height]
    cmd = makecmd('hsi_color_scale', args, optargs) 
    os.system(cmd)

def trigo(data1, func, data2, width, ):
    """*** Land Application Tools: Program trigo.c ***
    *** Copyright 2009, Gamma Remote Sensing, v1.1 3-Sep-2013 awi/clw ***
    
    *** Compute trigonometric functions of an input file ***
    
    usage: trigo <data1> <func> <data2> <width>
    
    input parameters: 
      data1  (input) angle data in radians (float)
      func   1: sin, 2: cos, 3: tan, 4: asin, 5: acos, 6:atan
      data2  (output) data calculated as func(data1) (float)
      width  number of samples/line
    
    """

    args = [data1, func, data2, width]
    optargs = []
    cmd = makecmd('trigo', args, optargs) 
    os.system(cmd)

def line_interp(inpu, outpu, width, ):
    """*** Land Application Tools: Program line_interp.c ***
    *** Copyright 2012, Gamma Remote Sensing, v3.2 5-Apr-2012 uw/clw ***
    *** 1-D linear interpolator to fill data gaps ***
    
    usage: line_interp <input file> <output file> <width>
    input parameters: 
      input file   input data containing 0.0 values (float) 
      output file  output data with 0.0 values replaced (float) 
      width        number of samples/line
    
    """

    args = [inpu, outpu, width]
    optargs = []
    cmd = makecmd('line_interp', args, optargs) 
    os.system(cmd)

def reallks(image, ML_image, width, rlks=None, azlks=None, start=None, nlines=None, r_start=None, nsamp=None):
    """*** Land Application Tools: Program reallks.c ***
    *** Copyright 2004, Gamma Remote Sensing, v3.2 23-Jun-2004 clw ***
    
    *** Multilooking for real valued image data ***
    
    usage: reallks <image> <ML_image> <width> [rlks] [azlks] [start] [nlines] [r_start] [nsamp]
    
    input parameters: 
      image     (input) 4-byte/value float input file
      ML_image  (output) 4-byte/value float output file
      width     width of image
      rlks      number of range looks (default=1)
      azlks     number of azimuth looks (default=1)
      start     starting line (default=1)
      nlines    number of lines (default=entire file, 0 for entire file)
      r_start   starting range (default=1)
      nsamp     number of range samples to extract (default = to end of line)
    
    """

    args = [image, ML_image, width]
    optargs = [rlks, azlks, start, nlines, r_start, nsamp]
    cmd = makecmd('reallks', args, optargs) 
    os.system(cmd)

def ave2pwr(pwr1, pwr2, pwr_out, width, scale_factor=None):
    """*** Land Application Tools: Program ave2pwr.c ***
    *** Copyright 2013, Gamma Remote Sensing, v4.2 3-Sep-2013 clw/uw ***
    
    *** Averaging of two co-registered intensity images ***
    
    usage: ave2pwr <pwr1> <pwr2> <pwr_out> <width> [scale_factor]
    
    input parameters: 
      pwr1          (input) intensity image 1 (float)
      pwr2          (input) intensity image 2 (float)
      pwr_out       (output) output file: (pwr1 + scale_factor*pwr2)/2. 
      width         number of samples/row for the input files
      scale_factor  scale factor for image 2 (absolute value, not dB) (default: 1.0)
    
    """

    args = [pwr1, pwr2, pwr_out, width]
    optargs = [scale_factor]
    cmd = makecmd('ave2pwr', args, optargs) 
    os.system(cmd)

def unw_to_cpx(unw, cpx, width, ):
    """*** Land Application Tools: Program unw_to_cpx.c ***
    *** Copyright 2003, Gamma Remote Sensing, v1.2 19-Aug-2003 uw/clw/ts ***
    *** convert unwrapped phase to an fcomplex interferogram ***
    
    usage: unw_to_cpx <unw> <cpx> <width>
    
    input parameters: 
      unw    (input) unwrapped phase (float)
      cpx    (output) interferogram file (fcomplex)
      width  samples per row
    """

    args = [unw, cpx, width]
    optargs = []
    cmd = makecmd('unw_to_cpx', args, optargs) 
    os.system(cmd)

def lin_comb(nfiles, f1, f2, constant, factor1, factor2, f_out, width, start=None, nlines=None, pixav_x=None, pixav_y=None, zero_flag=None):
    """*** Land Application Tools: lin_comb.c ***
    *** Copyright 2013, Gamma Remote Sensing, v4.6 26-Feb-2013 uw/clw ***
    *** Calculate linear combination of float data files ***
    
    usage: lin_comb <nfiles> <f1> <f2> <...> <constant> <factor1> <factor2> <...> <f_out> <width> [start] [nlines] [pixav_x] [pixav_y] [zero_flag]
    
    input parameters: 
      nfiles     number of input data files
      f1         (input) 1. input data file (float)
      f2         (input) 2. input data file (float)
      ...        (input) further input files
      constant   constant value to add to output
      factor1    factor1 to multiply with f1
      factor2    factor2 to multiply with f2
      ...        factors for further input files
      f_out      (output) output file (float)
      width      number of samples/row
      start      starting line (default: 1)
      nlines     number of lines to display (default 0: entire file)
      pixav_x    number of pixels to average in width (default: 1)
      pixav_y    number of pixels to average in height (default: 1)
      zero_flag  interpretation of 0.0 values:
                   0: interpreted as missing value (default)
                   1: 0.0 is valid data value
    
    """

    args = [nfiles, f1, f2,constant, factor1, factor2, f_out, width]
    optargs = [start, nlines, pixav_x, pixav_y, zero_flag]
    cmd = makecmd('lin_comb', args, optargs) 
    os.system(cmd)

def product_cpx(f1, f2, f_out, width, start=None, nlines=None, conjg_flg=None):
    """*** Land Application Tools: product_cpx.c ***
    *** Copyright 2012,  Gamma Remote Sensing, v1.2 18-Jan-2012 clw ***
    *** Calculate product of complex value data files ***
    
    usage: product_cpx <f1> <f2> <f_out> <width> [start] [nlines] [conjg_flg]
    
    input parameters: 
      f1         (input) data file 1 (fcomplex)
      f2         (input) data file 2 (fcomplex)
      f_out      (output) output file f1 * f2 (fcomplex)
      width      number of data samples/line
      start      starting line (default: 1)
      nlines     number of lines to process (enter - for default, all lines)
      conjg_flg  conjugation flag
                   0: none (default)
                   1: conjugate f2 before multiply
    """

    args = [f1, f2, f_out, width]
    optargs = [start, nlines, conjg_flg]
    cmd = makecmd('product_cpx', args, optargs) 
    os.system(cmd)

def ave_cpx(cpx_list, width, ave, start=None, nlines=None, zflag=None):
    """*** Calculate average of a stack of complex images (fcomplex format) ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 18-Jan-2012 clw ***
    
    usage: ave_cpx <cpx_list> <width> <ave> [start] [nlines] [zflag]
    
    input parameters:
      cpx_list (input) text file with names of co-registered images in column 1 (fcomplex)
      width    number of samples/line
      ave      (output) average of input images (fcomplex)
      start    starting line (default -: 1)
      nlines   number of lines to process (enter -  for default: entire file)
      zflag    zero flag:
                 0: interpret 0.0 as missing data value (default)
                 1: interpret 0.0 as valid data
    
    """

    args = [cpx_list, width, ave]
    optargs = [start, nlines, zflag]
    cmd = makecmd('ave_cpx', args, optargs) 
    os.system(cmd)

def uchar2float(infile, outfile, scale, exp, ):
    """*** Land Application Tools: Program uchar2float.c ***
    *** Copyright 2008, Gamma Remote Sensing, v3.3 25-Feb-2008 uw/clw ***
    *** Format transformation from unsigned char (1-byte) to float (4-byte) ***
    *** output = scale * (data + offset)^exp (for coherence use scale=0.00392, exp=1.0) ***
    
    usage: uchar2float <infile> <outfile> <scale> <exp>
    
    input parameters:
      data_in   (input) input data file (unsigned char, 1 byte/sample)
      data_out  (output output file (float)
      scale     scale factor (default: 1.0)
      exp       exponent (default: 1.0)
      offset    offset (default: 0.0)
    
    """

    args = [infile, outfile, scale, exp]
    optargs = []
    cmd = makecmd('uchar2float', args, optargs) 
    os.system(cmd)

def soil_moisture(pwr, pwr_ref, mv_ref, mv_out, width, dB_flag=None, start=None, nlines=None, param_a=None, param_b=None):
    """*** Land Application Tools: Program soil_moisture.c ***
    *** Copyright 2001, Gamma Remote Sensing, v3.1 22-Jan-2001 uw/clw ***
    *** Soil moisture retrieval from ERS data (Wegmuller et al., 1997) ***
    
    usage: soil_moisture <pwr> <pwr_ref> <mv_ref> <mv_out> <width> [dB_flag] [start] [nlines] [param_a] [param_b]
    
    input parameters: 
      <pwr>         backscatter intensity image, file of type float
      <pwr_ref>     reference backscatter intensity image, file of type float
      <mv_ref>      reference volumetric soil moisture image (absolute values), file of type float
      <mv_out>	     output volumetric soil moisture image, file of type float
      <width>       file width
      [dB_flag]     flag indicating if pwr values are in dB
                    (default:0=linear scale, 1=dB values)
      [start]       starting line (default = 1)
      [nlines]      number of lines to process (default = 0: all lines)
      [param_a]     model parameter a (default = 8.56)
      [param_b]     model parameter b (default = 1.56)
    
    """

    args = [pwr, pwr_ref, mv_ref, mv_out, width]
    optargs = [dB_flag, start, nlines, param_a, param_b]
    cmd = makecmd('soil_moisture', args, optargs) 
    os.system(cmd)

def takethat_dem_par(data_in, width, positions, DEM_par, report, mode=None, zero_flag=None, nn_flag=None, print_flag=None):
    """*** Land Application Tools: Program takethat_dem_par.c ***
    *** Copyright 2004, Gamma Remote Sensing, v1.2 8-Apr-2004 ts/clw ***
    
    *** Extraction of values along profiles, in polygon regions, or from indicated positions ***
    *** with position coordinates indicated as map coordinates ***
    
    usage: takethat_dem_par <data_in> <width> <positions> <DEM_par> <report> [mode] [zero_flag] [nn_flag] [print_flag]
    
    input parameters:
      data_in     (input) input data file (float format)
      width       samples per row of data_in
      positions   (input) text file containing list of positionsin map coordinates:
                    geo. coord. (EQA): longitude latitude   map proj. (e.g. UTM): easting northing
      DEM_par     (input) DEM/MAP parameter file used to convert coordiantes to pixel numbers
      report      (output) text file containing extracted values
      mode        data extraction mode (default = 0)
                  	0 : extract values along the profile connecting the indicated positions
                  	1 : extract values in the polygon region specified by the indicated positions
                  	2 : extract values at the indicated positions
      zero_flag   interpret 0.0 as missing value
                  (default=0: 0.0 indicates missing value, 1: 0.0 is valid value)
      nn_flag     resampling algorithm flag in the case of extraction along a profile
                  (default=0: spline interpolation, 1: nearest neighbor)
      print_flag  print flag (default=0:print values at all positions  1:print only positions with valid data)
    
    """

    args = [data_in, width, positions, DEM_par, report]
    optargs = [mode, zero_flag, nn_flag, print_flag]
    cmd = makecmd('takethat_dem_par', args, optargs) 
    os.system(cmd)

def histogram_ras(ras_in, polygon, histograms, mean_stdev, percent, lr_flag=None, start=None, stop=None):
    """*** Land Application Tools: Program histogram_ras.c ***
    *** Copyright 2001, Gamma Remote Sensing, v3.4 2-Jul-2001 clw/uw ***
    *** Calculation of histograms for polygon regions for a SUN raster or BMP format image ***
    
    usage: histogram_ras <ras_in> <polygon> <histograms> <mean/stdev> <percent> [lr_flag] [start] [stop] 
    
    input parameters: 
      ras_in        input 8-bit SUN raster/BMP image file (SUN raster: *.ras, BMP image format: *.bmp)
      polygon       polygon data file
      histograms    calculated histograms (output)
      mean/stdev    calculated mean/stdev (output)
      percent       histogram in terms of percent of total pixels (output)
      lr_flag       input left/right flipped (default=1: flipped; -1: not flipped)
      start         first histogram class (default=0)
      stop          last histogram class (default=all values)
    
    """

    args = [ras_in, polygon, histograms, mean_stdev, percent]
    optargs = [lr_flag, start, stop]
    cmd = makecmd('histogram_ras', args, optargs) 
    os.system(cmd)

def mask_op(mask_1, mask_2, mask_out, mode, ):
    """*** ISP mask_op.c pixel basis logical mask operations ***
    *** Copyright 2004, Gamma Remote Sensing, v1.0 13-May-2004 clw ***
    
    usage: mask_op <mask-1> <mask-2> <mask_out> <mode>
    
    input parameters: 
      mask-1    (input) mask 1 SUN raster *.ras, or BMP *.bmp format, 8-bits/pixel
      mask-2    (input) mask 2 SUN raster *.ras, or BMP *.bmp format, 8-bits/pixel
      mask_out  (output) output mask SUN raster *.ras, or BMP *.bmp format
      mode      logical operation mode on a single pixel basis:
                  0:  mask-1 AND mask-2
                  1:  mask-1 OR  mask-2
                  2:  NOT mask-1
    
    """

    args = [mask_1, mask_2, mask_out, mode]
    optargs = []
    cmd = makecmd('mask_op', args, optargs) 
    os.system(cmd)

def cpxlks(CMPLX, ML_CMPLX, width, rlks=None, azlks=None, start=None, nlines=None, r_start=None, nsamp=None):
    """*** Land Application Tools: Program cpxlks.c ***
    *** Copyright 2004, Gamma Remote Sensing, v3.1 22-Jun-2004 clw/uw ***
    
    *** Multilooking of complex valued image ***
    
    usage: cpxlks <CMPLX> <ML_CMPLX> <width> [rlks] [azlks] [start] [nlines] [r_start] [nsamp]
    
    input parameters: 
      CMPLX     (input) fcomplex image file
      ML_CMPLX  (output) output multilook fcomplex image file
      width     width of complex image
      rlks      number of range looks (default=1)
      azlks     number azimuth looks (default=1)
      start     starting line (default=1)
      nlines    number of lines (default=entire file, 0 for entire file)
      r_start   starting range (default=1)
      nsamp     number of range samples to extract (default = to end of line)
    
    """

    args = [CMPLX, ML_CMPLX, width]
    optargs = [rlks, azlks, start, nlines, r_start, nsamp]
    cmd = makecmd('cpxlks', args, optargs) 
    os.system(cmd)

def average_filter(input, output, width, bx, by=None, wgt_flag=None):
    """*** Land Application Tools: Program average_filter.c ***
    *** Copyright 2004, Gamma Remote Sensing, v3.3 14-May-2004 clw/uw ***
    *** Running average filter (float data)  ***
    usage: average_filter <input> <output> <width> <bx> [by] [wgt_flag]
    
    input parameters:
      input     (input) input data file (float)
      output    (output) output filtered data file (float)
      width     number of samples/row
      bx        filter size in x direction (number of cols)
      by        filter size in y direction (number of rows)
                (default = bx)
      wgt_flag  weighting function selection:
                0: (default) no weighting function applied
                1: linear weighting function
                2: Gaussian weighting function
    
    """

    args = [input, output, width, bx]
    optargs = [by, wgt_flag]
    cmd = makecmd('average_filter', args, optargs) 
    os.system(cmd)

def temp_filt_ad(data_tab, width, zero_flag=None, loffset=None, nlines=None):
    """*** Land Application Tools : Program temp_filt_ad ***
    *** Copyright 2013 Gamma Remote Sensing, v3.7 6-May-2013 clw/uw ***
    *** Multi-temporal filtering of registered data sets using adaptive spatial mean estimate ***
    
    usage: temp_filt_ad <data_tab> <width> [zero_flag] [loffset] [nlines]
    
    input parameters:
      data_tab   (input) three column list of the names of input and output data files (float)
                   input file 1  spatially filtered file 1  output file 1
                   input file 2  spatially filtered file 2  output file 2
                    ...              ...                   ...
      width      number of samples/row
      zero_flag  zero data flag:
                   0: 0.0 interpreted as missing value (default)
                   1: 0.0 interpreted as valid data value
      loffset    number of lines offset to starting line (default = 0)
      nlines     number of lines to process (default: 0 = entire file)
    
    """

    args = [data_tab, width]
    optargs = [zero_flag, loffset, nlines]
    cmd = makecmd('temp_filt_ad', args, optargs) 
    os.system(cmd)

def texture(data_in, format_flag, texture, width, type=None, bx=None, by=None, r_looks=None, az_looks=None, weights_flag=None, data_in_mean=None):
    """*** Land Application Tools: Program texture.c ***
    *** Copyright 2001, Gamma Remote Sensing, v4.4 23-Jan-2001 uw/clw ***
    *** Estimation of image texture defined as stdev/mean or log<I>-<logI> ***
    
    usage: texture <data_in> <format_flag> <texture> <width> [type] [bx] [by] [r_looks] [az_looks] [weights_flag] [data_in_mean]
    
    input parameters: 
      data_in      (input) data file (float, fcomplex, or scomplex)
      format_flag  format of data_in: 0: float, 1: fcomplex, 2: scomplex)
      texture      (output) image texture (float) defined as normalized second moment
      width        number of samples/row
      type         texture measure flag (default=0)
                   0:  texture defined as stdev/mean
                   1:  log<I>-<logI>
      bx           estimation window size in input image range pixels
                   (default = 15)
      by           estimation window size in input image azimuth pixels
                   (default = bx)
      r_looks      range multilooking (in range pixels)
                   (default = 1)
      az_looks     azimuth multilooking (in azimuth pixels)
                   (default = 1)
      wgt_flag     weighting function: 
                     0: no weighting function applied (default)
                     1: linear weighting function
                     2: Gaussian weighting function
      data_in_mean (input) first moment of input data file (float)
    
    """

    args = [data_in, format_flag, texture, width]
    optargs = [type, bx, by, r_looks, az_looks, weights_flag, data_in_mean]
    cmd = makecmd('texture', args, optargs) 
    os.system(cmd)

def temp_filt(data_tab, width, waz=None, wr=None, wt_flag=None, zero_flag=None, loff=None, nlines=None):
    """*** Land Application Tools : Program  temp_filt ***
    *** Copyright 2013 Gamma Remote Sensing, v3.8 6-May-2013 clw/uw ***
    *** Multi-temporal filtering of registered data sets of float format data ***
    
    usage: temp_filt <data_tab> <width> [waz] [wr] [wt_flag] [zero_flag] [loff] [nlines]
    input parameters:
      data_tab   (input) two column list of the names of input and output data files (float)
                   input file 1   output file 1
                   input file 2   output file 2
                    ...            ...
      width      number of samples/row
      waz        spatial averaging filter width in azimuth pixels (default =  1.00)
      wr         spatial averaging filter width in range pixels (default =  1.00)
      wt_flag    weighting function flag
                   0: uniform (default)
                   1: linear)
                   2: Gaussian)
      zero_flag  zero data flag:
                   0: 0.0 interpreted as missing value (default)
                   1: 0.0 interpreted as valid data value
      loff       number of lines offset to starting line (default: 0)
      nlines     number of lines to process (0: entire file (default))
    
    """

    args = [data_tab, width]
    optargs = [waz, wr, wt_flag, zero_flag, loff, nlines]
    cmd = makecmd('temp_filt', args, optargs) 
    os.system(cmd)

def stokes_qm(S, S_par, m=None, s2chi=None, s2psi=None, m_l=None, m_c=None, lp_ratio=None, cp_ratio=None, mu=None, delta=None, alpha=None, phi=None):
    """*** Calculate quantitative measures derived from Stokes parameters (e.g. degree of polarization) ***
    *** Copyright 2013, Gamma Remote Sensing, v1.7 28-Jun-2013 clw ***
    
    usage: stokes_qm <S> <S_par> [m] [s2chi] [s2psi] [m_l] [m_c] [lp_ratio] [cp_ratio] [mu] [delta] [alpha] [phi]
    
    input parameters:
      S         (input) root file name of 4 Stokes parameter files with extensions .s0, .s1, .s2, .s3 (float)
      S_par     (input) image parameter file associated with the Stokes parameter data files
      m         (output) degree of polarization: sqrt(s1**2 + s2**2 + s3**2)/s0 (enter - for none) (float)
      s2chi     (output) sine of the latitude of the Stokes vector on the Poincare sphere: sin(2*chi) = -s3/(m*s0)
      s2psi     (output) sine of the longitude of the Stokes vector on the Poincare sphere: sin(2*psi) = s2/sqrt(s1**2 + s2**2))
      m_l       (output) degree of linear polarization: sqrt(s1**2 + s2**2)/s0  (enter - for none) (float)
      m_c       (output) degree of circular polarization: s3/s0 (enter - for none) (float)
      lp_ratio  (output) linear polarization ratio: (s0 - s1)/(s0 + s1) 0<=lp_ratio (enter - for none) (float)
      cp_ratio  (output) circular polarization ratio: (s0 - s3)/(s0 + s3) 0 <= cp_ratio (enter - for none) (float)
      mu        (output) coherency parameter |mu_xy|: sqrt(s2**2 + s3**2)/sqrt(s0**2 - s1**2) (enter - for none) (float)
      delta     (output) relative H and V phase difference atan(s3/s2) (enter - for none) (radians, float)
      alpha     (output) alpha parameter in the compact H/alpha decomposition: 0.5*atan(sqrt(s1**2 + s2**2)/s3) (enter - for none)(radians, float)
      phi       (output) longitude of Stokes vector 2*psi: atan(s2/s1) (enter - for none) (radians, float)
    """

    args = [S, S_par]
    optargs = [m, s2chi, s2psi, m_l, m_c, lp_ratio, cp_ratio, mu, delta, alpha, phi]
    cmd = makecmd('stokes_qm', args, optargs) 
    os.system(cmd)

def median_filter(input, output, width, bx, by=None):
    """*** Land Application Tools: Program median_filter.c ***
    *** Copyright 2004, Gamma Remote Sensing, v1.0 14-May-2004 awi/clw/uw ***
    *** Median filter (float data)  ***
    
    usage: median_filter <input> <output> <width> <bx> [by] 
    
    input parameters:
      input   (input) input data file
      output  (output) output data file (filtered)
      width   number of samples/row
      bx      filter size in x direction (number of cols)
      by      filter size in y direction (number of rows)
              (default = bx)
    
    """

    args = [input, output, width, bx]
    optargs = [by]
    cmd = makecmd('median_filter', args, optargs) 
    os.system(cmd)

def polyx_phase(data, width, polygon, report, ):
    """*** Land Application Tools: Program polyx_phase ***
    *** Copyright 2005, Gamma Remote Sensing, v3.2 23-Sep-2005 uw/clw ***
    *** Extraction of phase for polygon regions of complex valued image ***
    
    usage: polyx_phase <data> <width> <polygon> <report>
    
    input parameters: 
      data     (input) input data file (fcomplex)
      width    width in pixels
      polygon  (input) polygon data file 
      report   (output) report on region statistics (text format)
    
    """

    args = [data, width, polygon, report]
    optargs = []
    cmd = makecmd('polyx_phase', args, optargs) 
    os.system(cmd)

def lin_comb_ref(f1, f2, constant, factor1, factor2, f_out, width, roff=None, loff=None, nr=None, nl=None, zflag=None):
    """*** Land Application Tools: lin_comb_ref.c ***
    *** Copyright 2013, Gamma Remote Sensing, v1.0 11-Feb-2013 clw/uw ***
    *** Calculate linear combination of 2 images (float) with option to add or subtract the scaled average of a specified reference region ***
    
    usage: lin_comb_ref <f1> <f2> <constant> <factor1> <factor2> <f_out> <width> [roff] [loff] [nr] [nl] [zflag]
    
    input parameters:
      f1         (input) input data file 1 (float)
      f2         (input) input data file 2,  reference scene if this mode selected by selecting a region with roff and loff (float)
      constant   constant value to add to output
      factor1    factor1 to multiply with f1
      factor2    factor2 to multiply with f2, or reference region average, set to -1.0 to subtract reference region value
      f_out      (output) output file (float)
      width      number of samples/row
      roff       range pixel offset to center of the reference region in f2, (default: -1, otherwise center of reference region)
      loff       line offset to center of the reference region
      nr         number of range pixels to average in the reference region (enter - for default = 16)
      nl         number of lines average in the reference region (enter - for default= 16)
      zflag      interpretation of 0.0 data values:
                   0: interpreted as missing value (default)
                   1: 0.0 is valid data value
    
    """

    args = [f1, f2, constant, factor1, factor2, f_out, width]
    optargs = [roff, loff, nr, nl, zflag]
    cmd = makecmd('lin_comb_ref', args, optargs) 
    os.system(cmd)

def m_alpha(s0, m, alpha, S_par, c1, c, c3, ):
    """*** Calculate m-alpha decomposition from Stokes parameters ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 3-Jul-2013 clw ***
    
    usage: m-alpha <s0> <m> <alpha> <S_par> <c1> <c2 <c3>
    
    input parameters:
      s0     (input) Stokes parameter for the total power in the H and V polarizations <|E_h|**2 + |E_v|**2>
      m      (input) degree of polarization: sqrt(s1**2 + s2**2 + s3**2)/s0 (float)
      alpha  (input) 0.5*atan(sqrt(s1**2 + s2**2)/s3):
                0.0  < alpha < PI/4 LCP
                PI/4 < alpha < PI/2 RCP
      S_par  (input) MLI image parameter file associated with the Stokes parameter data files
      c1     (output) s0 * m * (1 + cos(2*alpha))/2 (float)
      c2     (output) s0 * (1.0-m) depolarized component (float)
      c3     (output) s0 * m * (1 - cos(2*alpha))/2 (float)
    """

    args = [s0, m, alpha, S_par, c1, c, c3]
    optargs = []
    cmd = makecmd('m_alpha', args, optargs) 
    os.system(cmd)

def polcovar(SLC_1, SLC_2, SLC_3, SLC1_par, SLC2_par, SLC3_par, C, C_par, rlks, azlks, loff=None, nlines=None):
    """*** Calculate covariance matrix C elements from HH, HV, and VV SLC data ***
    *** Copyright 2013, Gamma Remote Sensing, v1.2 12-May-2013 clw ***
    
    usage: polcovar <SLC-1> <SLC-2> <SLC-3> <SLC1_par> <SLC2_par> <SLC3_par> <C> <C_par> <rlks> <azlks> [loff] [nlines]
    
    input parameters:
      SLC-1     (input) HH single-look complex image (scomplex or fcomplex format)
      SLC-2     (input) HV single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
      SLC-3     (input) VV single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
      SLC1_par  (input) HH SLC image parameter file of SLC-1
      SLC2_par  (input) HV SLC image parameter file of SLC-2 coregistered with SLC-1
      SLC3_par  (input) VV SLC image parameter file of SLC-3 coregistered with SLC-1
      C         (output) root file name of covariance matrix elements (e.g. scene_id): .c11, .c22, .c33 (float), .c12, .c13, .c23 (fcomplex)
      C_par     (output) MLI image parameter file associated with the covariance matrix element data files
      rlks      number of range looks used to calculate covariances
      azlks     number of azimuth looks used to calculate covariance
      loff      offset to starting line (default: 0)
      nlines    number of SLC lines to process (enter - for default: entire file)
    
    """

    args = [SLC_1, SLC_2, SLC_3, SLC1_par, SLC2_par, SLC3_par, C, C_par, rlks, azlks]
    optargs = [loff, nlines]
    cmd = makecmd('polcovar', args, optargs) 
    os.system(cmd)

def single_class_mapping(nfiles, f1, lt1, ut1, fn, ltn, utn, ras_out, width, start=None, nlines=None, pixav_x=None, pixav_y=None, LR=None):
    """*** Land Application Tools: Program single_class_mapping.c ***
    *** Copyright 2001, Gamma Remote Sensing, v3.3 21-Feb-2001 uw/clw ***
    
    *** Classification based on multiple input files using threshold table ***
    
    usage: single_class_mapping <nfiles> <f1> <lt1> <ut1>... <fn> <ltn> <utn> <ras_out> <width> [start] [nlines][pixav_x] [pixav_y] [LR]
    
    input parameters: 
      nfiles    number of input data files
      f1        (input) 1. input data file (float) 
      lt1       lower threshold for file 1
      ut1       upper threshold for file 1
      ...       (input) further input files
      fn        (input) last input data file (float) 
      ltn       lower threshold for last file
      utn       upper threshold for last file
      ras_out   (output) output 8-bit SUN raster BMP format image file (SUN raster file: *.ras,  BMP file: *.bmp)
      width     number of samples/row
      start     starting line (default=1)
      nlines    number of lines to display (default: 0=entire file)
      pixav_x   number of pixels to average in width (default=1)
      pixav_y   number of pixels to average in height (default=1)
      LR        LR=1 normal, LR=-1 mirror image (default=-1)
    
    """

    args = [nfiles, f1, lt1, ut1, fn, ltn, utn, ras_out, width]
    optargs = [start, nlines, pixav_x, pixav_y, LR]
    cmd = makecmd('single_class_mapping', args, optargs) 
    os.system(cmd)

def mt_lee_filt(im_list, ref_image, width, winsz, L_ref, L, cthres, out_list, ref_out=None, b_coeff=None, filt_num=None, msr=None, ctr=None):
    """*** Multi-temporal direction Lee adaptive filter ***
    *** Copyright 2014, Gamma Remote Sensing, v2.0 14-Feb-2014 clw ***
    
    usage: mt_lee_filt <im_list> <ref_image> <width> <winsz> <L_ref> <L> <cthres> <out_list> [ref_out] [b_coeff] [filt_num] [msr] [ctr]
    
    input parameters:
      im_list    (input) text file with names of co-registered float images including path, (enter - for none)
      ref_image  (input) reference image used to generate the filter weights
                 NOTE: the reference scene should have the same dimensions as the data files in the im_list
      width      number of samples/line
      winsz      size of the Lee filter window (valid values: 7, 13, 19)
      L_ref      effective number of looks (ENL) in the reference image (float)
      L          ENL of the images in the im_list used for local determination of the the MMSE weight for each image in the im_list (float)
                 NOTE: enter - to use the MMSE filter weight derived from the reference image for all images in the im_list
      cthres     directional contrast threshold to determine if the directional filter should be applied (0->4)(enter - for default: 1.500)
                 NOTE: setting cthres=0.0, forces the directional filter to be used at all times, setting cthres=4.0 blocks all directional filtering
      out_list   (input)  list of filtered output data files, number of entries in the im_list and out_list must match, (enter - for none)
      ref_out    (output) filtered reference image
      b_coeff    (output) MMSE weighting coefficient calculated from the mean to sigma ratio and L for each sample (float)
      filt_num   (output) selected structural filter number (0-->7) (byte)
      msr        (output) mean/sigma ratio where the mean is the local mean and sigma the local standard deviation of the intensity image in the filter window
      ctr        (output) directional contrast estimate used to determine if the directional filter is applied
    
    """

    args = [im_list, ref_image, width, winsz, L_ref, L, cthres, out_list]
    optargs = [ref_out, b_coeff, filt_num, msr, ctr]
    cmd = makecmd('mt_lee_filt', args, optargs) 
    os.system(cmd)

def takecut(data_in, width, report, mode, pos, pr_flag=None):
    """*** Land Application Tools: Program takecut.c ***
    *** Copyright 2004, Gamma Remote Sensing, v1.0 19-Nov-2004 clw ***
    *** Extract data values along a line or vertical cut of a float data file ***
    
    usage: takecut <data_in> <width> <report> <mode> <pos> [pr_flag]
    
    input parameters:
      data_in  (input) input data file (float)
      width    samples per row of data_in
      report   (output) text file containing extracted values
      mode     data extraction mode (default = 0)
                 0 : extract values along a line 
                 1 : extract values along a vertical cut
      pos      line or across-track sample number (starting with 0)
      pr_flag  print option:
                 0: print values at all positions (default)
                 1: print only positions with valid data (data != 0.0)
    
    """

    args = [data_in, width, report, mode, pos]
    optargs = [pr_flag]
    cmd = makecmd('takecut', args, optargs) 
    os.system(cmd)

def ras_to_rgb(red_channel, green_channel, blue_channel, ras_out, LR=None, null_flag=None):
    """*** Land Application Tools: Program ras_to_rgb.c ***
    *** Copyright 2011, Gamma Remote Sensing, v4.4 8-Sep-2011 uw/clw ***
    *** Combine 3 raster images (SUN or BMP) into a composite 24-bit RGB (red/green/blue) raster image ***
    
    usage: ras_to_rgb <red_channel> <green_channel> <blue_channel> <ras_out> [LR] [null_flag]
    
    input parameters: 
      <red channel>    (input) 8-bit SUN raster or BMP format image to be used for red channel of RGB
      <green channel>  (input) 8-bit SUN raster or BMP format image to be used for green channel of RGB
      <blue channel>   (input) 8-bit SUN raster or BMP format image to be for blue channel of RGB
      <ras_out>        (output) 24-bit SUN raster or BMP format image
      [LR]             image mirror flag:
                         1: normal (default)
                        -1: mirror image
      [null_flag]      zero value flag:
                          0: same as other data (default)
                          1: if one channel is 0, set to (0,0,0)
    
    """

    args = [red_channel, green_channel, blue_channel, ras_out]
    optargs = [LR, null_flag]
    cmd = makecmd('ras_to_rgb', args, optargs) 
    os.system(cmd)

def frame(data_in, data_out, width, format_flag, dx1, dx2, dy1, dy2, null_flag=None, all_flag=None, null_value=None, frame_value=None):
    """*** Land Application Tools: Program frame.c ***
    *** Copyright 2006, Gamma Remote Sensing, v3.5 22-Nov-2006 uw/clw/ts ***
    *** Replace values near image edges with user-defined value ***
    
    
    usage: frame <data_in> <data_out> <width> <format_flag> <dx1> <dx2> <dy1> <dy2> [null_flag] [all_flag] [null_value] [frame_value]
    
    input parameters:
      data_in      (input) input data file
      data_out     (output) data file
      width        width of input data file
      format_flag  input/output data format flag
                     0: float (REAL*4)
                     1: fcomplex (REAL*4, REAL*4)
                     2: int (INTEGER*4)
                     3: short (INTEGER*2)
                     4: unsigned char
                     5: SUN or BMP raster image (8, or 24 bit)
      dx1          left side frame width (pixels)
      dx2          right side frame width (pixels)
      dy1          top side frame width (lines)
      dy2          bottom side frame width (lines)
      null_flag    flag indicating if new frame should be added to the existing border around the image
                     0: absolute border
                     1: take into account existing image  border (default)
      all_flag     flag to specify classification of null values in SUN raster and BMP format files
                     0: require only one component of pixel RGB value to be NULL for NULL class
                     1: require that all components of pixel RGB value to be NULL for NULL class (default)
      null_value   NULL values
                     float, int, short, unsigned char: default = 0.0
                     fcomplex: default = 0.0 0.0
                     SUN/BMP raster image: default = 0 0 0
      frame_value  value to use for frame
                     float, int, short, unsigned char: default = 0.0
                     fcomplex: default = 0.0 0.0
                     SUN/BMP raster image default = 0 0 0
    
    """

    args = [data_in, data_out, width, format_flag, dx1, dx2, dy1, dy2]
    optargs = [null_flag, all_flag, null_value, frame_value]
    cmd = makecmd('frame', args, optargs) 
    os.system(cmd)

def ras_to_hsi(HUE, SATURATION, INTENSITY, ras_out, LR=None, cflg=None):
    """*** Land Application Tools: Program ras_to_hsi.c ***
    *** Copyright 2011, Gamma Remote Sensing, v4.3 8-Sep-2011 uw/clw ***
    
    *** Combine 3 raster images (SUN or BMP) into a HSI (hue/saturation/intensity) 24-bit RGB raster image ***
    
    usage: ras_to_hsi <HUE> <SATURATION> <INTENSITY> <ras_out> [LR] [cflg]
    
    input parameters: 
      <HUE>         input)  8-bit SUN or BMP format raster image to be used for HUE channel
      <SATURATION>  (input) 8-bit SUN or BMP format raster image to be used for SATURATION channel
      <INTENSITY>   (input) 8-bit SUN or BMP format raster image to be used for INTENSITY channel
      <ras_out>     (output) 24-bit SUN raster or BMP format file 
      [LR]          image mirror flag:
                      1: normal (default)
                     -1: mirror image
      [cflg]        color table flag
                      0: use (red+green+blue)/3 as value (default)
                      1: scale input values linearly to 255
    
    """

    args = [HUE, SATURATION, INTENSITY, ras_out]
    optargs = [LR, cflg]
    cmd = makecmd('ras_to_hsi', args, optargs) 
    os.system(cmd)

def comb_hsi(ras1, ras2, ras_out, ):
    """
    *** Land Application Tools: Program comb_hsi.c ***
    *** Copyright 2001, Gamma Remote Sensing, v1.7 20-Aug-2001 ts/uw/clw ***
    
    *** Combine the color (HUE and SATURATION) of the first raster image ***
    *** with the brightness (INTENSITY) of the second raster image ***
    
    usage: comb_hsi <ras1> <ras2> <ras_out>
    
    input parameters: 
      <ras1>      input 8-bit SUN raster/BMP image file (HUE+ SATURATION) (SUN raster: *.ras, BMP image format: *.bmp)
      <ras2>      input 8-bit SUN raster/BMP image file (INTENSITY) (SUN raster: *.ras, BMP image format: *.bmp)
      <ras_out>   output 24-bit SUN raster/BMP image file (INTENSITY) (SUN raster: *.ras, BMP image format: *.bmp)
    
    """

    args = [ras1, ras2, ras_out]
    optargs = []
    cmd = makecmd('comb_hsi', args, optargs) 
    os.system(cmd)

def wolf(SLC_1, SLC_2, SLC1_par, SLC2_par, J, J_par, rlks, azlks, loff=None, nlines=None):
    """*** Calculate Wolf coherence matrix from H and V polarized SLC images ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 5-Jul-2013 clw ***
    
    usage: wolf <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <J> <J_par> <rlks> <azlks> [loff] [nlines]
    
    input parameters:
      SLC-1     (input) H polarized (HH or VH) single-look complex image (scomplex or fcomplex format)
      SLC-2     (input) V polarized (VV or HV) single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
      SLC1_par  (input) SLC image parameter file of SLC-1
      SLC2_par  (input) SLC image parameter file of SLC-2 coregistered with SLC-1
      J         (output) root file name of Wolf coherence matrix elements (e.g. scene_id): .j11, .j22, (float), .j12 (fcomplex)
      J_par     (output) MLI image parameter file associated with the Wolf coherence matrix element data files
      rlks      number of range looks used to calculate coherence
      azlks     number of azimuth looks used to calculate coherence
      loff      offset to starting line (default: 0)
      nlines    number of SLC lines to process (enter - for default: entire file)
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, J, J_par, rlks, azlks]
    optargs = [loff, nlines]
    cmd = makecmd('wolf', args, optargs) 
    os.system(cmd)

def gamma_map(input_data, output_data, width, nlooks, bx, by=None):
    """*** Land Application Tools: Program gamma_map.c ***
    *** Copyright 2004, Gamma Remote Sensing, v1.0 19-April-2004 awi ***
    *** Gamma Map Filter (Lopes et al., 1990) ***
    
    usage: gamma_map <input_data> <output_data> <width> <nlooks> <bx> [by] 
    
    input parameters: 
      input_data   (input) input intensity file
      output_data  (output) output intensity file (filtered)
      width        number of samples/row
      nlooks       number of looks
      bx           filter size in x direction (number of cols) 
      by           filter size in y direction (number of rows) 
                   (default = bx)
    
    """

    args = [input_data, output_data, width, nlooks, bx]
    optargs = [by]
    cmd = makecmd('gamma_map', args, optargs) 
    os.system(cmd)

def float2uchar(infile, outfile, scale, exp, ):
    """*** Land Application Tools: Program float2uchar.c ***
    *** Copyright 2001, Gamma Remote Sensing, v3.4 23-Jan-2001 uw/clw ***
    
    *** Format transformation from float (4-byte) to unsigned char (1-byte) ***
    *** outfile = scale*infile^exp (for coherence use scale=255.0, exp=1.0) ***
    
    usage: float2uchar <infile> <outfile> <scale> <exp>
    
    input parameters: 
      infile   input file (float, 4 bytes/sample)
      outfile  output file (unsigned char, 1 byte/sample)
      scale    scale factor (default=1.0)
      exp      exponent (default=1.0)
    
    """

    args = [infile, outfile, scale, exp]
    optargs = []
    cmd = makecmd('float2uchar', args, optargs) 
    os.system(cmd)

def mt_lee_filt_cpx(cpx_list, ref_image, width, winsz, L_ref, cthres, out_list, ref_out=None, b_coeff=None, filt_num=None, msr=None, ctr=None):
    """*** Multi-temporal Lee directional adaptive filter for complex data ***
    *** Copyright 2014, Gamma Remote Sensing, v1.3 14-Feb-2014 clw ***
    
    usage: mt_lee_filt_cpx <cpx_list> <ref_image> <width> <winsz> <L_ref> <cthres> <out_list> [ref_out] [b_coeff] [filt_num] [msr] [ctr]
    
    input parameters:
      cpx_list    (input) text file with names of co-registered fcomplex data files including path, (enter - for none)
      ref_image  (input) reference intensity image used to generate the filter weights (float)
                 NOTE: the reference scene should have the same dimensions as the data files in the cpx_list
      width      number of samples/line
      winsz      size of the Lee filter window (valid values: 7, 13, 19)
      L_ref      effective number of looks in the reference image (float)
      cthres     directional contrast threshold to determine if the directional filter should be applied (0->4)(enter - for default: 1.500)
                 NOTE:  setting cthres=0.0, forces the directional filter to be used at all times, setting cthres=4.0 blocks all directional filtering
      out_list   (input)  list of filtered output data files, number of entries in the cpx_list and out_list must match, (enter - for none)
      ref_out    (output) filtered reference image
      b_coeff    (output) MMSE weighting coefficient calculated from the mean to sigma ratio and L for each sample in the reference image (float)
      filt_num   (output) selected structural filter number (0-->7) (byte)
      msr        (output) mean/sigma ratio where the mean is the local mean and sigma the local standard deviation of the intensity image in the filter window
      ctr        (output) directional contrast estimate used to determine if the directional filter is applied
    
    """

    args = [cpx_list, ref_image, width, winsz, L_ref, cthres, out_list]
    optargs = [ref_out, b_coeff, filt_num, msr, ctr]
    cmd = makecmd('mt_lee_filt_cpx', args, optargs) 
    os.system(cmd)

def float2short(infile, outfile, scale, exp, ):
    """*** Land Application Tools: Program float2short.c ***
    *** Copyright 2000, Gamma Remote Sensing, v3.3 21-June-2000 uw/clw ***
    
    *** float2short: convert floating point data to short integer format ***
    
    usage: float2short <infile> <outfile> <scale> <exp>
    
    input parameters: 
      infile   (input) input data file (float, 4 bytes/sample)
      outfile  (output) output file (short integers, 2 bytes/sample)
      scale    scale factor (default=1.0)
      exp      exponent (default=1.0)
      neg      flag to indicate if negative output values are allowed
               (default=0: not allowed, 1: allowed)
    
    output = scale*input^exp  (for SAR backscatter use scale=1000.0, exp=0.5)
    
    """

    args = [infile, outfile, scale, exp]
    optargs = []
    cmd = makecmd('float2short', args, optargs) 
    os.system(cmd)

def short2float(infile, outfile, scale=None, exp=None):
    """*** Land Application Tools: Program short2float.c ***
    *** Copyright 2004, Gamma Remote Sensing, v3.4 10-Aug-2004 uw/clw ***
    *** Convert short 2-byte integers to floating point format ***
    
    usage: short2float <infile> <outfile> [scale] [exp]
    
    input parameters: 
      infile   (input) input data file (short signed integers)
      outfile  (output) output data file (float)
      scale    scale factor (default: 1.0)
      exp      exponent (default: 1.0)
    
    NOTE: output = scale*input^exp  (for SAR backscatter use scale=0.000001, exp=2.0)
    
    """

    args = [infile, outfile]
    optargs = [scale, exp]
    cmd = makecmd('short2float', args, optargs) 
    os.system(cmd)

def frost(pwr1, pwr1_frost, width, fx=None, sx=None, power=None):
    """*** Land Application Tools: Program frost.c ***
    *** Copyright 2008, Gamma Remote Sensing, v4.1 5-Sep-2008 uw/awi ***
    
    *** Minimum mean square error filter (similar to Frost et al., 1982) ***
    
    usage: frost <.pwr1> <.pwr1_frost> <width> [fx] [sx] [power]
    
    input parameters: 
      pwr1        (input) intensity image (float)
      pwr1_frost  (output) filtered intensity image (float)
      width       number of samples/row
      fx          filter size (default=5)
      sx          window size used for statistics  (default=5)
      power       exponent applied to window size parameter alpha 
                  (default:power=1.0 for model by Frost et al., 1982)
    
    """

    args = [pwr1, pwr1_frost, width]
    optargs = [fx, sx, power]
    cmd = makecmd('frost', args, optargs) 
    os.system(cmd)

def poly_math(data_in, data_out, width, polygon, offset_scale=None, mode=None, offset=None, scale=None):
    """*** Land Application Tools: Program poly_math ***
    *** Copyright 2008, Gamma Remote Sensing, v1.1 5-Sep-2008 clw/uw ***
    *** Mathematical operations on polygonal regions (float data) ***
    
    usage: poly_math <data_in> <data_out> <width> <polygon> [offset_scale] [mode] [offset] [scale]
    
      input parameters:
      data_in       (input) input data file (float)
      data_out      (output) output data file (float)
      width         data line width in samples
      polygon       text file containing polygon vertices
      offset_scale  text file containing offset and scale factors for data in each polygon,
                    enter - to read offset and scale from the command line
      mode          data mode (0:copy all data, 1:copy data only within polygons, background set to 0.0)
      offset        constant offset applied for all data within polygons, (default=0.0)
      scale         scale factor for all output polygon (default=1.0)
    
    """

    args = [data_in, data_out, width, polygon]
    optargs = [offset_scale, mode, offset, scale]
    cmd = makecmd('poly_math', args, optargs) 
    os.system(cmd)

def restore_float(inpu, outpu, width, interpolation_limit=None):
    """*** Land Application Tools: Program restore_float.c ***
    *** Copyright 2008, Gamma Remote Sensing, v4.2 5-Sep-2008 uw/clw ***
    
    *** Interpolator to fill NULL values by interpolation ***
    
    usage: restore_float <input file> <output file> <width> [interpolation_limit]
    
    input parameters: 
      input file    (input) data containing 0.0 values (float) 
      output file   (output) data with 0.0 values replaced (float) 
      width         line width (samples) 
      interp_limit  max. gap that is interpolated (default:10 samples) 
    
    """

    args = [inpu, outpu, width]
    optargs = [interpolation_limit]
    cmd = makecmd('restore_float', args, optargs) 
    os.system(cmd)

def polyx(data, width, polygon, report, mode_flag=None):
    """*** Land Application Tools: Program polyx ***
    *** Copyright 2013, Gamma Remote Sensing, v3.4 18-Mar-2013 uw/clw ***
    *** Extraction of values from polygonal regions ***
    
    usage: polyx <data> <width> <polygon> <report> [mode_flag]
    
      input parameters: 
      data       (input) data file (float)
      width      image width in range pixels
      polygon    (input) polygon data file
      report     (output) report on region statistics (text format)
      mode_flag  report print mode
                   0: i, mean, stdev (linear scale) (default)
                   1: i, mean, stdev (dB scale)
                   2: i, mean (linear scale)
                   3: i, mean (dB scale)
                   4: mean    (linear scale)
                   5: mean    (dB scale)
                   6: stdev   (linear scale)
                   7: stdev   (dB scale)
    
    """

    args = [data, width, polygon, report]
    optargs = [mode_flag]
    cmd = makecmd('polyx', args, optargs) 
    os.system(cmd)

def temp_lin_var(data_tab, mean, stdev, width, waz=None, wr=None, wt_flag=None, zero_flag=None, loff=None, nlines=None, norm_pow=None):
    """*** Land Application Tools : Program  temp_lin_var ***
    *** Copyright 2012 Gamma Remote Sensing, v4.0 8-Mar-2013 clw/uw ***
    
    *** Calculation of temporal mean and variability (defined as stdev/pow(mean, norm_pow) of multiple data sets ***
    
    usage: temp_lin_var <data_tab> <mean> <stdev> <width> [waz] [wr] [wt_flag] [zero_flag] [loff] [nlines] [norm_pow]
    
    input parameters:
      data_tab   (input) single column list of the names of input data files (float)
      mean       (output) temporal mean (linear scale) (float)
      stdev      (output) temporal variability (stdev/pow(mean,norm_pow))(float)
      width      number of samples/row
      waz        spatial averaging filter width in azimuth pixels (default = 1.0)
      wr         spatial averaging filter width in range pixels (default = 1.0)
      wt_flag    weighting function
                   0: uniform (default)
                   1: linear
                   2: gaussian)
      zero_flag  zero_flag
                   0: data value 0.0 interpreted as missing data (default))
                   1: data interpreted
      loff       offset to starting line (default = 0)
      nlines     number of lines to process (0:entire file (default))
      norm_pow   temporal stdev is normalized with POW(mean,norm_pow)
                   0.0: normalized with 1.0
                   1.0: normalized with backscatter intensity (default)
    
    """

    args = [data_tab, mean, stdev, width]
    optargs = [waz, wr, wt_flag, zero_flag, loff, nlines, norm_pow]
    cmd = makecmd('temp_lin_var', args, optargs) 
    os.system(cmd)

def cc_monitoring(nfiles, f1, f2, ras_out, width, cc_thresh=None, start=None, nlines=None, pixav_x=None, pixav_y=None, LR=None):
    """*** Land Application Tools: Program cc_monitoring.c ***
    *** Copyright 2004, Gamma Remote Sensing, v3.3 23-Jun-2004 uw/clw ***
    
    *** Classification based on multiple input files using single threshold ***
    
    usage: cc_monitoring <nfiles> <f1> <f2> ... <ras_out> <width> [cc_thresh] [start] [nlines] [pixav_x] [pixav_y] [LR] 
    
    input parameters: 
      nfiles     number of input data files
      f1         (input) 1. input data file (float) 
      f2         (input) 2. input data file (float) 
      ...        (input) further input files
      ras_out    (output) output 8-bit image file (SUN raster: *.ras, BMP image format: *.bmp)
      width      number of samples/row
      cc_thresh  classification threshold (default=0.5) 
      start      starting line (default=1)
      nlines     number of lines to display (default: 0=entire file)
      pixav_x    number of pixels to average in width (default=1)
      pixav_y    number of pixels to average in height (default=1)
      LR         LR=1 normal, LR=-1 mirror image (default=-1)
    
    """

    args = [nfiles, f1, f2, ras_out, width]
    optargs = [cc_thresh, start, nlines, pixav_x, pixav_y, LR]
    cmd = makecmd('cc_monitoring', args, optargs) 
    os.system(cmd)

def m_chi(s0, m, s2chi, S_par, c1, c, c3, ):
    """*** Calculate m-chi decomposition from Stokes parameters ***
    *** Copyright 2013, Gamma Remote Sensing, v1.2 4-Jun-2013 clw ***
    
    usage: m-chi <s0> <m> <s2chi> <S_par> <c1> <c2 <c3>
    
    input parameters:
      s0     (input) Stokes parameter for the total power in the H and V polarizations <|E_h|**2 + |E_v|**2>
      m      (input) degree of polarization: sqrt(s1**2 + s2**2 + s3**2)/s0 (float)
      s2chi  (input) sin(2*chi) = s3/(m*s0), 2*chi: latitude of the Stokes vector on the Poincare sphere
                sin(2*chi) > 0  measured field is LCP
                sin(2*chi) < 0  measured field is RCP
      S_par  (input) MLI image parameter file associated with the Stokes parameter data files
      c1     (output) s0 * m * (1 + sin(2*chi))/2 (float)
      c2     (output) s0 * (1.0-m) depolarized component (float)
      c3     (output) s0 * m * (1 - sin(2*chi))/2 (float)
    """

    args = [s0, m, s2chi, S_par, c1, c, c3]
    optargs = []
    cmd = makecmd('m_chi', args, optargs) 
    os.system(cmd)

def product(data_1, data_2, product, width, bx=None, by=None, wgt_flag=None):
    """*** Land Application Tools: product ***
    *** Copyright 2013, Gamma Remote Sensing, v4.5 3-Sep-2013 uw/clw/of ***
    *** Calculate product of two images: (image 1)*(image 2) ***
    
    usage: product <data_1> <data_2> <product> <width> [bx] [by] [wgt_flag]
    
    input parameters:
      data_1   (input) data file 1 (float)
      data_2   (input) data file 2 (float)
      product  (output) product data_1 * data_2 (float)
      width    number of samples/row
      bx       box size in range for averaging (before multiplication, default = 5)
      by       box size in azimuth for averaging (before multiplication, default = bx
      wgt_flg  weighting mode
                 0: no weighting (default)
                 1: linear weighting
                 2: gaussian weighting
    
    """

    args = [data_1, data_2, product, width]
    optargs = [bx, by, wgt_flag]
    cmd = makecmd('product', args, optargs) 
    os.system(cmd)

def histogram(data_in, width, polygon, histograms, mean_stdev, start, stop, lin_log=None):
    """*** Land Application Tools: Program histogram.c ***
    *** Copyright 2013, Gamma Remote Sensing, v3.4 19-Mar-2013 clw/uw ***
    
    *** Calculation of histograms for polygon regions for files of floats ***
    
    usage: histogram <data_in> <width> <polygon> <histograms> <mean/stdev> <start> <stop> [lin/log]
    
    input parameters: 
      data_in     (input) correlations (float)
      width       image width in range pixels
      polygon     (input) polygon data file
      histograms  (output)calculated histograms
      mean/stdev  calculated mean/stdev
      start       start value for first histogram class
      stop        stop value for last histogram class
      lin/log       0: linear scaling used to select classes (default=0)
                    1: logarithmic scaling (dB-values) used
    
    """

    args = [data_in, width, polygon, histograms, mean_stdev, start, stop]
    optargs = [lin_log]
    cmd = makecmd('histogram', args, optargs) 
    os.system(cmd)

def m_delta(s0, m, delta, S_par, c1, c, c3, ):
    """*** Calculate m-delta decomposition from Stokes parameters ***
    *** Copyright 2013, Gamma Remote Sensing, v1.0 6-Jun-2013 clw ***
    
    usage: m-delta <s0> <m> <delta> <S_par> <c1> <c2 <c3>
    
    input parameters:
      s0     (input) Stokes parameter for the total power in the H and V polarizations <|E_h|**2 + |E_v|**2>
      m      (input) degree of polarization: sqrt(s1**2 + s2**2 + s3**2)/s0 (float)
      delta  (input) relative H-V phase: atan(s3/s2):
      S_par  (input) MLI image parameter file associated with the Stokes parameter data files
      c1     (output) s0 * m * (1 + sin(delta))/2 (float)
      c2     (output) s0 * (1.0-m) depolarized component (float)
      c3     (output) s0 * m * (1 - sin(delta))/2 (float)
    """

    args = [s0, m, delta, S_par, c1, c, c3]
    optargs = []
    cmd = makecmd('m_delta', args, optargs) 
    os.system(cmd)

def temp_log_var(data_tab, mean, stdev, width, waz=None, wr=None, wt_flag=None, zero_flag=None, loff=None, nlines=None, norm_pow=None):
    """*** Land Application Tools : Program  temp_log_var ***
    *** Copyright 2012 Gamma Remote Sensing, v1.8 18-Jan-2012 clw/uw ***
    
    *** Calculation of log-mean and log-stdev (temporal variability) of multiple data sets ***
    
    usage: temp_log_var <data_tab> <mean> <stdev> <width> [waz] [wr] [wt_flag] [zero_flag] [loff] [nlines] [norm_pow]
    
    input parameters:
      data_tab   (input) single column list of the names of input data files (float)
      mean       (output) temporal mean in dB (float)
      stdev      (output) temporal variability, standard deviation in dB (float))
      width      number of samples/row
      waz        spatial averaging filter width in azimuth pixels (default: 1.0)
      wr         spatial averaging filter width in range pixels (default: 1.0)
      wt_flag    weighting function
                   0: uniform (default)
                   1: linear
                   2: gaussian)
      zero_flag  zero_flag
                   0: data value 0.0 interpreted as missing data (default))
                   1: data interpreted
      loff       offset to starting line (default: 0)
      nlines     number of lines to process, 0:entire file (default)
    
    """

    args = [data_tab, mean, stdev, width]
    optargs = [waz, wr, wt_flag, zero_flag, loff, nlines, norm_pow]
    cmd = makecmd('temp_log_var', args, optargs) 
    os.system(cmd)

def ratio(pwr1, pwr2, ratio, width, bx=None, by=None, wgt_flag=None):
    """*** Land Application Tools: ratio.c ***
    *** Copyright 2012, Gamma Remote Sensing, v4.1 14-Sep-2012 uw/clw ***
    *** Estimate ratio betweeen images: (image 1)/(image 2) ***
    
    usage: ratio <pwr1> <pwr2> <ratio> <width> [bx] [by] [wgt_flag]
    
    input parameters: 
      pwr1     (input) intensity image of first scene
      pwr2     (input) intensity image of second scene
      ratio    (output) ratio file 
      width    number of samples/row
      bx       range averaging width (before ratioing, default = 5)
      by       azimuth averaging height (before ratioing, default = bx)
      wgt_flg  weighting flag
                 0: no weighting function applied (default)
                 1: linear weighting function
                 2: Gaussian weighting function
    
    """

    args = [pwr1, pwr2, ratio, width]
    optargs = [bx, by, wgt_flag]
    cmd = makecmd('ratio', args, optargs) 
    os.system(cmd)

def stokes(SLC_1, SLC_2, SLC1_par, SLC2_par, S, S_par, rlks, azlks, loff=None, nlines=None):
    """*** Calculate Stokes parameters from SLC images with H and V receive polarization and same transmit polarization ***
    *** Copyright 2013, Gamma Remote Sensing, v1.4 6-Jun-2013 clw ***
    
    usage: stokes <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <S> <S_par> <rlks> <azlks> [loff] [nlines]
    
    input parameters:
      SLC-1     (input) HH, VV, RH, LH single-look complex image (scomplex or fcomplex format)
      SLC-2     (input) HV, VH, RV, LV single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
                NOTE: 1. SLC images must be a co-pol and cross-pol pair with the same transmit polarization
                         and linear receive polarization (HH, HV), (VV, VH), (RH, RV), or (LH, LV)
                      2. SCOMPLEX SLC data are scaled by .001 to increase dynamic range
      SLC1_par  (input) SLC image parameter file of SLC-1
      SLC2_par  (input) SLC image parameter file of SLC-2 coregistered with SLC-1
      S         (output) root file name of 4 Stokes parameter files with extensions .s0, .s1, .s2, .s3, (float)
                NOTE: expressed in the H,V polarization basis, BSA convention, the Stokes parameters given by:
                   s0: <|E_H|**2 + |E_V|**2>
                   s1: <|E_H|**2 - |E_V|**2>
                   s2: 2Re<E_H E_V*>
                   s3: -2Im<E_H E_V*>
      S_par     (output) MLI image parameter file associated with the Stokes parameter data files (enter - for none)
      rlks      number of range looks used to calculate Stokes parameters
      azlks     number of azimuth looks used to calculate Stokes parameters
      loff      offset to starting line (default: 0)
      nlines    number of SLC lines to process (enter - for default: entire file)
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, S, S_par, rlks, azlks]
    optargs = [loff, nlines]
    cmd = makecmd('stokes', args, optargs) 
    os.system(cmd)

# def validate(ras1, nclass1, class1[1],,  class1[n], ras2, nclass2, class2[1],,  class2[n], ras_out, poly, matrix, matrix_flag=None, accuracy_flag=None):
#     """
#     *** Land Application Tools: Program validate.c ***
#     *** Copyright 2013, Gamma Remote Sensing, v1.4 23-Aug-2013 ts/uw/clw ***
#     *** Validate a landuse map with an available landuse inventory (8-bit SUN raster/BMP image files) ***
#     *** for each class combination (all) or for a 2 classes combination ***
    
#     usage: validate <ras1> <nclass1> <class1[1]> <...> <class1[n]> <ras2> <nclass2> <class2[1]> <...> <class2[n]> <ras_out> <poly> <matrix> [matrix_flag] [accuracy_flag]
    
#     input parameters:
#       rasf_map        (input) first input data file with INSAR landuse map (SUN raster: *.ras, BMP: *.bmp)
#       nclass1	      number of classes to use for first input file (max. 16), 0 for all
#       class1[1]       value for first class
#       class1[2]       value for second class
#       ...	      further classes of first input file
#       class1[n]       value for last class
#       rasf_inventory  (input) second input data file with available landuse inventory (SUN raster: *.ras, BMP: *.bmp)
#       nclass2	      number of classes to use for second input file (max. 16), 0 for all
#       class2[1]       value for first class
#       class2[2]       value for second class
#       ...	      further classes of second input file
#       class2[n]       value for last class
#       ras_out	      (output) 8-bit raster image file (SUN raster: *.ras, BMP: *.bmp)
#       poly	      (input) polygon data file (input)
#       matrix	      (output) confusion matrix (output text file)
#       matrix_flag     confusion matrix output flag
#     			0: number of points
#     			1: normalized to 1 (default)
#     			2: normalized to 100 (percent)
#       accuracy_flag   compute user/prod./over. accuracies (1=YES, 0=NO, default=YES)
    
#     """

#     args = [ras1, nclass1, class1[1], , class1[n], ras2, nclass2, class2[1], , class2[n], ras_out, poly, matrix]
#     optargs = [matrix_flag, accuracy_flag]
#     cmd = makecmd('validate', args, optargs) 
#     os.system(cmd)

def lee(input_data, output_data, width, nlooks, bx, by=None):
    """*** Land Application Tools: Program lee.c ***
    *** Copyright 2008, Gamma Remote Sensing, v1.1 5-Sep-2008 awi ***
    *** Lee Filter (Lee, 1980) ***
    
    usage: lee <input_data> <output_data> <width> <nlooks> <bx> [by] 
    
    input parameters: 
      input_data   (input) input intensity file
      output_data  (output) output intensity file (filtered)
      width        number of samples/row
      nlooks       number of looks
      bx           filter size in x direction (number of cols) 
      by           filter size in y direction (number of rows) 
                   (default = bx)
    
    """

    args = [input_data, output_data, width, nlooks, bx]
    optargs = [by]
    cmd = makecmd('lee', args, optargs) 
    os.system(cmd)

def multi_stat(im_list, width, im_out, mode, rank, nmin=None):
    """*** Sort MLI image values in a stack and specify the rank of the output image ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 30-Oct-2012 clw ***
    
    usage: multi_stat <im_list> <width> <im_out> <mode> <rank> [nmin]
    
    input parameters:
      im_list  (input) text file with names of co-registered float images including path
      width    number of samples/line
      im_out   (output) output filtered image (float)
      mode     data selection mode:
                 0: average
                 1: median
                 2: rank relative to minimum, rank=1 for minimum
                 3: rank relative to maximum, rank=1 for maximum
                 4: percentile, 0 (minimum) --> 100 (maximum)
      rank     rank value in modes 2 and 3, percentile in mode 4 (0-->100), ignored for average or median
      nmin     minimum number of valid image values required to sort (default: nfiles/2)
    
    """

    args = [im_list, width, im_out, mode, rank]
    optargs = [nmin]
    cmd = makecmd('multi_stat', args, optargs) 
    os.system(cmd)

def multi_class_mapping(nfiles, f1, f2, fn, classf, ras_out, width, start=None, nlines=None, pixav_x=None, pixav_y=None, LR=None, color_flag=None):
    """*** Land Application Tools: Program multi_class_mapping.c ***
    *** Copyright 2005, Gamma Remote Sensing, v3.4 3-May-2005 uw/clw ***
    
    *** Classification based on multiple input files using threshold table ***
    
    usage: multi_class_mapping <nfiles> <f1> <f2> ... <fn> <classf> <ras_out> <width> [start] [nlines] [pixav_x] [pixav_y] [LR] [color_flag]
    
    input parameters:
      nfiles      number of input data files
      f1          (input) 1. input data file (float)
      f2          (input) 2. input data file (float)
      ...         (input) further input files
      fn          (input) last input data file (float)
      classf      (input) decison thresholds (text format)
                  each line contains thresholds for a single class for each input file lower and upper thresholds are requested
      ras_out     (output) output 8-bit SUN raster, BMP format image file (SUN raster file: *.ras,  BMP file: *.bmp)
      width       number of samples/row
      start       starting line (default=1)
      nlines      number of lines to display (default: 0=entire file)
      pixav_x     number of pixels to average in width (default=1)
      pixav_y     number of pixels to average in height (default=1)
      LR          LR=1 normal, LR=-1 mirror image (default=-1)
      color_flag  flag indicating if a color table is defined in file classf
                  (default=0: no color table defined; 1: color table defined)
    
    """

    args = [nfiles, f1, f2, fn, classf, ras_out, width]
    optargs = [start, nlines, pixav_x, pixav_y, LR, color_flag]
    cmd = makecmd('multi_class_mapping', args, optargs) 
    os.system(cmd)

def takethat(data_in, width, positions, report, mode=None, zero_flag=None, nn_flag=None, print_flag=None):
    """*** Land Application Tools: Program takethat.c ***
    *** Copyright 2004, Gamma Remote Sensing, v2.5 9-Apr-2004 ts/clw ***
    *** Extraction of data values along profiles, in polygon regions, or from indicated positions ***
    
    usage: takethat <data_in> <width> <positions> <report> [mode] [zero_flag] [nn_flag] [print_flag]
    
    input parameters:
      data_in     (input) input data file (float)
      width       samples per row of data_in
      positions   (input) text file containing list of positions: column number, row number
      report      (output) text file containing extracted values
      mode        data extraction mode (default = 0)
                  	0 : extract values along the profile connecting the indicated positions
                  	1 : extract values in the polygon region specified by the indicated positions
                  	2 : extract values at the indicated positions
      zero_flag   interpret 0.0 as missing value
                  (default=0: 0.0 indicates missing value, 1: 0.0 is valid value)
      nn_flag     resampling algorithm flag in the case of extraction along a profile
                  (default=0: spline interpolation, 1: nearest neighbor)
      print_flag  print flag (default=0:print values at all positions  1:print only positions with valid data)
    
    """

    args = [data_in, width, positions, report]
    optargs = [mode, zero_flag, nn_flag, print_flag]
    cmd = makecmd('takethat', args, optargs) 
    os.system(cmd)

def enh_lee(input_data, output_data, width, nlooks, damp, bx, by=None):
    """*** Land Application Tools: Program enh_lee.c ***
    *** Copyright 2008, Gamma Remote Sensing, v1.2 20-Nov-2008 awi ***
    
    *** Enhanced Lee Filter (Lopes et al., 1990) ***
    usage: enh_lee <input_data> <output_data> <width> <nlooks> <damp> <bx> [by] 
    
    input parameters: 
      input_data   (input) input intensity file
      output_data  (output) output intensity file (filtered)
      width        number of samples/row
      nlooks       number of looks
      damp         damping constant of filter
      bx           filter size in x direction (number of cols) 
      by           filter size in y direction (number of rows) 
                   (default = bx)
    
    """

    args = [input_data, output_data, width, nlooks, damp, bx]
    optargs = [by]
    cmd = makecmd('enh_lee', args, optargs) 
    os.system(cmd)

def mask_class(class_map, file_in, file_out, format_flag, LR, selection_flag, n_class, class_1, class_n, null_value=None):
    """*** Land Application Tools: Program mask_class.c ***
    *** Copyright 2013, Gamma Remote Sensing, v4.6 6-May-2013 uw/clw ***
    *** Set image data values belonging to a single class or set of classes or the complement to a user-defined value ***
    
    usage: mask_class <class_map> <file_in> <file_out> <format_flag> <LR> <selection_flag> <n_class> <class_1> ... <class_n> [null_value]
    
    input parameters: 
      <class_map>      class map (8-bit SUN raster(*.ras), BMP (*.bmp) class map file)
      <file_in>        input data file
      <file_out>       output data file
      <format_flag>    input/output data format flag
                       (0: float, 1: fcomplex, 2: SUN (*.ras) or BMP (*.bmp) raster image, 3: unsigned char )
      <LR>             left/right sense of class_mask image file 1:normal, -1:mirror image
      <selection_flag> 1: mask is the region belonging to the selected classes
                      -1: mask is the complement of the region belonging to the selected classes
      <n_class>        number of classes to consider, up to 20 classes can be defined
      <class_1>        class map value 1
      ...              <n_class> class map values must be entered
      [class_n]        class map value n
      [null_value]     value written within the masked region:
                         float: default = 0.0
                         complex specify real and imaginary components: default = 0.0 0.0
                         SUN, BMP raster image specify RGB: default RGB = 0 0 0
                         unsigned char: default = 0
    
    """

    args = [class_map, file_in, file_out, format_flag, LR, selection_flag, n_class, class_1, class_n]
    optargs = [null_value]
    cmd = makecmd('mask_class', args, optargs) 
    os.system(cmd)

def lin_comb_cpx(nfiles, f1, f2, constant_r, constant_i, factor1_r, factor1_i, factor2_r, factor2_i, f_out, width, start=None, nlines=None, pixav_x=None, pixav_y=None, zero_flag=None):
    """*** Land Application Tools: lin_comb_cpx.c ***
    *** Copyright 2012, Gamma Remote Sensing, v1.3 18-Jan-2012 aw/uw/clw ***
    *** Calculate linear combination of fcomplex data files ***
    
    usage: lin_comb_cpx <nfiles> <f1> <f2> <...> <constant_r> <constant_i> <factor1_r> <factor1_i> <factor2_r> <factor2_i> <...> <f_out> <width> [start] [nlines][pixav_x] [pixav_y] [zero_flag]
    
    input parameters: 
      nfiles      number of input data files
      f1          (input) 1. input data file (fcomplex)
      f2          (input) 2. input data file (fcomplex)
      ...         (input) further input files
      constant_r  constant value (real part) to add to output
      constant_i  constant value (imaginary part) to add to output
      factor1_r   factor1 (real part) to multiply with f1
      factor1_i   factor1 (imaginary part) to multiply with f1
      factor2_r   factor2 (real part) to multiply with f2
      factor2_i   factor2 (imaginary part) to multiply with f2
      ...         factors for further input files
      f_out       (output) output file (fcomplex)
      width       number of samples/row
      start       starting line (default=1)
      nlines      number of lines to display (default: 0=entire file)
      pixav_x     number of pixels to average in width (default=1)
      pixav_y     number of pixels to average in height (default=1)
      zero_flag   interpretation of 0.0 values:
                    0: interpreted as missing value (default)
                    1: 0.0 is valid data value
    
    """

    args = [nfiles, f1, f2, constant_r, constant_i, factor1_r, factor1_i, factor2_r, factor2_i, f_out, width]
    optargs = [start, nlines][pixav_x, pixav_y, zero_flag]
    cmd = makecmd('lin_comb_cpx', args, optargs) 
    os.system(cmd)

def drawthat(ras_in, ras_out, pt_list, mode, r=None, g=None, b=None, xs=None, zflg=None):
    """*** Land Application Tools Program drawthat.c ***
    *** Copyright 2012, Gamma Remote Sensing, v2.2 18-Jan-2012 clw ***
    *** Draw an arc, polygon, cross, or point on a SUN/BMP raster image ***
    
    usage: drawthat <ras_in> <ras_out> <pt_list> <mode> [r] [g] [b] [xs] [zflg]
    
    input parameters:
      ras_in   (input) raster image (SUN raster or BMP format)
      ras_out  (output) raster image with crosses drawn at points (SUN raster of BMP format)
      pt_list  (input) point list (text)
      mode     drawing mode (default = 0) 
                 0: draw an arc connecting points
                 1: draw a filled polygon specified by the points
                 2: draw cross
      r        line color value red   (0 --> 255) default: 255
      g        line color value green (0 --> 255) default: 255
      b        line color value blue  (0 --> 255) default: 0
      xs       size of cross in pixels, set to 1 for single points (default: 1)
      zflg     zero image flag (default=0:retain image values  1:set all image values to 0 except points)
    
    """

    args = [ras_in, ras_out, pt_list, mode]
    optargs = [r, g, b, xs, zflg]
    cmd = makecmd('drawthat', args, optargs) 
    os.system(cmd)

def linear_to_dB(data_in, data_out, width, inverse_flag=None, null_value=None):
    """*** Land Application Tools: Program linear_to_dB.c ***
    *** Copyright 2008, Gamma Remote Sensing, v3.2 5-Sep-2008 uw/clw ***
    *** Conversion of data between linear and dB scale ***
    
    usage: linear_to_dB <data_in> <data_out> <width> [inverse_flag] [null_value]
    
    input parameters:
      data_in       (input) image data file (float)
      data_out      (output) output image data file (float)
      width         number of samples/line
      inverse_flag  flag indicating direction of conversion (default=0)
                      0: converts linear scale (input) to dB scale (output)
                      1: converts dB values (input) to linear scale (output))
      null_value    null value to use for values that are <= 0 in linear scale, default=0.0)
    
    """

    args = [data_in, data_out, width]
    optargs = [inverse_flag, null_value]
    cmd = makecmd('linear_to_dB', args, optargs) 
    os.system(cmd)

def polcoh(SLC_1, SLC_2, SLC_3, SLC1_par, SLC2_par, SLC3_par, T, T_par, rlks, azlks, loff=None, nlines=None):
    """*** Calculate coherence matrix T elements from Pauli decomposition alpha, beta, gamma ***
    *** Copyright 2013, Gamma Remote Sensing, v1.3 5-Jul-2013 clw ***
    
    usage: polcoh <SLC-1> <SLC-2> <SLC-3> <SLC1_par> <SLC2_par> <SLC3_par> <T> <T_par> <rlks> <azlks> [loff] [nlines]
    
    input parameters:
      SLC-1     (input) alpha single-look complex image (scomplex or fcomplex format)
      SLC-2     (input) beta single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
      SLC-3     (input) gamma single-look complex image coregistered with SLC-1 (scomplex or fcomplex format)
      SLC1_par  (input) SLC image parameter file of SLC-1
      SLC2_par  (input) SLC image parameter file of SLC-2 coregistered with SLC-1
      SLC3_par  (input) SLC image parameter file of SLC-3 coregistered with SLC-1
      T         (output) root file name of coherence matrix elements (e.g. scene_id): .t11, .t22, .t33 (float), .t12, .t13, .t23 (fcomplex)
      T_par     (output) MLI image parameter file associated with the coherence matrix element data files
      rlks      number of range looks used to calculate coherences
      azlks     number of azimuth looks used to calculate coherence
      loff      offset to starting line (default: 0)
      nlines    number of SLC lines to process (enter - for default: entire file)
    
    """

    args = [SLC_1, SLC_2, SLC_3, SLC1_par, SLC2_par, SLC3_par, T, T_par, rlks, azlks]
    optargs = [loff, nlines]
    cmd = makecmd('polcoh', args, optargs) 
    os.system(cmd)

def ras_clist(clist, ras_in, ras_out, xsf=None, ysf=None, r=None, g=None, b=None, xs=None, zflg=None, mflg=None):
    """*** DIFF Program ras_clist.c ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 23-Aug-2013 clw ***
    *** Draw point list locations on a SUN/BMP raster image ***
    
    usage: ras_clist <clist> <ras_in> <ras_out> [xsf] [ysf] [r] [g] [b] [xs] [zflg] [mflg]
    
    input parameters:
      clist    (input) list of x,y pixel coordinates (text format)
      ras_in   (input) raster image (SUN raster or BMP format)
      ras_out  (output) raster image with crosses drawn at points (SUN raster of BMP format)
      xsf      horizontal scale factor for image relative to the coordinates (default: 1.0)
      ysf      vertical scale factor for image relative to the coordinates (default: 1.0)
      r        line color value red   (0 --> 255) default: 255
      g        line color value green (0 --> 255) default: 255
      b        line color value blue  (0 --> 255) default: 0
      xs       size of cross in pixels, set to 1 for single points (default: 1)
      zflg     zero image flag (default=0:retain image values  1:set all image values to 0 except crosses)
    """

    args = [clist, ras_in, ras_out]
    optargs = [xsf, ysf, r, g, b, xs, zflg, mflg]
    cmd = makecmd('ras_clist', args, optargs) 
    os.system(cmd)

def look_vector(SLC_par, OFF_par, DEM_par, DEM, lv_theta, lv_phi, ):
    """*** Calculation of SAR look-vector direction (pointing towards SAR) in map geometry ***
    *** Copyright 2005, Gamma Remote Sensing, v1.3, 6-Apr-2005 uw/ts/clw ***
    
    usage: look_vector <SLC_par> <OFF_par> <DEM_par> <DEM> <lv_theta> <lv_phi>
    
    input parameters:
      SLC_par   (input) parameter file of SLC
      OFF_par   (input) ISP offset/interferogram parameter file (enter - for SLC or MLI data)
      DEM_par   (input) DEM/MAP parameter file
      DEM       (input) DEM data file or constant height value
      lv_theta  (output) SAR look-vector elevation angle (at each map pixel)
                  lv_theta: PI/2 -> up  -PI/2 -> down
      lv_phi    (output) SAR look-vector orientation angle at each map pixel
                  lv_phi: 0 -> East  PI/2 -> North
    
    """

    args = [SLC_par, OFF_par, DEM_par, DEM, lv_theta, lv_phi]
    optargs = []
    cmd = makecmd('look_vector', args, optargs) 
    os.system(cmd)

def gc_map(MLI_par, OFF_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table, lat_ovr=None, lon_ovr=None, sim_sar=None, u=None, v=None, inc=None, psi=None, pix=None, ls_map=None, frame=None, ls_mode=None, r_ovr=None):
    """*** Calculate lookup table and DEM related products for terrain-corrected geocoding ***
    *** Copyright 2012, Gamma Remote Sensing, v5.7 4-Nov-2012 clw/uw/of ***
    
    usage: gc_map <MLI_par> <OFF_par> <DEM_par> <DEM> <DEM_seg_par> <DEM_seg> <lookup_table> [lat_ovr] [lon_ovr] [sim_sar] [u] [v] [inc] [psi] [pix] [ls_map] [frame] [ls_mode] [r_ovr]
    
    input parameters:
      MLI_par         (input) ISP MLI or SLC image parameter file (slant range geometry)
      OFF_par         (input) ISP offset/interferogram parameter file (enter - if geocoding SLC or MLI data)
      DEM_par         (input) DEM/MAP parameter file
      DEM             (input) DEM data file (or constant height value)
      DEM_seg_par     (input/output) DEM/MAP segment parameter file used for output products
    
      NOTE: If the DEM_seg_par already exists, then the output DEM parameters will be read from this file
            otherwise they are estimated from the image data.
    
      DEM_seg         (output) DEM segment used for output products, interpolated if lat_ovr > 1.0  or lon_ovr > 1.0
      lookup_table    (output) geocoding lookup table (fcomplex)
      lat_ovr         latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr         longitude or easting output DEM oversampling factor (enter - for default: 1.0)
      sim_sar         (output) simulated SAR backscatter image in DEM geometry
      u               (output) zenith angle of surface normal vector n (angle between z and n)
      v               (output) orientation angle of n (between x and projection of n in xy plane)
      inc             (output) local incidence angle (between surface normal and look vector)
      psi             (output) projection angle (between surface normal and image plane normal)
      pix             (output) pixel area normalization factor
      ls_map          (output) layover and shadow map (in map projection)
      frame           number of DEM pixels to add around area covered by SAR image (enter - for default = 8)
      ls_mode         output lookup table values in regions of layover, shadow, or DEM gaps (enter - for default)
                        0: set to (0.,0.)
                        1: linear interpolation across these regions (default)
                        2: actual value
                        3: nn-thinned
      r_ovr           range over-sampling factor for nn-thinned layover/shadow mode(enter - for default: 2.0)
    
    NOTE: enter - as output filename to avoid creating the corresponding output file
    
    """

    args = [MLI_par, OFF_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table]
    optargs = [lat_ovr, lon_ovr, sim_sar, u, v, inc, psi, pix, ls_map, frame, ls_mode, r_ovr]
    cmd = makecmd('gc_map', args, optargs) 
    os.system(cmd)

def diff_ls_unw(int_1, unw_2, DIFF_par, diff_int, int_type=None, ph_flag=None):
    """*** Three-pass differential interferometry ***
    *** Copyright 2008, Gamma Remote Sensing, v2.3 26-Nov-2008 clw/uw ***
    
    usage: diff_ls_unw <int-1> <unw-2> <DIFF_par> <diff_int> [int_type] [ph_flag]
    
    input parameters:
      int-1     (input) complex or unwrapped interferogram 1 file name
      unw-2     (input) unwrapped interferogram 2 file name
      DIFF_par  (input) interferogram/offset processing parameters
      diff_int  (output)  phase difference of interferograms: (int-1 - scaled(unw-2)) file name
      int_type  int-1 type: 0: unwrapped phase, 1:complex interferogram (default: 0)
      ph_flag   phase conjugation flag: 0:normal, 1:conjugate phase (default: 0)
    
    """

    args = [int_1, unw_2, DIFF_par, diff_int]
    optargs = [int_type, ph_flag]
    cmd = makecmd('diff_ls_unw', args, optargs) 
    os.system(cmd)

def sub_phase(int_1, unw_2, DIFF_par, diff_int, int_1_type, mode=None):
    """*** Add or subtract unwrapped phase from an interferogram ***
    *** Copyright 2013, Gamma Remote Sensing, v2.5 14-Mar-2013 clw/uw ***
    
    usage: sub_phase <int-1> <unw-2> <DIFF_par> <diff_int> <int-1_type> [mode]
    
    input parameters:
      int-1       (input) complex or unwrapped phase interferogram 1
      unw-2       (input) unwrapped phase interferogram 2 (float)
      DIFF_par    (input) DIFF/GEO parameter file
      diff_int    (output) differential interferogram
      int-1_type  int-1 interferogram type:
                    0: unwrapped phase (float)
                    1: complex interferogram (fcomplex)
      mode        subtract or add unwrapped phase:
                    0: subtract unw-2 phase from int-1 (default)
                    1: add unw-2 phase to int-1
    
    """

    args = [int_1, unw_2, DIFF_par, diff_int, int_1_type]
    optargs = [mode]
    cmd = makecmd('sub_phase', args, optargs) 
    os.system(cmd)

def WSS_interp(SLC_1, SLC_2, SLC1_par, SLC2_par, DIFF_par, SLC_2R, SLC2R_par, ):
    """*** ASAR Wide-Swath complex image resampling using 2-D SINC interpolation ***
    *** Copyright 2008, Gamma Remote Sensing, v1.7 14-May-2008 clw ***
    
    usage: WSS_interp <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <DIFF_par> <SLC-2R> <SLC2R_par>
    
    input parameters:
      SLC-1      (input) ASAR Wide-Swath SLC_1 reference image
      SLC-2      (input) ASAR Wide-Swath SLC-2 image to be resampled to the geometry of the SLC-1 reference image
      SLC1_par   (input) ASAR Wide-Swath SLC-1 image parameter file
      SLC2_par   (input) ASAR Wide-Swath SLC-2 image parameter file
      DIFF_par   (input) DIFF/GEO offset parameter file with offset model
      SLC-2R     (output) ASAR Wide-Swath SLC-2R coregistered to SLC-1
      SLC2R_par  (output) ASAR Wide-Swath SLC-2R image parameter file for coregistered image
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, DIFF_par, SLC_2R, SLC2R_par]
    optargs = []
    cmd = makecmd('WSS_interp', args, optargs) 
    os.system(cmd)

def diff_ls_fit(unw_1, unw_2, DIFF_par, nr=None, naz=None, overlay=None, plot_data=None):
    """*** Three-pass differential interferometry SVD to solve for the the L.S.offset ***
    *** Copyright 2013, Gamma Remote Sensing AG, v1.6 23-Aug-2013 clw/uw ***
    
    usage: diff_ls_fit <unw-1> <unw-2> <DIFF_par> [nr] [naz] [overlay] [plot_data]
    
    input parameters: 
      unw-1       (input) unwrapped interferogram 1
      unw-2       (input) unwrapped interferogram 2
      DIFF_par    (input)) DIFF/GEO parameter file for the scene
      nr          number of range samples (default=32)
      naz         number of azimuth samples (default=32)
      overlay     (input) SUN raster/BMP format overlay file, regions flagged with 0 are excluded ( - for no overlay file)
      plot_data   (output) plot data file compatible with xmgr
    
    """

    args = [unw_1, unw_2, DIFF_par]
    optargs = [nr, naz, overlay, plot_data]
    cmd = makecmd('diff_ls_fit', args, optargs) 
    os.system(cmd)

def phase_sim_orb(SLC1_par, SLC2R_par, OFF_par, hgt, sim_unw, SLC_ref_par=None, def_=None, delta_t=None, int_mode=None, ph_mode=None):
    """*** Simulate unwrapped interferometric phase using DEM height and deformation rate using orbit state vectors ***
    *** Copyright 2013, Gamma Remote Sensing, v2.2 3-Sep-2013 clw ***
    
    usage: phase_sim_orb <SLC1_par> <SLC2R_par> <OFF_par> <hgt> <sim_unw> [SLC_ref_par] [def] [delta_t] [int_mode] [ph_mode]
    
    input parameters:
      SLC1_par    (input) SLC parameter file of reference SLC-1
      SLC2R_par   (input) SLC parameter file of resampled SLC-2
      OFF_par     (input) ISP offset/interferogram parameter file
      hgt         (input) height map in the same geometry as the interferogram (meters, float, enter - for none)
      sim_unw     (output) simulated unwrapped interferometric phase
      SLC_ref_par (input) SLC parameter file of the image used for geometric coregistration (enter - for none)
      def         (input) LOS deformation rate map (meters/yr, float, enter - for none)
      delta_t     (input) interferogram time interval (days, required for deformation modeling, enter - for none)
      int_mode    (input) interferometric acquisition mode:
                    0: single-pass mode (Tandem-X)
                    1: repeat-pass mode (default)
      ph_mode     phase offset mode:
                    0: absolute phase (default)
                    1: subtract phase offset that is a multiple of 2PI to improve precision
    
    """

    args = [SLC1_par, SLC2R_par, OFF_par, hgt, sim_unw]
    optargs = [SLC_ref_par, def_, delta_t, int_mode, ph_mode]
    cmd = makecmd('phase_sim_orb', args, optargs) 
    os.system(cmd)

def dispmap(unw, hgt, SLC_par, OFF_par, disp_map, mode=None):
    """*** Conversion of unwrapped differential phase to displacement map (m) ***
    *** Copyright 2014, Gamma Remote Sensing, v1.5 24-Feb-2014 uw/clw  ***
    usage: dispmap <unw> <hgt> <SLC_par> <OFF_par> <disp_map> [mode]
    
    input parameters: 
      unw       (input) unwrapped phase (radians) (float)
      hgt       (input) height map (enter - for none, reference height set to 0.0) (float)
      SLC_par   (input) SLC/MLI image parameter file of the reference scene
      OFF_par   (input) ISP/offset parameter file (enter - for none)
                NOTE: if OFF_par not provide, geometry and pixel spacing are determined from the MLI image parameters
      disp_map  (output) displacement map (float)
      mode      flag indicating displacement mode:
                  0: along look vector [m] (+: towards sensor) (default)
                  1: vertical displacement [m] (+: increasing height)
                  2: horizontal displacement [m] (+: decreasing ground range)
    """

    args = [unw, hgt, SLC_par, OFF_par, disp_map]
    optargs = [mode]
    cmd = makecmd('dispmap', args, optargs) 
    os.system(cmd)

def par_RISAT_geo(annotation_XML, GeoTIFF, polarization, DEM_par, MLI_par, MLI, ):
    """*** Generate DEM parameter file, MLI image, and MLI parameter file for RISAT-1 GeoTIFF products ***
    *** Copyright 2013, Gamma Remote Sensing, v1.2 clw 19-Feb-2013 ***
    
    usage: par_RISAT_geo <annotation_XML> <GeoTIFF> <polarization> <DEM_par> <MLI_par> <MLI>
    
    input parameters:
      annotation_XML (input) RISAT-1 product annotation XML file (product.xml)
      GeoTIFF        (input) image data file in GeoTIFF format (imagery_pp.tif)
      polarization   (input) polarization RV, RH of the GeoTIFF image data
      DEM_par        (output) DIFF/GEO DEM parameter file
      MLI_par        (output) ISP image parameter file (example: yyyymmdd_pp.mli.par)
      MLI            (output) SLC data file (example: yyyymmdd_pp.mli)
    """

    args = [annotation_XML, GeoTIFF, polarization, DEM_par, MLI_par, MLI]
    optargs = []
    cmd = makecmd('par_RISAT_geo', args, optargs) 
    os.system(cmd)

def par_TX_geo(annotation_XML, GEOTIFF, MLI_par, DEM_par, GEO, pol=None):
    """*** Generate DEM parameter and image files for Terrasar-X GEC and EEC data  ***
    *** Copyright 2012, Gamma Remote Sensing, v1.1 9-Aug-2012 awi/clw ***
    
    usage: par_TX_geo <annotation_XML> <GEOTIFF> <MLI_par> <DEM_par> <GEO> [pol]
    
    input parameters:
      annotation_XML (input) Terrasar-X product annotation XML file
      GEOTIFF        (input) image data file in geotiff format
      MLI_par        (output) ISP image parameter file (example: yyyymmdd.mli.par)
      DEM_par        (output) DIFF/GEO DEM parameter file (example: yyyymmdd.dem_par)
      GEO            (output) geocoded and calibrated image data file (example: yyyymmdd.geo)
      pol            polarisation HH, HV, VH, VV (default: first polarisation found in the annotation_XML)
    
    """

    args = [annotation_XML, GEOTIFF, MLI_par, DEM_par, GEO]
    optargs = [pol]
    cmd = makecmd('par_TX_geo', args, optargs) 
    os.system(cmd)

def SLC_interp_lt(SLC_2, SLC1_par, SLC2_par, lookup_table, MLI1_par, MLI2_par, OFF_par, SLC_2R, SLC2R_par, blk_size=None):
    """*** SLC image resampling via a lookup table and SINC interpolation ***
    *** Copyright 2014, Gamma Remote Sensing, v2.5 6-Feb-2014 clw ***
    
    usage: SLC_interp_lt <SLC-2> <SLC1_par> <SLC2_par> <lookup_table> <MLI1_par> <MLI2_par> <OFF_par> <SLC-2R> <SLC2R_par> [blk_size]
    
    input parameters:
      SLC2          (input) SLC-2 image to be resampled to the geometry of the SLC-1 reference image
      SLC1_par      (input) SLC/MLI ISP image parameter file of the SLC-1 reference
      SLC2_par      (input) SLC/MLI ISP image parameter file of SLC-2
      lookup_table  (input) lookup_table relating SLC-2 to SLC-1
      MLI1_par      (input) SLC/MLI ISP image parameter file of reference MLI (lookup table dimension)
      MLI2_par      (input) SLC/MLI ISP image parameter file of MLI2 (lookup table values)
      OFF_par       (input) ISP offset/interferogram parameter file used for refinement (enter - for none)
      SLC-2R        (output) single-look complex image 2 coregistered to SLC-1
      SLC2R_par     (output) SLC-2R ISP image parameter file for coregistered image
      blksz         number of lines/block (enter - for default, minimum value: 128
    
    """

    args = [SLC_2, SLC1_par, SLC2_par, lookup_table, MLI1_par, MLI2_par, OFF_par, SLC_2R, SLC2R_par]
    optargs = [blk_size]
    cmd = makecmd('SLC_interp_lt', args, optargs) 
    os.system(cmd)

def geocode_back(data_in, width_in, gc_map, data_out, width_out, nlines_out=None, interp_mode=None, format_flag=None, lr_in=None, lr_out=None):
    """*** Geocoding of image data using lookup table values  ***
    *** Copyright 2013, Gamma Remote Sensing, v3.0 14-Oct-2013 clw/uw/of ***
    
    usage: geocode_back <data_in> <width_in> <gc_map> <data_out> <width_out> [nlines_out] [interp_mode] [format_flag] [lr_in] [lr_out]
    
    input parameters:
      data_in      (input) data file (format as specified by format_flag parameter)
      width_in     width of input data file
      gc_map       (input) lookup table containing pairs of real-valued input data coordinates
      data_out     (output) output data file
      width_out    width of gc_map lookup table, output file has the same width
      nlines_out   number of lines of output data file (enter 0 or - for default: number of lines in gc_map)
      interp_mode  interpolation mode 
                     0: nearest-neighbor (default)
                     1: bicubic spline
                     2: bicubic-log spline
      format_flag  input/output data format
                     0: FLOAT (default)
                     1: FCOMPLEX
                     2: SUN raster/BMP format
                     3: UNSIGNED CHAR
                     4: SHORT
                     5: DOUBLE
      lr_in        input SUN raster/BMP format left/write flipped (default = 1: not flipped, -1: flipped)
      lr_out       output SUN raster/BMP format left/write flipped (default = 1: not flipped, -1: flipped)
    
    """

    args = [data_in, width_in, gc_map, data_out, width_out]
    optargs = [nlines_out, interp_mode, format_flag, lr_in, lr_out]
    cmd = makecmd('geocode_back', args, optargs) 
    os.system(cmd)

def interp_real(image_2, DIFF_par, resamp_image, loff=None, nlines=None, imode=None):
    """*** Resample a FLOAT 2D image using bicubic spline interpolation (Cattmul-Rom) ***
    *** Copyright 2011, Gamma Remote Sensing, v2.1 15-Aug-2011 clw/uw ***
    
    usage: interp_real <image-2> <DIFF_par> <resamp_image> [loff] [nlines] [imode]
    
    input parameters:
      image-2       FLOAT image to be resampled to geometry of reference image-1
      DIFF_par      DIFF parameter file containing range and azimuth offset polynomial coefficients
      resamp_image  output resampled image in the geometry of image-1
      loff          starting line in map (relative to start of image-1)
      nlines        number of lines to interpolate (enter - for all lines in the file)
      imode         interpolation mode
                      0: nearest neighbor
                      1: bicubic spline (default)
                      2: bicubic log-spline
    
    """

    args = [image_2, DIFF_par, resamp_image]
    optargs = [loff, nlines, imode]
    cmd = makecmd('interp_real', args, optargs) 
    os.system(cmd)

def dispmap_vec(DEM_par, dispmap, lv_theta, lv_phi, fv_theta, fv_phi, dv_norm, dv_theta=None, dv_phi=None, dv_x=None, dv_y=None, dv_z=None, mask_angle=None):
    """*** Calculation of displacement vector field from displacement direction and measured component ***
    *** Copyright 2008, Gamma Remote Sensing, v1.2, 16-Jan-2008 uw,ts,cw ***
    
    usage: dispmap_vec <DEM_par> <dispmap> <lv_theta> <lv_phi> <fv_theta> <fv_phi> <dv_norm> [dv_theta] [dv_phi] [dv_x] [dv_y] [dv_z] [mask_angle]
    
    input parameters:
      DEM_par     (input) DEM/MAP parameter file
      dispmap     (input) displacement observation (along look-vector) (float)
      lv_theta    (input) look-vector elevation angle (file  or constant value in deg.)
      lv_phi      (input) look-vector orientation angle (file or constant value in deg.)
      fv_theta    (input) flow-vector elevation angle (file or constant value in deg.)
      fv_phi      (input) flow-vector orientation angle (file or constant value in deg.)
      dv_norm     (output) norm of 3-dim displacement vector (float)
      dv_theta    (output) elevation angle of 3-dim displacement vector (float)
      dv_phi      (output) orientation angle of 3-dim displacement vector (float)
      dv_x        (output) easting  component of 3-dim displacement vector (float)
      dv_y        (output) northing component of 3-dim displacement vector (float)
      dv_z        (output) vertical component of 3-dim displacement vector (float)
      mask_angle  cutoff angle in degrees between the look and normal vector to mask instable results (default=5)
    
      NOTE: select - to avoid creation of the corresponding output file
      NOTE: In the case where the dv_theta file is not specified, the sign of the displacement is conserved
    
    """

    args = [DEM_par, dispmap, lv_theta, lv_phi, fv_theta, fv_phi, dv_norm]
    optargs = [dv_theta, dv_phi, dv_x, dv_y, dv_z, mask_angle]
    cmd = makecmd('dispmap_vec', args, optargs) 
    os.system(cmd)

# def mosaic(nfiles, data_in1, DEM_par1, data_in2, DEM_par2,, ,  data_out, DEM_parout, mode, format_flag, ):
#     """*** Mosaic geocoded images with same format, map projection, and pixel spacing parameters ***
#     *** Supported formats: float, fcomplex, int, short, unsigned char, SUN raster, and BMP image files ***
#     *** Copyright 2005, Gamma Remote Sensing, v2.6 13-Dec-2005 awi/ts/uw/clw ***
    
#     usage: mosaic <nfiles> <data_in1> <DEM_par1> <data_in2> <DEM_par2> <...> <..> <data_out> <DEM_parout> <mode> <format_flag>
    
#     input parameters:
#       nfiles       number of input data files (1,2,...)
#       data_in1     (input) 1. input data file
#       DEM_par1     (input) DEM/MAP parameter file of 1. input data file
#       data_in2     (input) 2. input data file
#       DEM_par2     (input) DEM/MAP parameter file of 2. input data file
#       ...          (input) further input data files
#       ...          (input) DEM/MAP parameter file of further input data files
#       data_out     (output) output data file
#       DEM_parout   (input/output) DEM parameter file describing the output mosaic map
#       mode         mosaic processing mode
#                      0: value of prior image preferred in the case of multiple valid input values
#                      1: average of multiple valid input values calculated
#       format_flag  input/output data format flag
#                      0: FLOAT (REAL*4)
#                      1: FCOMPLEX (pairs of float)
#                      2: INTEGER (INTEGER*4)
#                      3: SHORT (INTEGER*2)
#                      4: UNSIGNED CHAR
#                      5: SUN raster or BMP image files (8 or 24 bit)
    
#     NOTE: If the output DEM parameter file already exists, then the bounds of the mosaic are read
#     from from that file, otherwise bounds including all input data will be computed and output
    
#     """

#     args = [nfiles, data_in1, DEM_par1, data_in2, DEM_par2, , , data_out, DEM_parout, mode, format_flag]
#     optargs = []
#     cmd = makecmd('mosaic', args, optargs) 
#     os.system(cmd)

def WSS_mosaic(WSS_tab, MLI_par, WSS_data, type, ):
    """*** Mosiac ASAR WSS interferograms and MLI data ***
    *** Copyright 2013, Gamma Remote Sensing, v1.3 21-Oct-2013 clw ***
    
    usage: WSS_mosaic <WSS_tab> <MLI_par> <WSS_data> <type>
    
    input parameters:
      WSS_tab   (input) two column list of MLI data files and MLI image parameter files for each sub-swath (including paths)
                        required file order: SS1, SS2, SS3, SS4, SS5
      MLI_par   (output) ISP image parameter file for the mosaicked WSS MLI image or interferogram
      WSS_data  (output) WSS mosaicked MLI image or interferogram
      type      WSS data type:
                  0: FLOAT
                  1: FCOMPLEX
    
    """

    args = [WSS_tab, MLI_par, WSS_data, type]
    optargs = []
    cmd = makecmd('WSS_mosaic', args, optargs) 
    os.system(cmd)

def scale_base(unw_2, scaled_unw_2, baseline_1, SLC1_par_1, OFF_par_1, baseline_2, SLC_1_par_2, OFF_par_2, int_type, ):
    """*** scale unwrapped phase according to baseline ratio ***
    *** Copyright 2005, Gamma Remote Sensing, v1.1 29-Apr-2005 ts/clw ***
    
    usage: scale_base <unw-2> <scaled_unw-2> <baseline-1> <SLC1_par-1> <OFF_par-1> <baseline-2> <SLC-1_par-2> <OFF_par-2> <int_type>
    
    input parameters:
      unw-2         (input) unwrapped interferogram file (to be scaled)
      scaled_unw-2  (output) scaled unwrapped interferogram 2
      baseline-1    (input) baseline file of interferogram 1
      SLC1_par-1    (input) parameter file of SLC-1 in interferogram 1
      OFF_par-1     (input) ISP interferogram/offset parameters of interferogram 1
      baseline-2    (input) baseline file of interferogram 2 (to be scaled)
      SLC1_par-2    (input) parameter file of SLC-1 in interferogram 2 (to be scaled)
      OFF_par-2     (input) ISP interferogram/offset parameters of interferogram 2 (to be scaled)
      int_type      interferogram type 0: unflattened (default)  1: flattened (*.flt)
    
    """

    args = [unw_2, scaled_unw_2, baseline_1, SLC1_par_1, OFF_par_1, baseline_2, SLC_1_par_2, OFF_par_2, int_type]
    optargs = []
    cmd = makecmd('scale_base', args, optargs) 
    os.system(cmd)

def interp_cpx(image_2, DIFF_par, resamp_image, loff=None, nlines=None, imode=None):
    """*** Resample a complex floating point image using bicubic spline or SINC interpolation ***
    *** Copyright 2011, Gamma Remote Sensing, v2.1 15-Aug-2011 clw/uw ***
    
    usage: interp_cpx <image-2> <DIFF_par> <resamp_image> [loff] [nlines] [imode]
    
    input parameters:
      image-2       FCOMPLEX image to be resampled to geometry of reference image-1
      DIFF_par      DIFF/GEO parameter file containing range and azimuth offset polynomial coefficients
      resamp_image  output resampled image in the geometry of image-1
      loff          starting line in map (relative to start of image-1)
      nlines        number of lines to interpolate (enter - for all lines in the file)
      imode         interpolation mode
                      0: nearest-neighbor
                      1: bicubic spline (Cattmul-Rom)
                      2: SINC (9 point, default)
    
    """

    args = [image_2, DIFF_par, resamp_image]
    optargs = [loff, nlines, imode]
    cmd = makecmd('interp_cpx', args, optargs) 
    os.system(cmd)

def pixel_area(MLI_par, DEM_par, DEM, lookup_table, ls_map, inc_map, pix_sigma0, pix_gamma0=None, nstep=None):
    """*** Calculate terrain-based sigma0 and gammma0 normalization area in slant-range geometry ***
    *** Copyright 2014, Gamma Remote Sensing, v1.8 6-May-2014 of/clw/uw  ***
    
    usage: pixel_area <MLI_par> <DEM_par> <DEM> <lookup_table> <ls_map> <inc_map> <pix_sigma0> [pix_gamma0] [nstep]
    
    input parameters:
      MLI_par       (input) ISP MLI or SLC image parameter file (slant range geometry)
      DEM_par       (input) DEM/MAP parameter file
      DEM           (input) DEM data file (or constant height value)
      lookup_table  (input) geocoding lookup table (fcomplex)
      ls_map        (input) layover and shadow map in map geometry, (enter - for none)
      inc_map       (input) local incidence angle map in map geometry, (enter - for none)
      pix_sigma0    (output) sigma nought normalization area (float, enter - for none)
      pix_gamma0    (output) gamma nought normalization area (float, enter - for none)
      nstep         number steps to divide each dimension of the map pixels (default: 10)
    
    """

    args = [MLI_par, DEM_par, DEM, lookup_table, ls_map, inc_map, pix_sigma0]
    optargs = [pix_gamma0, nstep]
    cmd = makecmd('pixel_area', args, optargs) 
    os.system(cmd)

def offset_pwr_list(SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, clist_RDC, clist_MAP, offs, snr, nx, ny, rwin=None, azwin=None, offsets=None, n_ovr=None, thres=None, pflag=None):
    """*** Offsets between SLC images at positions specified by a list using intensity cross-correlation ***
    *** Copyright 2012, Gamma Remote Sensing, v1.6 clw/uw 17-Apr-2012 ***
    
    usage: offset_pwr_list <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <OFF_par> <clist_RDC> <clist_MAP> <offs> <snr> <nx> <ny> [rwin] [azwin] [offsets] [n_ovr] [thres] [pflag]
    
    input parameters: 
      SLC-1      (input) single-look complex image 1 (reference)
      SLC-2      (input) single-look complex image 2
      SLC1_par   (input) SLC-1 ISP image parameter file
      SLC2_par   (input) SLC-2 ISP image parameter file
      OFF_par    (input) ISP offset/interferogram parameter file
      clist_RDC  (input) list of x,y pixel coordinates in the reference SLC image geometry (Range-Doppler Coordinates) (text format)
      clist_MAP  (input) list of x,y pixel coordinates in the map projection geometry (text format)
      offs       (output) offset estimate 2d map (fcomplex)
      snr        (output) offset estimation 2d SNR map (float)
      nx         width of 2D offset map in MAP gemometry
      ny         height of 2D offset map in MAP geometry
      rwin       search window size (range pixels, (enter - for default from offset parameter file))
      azwin      search window size (azimuth pixels, (enter - for default from offset parameter file))
      offsets    (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr      SLC oversampling factor (integer 2**N (1,2,4) default = 2)
      thres      offset estimation quality threshold (enter - for default from offset parameter file)
      pflag      print flag
                   0: print offset summary
                   1: print all offset data (default))
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, OFF_par, clist_RDC, clist_MAP, offs, snr, nx, ny]
    optargs = [rwin, azwin, offsets, n_ovr, thres, pflag]
    cmd = makecmd('offset_pwr_list', args, optargs) 
    os.system(cmd)

def data2xyz(DEM_par, data, data_xyz, ):
    """*** Convert geocoded float data to triplets of float: (northing, easting, data_value) ***
    *** Copyright 2010, Gamma Remote Sensing, v1.0 30-Jun-2010 clw/uw ***
    
    usage: data2xyz <DEM_par> <data> <data_xyz>
    input parameters:
      DEM_par  (input) DEM parameter file
      data     (input) float format data with same dimensions as given in the DEM_par
      data_xyz (output) data triplets in 4-byte float format (northing, easting, data_value)
    """

    args = [DEM_par, data, data_xyz]
    optargs = []
    cmd = makecmd('data2xyz', args, optargs) 
    os.system(cmd)

def rdc_trans(MLI1_par, DEM_RDC, MLI2_par, lt, ):
    """*** Derive lookup table for SLC/MLI coregistration (considering terrain heights) ***
    *** Copyright 2013, Gamma Remote Sensing, v1.7 14-Oct-2013 clw/uw ***
    
    usage: rdc_trans <MLI1_par> <DEM_RDC> <MLI2_par> <lt>
    
    input parameters:
      MLI1_par  (input) SLC/MLI ISP image parameter file of the reference scene (lookup table and DEM_RDC dimensions)
      DEM_RDC   (input) height map in RDC of MLI-1 (float, or constant height value)
      MLI2_par  (input) SLC/MLI ISP image parameter file of MLI-2 (lookup table values)
      lt        (output) lookup table to resample MLI-2 to the reference MLI-1 geometry(fcomplex)
    """

    args = [MLI1_par, DEM_RDC, MLI2_par, lt]
    optargs = []
    cmd = makecmd('rdc_trans', args, optargs) 
    os.system(cmd)

def dispmap_vec2(DEM_par, DEM, dispmap1, lv1_theta, lv1_phi, dispmap2, lv2_theta, lv2_phi, dv_norm, dv_theta=None, dv_phi=None, dv_x=None, dv_y=None, dv_z=None, mask_angle=None, mode=None, ax_north=None, ax_east=None):
    """*** Calculation of displacement vector field from 2 measured components (asc./desc.) ***
    *** Copyright 2008, Gamma Remote Sensing, v1.4, 16-Jan-2008 uw,ts,cw ***
    
    usage: dispmap_vec2 <DEM_par> <DEM> <dispmap1> <lv1_theta> <lv1_phi> <dispmap2> <lv2_theta> <lv2_phi> <dv_norm> [dv_theta] [dv_phi] [dv_x] [dv_y] [dv_z] [mask_angle] [mode] [ax_north] [ax_east]
    
    input parameters:
      DEM_par     (input) DEM/MAP parameter file
      DEM         (input) DEM data file (or constant height value)
      dispmap1    (input) 1. displacement observation (along look-vector 1) (float)
      lv1_theta   (input) look-vector 1 elevation angle (file or constant value in deg.)
      lv1_phi     (input) look-vector 1 orientation angle (file or constant value in deg.)
      dispmap2    (input) 2. displacement observation (along look-vector 2) (float)
      lv2_theta   (input) look-vector 2 elevation angle (file or constant value in deg.)
      lv2_phi     (input) look-vector 2 orientation angle (file or constant value in deg.)
      dv_norm     (output) norm of 3-dim displacement vector (float)
      dv_theta    (output) elevation angle of 3-dim displacement vector (float)
      dv_phi      (output) orientation angle of 3-dim displacement vector (file of float)
      dv_x        (output) easting  component of 3-dim displacement vector (float)
      dv_y        (output) northing component of 3-dim displacement vector (float)
      dv_z        (output) vertical component of 3-dim displacement vector (float)
      mask_angle  cutoff angle in degrees between the look and normal vectors to mask inaccurate results (default=2)
      mode        displacement model mode (default=0):
                    0: displacement along terrain surface
                    1: displacement towards center axis
      ax_north    displacement center axis northing/latitude
      ax_east     displacement center axis easting/longitude
    
      NOTE: select - to avoid creation of the corresponding output file
    
    """

    args = [DEM_par, DEM, dispmap1, lv1_theta, lv1_phi, dispmap2, lv2_theta, lv2_phi, dv_norm]
    optargs = [dv_theta, dv_phi, dv_x, dv_y, dv_z, mask_angle, mode, ax_north, ax_east]
    cmd = makecmd('dispmap_vec2', args, optargs) 
    os.system(cmd)


def gec_map(SLC_par, OFF_par, DEM_par, href, DEM_seg_par, lookup_table, lat_ovr=None, lon_ovr=None):
    """*** Calculate geocoding lookup table for ellipsoid correction of slant-range images ***
    *** Copyright 2013, Gamma Remote Sensing, v3.4 27-May-2013 clw/uw ***
    
    usage: gec_map <SLC_par> <OFF_par> <DEM_par> <href> <DEM_seg_par> <lookup_table> [lat_ovr] [lon_ovr]
    
    input parameters:
      SLC_par       (input) ISP SLC-1/MLI parameter file
      OFF_par       (input) ISP offset/interferogram parameter file, enter - if geocoding SLC or MLI data
      DEM_par       (input) DEM parameter file
      href          (input) elevation reference [m]
      DEM_seg_par   (input/output) DEM segment parameter file used for geocoding
    
      NOTE: If the DEM_seg_par already exists, then the output DEM parameters will be read from this file
            otherwise they are estimated from the image data.
    
      lookup_table  (output) geocoding lookup table
      lat_ovr       latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr       longitude or easting output DEM oversampling factor (enter - for default: 1.0)
    """

    args = [SLC_par, OFF_par, DEM_par, href, DEM_seg_par, lookup_table]
    optargs = [lat_ovr, lon_ovr]
    cmd = makecmd('gec_map', args, optargs) 
    os.system(cmd)

def extract_gcp(DEM_rdc, OFF_par, GCP, nr, naz, mask=None):
    """*** Extract GCPs from a DEM in range-Doppler coordinates ***
    *** Copyright 2013, Gamma Remote Sensing, v2.1 23-Aug-2013 clw/uw ***
    
    usage: extract_gcp <DEM_rdc> <OFF_par> <GCP> <nr> <naz> [mask]
    
    input parameters: 
      DEM_rdc  (input) DEM in range-Doppler coordinates
      OFF_par  (input) ISP offset/interferogram parameter file
      GCP      (output) GCP height data file (text format)
      nr       number of GCP points in range (default=32)
      naz      number of GCP points in azimuth (default=32)
      mask     (input) mask image, output set to 0.0 if mask=0 (SUN raster or BMP format)
    
    """

    args = [DEM_rdc, OFF_par, GCP, nr, naz]
    optargs = [mask]
    cmd = makecmd('extract_gcp', args, optargs) 
    os.system(cmd)

def offset_fitm(offs, snr, DIFF_par, coffs=None, coffsets=None, thres=None, npoly=None, interact_mode=None):
    """*** Range and azimuth offset polynomial estimation using SVD ***
    *** Copyright 2014, Gamma Remote Sensing, v3.6 4-Apr-2014 clw/uw ***
    
    usage: offset_fitm <offs> <snr> <DIFF_par> [coffs] [coffsets] [thres] [npoly] [interact_mode]
    
    input parameters:
      offs          (input) range and azimuth offset estimates (fcomplex)
      snr           (input) SNR values of offset estimates (float)
      DIFF_par      (input) DIFF/GEO parameter file for the scene
      coffs         (output) culled range and azimuth offset estimates (fcomplex, enter - for none)
      coffsets      (output) culled offset estimates and SNR values (text format, enter - for none)
      thres         SNR threshold (enter - for default from DIFF_par)
      npoly         number of model polynomial parameters (enter - for default, 1, 3, 4, 6, default: 4)
      interact_mode interactive culling of input data:
                      0: off (default)
                      1: on
    
    """

    args = [offs, snr, DIFF_par]
    optargs = [coffs, coffsets, thres, npoly, interact_mode]
    cmd = makecmd('offset_fitm', args, optargs) 
    os.system(cmd)

def map_section(DEM_par, n1, e1, n2, e2, post_north, post_east, DEM_par2, lt=None, MLI_par1=None, MLI_par2=None, cflg=None, lt2=None, MLI_coord=None):
    """*** Generate the DEM parameter file of a specfied region and extract this region from a geocoding lookup table ***
    *** Copyright 2014, Gamma Remote Sensing, v1.3 5-May-2014 clw ***
    
    usage: map_section <DEM_par> <n1> <e1> <n2> <e2> <post_north> <post_east> <DEM_par2> [lt] [MLI_par1] [MLI_par2] [cflg] [lt2] [MLI_coord]
    
    input parameters:
      DEM_par     (input) DEM parameter file describing the input map geometry
      north1      northing map coordinate of the upper-left corner of the map segment (enter - for value in DEM_par)
      east1       easting  map coordinate of the upper-left corner of the map segment (enter - for value in DEM_par)
      north2      northing map coordinate of the lower-right corner of the map segment (enter - for value in DEM_par)
      east2       easting  map coordinate of the lower-right corner of the map segment (enter - for value in DEM_par)
                  NOTE: units for the corner coordinates depend on the projection specified in the DEM_par
      post_north  posting of the output lookup table for map samples in northing (enter - to keep the posting in the DEM_par)
                  NOTE: post_north must be a negative number to prevent the DEM from flipping north/south when displayed
      post_east   posting of the output lookup table in map coordinates in easting  (enter - to keep the posting in the DEM_par)
                  NOTE: post_east must be a positive number to prevent the DEM from flipping east/west when displayed
      DEM_par2    (output) DEM parameter file describing the output lookup table
      lt          (input) lookup table for the transformation from map geometry to radar Range-Doppler Coordinates (RDC) (enter - for none)
                  NOTE: The lookup table has the dimensions described by the DEM_par
      MLI_par1    (input) MLI image parameter file with the geometry used for the lookup table values (enter - for none)
      MLI_par2    (input) MLI image parameter file with the geometry for the image that will be terrain geocoded (enter - to be the same as MLI_par1)
                  NOTE: The MLI_par files must be generated from the same SLC image but can have a different number of looks in range and azimuth
      cflg        lookup table coordinate shift flag (enter - for default):
                    0: do not subtract range and azimuth offsets from the lookup table values (default)
                    1: subtract range and azimuth offsets from the lookup table values
      lt2         (output) lookup table segment with the specified posting: post_north and  post_east (enter - for none)
      MLI_coord   (output) rectangular region specified in MLI and SLC coordinates covering the region of the lookup table (text format)
                    range_offset   range samples   azimuth_offset  azimuth_lines
    
    """

    args = [DEM_par, n1, e1, n2, e2, post_north, post_east, DEM_par2]
    optargs = [lt, MLI_par1, MLI_par2, cflg, lt2, MLI_coord]
    cmd = makecmd('map_section', args, optargs) 
    os.system(cmd)

def pol2rec(data1, SLC_par1, data2, SLC_par2, pix_size, type, mode=None, xmin=None, nx=None, ymin=None, ny=None, rmax=None):
    """*** Polar to rectangular conversion for GPRI SLC and MLI image data ***
    *** Copyright 2014, Gamma Remote Sensing, v1.9 clw 21-May-2014 ***
    
    usage: pol2rec <data1> <SLC_par1> <data2> <SLC_par2> <pix_size> <type> [mode] [xmin] [nx] [ymin] [ny] [rmax]
    
    input parameters:
      data1    (input) GPRI data in polar format
      SLC_par1 (input) GPRI SLC image parameter file describing the SLC image geometry
      data2    (output) GPRI output image in rectangular format
      SLC_par2 (output) output SLC/MLI image parameter file for the output image
      pix_size (output) output pixel size (meters)
      type     input data type:
                 0: FLOAT
                 1: FCOMPLEX
      mode     interpolation algorithm
                 0: nearest-neighbor
                 1: bicubic spline (default)
      xmin     starting x coordinate (enter - for default: calculated from image)
      nx       number of x samples in the output image (enter - for default: calculated from image
      ymin     starting y coordinate (enter - for default: calculated from image)
      ny       number of y samples in the output image (enter - for default: calculated from image
      rmax     maximum slant range in the GPRI image to resample (enter - for default: maximum slant range of the input image)
               NOTE: center image line of the scan defines the direction of the X axis
    """

    args = [data1, SLC_par1, data2, SLC_par2, pix_size, type]
    optargs = [mode, xmin, nx, ymin, ny, rmax]
    cmd = makecmd('pol2rec', args, optargs) 
    os.system(cmd)

def par_UAVSAR_geo(ann, SLC_MLI_par, DEM_par, ):
    """*** Generate DEM parameter file for GRD products from UAVSAR annotation file (ann) ***
    *** Copyright 2012, Gamma Remote Sensing, v1.2 7-Nov-2012 clw***
    
    usage: par_UAVSAR_geo <ann> <SLC/MLI_par> <DEM_par>
    
    input parameters: 
      ann           (input) UAVSAR annotation file (*ann.txt)
      SLC/MLI_par   (output) ISP image parameter file for FLOAT format MLC products
      DEM_par       (output) DIFF/GEO DEM parameter file
    """

    args = [ann, SLC_MLI_par, DEM_par]
    optargs = []
    cmd = makecmd('par_UAVSAR_geo', args, optargs) 
    os.system(cmd)

def dem_trans(DEM1_par, DEM1, DEM2_par, DEM2, lat_ovr=None, lon_ovr=None, datum_shift=None, bflg=None, lookup_table=None):
    """*** DEM resampling from one map projection to another ***
    *** Copyright 2013, Gamma Remote Sensing, v2.8 26-Aug-2013 uw/clw ***
    
    usage: dem_trans <DEM1_par> <DEM1> <DEM2_par> <DEM2> [lat_ovr] [lon_ovr] [datum_shift] [bflg] [lookup_table]
    
    input parameters:
      DEM1_par      (input) DEM parameter file of the input DEM
      DEM1          (input) input DEM data
      DEM2_par      (input/output) DEM parameter file of the resampled input DEM 
                    NOTE: if DEM2_par does not exist, it is created using the projection parameters from DEM1_par
      DEM2          (output) DEM in transformed coordinates specified by DEM2_par
      lat_ovr       lat/northing DEM oversampling factor (enter - for default: 2.0)
      lon_ovr       lon/easting DEM oversampling factor (enter - for default: 2.0)
                    NOTE: lat_ovr and lon_ovr are only considered if DEM2_par does not exist
      datum_shift   vertical datum shift flag, enter - for default:
                      0: output DEM heights not corrected for datum shift between projections (default)
                      1: output DEM heights corrected for datum shift between projections
      bflg          output DEM bounds flag (enter - for default):
                      0: use DEM bounds specified by DEM2_par (default)
                      1: determine bounds from DEM1_par in the projection specified by DEM2_par
      lookup_table  (output) complex-valued lookup table (DEM2 -> DEM1)
    
    """

    args = [DEM1_par, DEM1, DEM2_par, DEM2]
    optargs = [lat_ovr, lon_ovr, datum_shift, bflg, lookup_table]
    cmd = makecmd('dem_trans', args, optargs) 
    os.system(cmd)

def gc_GPRI_map(MLI_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table, lat_ovr=None, lon_ovr=None, sim_sar=None, u=None, v=None, inc=None, psi=None, pix=None, ls_map=None, frame=None):
    """*** Geocoding lookup table derivation for terrain corrected geocoding of GPRI data ***
    *** Copyright 2014, Gamma Remote Sensing, v1.6 28-May-2014 clw/of ***
    
    
    usage: gc_GPRI_map <MLI_par> <DEM_par> <DEM> <DEM_seg_par> <DEM_seg> <lookup_table> [lat_ovr] [lon_ovr] [sim_sar] [u] [v] [inc] [psi] [pix] [ls_map] [frame]
    
    input parameters:
      MLI_par       (input) GPRI MLI image parameter file (slant range geometry)
      DEM_par       (input) DEM/MAP parameter file
      DEM           (input) DEM data file (or constant height value)
      DEM_seg_par   (input/output) DEM/MAP segment parameter file used for geocoding
                      NOTE: if this file exists, then the bounds of the DEM segment used for geocoding
                      are read from the parameter file, otherwise the bounds are estimated
                      using the SLC parameters and state vectors, and written to the new parameter file
      DEM_seg       (output) segment of DEM used for geocoding, interpolated if lat_ovr > 1  or lon_ovr > 1
      lookup_table  (output) geocoding lookup table
      lat_ovr       latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr       longitude or easting output DEM oversampling factor (enter - for default: 1.0)
      sim_sar       (output) simulated SAR backscatter image (in DEM geometry)
      lv_theta      (output) look-vector elevation angle (radians)
      lv_phi        (output) look-vector orientation angle (radians)
      u             (output) zenith angle of surface normal vector n (angle between z and n)
      v             (output) orientation angle of n (between x and projection of n in xy plane)
      inc           (output) local incidence angle (between surface normal and look vector)
      psi           (output) projection angle (between surface normal and image plane normal)
      pix           (output) pixel area normalization factor
      ls_map        (output) layover and shadow map (in map projection)
      frame         number of DEM pixels to add around area covered by SAR image (default = 2)
    
      NOTE: enter - as filename to avoid creation of the corresponding output file
    
    """

    args = [MLI_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table]
    optargs = [lat_ovr, lon_ovr, sim_sar, u, v, inc, psi, pix, ls_map, frame]
    cmd = makecmd('gc_GPRI_map', args, optargs) 
    os.system(cmd)

def phase_sim(SLC1_par, OFF_par, baseline, hgt, sim_unw, ph_flag=None, bflag=None, def_=None, delta_t=None, int_mode=None, SLC2R_par=None, ph_mode=None):
    """*** Simulate unwrapped interferometric phase using DEM height and deformation rate ***
    *** Copyright 2013, Gamma Remote Sensing, v3.7 3-Sep-2013 clw/uw ***
    
    usage: phase_sim <SLC1_par> <OFF_par> <baseline> <hgt> <sim_unw> [ph_flag] [bflag] [def] [delta_t] [int_mode] [SLC2R_par] [ph_mode]
    
    input parameters:
      SLC1_par  (input) parameter file of reference SLC-1
      OFF_par   (input) ISP offset/interferogram parameter file
      baseline  (input) baseline parameter file
      hgt       (input) height map in the same geometry as the interferogram (meters, float, enter - for none)
      sim_unw   (output) simulated unwrapped interferometric phase
      ph_flag   range phase trend selection:
                  0: unflattened interferogram (default)
                  1: flattened interferogram
      bflag     baseline selection:
                  0: initial baseline (default)
                  1: precision baseline
      def       (input) LOS deformation rate map (meters/yr, float, enter - for none)
      delta_t   (input) interferogram time interval (days, required for deformation modeling, enter - for none)
      int_mode  (input) interferometric acquisition mode  (enter - for default):
                  0: single-pass mode (Tandem-X)
                  1: repeat-pass mode (default)
      SLC2R_par (input) parameter file of resampled SLC, required if SLC-2 frequency differs from SLC-1, (enter - for none)
      ph_mode   phase offset mode:
                  0: absolute phase (default)
                  1: subtract phase at image center (height = 0.0) rounded to the nearest multiple of 2PI
    
    """

    args = [SLC1_par, OFF_par, baseline, hgt, sim_unw]
    optargs = [ph_flag, bflag, def_, delta_t, int_mode, SLC2R_par, ph_mode]
    cmd = makecmd('phase_sim', args, optargs) 
    os.system(cmd)

def gc_map_fine(gc_in, width, DIFF_par, gc_out, ref_flg=None):
    """*** Geocoding lookup table refinement using DIFF_par offset polynomials ***
    *** Copyright 2011, Gamma Remote Sensing, v2.1 8-Apr-2011 clw/uw ***
    
    usage: gc_map_fine <gc_in> <width> <DIFF_par> <gc_out> [ref_flg]
    
    input parameters:
      gc_in     (input) geocoding lookup table
      width     width of geocoding lookup table (samples)
      DIFF_par  DIFF/GEO parameter file containing offset polynomial coefficients
      gc_out    (output) refined geocoding lookup table
      ref_flg   reference image flag (offsets are measured relative to the reference image): 
                  0: actual SAR image
                  1: simulated SAR image (default)
    """

    args = [gc_in, width, DIFF_par, gc_out]
    optargs = [ref_flg]
    cmd = makecmd('gc_map_fine', args, optargs) 
    os.system(cmd)

def gc_insar(SLC_par, OFF_par, hgt, DEM_par, lookup_table, ):
    """*** Derive complex valued lookup table for terrain corrected geocoding (based on heights in SAR geometry) ***
    *** Copyright 2011, Gamma Remote Sensing, v1.5 28-Aug-2011 cw/uw ***
    
    usage: gc_insar <SLC_par> <OFF_par> <hgt> <DEM_par> <lookup_table>
    
    input parameters:
      SLC_par       (input) SLC parameter file of reference SLC
      OFF_par       (input) ISP offset/interferogram parameter file
      hgt           (input) height map in SAR geometry (float format)
      DEM_par       (input) DEM parameters (characterizing map projection)
      lookup_table  (output) geocoding lookup table (fcomplex format)
    
    """

    args = [SLC_par, OFF_par, hgt, DEM_par, lookup_table]
    optargs = []
    cmd = makecmd('gc_insar', args, optargs) 
    os.system(cmd)

def map_trans(DEM1_par, data1, DEM2_par, data2, lat_ovr=None, lon_ovr=None, interp_mode=None, format=None, bflg=None, lookup_table=None):
    """*** Data resampling from one map projection to another map projection ***
    *** Copyright 2013, Gamma Remote Sensing, v2.8 26-Aug-2013 uw/clw ***
    
    usage: map_trans <DEM1_par> <data1> <DEM2_par> <data2> [lat_ovr] [lon_ovr] [interp_mode] [format] [bflg] [lookup_table]
    
    input parameters:
      DEM1_par      (input) DEM parameter file of input data
      data1         (input) input data in the geometry of DEM1_par
      DEM2_par      (input/output) DEM parameter file of the output data
                    NOTE: if DEM2_par does not exist, it is created using the map projection parameters from DEM1_par
      data2         (output) input data1 resampled into the geometry of DEM2_par
      lat_ovr       lat/northing DEM oversampling factor (enter - for default: 2.0)
      lon_ovr       lon/easting DEM oversampling factor (enter - for default: 2.0)
                    NOTE: lat_ovr and lon_ovr are only considered if DEM2_par does not exist
      interp_mode   interpolation mode (enter - for default):
                      0: nearest neighbor
                      1: bicubic (default)
                      2: bicubic-log
      format        data format (enter - for default):
                      0: float (default)
                      1: fcomplex or geocoding lookup table
                      2: SUN/BMP raster file
                      3: unsigned char
                      4: short integer
      bflg          output DEM bounds flag (enter - for default):
                      0: use DEM bounds specified by DEM2_par (default)
                      1: determine bounds from DEM1_par in the projection specified by DEM2_par
      lookup_table  (output) complex-valued lookup table (DEM2 -> DEM1)
    
    """

    args = [DEM1_par, data1, DEM2_par, data2]
    optargs = [lat_ovr, lon_ovr, interp_mode, format, bflg, lookup_table]
    cmd = makecmd('map_trans', args, optargs) 
    os.system(cmd)

def geocode(gc_map, data_in, width_in, data_out, width_out, nlines_out=None, interp_mode=None, format_flag=None, lr_in=None, lr_out=None, n_ovr=None, rad_max=None, nintr=None):
    """*** Forward geocoding transformation using a lookup table ***
    *** Copyright 2013, Gamma Remote Sensing, v3.8 23-Aug-2013 uw/clw/of ***
    
    usage: geocode <gc_map> <data_in> <width_in> <data_out> <width_out> [nlines_out] [interp_mode] [format_flag] [lr_in] [lr_out] [n_ovr] [rad_max] [nintr]
    
    input parameters:
      gc_map       (input) lookup table containing pairs of real-valued output data coordinates
      data_in      (input) data file (format as specified by format_flag parameter)
      width_in     width of input data file and gc_map lookup table
      data_out     (output) output data file
      width_out    width of output data file
      nlines_out   number of lines for the output data file (enter - for default: all lines)
      interp_mode  resampling interpolation mode
                     0: 1/dist (default)
                     1: nearest neighbor
                     2: SQR(1/dist)
                     3: constant
                     4: Gauss weighting
      format_flag  input/output data format
                     0: FLOAT (default)
                     1: FCOMPLEX
                     2: SUN raster/BMP format
                     3: UNSIGNED CHAR
                     4: SHORT
                     5: SCOMPLEX
                     6: DOUBLE
      lr_in        input SUN raster/BMP format left/write flipped (default:1: not flipped, -1: flipped)
      lr_out       output SUN raster/BMP format left/write flipped (default: 1: not flipped, -1: flipped)
      n_ovr        interpolation oversampling factor (default: 2)
      rad_max      maximum interpolation search radius (default 4*n_ovr: 8)
      nintr        number of points required for interpolation when not nearest neighbor (default: 4)
    
    """

    args = [gc_map, data_in, width_in, data_out, width_out]
    optargs = [nlines_out, interp_mode, format_flag, lr_in, lr_out, n_ovr, rad_max, nintr]
    cmd = makecmd('geocode', args, optargs) 
    os.system(cmd)

def rotate_image(data_in, width_in, angle, data_out, width_out, nlines_out, interp_mode=None, format_flag=None):
    """*** Rotate an image about the image center by a specifed angle ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 23-Aug-2013 clw/uw ***
    
    usage: rotate_image <data_in> <width_in> <angle> <data_out> <width_out> <nlines_out> [interp_mode] [format_flag]
    
    input parameters:
      data_in      (input) input data file (format as specified by format_flag parameter)
      width_in     width of input data
      angle        rotation angle, angle > 0: counter-clockwise rotation (deg.)
      data_out     (output) output data file
      width_out    output image width, (enter - for default)
      nlines_out   number of lines in the output image, (enter - for default)
      interp_mode  interpolation mode:
                     0: nearest-neighbor
                     1: bicubic spline (default)
                     2: bicubic-log spline
      format_flag  input and output data format
                     0: FLOAT (default)
                     1: FCOMPLEX
                     2: SUN raster/BMP format
                     3: UNSIGNED CHAR
                     4: SHORT
    """

    args = [data_in, width_in, angle, data_out, width_out, nlines_out]
    optargs = [interp_mode, format_flag]
    cmd = makecmd('rotate_image', args, optargs) 
    os.system(cmd)

def offset_pwrm(MLI_1, MLI_2, DIFF_par, offs, snr, rwin=None, azwin=None, offsets=None, n_ovr=None, nr=None, naz=None, thres=None, pflag=None):
    """*** Offsets between MLI images using intensity cross-correlation ***
    *** Copyright 2012, Gamma Remote Sensing, v3.7 clw/uw 6-Apr-2012 ***
    
    usage: offset_pwrm <MLI-1> <MLI-2> <DIFF_par> <offs> <snr> [rwin] [azwin] [offsets] [n_ovr] [nr] [naz] [thres] [pflag]
    
    input parameters: 
      MLI-1     (input) real valued intensity image 1 (reference)
      MLI-2     (input) real valued intensity image 2
      DIFF_par  DIFF/GEO parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rwin      search window size (range pixels, (enter - for default from offset parameter file))
      azwin     search window size (azimuth pixels, (enter - for default from offset parameter file))
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     image oversampling factor (integer 2**N (1,2,4) default = 2)
      nr        number of offset estimates in range direction (enter - for default from offset parameter file)
      naz       number of offset estimates in azimuth direction (enter - for default from offset parameter file)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      pflag     print flag
                  0: print offset summary
                  1: print all offset data (default))
    
    """

    args = [MLI_1, MLI_2, DIFF_par, offs, snr]
    optargs = [rwin, azwin, offsets, n_ovr, nr, naz, thres, pflag]
    cmd = makecmd('offset_pwrm', args, optargs) 
    os.system(cmd)

def phase_sum(im_list, width, sum, start=None, nlines=None, pixav_x=None, pixav_y=None, zflag=None, nmin=None):
    """*** Calculate sum of a stack of images (float format) ***
    *** Copyright 2012, Gamma Remote Sensing, v1.0 22-Jan-2012 clw/awi/ts ***
    
    usage: phase_sum <im_list> <width> <sum> [start] [nlines] [pixav_x] [pixav_y] [zflag] [nmin]
    
    input parameters:
      im_list  (input) text file with names of co-registered images in column 1 (float)
      width    number of samples/line
      sum      (output) sum of input image data files (float)
      start    starting line (default: 1)
      nlines   number of lines to process (enter -  for default: entire file)
      pixav_x  number of pixels to average in width  (default: 1)
      pixav_y  number of pixels to average in height (default: 1)
      zflag    zero flag:
                 0: interpret 0.0 as missing data value (default)
                    NOTE: pixels wwith 0.0 in the stack have also 0.0 in the sum
                 1: interpret 0.0 as valid data
    
    """

    args = [im_list, width, sum]
    optargs = [start, nlines, pixav_x, pixav_y, zflag, nmin]
    cmd = makecmd('phase_sum', args, optargs) 
    os.system(cmd)

def quad_fit(unw, DIFF_par, dr=None, daz=None, overlay=None, plot_data=None, model=None):
    """*** Estimate 2D quadratic model phase function from an unwrapped differential interferogram  ***
    *** Copyright 2013, Gamma Remote Sensing AG, v2.2 23-Aug-2013 clw/uw ***
    
    usage: quad_fit <unw> <DIFF_par> [dr] [daz] [overlay] [plot_data] [model]
    
    input parameters:
      unw        (input) unwrapped differential interferogram (float)
      DIFF_par   (input) differential interferogram parameters
      dr         range spacing in pixels between phase samples (enter - for default: 16)
      daz        azimuth spacing in lines between phase samples (enter - for default: 16)
      overlay    SUN raster/BMP format overlay mask, 0 pixels are excluded (enter - for none)
      plot_data  (output) plot data file (enter - for none)
      model      polynomial phase model:
                   0: a0 + a1*x + a2*y + a3*x*y + a4*x^2 + a5*y^2 (default)
                   1: a0 + a4*x^2 + a5*y^2:      a1 = 0.0, a2 = 0.0, a3 = 0.0
                   2: a0 + a1*x + a2*y + a3*x*y: a4 = 0.0, a5 = 0.0
                   3: a0 + a1*x + a2*y:          a3 = 0.0, a4 = 0.0, a5 = 0.0
    """

    args = [unw, DIFF_par]
    optargs = [dr, daz, overlay, plot_data, model]
    cmd = makecmd('quad_fit', args, optargs) 
    os.system(cmd)

def dem_xyz(DEM_par, DEM, DEM_XYZ, ):
    """*** DEM transformation to Cartesian XYZ coordinates ***
    *** Copyright 2010, Gamma Remote Sensing, v1.0 31-May-2010 clw/uw ***
    
    usage: dem_xyz <DEM_par> <DEM> <DEM_XYZ>
    input parameters:
      DEM_par  (input) DEM parameter file DEM
      DEM      (input) input DEM file or constant height value
      DEM_XYZ  (output) DEM samples in Cartesian XYZ coordinates (float)
    """

    args = [DEM_par, DEM, DEM_XYZ]
    optargs = []
    cmd = makecmd('dem_xyz', args, optargs) 
    os.system(cmd)

def init_offsetm(MLI_1, MLI_2, DIFF_par, rlks=None, azlks=None, rpos=None, azpos=None, offr=None, offaz=None, thres=None, patch=None, cflag=None):
    """*** Initial offset estimation for multi-look intensity images ***
    *** Copyright 2013, Gamma Remote Sensing, v3.5 2-May-2013 clw/uw ***
    
    usage: init_offsetm <MLI-1> <MLI-2> <DIFF_par> [rlks] [azlks] [rpos] [azpos] [offr] [offaz] [thres] [patch] [cflag]
    
    input parameters: 
      MLI-1     (input) intensity image 1 (float) (reference)
      MLI-2     (input) intensity image 2 (float)
      DIFF_par  DIFF/GEO parameter file
      rlks      number of range looks (enter - for default: 1)
      azlks     number of azimuth looks (enter - for default: 1)
      rpos      center of region for comparision in range (enter - for default: image center
      azpos     center of region for comparision in azimuth (enter - for default: image center)
      offr      initial range offset (enter - for default from DIFF_par)
      offaz     initial azimuth offset (enter - for default from DIFF_par)
      thres     correlation SNR threshold (enter -  for default:  7.000)
      patch     correlation patch size (128,256,512,1024, enter - for default: 1024)
      cflag     copy offsets to the range and azimuth offset polynomials in DIFF_par
                  0: no
                  1: yes (default)
    
    """

    args = [MLI_1, MLI_2, DIFF_par]
    optargs = [rlks, azlks, rpos, azpos, offr, offaz, thres, patch, cflag]
    cmd = makecmd('init_offsetm', args, optargs) 
    os.system(cmd)

def offset_pwr_trackingm(MLI_1, MLI_2, DIFF_par, offs, snr, rwin=None, azwin=None, offsets=None, n_ovr=None, thres=None, rstep=None, azstep=None, rstart=None, rstop=None, azstart=None, azstop=None, pflag=None):
    """*** Offset tracking between MLI images using intensity cross-correlation ***
    *** Copyright 2014, Gamma Remote Sensing, v3.5 clw/uw/ts 26-May-2014 ***
    
    usage: offset_pwr_trackingm <MLI-1> <MLI-2> <DIFF_par> <offs> <snr> [rwin] [azwin] [offsets] [n_ovr] [thres] [rstep] [azstep] [rstart] [rstop] [azstart] [azstop] [pflag]
    
    input parameters: 
      MLI-1     (input) real valued intensity image 1 (reference)
      MLI-2     (input) real valued intensity image 2
      DIFF_par  DIFF/GEO parameter file
      offs      (output) offset estimates (fcomplex)
      snr       (output) offset estimation SNR (float)
      rwin      search window size (range pixels, (enter - for default from offset parameter file))
      azwin     search window size (azimuth pixels, (enter - for default from offset parameter file))
      offsets   (output) range and azimuth offsets and SNR data in text format, enter - for no output
      n_ovr     image over-sampling factor (integer 2**N (1,2,4) default: 2)
      thres     offset estimation quality threshold (enter - for default from offset parameter file)
      rstep     step in range pixels (enter - for default: rwin/2)
      azstep    step in azimuth pixels (enter - for default: azwin/2)
      rstart    offset to starting range pixel (enter - for default: 0)
      rstop     offset to ending range pixel (enter - for default: nr-1)
      azstart   offset to starting azimuth line (enter - for default: 0)
      azstop    offset to ending azimuth line  (enter - for default: nlines-1)
      pflag     print flag
                  0: print offset summary
                  1: print all offset data (default))
    
    """

    args = [MLI_1, MLI_2, DIFF_par, offs, snr]
    optargs = [rwin, azwin, offsets, n_ovr, thres, rstep, azstep, rstart, rstop, azstart, azstop, pflag]
    cmd = makecmd('offset_pwr_trackingm', args, optargs) 
    os.system(cmd)

def WSS_intf(SLC_1, SLC_2R, SLC1_par, SLC2R_par, OFF_par, interf, rlks=None, sps_flg=None, azf_flg=None, m_flg=None, boff=None, bstep=None, bmax=None):
    """*** Calculate multi-look interferogram from ASAR Wide-Swath SLC images ***
    *** Copyright 2006, Gamma Remote Sensing v2.1 21-Apr-2006 clw ***
    
    usage: WSS_intf <SLC-1> <SLC-2R> <SLC1_par> <SLC2R_par> <OFF_par> <interf> [rlks] [sps_flg] [azf_flg] [m_flg] [boff] [bstep] [bmax]
    
    input parameters:
      SLC-1     (input) reference ASAR Wide-Swath SLC image
      SLC-2R    (input) resampled ASAR Wide-Swath SLC image coregistered to SLC-1
      SLC1_par  (input) ASAR Wide-Swath SLC image parameter file
      SLC2R_par (input) co-registered ASAR Wide-Swath SLC-2R image parameter file
      OFF_par   (output) ISP offset/interferogram parameter file
      interf    (output) interferogram from SLC-1 and SLC-2R
      rlks      number of range looks (enter - for default: 1)
      sps_flg   range spectral shift flag:
                  1: apply range spectral shift filter (default)
                  0: do not apply range spectral shift filter
      azf_flg   azimuth common band filter flag:
                  1: apply azimuth common band filter (default)
                  0: do not apply azimuth common band filter
      m_flg     output magnitude flag:
                  0: set output interferogram magnitude to 1.0 for non-zero samples (default)
                  1: retain multi-burst interferometric magnitude
      boff      offset to first burst to interfere (default = 0)
      bstep     burst step (default = 1)
      bmax      last burst to interfere (default = to end of SLC-1)
    
    """

    args = [SLC_1, SLC_2R, SLC1_par, SLC2R_par, OFF_par, interf]
    optargs = [rlks, sps_flg, azf_flg, m_flg, boff, bstep, bmax]
    cmd = makecmd('WSS_intf', args, optargs) 
    os.system(cmd)

def sarpix_coord(SLC_par, OFF_par=None, DEM_par=None, azlin=None, rpix=None, ref_hgt=None):
    """*** Transform SAR image coordinates to map projection or geographic (lat/lon) coordinates ***
    *** Copyright 2013, Gamma Remote Sensing, v1.7, 8-Mar-2013 uw/clw ***
    
    usage: sarpix_coord <SLC_par> [OFF_par] [DEM_par] [azlin] [rpix] [ref_hgt]
    
    input parameters:
      SLC_par  (input) ISP SLC/MLI parameter file
      OFF_par  (input) ISP offset/interferogram parameter file, enter - for SLC or MLI data
      DEM_par  (input) DEM parameter file, enter - for WGS84 lat/lon
      azlin    SAR image azimuth line
      rpix     SAR image slant range pixel number
      ref_hgt  reference height (m) in map datum
    
    """

    args = [SLC_par]
    optargs = [OFF_par, DEM_par, azlin, rpix, ref_hgt]
    cmd = makecmd('sarpix_coord', args, optargs) 
    os.system(cmd)

def offset_trackingm(offs, snr, MLI_par, DIFF_par, coffs_map, coffsets=None, mode=None, thres=None, poly_flag=None):
    """*** Conversion of range and azimuth offsets files to displacement map for intensity offsets ***
    *** Copyright 2013, Gamma Remote Sensing, v1.8 28-Nov-2013 clw/ts ***
    
    usage: offset_trackingm <offs> <snr> <MLI_par> <DIFF_par> <coffs_map> [coffsets] [mode] [thres] [poly_flag]
    
    input parameters: 
      offs       (input) range and azimuth offset estimates (fcomplex)
      snr        (input) SNR values of offset estimates (float)
      MLI_par    (input) MLI parameter file of reference MLI
      DIFF_par   (input) offset parameter file used in the offset tracking
      coffs_map  (output) range and azimuth displacement estimates (fcomplex)
      coffsets   (output) range and azimuth displacement estimates and SNR values (enter - for none) (text)
      mode       flag indicating displacement mode:
                   0: displacement in range and azimuth pixels
                   1: displacement in meters in slant range and azimuth directions
                   2: displacement in meters in ground range and azimuth directions (default)
      thres      SNR threshold to accept offset value (default from DIFF_par)
      poly_flag  flag indicating if trend calculated using offset polynomials from OFF_par is subtracted:
                   0: do not subtract polynomial trend from offset data
                   1: subtract polynomial trend from offset data (default)
    
    """

    args = [offs, snr, MLI_par, DIFF_par, coffs_map]
    optargs = [coffsets, mode, thres, poly_flag]
    cmd = makecmd('offset_trackingm', args, optargs) 
    os.system(cmd)

def SLC_diff_intf(SLC_1, SLC_2R, SLC1_par, SLC2R_par, OFF_par, sim_unw, diff_int, rlks, azlks, sps_flg=None, azf_flg=None, rbw_min=None, rp1_flg=None, rp2_flg=None):
    """*** Differential interferogram generation from co-registered SLCs and a simulated interferogram ***
    *** Copyright 2013, Gamma Remote Sensing, v2.6 clw/uw 14-Oct-2013 ***
    
    usage: SLC_diff_intf <SLC-1> <SLC-2R> <SLC1_par> <SLC2R_par> <OFF_par> <sim_unw> <diff_int> <rlks> <azlks> [sps_flg] [azf_flg] [rbw_min] [rp1_flg] [rp2_flg]
    
    input parameters:
      SLC-1      (input) single-look complex image 1 (reference)
      SLC-2R     (input) single-look complex image 2 coregistered to SLC-1
      SLC1_par   (input) SLC-1 ISP image parameter file
      SLC2R_par  (input) SLC-2R ISP image parameter file for the co-registered image
      OFF_par    (input) ISP offset/interferogram parameter file
      sim_unw    (input) simulated unflattened and unwrapped interferogram, same width as output interferogram (float)
      diff_int   (output) differential interferogram (complex)
      rlks       number of range looks for the output interferogram
      azlks      number of azimuth looks for the simulated and output interferogram
      sps_flg    range spectral shift flag:
                   1: apply spectral shift filter (default)
                   0: do not apply spectral shift filter
      azf_flg    azimuth common band filter flag:
                   1: apply azimuth common band filter (default)
                   0: do not apply azimuth common band filter
      rbw_min    minimum range bandwidth fraction (0.1 --> 1.0)(default:  0.250)
      rp1_flg    SLC-1 range phase mode
                   0: nearest approach (0-Doppler) phase
                   1: ref. function center (Doppler centroid) (default)
      rp2_flg    SLC-2 range phase mode
                   0: nearest approach (0-Doppler) phase
                   1: ref. function center (Doppler centroid) (default)
    
    """

    args = [SLC_1, SLC_2R, SLC1_par, SLC2R_par, OFF_par, sim_unw, diff_int, rlks, azlks]
    optargs = [sps_flg, azf_flg, rbw_min, rp1_flg, rp2_flg]
    cmd = makecmd('SLC_diff_intf', args, optargs) 
    os.system(cmd)

def dh_map_orb(SLC1_par, SLC2R_par, OFF_par, hgt, dp, dpdh, dh, SLC_ref_par=None, int_mode=None):
    """*** Calculate delta height from differential interferometric phase using state vectors for baseline calculation ***
    *** Copyright 2013, Gamma Remote Sensing, v1.0 30-Aug-2013 clw ***
    
    usage: dh_map_orb <SLC1_par> <SLC2R_par> <OFF_par> <hgt> <dp> <dpdh> <dh> [SLC_ref_par] [int_mode]
    
    input parameters:
      SLC1_par    (input) SLC parameter file of reference SLC-1
      SLC2R_par   (input) SLC parameter file of resampled SLC-2
      OFF_par     (input) ISP offset/interferogram parameter file
      hgt         (input) height map in the same geometry as the interferogram, or constant value (m) (float, enter - for none))
      dp          (input) interferogram phase - simulated phase (radians) (float, enter - for none)
      dpdh        (output) sensitivity of interf. phase with respect to height calculated using state vectors and hgt (rad/m) (float, enter - for none)
      dh          (output) height difference calculated from dp using dpdh (m) (float, enter - for none)
      SLC_ref_par (input) SLC parameter file of the image used for geometric coregistration (enter - for none)
      int_mode    (input) interferometric acquisition mode:
                    0: single-pass mode (Tandem-X)
                    1: repeat-pass mode (default)
    
    """

    args = [SLC1_par, SLC2R_par, OFF_par, hgt, dp, dpdh, dh]
    optargs = [SLC_ref_par, int_mode]
    cmd = makecmd('dh_map_orb', args, optargs) 
    os.system(cmd)

def par_EORC_PALSAR_geo(CEOS_leader, MLI_par, DEM_par, CEOS_data, MLI=None):
    """*** Reformat EORC PALSAR Level 1.5 CEOS format terrain-geocoded data ***
    *** Copyright 2010, Gamma Remote Sensing, v1.2 25-Oct-2012 clw ***
    
    usage: par_EORC_PALSAR_geo <CEOS_leader> <MLI_par> <DEM_par> <CEOS_data> [MLI]
    
    input parameters:
      CEOS_leader  (input) CEOS leader file for PALSAR Level 1.5 terrain-geocoded data (LED...P1.5GUA)
      MLI_par      (output) ISP image parameter file (example: yyyymmdd_map.mli.par)
      DEM_par      (output) DIFF/GEO DEM parameter file
      CEOS_data    (input) PALSAR CEOS format Level 1.5 terrain-geocoded MLI data (IMG...P1.5GUA)
      MLI          (output) reformatted PALSAR terrain-geocoded MLI image (example: yyyymmdd_map.mli, enter - for none)
    
    """

    args = [CEOS_leader, MLI_par, DEM_par, CEOS_data]
    optargs = [MLI]
    cmd = makecmd('par_EORC_PALSAR_geo', args, optargs) 
    os.system(cmd)

def WSS_interp_lt(SLC_1, SLC_2, SLC1_par, SLC2_par, lookup_table, MLI1_par, MLI2_par, DIFF_par1, SLC_2R, SLC2R_par, DIFF_par2, ):
    """*** ASAR Wide-Swath SLC complex image resampling via a lookup table and  2-D SINC interpolation ***
    *** Copyright 2011, Gamma Remote Sensing, v2.1 30-Aug-2011 clw/awi ***
    
    usage: WSS_interp_lt <SLC-1> <SLC-2> <SLC1_par> <SLC2_par> <lookup_table> <MLI1_par> <MLI2_par> <DIFF_par1> <SLC-2R> <SLC2R_par> <DIFF_par2>
    
    input parameters:
      SLC-1         (input) ASAR Wide-Swath SLC_1 reference image
      SLC-2         (input) ASAR Wide-Swath SLC-2 image to be resampled to the geometry of the WSS SLC-1 reference image
      SLC1_par      (input) ASAR Wide-Swath SLC-1 image parameter file
      SLC2_par      (input) ASAR Wide-Swath SLC-2 image parameter file
      lookup_table  (input) lookup_table relating MLI-2 to MLI-1 created from SLC-2 and SLC-1
      MLI1_par      (input) SLC/MLI ISP image parameter file of reference MLI-1 (lookup table dimension)
      MLI2_par      (input) SLC/MLI ISP image parameter file of MLI-2 (lookup table values)
      DIFF_par1     (input) DIFF/GEO offset parameter file used for lookup table refinement MLI-1, MLI-2R (enter - for none)
      SLC-2R        (output) ASAR Wide-Swath SLC-2 resampled and co-registered to SLC-1
      SLC2R_par     (output) ASAR Wide-Swath image parameter file for the co-registered resampled image
      DIFF_par2     (output) DIFF/GEO offset parameter file for MLI-1, MLI-2 (enter - for none)
    
    """

    args = [SLC_1, SLC_2, SLC1_par, SLC2_par, lookup_table, MLI1_par, MLI2_par, DIFF_par1, SLC_2R, SLC2R_par, DIFF_par2]
    optargs = []
    cmd = makecmd('WSS_interp_lt', args, optargs) 
    os.system(cmd)

def coord_to_sarpix(SLC_par, OFF_par, DEM_par, north_lat, east_lon, hgt, DIFF_par=None):
    """*** Calculate SAR pixel coordinates (iazpix,jrpix) of geographic coordinate ***
    *** Copyright 2011, Gamma Remote Sensing, v1.6 30-Aug-2011 uw/clw ***
    
    usage: coord_to_sarpix <SLC_par> <OFF_par> <DEM_par> <north/lat> <east/lon> <hgt> [DIFF_par]
    
    input parameters:
      SLC_par    (input) ISP SLC/MLI parameter file
      OFF_par    (input) ISP offset/interferogram parameter file, enter - for SLC or MLI data
      DEM_par    (input) DEM/MAP parameter file defining geometry of input coordinates (enter - for WGS84 lat/lon)
      north/lat  (input) northing (m) or latitude (deg.)
      east/lon   (input) easting (m) or longitude (deg.)
      hgt        (input) height (m) in map datum
      DIFF_par   [input] DIFF/GEO parameter file containing refinement polynomial coefficients
    
      NOTE: When the DIFF_par is specified, it is assumed that the simulated image is the reference for applying the geocoding correction
    
    """

    args = [SLC_par, OFF_par, DEM_par, north_lat, east_lon, hgt]
    optargs = [DIFF_par]
    cmd = makecmd('coord_to_sarpix', args, optargs) 
    os.system(cmd)

def par_JERS_geo(CEOS_leader, CEOS_data, MLI_par, DEM_par, GEO=None):
    """*** Import JERS-1 L2.1 CEOS data (geocoded)  ***
    *** Copyright 2008, Gamma Remote Sensing, v1.0 15-Apr-2008 awi         ***
    
    usage: par_JERS_geo <CEOS_leader> <CEOS_data> <MLI_par> <DEM_par> [GEO]
    
    input parameters:
    CEOS_leader  (input) CEOS leader file for JERS-1 L2 geocoded data (SARL_*.DAT)
    CEOS_data    (input) JERS-1 CEOS format L2 terrain-geocoded MLI data (IMOP_*.DAT)
    MLI_par      (output) ISP image parameter file (example: yyyymmdd_map.mli.par)
    DEM_par      (output) DIFF/GEO DEM parameter file (example: yyyymmdd.dem_par)
    GEO          (output) calibrated and geocoded JERS-1 image (example: yyyymmdd_map.mli, enter - for none)
    
    """

    args = [CEOS_leader, CEOS_data, MLI_par, DEM_par]
    optargs = [GEO]
    cmd = makecmd('par_JERS_geo', args, optargs) 
    os.system(cmd)

def stacking(diff_tab, width, ph_rate, sig_ph_rate, sig_ph, roff, loff, nr=None, nl=None, np_min=None, tscale=None):
    """*** DIFF: Program stacking.c ***
    *** Copyright 2013, Gamma Remote Sensing, v2.1 19-Aug-2013 clw ***
    *** Estimate phase rate by stacking of multiple unwrapped differential interferograms ***
    
    usage: stacking <diff_tab> <width> <ph_rate> <sig_ph_rate> <sig_ph> <roff> <loff> [nr] [nl] [np_min] [tscale]
    
    input parameters: 
      diff_tab    (input) 2 column list of unwrapped diff. interferograms and delta_T values in days (text)
      width       number of samples/line of the interferograms in the stack
      ph_rate     (output) average phase rate determined from a weighted sum of phases (radians/year, float)
      sig_ph_rate (output) standard deviation of the estimated phase rate (radians/year, float)
      sig_ph      (output) standard deviation of the residual phases (enter - for none, radians, float)
      roff        range pixel offset to center of the phase reference region
      loff        line offset to center of the phase reference region
      nr          number of range pixels to average in the phase reference region (enter - for default:16)
      nl          number of lines average in the phase reference region (enter - for default: 16)
      np_min      min. number of phase values required to accept phase rate estimate (enter - for default: all files)
      tscale      time scale used for phase rate calculation:
                    0: radians/day
                    1: radians/year (default)
    
    """

    args = [diff_tab, width, ph_rate, sig_ph_rate, sig_ph, roff, loff]
    optargs = [nr, nl, np_min, tscale]
    cmd = makecmd('stacking', args, optargs) 
    os.system(cmd)

def gc_map_fd(MLI_par, fd_tab, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table, lat_ovr=None, lon_ovr=None, sim_sar=None, u=None, v=None, inc=None, psi=None, pix=None, ls_map=None, frame=None, ls_mode=None, r_ovr=None):
    """*** Calculate lookup table and DEM related products for terrain-corrected geocoding using a doppler polynomial table ***
    *** Copyright 2012, Gamma Remote Sensing, v1.5 5-Nov-2012 clw/uw/of ***
    
    usage: gc_map_fd <MLI_par> <fd_tab> <DEM_par> <DEM> <DEM_seg_par> <DEM_seg> <lookup_table> [lat_ovr] [lon_ovr] [sim_sar] [u] [v] [inc] [psi] [pix] [ls_map] [frame] [ls_mode] [r_ovr]
    
    input parameters:
      MLI_par         (input) ISP MLI or SLC image parameter file (slant range geometry)
      fdtab           (input)table of doppler polynomials at uniform azimuth time steps
      DEM_par         (input) DEM/MAP parameter file
      DEM             (input) DEM data file (or constant height value)
      DEM_seg_par     (input/output) DEM/MAP segment parameter file used for output products
    
      NOTE: If the DEM_seg_par already exists, then the output DEM parameters will be read from this file
            otherwise they are estimated from the image data.
    
      DEM_seg         (output) DEM segment used for output products, interpolated if lat_ovr > 1.0  or lon_ovr > 1.0
      lookup_table    (output) geocoding lookup table (fcomplex)
      lat_ovr         latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr         longitude or easting output DEM oversampling factor (enter - for default: 1.0)
      sim_sar         (output) simulated SAR backscatter image in DEM geometry
      u               (output) zenith angle of surface normal vector n (angle between z and n)
      v               (output) orientation angle of n (between x and projection of n in xy plane)
      inc             (output) local incidence angle (between surface normal and look vector)
      psi             (output) projection angle (between surface normal and image plane normal)
      pix             (output) pixel area normalization factor
      ls_map          (output) layover and shadow map (in map projection)
      frame           number of DEM pixels to add around area covered by SAR image (enter - for default = 8)
      ls_mode         output lookup table values in regions of layover, shadow, or DEM gaps (enter - for default)
                        0: set to (0.,0.)
                        1: linear interpolation across these regions (default)
                        2: actual value
                        3: nn-thinned
      r_ovr           range over-sampling factor for nn-thinned layover/shadow mode(enter - for default: 2.0)
    
    NOTE: enter - as output filename to avoid creating the corresponding output file
    
    """

    args = [MLI_par, fd_tab, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table]
    optargs = [lat_ovr, lon_ovr, sim_sar, u, v, inc, psi, pix, ls_map, frame, ls_mode, r_ovr]
    cmd = makecmd('gc_map_fd', args, optargs) 
    os.system(cmd)

def gec_map_grd(GRD_par, DEM_par, href, DEM_seg_par, lookup_table, lat_ovr=None, lon_ovr=None):
    """*** Calculate geocoding lookup table for ellipsoid correction of ground-range images ***
    *** Copyright 2013, Gamma Remote Sensing, v3.5 27-May-2013 clw/uw ***
    
    usage: gec_map_grd <GRD_par> <DEM_par> <href> <DEM_seg_par> <lookup_table> [lat_ovr] [lon_ovr]
    
    input parameters:
      GRD_par       (input) ground-range ISP image parameter file
      DEM_par       (input) DEM parameter file
      href          (input) elevation reference [m]
      DEM_seg_par   (input/output) DEM segment parameter file used for geocoding
    
      NOTE: If the DEM_seg_par already exists, then the output DEM parameters will be read from this file
            otherwise they are estimated from the image data.
    
      lookup_table  (output) geocoding lookup table
      lat_ovr       latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr       longitude or easting output DEM oversampling factor (enter - for default: 1.0)
    """

    args = [GRD_par, DEM_par, href, DEM_seg_par, lookup_table]
    optargs = [lat_ovr, lon_ovr]
    cmd = makecmd('gec_map_grd', args, optargs) 
    os.system(cmd)

def MLI_interp_lt(MLI_2, MLI1_par, MLI2_par, lookup_table, MLI3_par, MLI4_par, DIFF_par, SLC_2R, SLC2R_par, ):
    """*** MLI image resampling via a lookup table and SINC interpolation ***
    *** Copyright 2011, Gamma Remote Sensing, v1.4 30-Aug-2011 clw/awi ***
    
    usage: MLI_interp_lt <MLI-2> <MLI1_par> <MLI2_par> <lookup_table> <MLI3_par> <MLI4_par> <DIFF_par> <SLC-2R> <SLC2R_par>
    
    input parameters:
      MLI-2         (input) MLI-2 image to be resampled to the geometry of the MLI-1 reference image
      MLI1_par      (input) ISP image parameter file of the MLI-1 reference
      MLI2_par      (input) ISP image parameter file of MLI-2
      lookup_table  (input) lookup_table relating MLI-2 to MLI-1
      MLI3_par      (input) ISP image parameter file of reference scene MLI-3 (lookup table dimension)
      MLI4_par      (input) ISP image parameter file of MLI-4 (lookup table values)
      DIFF_par      (input) DIFF parameter file used for refinement (enter - for none)
      MLI-2R        (output) MLI-2 coregistered to MLI-1
      MLI2R_par     (output) MLI-2R ISP image parameter file for coregistered image
    """

    args = [MLI_2, MLI1_par, MLI2_par, lookup_table, MLI3_par, MLI4_par, DIFF_par, SLC_2R, SLC2R_par]
    optargs = []
    cmd = makecmd('MLI_interp_lt', args, optargs) 
    os.system(cmd)

def gc_map_inversion(gc_map, width_in, gc_map_out, width_out, nlines_out=None, interp_mode=None, n_ovr=None, rad_max=None, nintr=None):
    """*** Invert geocoding lookup table ***
    *** Copyright 2011, Gamma Remote Sensing, v3.3 25-Aug-2011 uw/clw ***
    
    usage: gc_map_inversion <gc_map> <width_in> <gc_map_out> <width_out> [nlines_out] [interp_mode] [n_ovr] [rad_max] [nintr]
    
    input parameters:
      gc_map         lookup table containing pairs of real-valued coordinates (fcomplex)
      width_in       width of input lookup table lines
      gc_map_out     inverted geocoding lookup table
      width_out      width of output data file
      nlines_out     number of lines of output data file (default=0: all lines up to highest value)
      interp_mode    resampling interpolation mode
                       0: 1/dist (default)
                       1: nearest-neighbor
                       2: 1/dist**2
                       3: constant
                       4: Gaussian
      n_ovr          interpolation oversampling factor (default: 2)
      rad_max        maximum interpolation search radius (default: 4*n_ovr= 8)
      nintr          number of points required for interpolation when not nearest-neighbor (default: 4)
    
    """

    args = [gc_map, width_in, gc_map_out, width_out]
    optargs = [nlines_out, interp_mode, n_ovr, rad_max, nintr]
    cmd = makecmd('gc_map_inversion', args, optargs) 
    os.system(cmd)

def gc_map_grd(MLI_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table, lat_ovr=None, lon_ovr=None, sim_sar=None, u=None, v=None, inc=None, psi=None, pix=None, ls_map=None, frame=None, ls_mode=None, r_ovr=None):
    """*** Calculate lookup table and DEM related products for terrain-corrected geocoding of ground range images ***
    *** Copyright 2012, Gamma Remote Sensing, v5.6 4-Nov-2012 clw/uw/of  ***
    *** Derive DEM products in DEM/MAP geometry:                          ***
    ***            local normal spherical angles, u, v                    ***
    ***            simulated SAR intensity (in map geometry), simsar      ***
    ***            local incidence angle, linc                            ***
    ***            projection angle, psi                                  ***
    ***            pixel area normalization factor, pix                   ***
    ***            layover / shadow map, ls_map                           ***
    
    usage: gc_map_grd <MLI_par> <DEM_par> <DEM> <DEM_seg_par> <DEM_seg> <lookup_table> [lat_ovr] [lon_ovr] [sim_sar] [u] [v] [inc] [psi] [pix] [ls_map] [frame] [ls_mode] [r_ovr]
    
    input parameters:
      GRD_par         (input) ground-range image ISP parameter file (format as SLC/MLI_par)
      DEM_par         (input) DEM/MAP parameter file
      DEM             (input) DEM data file (or constant height value)
      DEM_seg_par     (input/output) DEM/MAP segment parameter file used for output products
    
      NOTE: If the DEM_seg_par already exists, then the output DEM parameters will be read from this file
            otherwise they are estimated from the image data.
    
      DEM_seg         (output) DEM segment used for output products, interpolated if lat_ovr > 1.0  or lon_ovr > 1.0
      lookup_table    (output) geocoding lookup table (fcomplex)
      lat_ovr         latitude or northing output DEM oversampling factor (enter - for default: 1.0)
      lon_ovr         longitude or easting output DEM oversampling factor (enter - for default: 1.0)
      sim_sar         (output) simulated SAR backscatter image in DEM geometry
      u               (output) zenith angle of surface normal vector n (angle between z and n)
      v               (output) orientation angle of n (between x and projection of n in xy plane)
      inc             (output) local incidence angle (between surface normal and look vector)
      psi             (output) projection angle (between surface normal and image plane normal)
      pix             (output) pixel area normalization factor
      ls_map          (output) layover and shadow map (in map projection)
      frame           number of DEM pixels to add around area covered by SAR image (enter - for default = 8)
      ls_mode         output lookup table values in regions of layover, shadow, or DEM gaps (enter - for default)
                        0: set to (0.,0.)
                        1: linear interpolation across these regions (default)
                        2: actual value
                        3: nn-thinned
      r_ovr           range over-sampling factor for nn-thinned layover/shadow mode(enter - for default: 2.0)
    
    NOTE: enter - as output filename to avoid creating the corresponding output file
    
    """

    args = [MLI_par, DEM_par, DEM, DEM_seg_par, DEM_seg, lookup_table]
    optargs = [lat_ovr, lon_ovr, sim_sar, u, v, inc, psi, pix, ls_map, frame, ls_mode, r_ovr]
    cmd = makecmd('gc_map_grd', args, optargs) 
    os.system(cmd)

def base_add(base_1, base_2, base_out, mode=None):
    """*** Addition/subtraction of 2 baseline files ***
    *** Copyright 2008, Gamma Remote Sensing, v3.4 1-Dec-2008 clw/uw ***
    
    usage: base_add <base_1> <base_2> <base_out> [mode]
    
    input parameters: 
      base_1    (input) input baseline file 1
      base_2    (input) input baseline file 2
      base_out  (output) output baseline file
      mode      add/subtract mode (default=1: add, -1: subtract (base_1-base_2))
    
    """

    args = [base_1, base_2, base_out]
    optargs = [mode]
    cmd = makecmd('base_add', args, optargs) 
    os.system(cmd)

def dem_x_y_z(DEM_par, DEM, DEM_X, DEM_Y, DEM_Z, format_flag=None):
    """*** DEM transformation to WGS84 Cartesian X, Y, and Z coordinates (3 files) ***
    *** Copyright 2013, Gamma Remote Sensing, v1.1 25-Mar-2013 clw/uw/of ***
    
    usage: dem_x_y_z <DEM_par> <DEM> <DEM_X> <DEM_Y> <DEM_Z> [format_flag]
    input parameters:
      DEM_par      (input) DEM parameter file
      DEM          (input) DEM file or constant height value
      DEM_X        (output) X coordinates of grid points of the DEM
      DEM_Y        (output) Y coordinates of grid points of the DEM
      DEM_Z        (output) Z coordinates of grid points of the DEM
      format_flag   output data format
                      0: FLOAT (default)
                      1: DOUBLE 
    """

    args = [DEM_par, DEM, DEM_X, DEM_Y, DEM_Z]
    optargs = [format_flag]
    cmd = makecmd('dem_x_y_z', args, optargs) 
    os.system(cmd)

def create_dem_par(DEM_par, SLC_par=None, terra_alt=None, delta_y=None, delta_x=None):
    """*** DEM/MAP parameter file creation/modification ***
    *** Copyright 2013, Gamma Remote Sensing, v4.2 26-Nov-2013 clw/uw/ts ***
    
    usage: create_dem_par <DEM_par> [SLC_par] [terra_alt] [delta_y] [delta_x]
    
    input parameters:
      DEM_par    (input/output) DIFF/GEO DEM parameter file
      SLC_par    (input) ISP SLC/MLI image parameter file
      terra_alt  nominal terrain altitude used to calculate  bounds of the radar image, default: 0.0 m
      delta_y    DEM y line spacing for new DEM_par file
                 geographical coordinates (EQA projection latitude) default: -5.000000e-04 deg.
                 all other map projections default: -50.0000 m.
      delta_x    DEM x sample spacing for new DEM_par file
                 geographical coordinates (EQA projection) default: 5.000000e-04 deg.
                 all other map projections default: 50.0000 m.
    """

    args = [DEM_par]
    optargs = [SLC_par, terra_alt, delta_y, delta_x]
    cmd = makecmd('create_dem_par', args, optargs) 
    os.system(cmd)

def dem_RDC_list(DEM_par1, gc_map, MLI_par, mask, clist_RDC, clist_MAP, DEM_par2, s_north, s_east, ):
    """*** Generate coordinate lists in Range-Doppler Coordinates (RDC) and DEM coordinates using a lookup table ***
    *** Copyright 2013, Gamma Remote Sensing, v1.7 clw 23-Aug-2013 ***
    
    usage: dem_RDC_list <DEM_par1> <gc_map> <MLI_par> <mask> <clist_RDC> <clist_MAP> <DEM_par2> <s_north> <s_east>
    
    input parameters:
      DEM_par1   (input) DEM parameter file describing region covered by the geocoding lookup table
      gc_map     (input) geocoding lookup table containing RDC coordinates defined by DEM_par1
      MLI_par    (input) ISP image parameter file of MLI image associated with the geocoding lookup table
      mask       (input) mask file in SUN raster of BMP format to mask out regions in the map geometry for inclusion in the clist (enter - for none)
      clist_RDC  (output) list of x,y pixel coordinates in the reference SLC image geometry (Range-Doppler Coordinates) (text format)
      clist_MAP  (output) list of x,y pixel coordinates in the map projection geometry (text format)
      DEM_par2   (output) DEM parameter file associated with clist_MAP
      s_north    DEM northing sub-sampling factor for patch locations (int) (nominal: 10)
      s_east     DEM easting sub-sampling factor for patch locations (int) (nominal: 10)
    
    """

    args = [DEM_par1, gc_map, MLI_par, mask, clist_RDC, clist_MAP, DEM_par2, s_north, s_east]
    optargs = []
    cmd = makecmd('dem_RDC_list', args, optargs) 
    os.system(cmd)

def comb_interfs(int_1, int_2, base_1, base_2, factor_1, factor_2, width, combi_out, combi_base, sm=None):
    """*** Complex interferogram combination ***
    *** Copyright 2003, Gamma Remote Sensing, v1.2 21-May-2003 uw/ts ***
    
    usage: comb_interfs <int-1> <int-2> <base-1> <base-2> <factor-1> <factor-2> <width> <combi_out> <combi_base> [sm]
    
    input parameters:
      int-1       complex interferogram 1
      int-2       complex interferogram 2
      base-1      baseline file 1
      base-2      baseline file 2
      factor-1    phase scaling factor 1
      factor-2    phase scaling factor 2
      width       width of interferograms (samples)
      combi_int   output combined interferogram
      combi_base  output combined interferogram baseline file
      sm          magnitude scale factor (default=1.0)
    
      Remarks:
      Only the use of integer phase scaling factors (positive and negative) seems reasonable
        for the scaling and summing of wrapped phase images
      The coherence of the combined interferogram is estimated from the magnitude scale factor
        and the summed phase noise.
    
    """

    args = [int_1, int_2, base_1, base_2, factor_1, factor_2, width, combi_out, combi_base]
    optargs = [sm]
    cmd = makecmd('comb_interfs', args, optargs) 
    os.system(cmd)

def multi_mosaic(data_tab, data_out, DEM_par_out, mode, format_flag, ):
    """*** Mosaic geocoded images or DEM data with same format, map projection, and pixel spacing parameters ***
    *** Supported formats: float, fcomplex, int, short, unsigned char, SUN raster, and BMP image files ***
    *** Copyright 2011, Gamma Remote Sensing, v1.1 9-Mar-2011 awi/ts/uw/clw ***
    
    usage: multi_mosaic <data_tab> <data_out> <DEM_par_out> <mode> <format_flag>
    
    input parameters:
      data_tab     (input) list of data to mosaic (2 columns), one line for each input data file (text format):
                     data   DEM_par
      data_out     (output) output data file
      DEM_par_out  (output) DEM parameter file of output data file
                     NOTE: If this file exists, then the bounds of the mosaic are read from the parameter file, 
                     otherwise bounds including all input data will be computed and written to the parameter file
      mode         mosaicking mode
                     0: value of prior image preferred in the case of multiple valid input values
                     1: average of multiple valid input values calculated
      format_flag  input/output data format flag
                     0: float (REAL*4)
                     1: fcomplex (pairs of float)
                     2: int (INTEGER*4)
                     3: short (INTEGER*2)
                     4: unsigned char
                     5: SUN raster or BMP image files (8 or 24 bit)
    
    """

    args = [data_tab, data_out, DEM_par_out, mode, format_flag]
    optargs = []
    cmd = makecmd('multi_mosaic', args, optargs) 
    os.system(cmd)

def atm_mod(diff_unw, hgt, DIFF_par, model, dr=None, daz=None, overlay=None):
    """*** Evaluate linear regression of unwrapped phase with respect to height and calculate a model of the atmospheric phase ***
    *** Copyright 2014, Gamma Remote Sensing AG, v1.2 26-Feb-2014 clw ***
    
    usage: atm_mod <diff_unw> <hgt> <DIFF_par> <model> [dr] [daz] [overlay]
    
    input parameters:
      diff_unw  (input) unwrapped differential interferogram including atmospheric phase signal (float)
      hgt       (input) height coregistered to the interferogram
      DIFF_par  (input) differential interferogram parameters
      model     (output) modeled phase values from the regression of phase with respect to height (float)
      dr        range sample increment (enter - for default: 32)
      daz       azimuth sample increment (enter - for default: 32)
      overlay   (input) SUN raster/BMP format overlay mask, 0 pixels are excluded (enter - for default: none)
    
    """

    args = [diff_unw, hgt, DIFF_par, model]
    optargs = [dr, daz, overlay]
    cmd = makecmd('atm_mod', args, optargs) 
    os.system(cmd)

def offset_list_fitm(cp_list, DIFF_par, DEM_par, lookup_table=None, lt_type=None, type1=None, type2=None, coffsets=None, poly_order=None, interact_flag=None, trans_list=None):
    """*** Calculate fine registration polynomial for geocoding from control point list ***
    *** Copyright 2013, Gamma Remote Sensing, v2.7 3-Sep-2013 clw/ts/uw ***
    
    usage: offset_list_fitm <cp_list> <DIFF_par> <DEM_par> [lookup_table] [lt_type] [type1] [type2] [coffsets] [poly_order] [interact_flag] [trans_list] 
    
    input parameters:
      cp_list        (input) list with registration control point (CP) coordinates:
                       CP_nr  ref_col  ref_row  col2  row2
      DIFF_par       (input/output) DIFF&GEO parameter file (for output of fine registration polynomial)
      DEM_par        (input)DEM/MAP parameter file (enter - for none)
      lookup_table   (input)geocoding lookup table (required to convert between map pixel numbers and SAR pixel numbers)
      lt_type        lookup table type (enter - for default)
                       1: map_to_rdc (default)
                       2: rdc_to_map
      type1          reference coordinate type (enter - for default):
                       1: SAR (col,row) pixel numbers (default):
                       2: map (col,row) pixel numbers
                       3: map (easting, northing) coordinates)
      type2          image 2 coordinate type (enter - for default):
                       1: SAR (col,row) pixel numbers (default)
                       2: map (col,row) pixel numbers
                       3: map (easting, northing) coordinates)
      coffsets       (output) list of culled registration offsets (text file)
      poly_order     polynomial order parameter (1, 3, 4, 6, default: 4)
      interact_flag  interactive culling of input data (1=YES, 0=NO, default: NO)
      trans_list     transformed coordinate list using model
    
    """

    args = [cp_list, DIFF_par, DEM_par]
    optargs = [lookup_table, lt_type, type1, type2, coffsets, poly_order, interact_flag, trans_list]
    cmd = makecmd('offset_list_fitm', args, optargs) 
    os.system(cmd)

def quad_sub(int_1, DIFF_par, int_2, int_type, mode=None):
    """*** Subtract quadratic polynomial model phase function from a differential interferogram ***
    *** Copyright 2013, Gamma Remote Sensing, v1.6 23-Aug-2013 clw/uw ***
    
    usage: quad_sub <int-1> <DIFF_par> <int-2> <int_type> [mode]
    
    input parameters:
      int-1     (input) differential interferogram (float or fcomplex)
      DIFF_par  (input) DIFF parameter file
      int-2     (output) output interferogram
      int_type  interferogam type:
                  0: unwrapped interferometric phase (float)
                  1: fcomplex interferogram
      mode      add or subtract phase model:
                  0: subtract phase model from DIFF_par (default)
                  1: add phase model from DIFF_par
    """

    args = [int_1, DIFF_par, int_2, int_type]
    optargs = [mode]
    cmd = makecmd('quad_sub', args, optargs) 
    os.system(cmd)

def create_diff_par(PAR1, PAR2, DIFF_par, PAR_type=None, iflg=None):
    """*** Create DIFF/GEO parameter file for geocoding and differential interferometry ***
    *** Copyright 2013, Gamma Remote Sensing, v2.9 16-May-2013 clw/uw ***
    
    usage: create_diff_par <PAR1> <PAR2> <DIFF_par> [PAR_type] [iflg]
    
    input parameters:
      PAR1      (input) image parameter file 1 (see PAR_type option)
      PAR2      (input) image parameter file 2 (or - if not provided)
      DIFF_par  (input/output) DIFF/GEO parameter file
      PAR_type  PAR1 and PAR2 parameter file type (enter - for default):
                  0: OFF_par     ISP offset and interferogram parameters  (default)
                  1: SLC/MLI_par ISP SLC/MLI parameters
                  2: DEM_par     DIFF/GEO DEM parameters
      iflg      interactive mode flag (enter -  for default)
                  0: non-interactive
                  1: interactive (default)
    
    """

    args = [PAR1, PAR2, DIFF_par]
    optargs = [PAR_type, iflg]
    cmd = makecmd('create_diff_par', args, optargs) 
    os.system(cmd)

def dem_gradient(DEM_par, DEM, theta, phi, mag, type=None):
    """*** Calculate elevation and orientation angles of the DEM normal or gradient vector magnitude ***
    *** Copyright 2009, Gamma Remote Sensing, v1.3, 30-Dec-2009 uw/ts/cw ***
    
    usage: dem_gradient <DEM_par> <DEM> <theta> <phi> <mag> [type]
    
    input parameters:
      DEM_par  (input) DEM parameter file
      DEM      (input) DEM data file (or constant height value)
      theta    (output) elevation angle of DEM normal or gradient vector (float)
      phi      (output) orientation angle of DEM normal or gradient vector (float)
      mag      (output) magnitude of the gradient vector (float)
      type     vector type selection:
                 0: normal vector
                 1: gradient vector (default)
    
    """

    args = [DEM_par, DEM, theta, phi, mag]
    optargs = [type]
    cmd = makecmd('dem_gradient', args, optargs) 
    os.system(cmd)

    
